from django.conf.urls import url, include

from . import views
from .password import urls as password_urls


urlpatterns = [
    url(r'^login/$', views.login, name='login'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^logout/$', views.logout),
    url(r'^shopify/login/finalize/$', views.shopify_finalize),
    url(r'^password/', include(password_urls)),
]
