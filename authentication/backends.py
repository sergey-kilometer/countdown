import shopify
from django.conf import settings
from contextlib import suppress
from django.contrib.auth.hashers import check_password
from django.contrib.auth.backends import RemoteUserBackend

from facebookads.objects import AdUser
from facebookads.api import FacebookAdsApi

from .models import ShopifyUserData
from countdown_app.models import VideoMakerUser


def _save_shopify_data(shop):
    with suppress(Exception):
        ShopifyUserData(
            data=shop,
            myshopify_domain=shop.get('myshopify_domain')
        ).save()


class ShopUserBackend(RemoteUserBackend):

    def authenticate(self, myshopify_domain=None, token=None, register_source=None, **kwargs):
        if not myshopify_domain or not token:
            return
        try:
            user = VideoMakerUser.objects.get(myshopify_domain=myshopify_domain)
            user.token = token
            user.save(update_fields=['token'])
        except VideoMakerUser.DoesNotExist:
            with shopify.Session.temp(myshopify_domain, token):
                shop = shopify.Shop.current().to_dict()
                user = VideoMakerUser(
                    register_source=register_source,
                    myshopify_domain=myshopify_domain,
                    username=myshopify_domain,
                    token=token,
                    email=shop.get('email'),
                    name=shop.get('shop_owner')
                )
                user.save()
                _save_shopify_data(shop)
        return user


class RegularUserBackend(RemoteUserBackend):

    def authenticate(self, email=None, password=None, **kwargs):
        if email and password:
            with suppress(VideoMakerUser.DoesNotExist):
                user = VideoMakerUser.objects.get(email=email)
                if check_password(password, user.password):
                    return user
        return


class SharingUserBackend(RemoteUserBackend):

    def authenticate(self, password=None, **kwargs):
        try:
            user = VideoMakerUser.objects.get(password=password)
            return user
        except VideoMakerUser.DoesNotExist:
            return


class FacebookUserBackend(RemoteUserBackend):

    def authenticate(self, access_token=None, **kwargs):
        with suppress(Exception):
            if access_token:
                FacebookAdsApi.init(settings.FACEBOOK_ADS['APP_ID'], settings.FACEBOOK_ADS['APP_SECRET'], access_token)
                fb_user = AdUser(fbid='me').remote_read(fields=['email', 'name'])
                email = fb_user.get('email')
                name = fb_user.get('name')

                if email:
                    try:
                        user = VideoMakerUser.objects.get(email=email)
                        return user
                    except VideoMakerUser.DoesNotExist:
                        user = VideoMakerUser(
                            register_source=VideoMakerUser.WEBSITE,
                            username=email,
                            token=access_token,
                            email=email,
                            name=name
                        )
                        user.save()
                        return user
        return
