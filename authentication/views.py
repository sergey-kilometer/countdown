import shopify
from django.conf import settings
from django.contrib import auth
from django.contrib.auth import hashers
from django.shortcuts import render, resolve_url
from django.http import HttpResponse, HttpResponseRedirect

from shopify_auth.decorators import login_required

from countdown_app.models import VideoMakerUser
from countdown.decorators import anonymous_required


@anonymous_required
def login(request):
    if 'POST' == request.method:
        data = request.POST
        email = data.get('login-username').lower()
        user = auth.authenticate(request, email=email, password=data.get('login-password'))
        if user:
            auth.login(request, user)
            redirect_to = request.GET.get('next', resolve_url(settings.LOGIN_REDIRECT_URL))
            return HttpResponseRedirect(redirect_to)

        return render(request, 'login.html', context={'error': 'Invalid email or password'})

    return render(request, 'login.html')


@anonymous_required
def signup(request):
    if 'POST' == request.method:
        data = request.POST
        name = data.get('signup-name')
        email = data.get('signup-email').lower()
        password = data.get('signup-password')

        try:
            VideoMakerUser.objects.get(email=email)
            return render(request, 'signup.html', context={'error': 'User with such email already exist'})
        except VideoMakerUser.DoesNotExist:
            pass

        user = VideoMakerUser(
            myshopify_domain=None,
            username=email,
            name=name,
            email=email,
            password=hashers.make_password(password)
        )
        user.save()

        user = auth.authenticate(request, email=email, password=password)
        if user:
            auth.login(request, user)
            return HttpResponseRedirect(resolve_url(settings.LOGIN_REDIRECT_URL))

    return render(request, 'signup.html')


@login_required
def logout(request):
    auth.logout(request)
    return HttpResponse('OK')


@login_required
def shopify_finalize(request, *args, **kwargs):
    shop = request.POST.get('shop', request.GET.get('shop'))

    try:
        shopify_session = shopify.Session(shop, token=kwargs.get('token'))
        shopify_session.request_token(request.GET)

        request.user.myshopify_domain = shopify_session.url
        request.user.token = shopify_session.token
        request.user.save(update_fields=['token', 'myshopify_domain'])
    except Exception:
        return HttpResponseRedirect('/#/builder')

    return HttpResponseRedirect('/#/builder')
