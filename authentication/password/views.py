import os
import uuid
from django.conf import settings
from datetime import datetime
from mailsnake import MailSnake
from django.db.models import Q
from django.contrib.auth import hashers
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from ..models import ResetPasswordToken
from countdown_app.models import VideoMakerUser


@require_POST
@csrf_exempt
def password_reset(request):
    context = {'error': 'Account with such email doesnt exist'}
    email = request.POST.get('email')
    if email:
        try:
            VideoMakerUser.objects.get(email=email)
            token = uuid.uuid1()
            # create reset password token in database
            ResetPasswordToken.objects.filter(Q(token=token) | Q(email=email)).delete()
            ResetPasswordToken(token=token, email=email).save()
            # send email to reset password
            mail_snake = MailSnake('c61bHLwX5O5ZhYJMK8xJoA', api='mandrill')
            # create and send email message
            link = settings.DOMAIN_PATH + '/password/create/token/{}/'.format(str(token))
            template = open(os.path.dirname(__file__) + '/templates/password_reset.html', 'r')
            template = str(template.read()).replace('|link|', link)
            mail_snake.messages.send(message={
                'html': template,
                'subject': 'Topvid.com - Reset your password',
                'from_email': 'support@topvid.com',
                'from_name': 'Topvid.com',
                'to': [{'email': email, 'name': ''}]
            })
            context = {'message': 'Password reset link was sent to your email'}
        except VideoMakerUser.DoesNotExist:
            pass
    return render(request, 'login.html', context=context)


def password_create(request, token):
    context = {'token': token, 'valid': True}
    try:
        reset_password_token = ResetPasswordToken.objects.get(token=token)
        hours_delta = ((datetime.now() - reset_password_token.create_date).seconds / 60) / 60
        if hours_delta > 24:
            context['valid'] = False
    except ResetPasswordToken.DoesNotExist:
        context['valid'] = False
    return render(request, 'password_create.html', context=context)


@require_POST
@csrf_exempt
def password_check(request):
    data = request.POST
    try:
        # delete reset password token
        reset_password_token = ResetPasswordToken.objects.get(token=data.get('token'))
        reset_password_token.delete()
        hasher = hashers.get_hasher('pbkdf2_sha256')
        # save new password
        user = VideoMakerUser.objects.get(email=reset_password_token.email)
        user.password = hasher.encode(data.get('reset-password'), hasher.salt())
        user.save()
    except (ResetPasswordToken.DoesNotExist, VideoMakerUser.DoesNotExist):
        return HttpResponse('', status=400)
    return HttpResponse('', status=200)