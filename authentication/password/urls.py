from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^reset/', views.password_reset, name='password_reset'),
    url(r'^check/', views.password_check, name='password_check'),
    url(r'^create/token/(?P<token>[^/]+)/$', views.password_create)
]