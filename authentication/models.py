from django.db import models
from jsonfield import JSONField


class ResetPasswordToken(models.Model):
    token = models.CharField(max_length=255, primary_key=True)
    email = models.CharField(max_length=255, null=False)
    create_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'reset_password_tokens'
        unique_together = ('token', 'email')


class ShopifyUserData(models.Model):
    id = models.AutoField(primary_key=True)
    data = JSONField(default=None)
    myshopify_domain = models.CharField(max_length=150, unique=True)
    signup_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.myshopify_domain

    class Meta:
        db_table = 'shopify_user_data'
