import React from 'react'
import moment from 'moment'
import Select from 'react-select'
import DatePicker from 'react-datepicker'

import { MAPPED_TIMEZONES , DATETIME_FORMAT } from '../constants/settingsForm'


function DatePickerInput(props) {
    return (
        <button
            className="datepicker-input"
            onClick={ props.onClick }>
            { props.value }
        </button>
    )
}


export default class DateTimeZonePicker extends React.PureComponent {
    render() {
        let { timeZone, date, onChangeDate, showTimeSelect = false, showHour = false, minDate,
              onChangeTimeZone, className=""} = this.props;

        return (
            <div id="dateTimeZone" className={ className }>
                <DatePicker onChange={(...args) => {
                                const {datepicker} = this.refs;
                                datepicker.setOpen(false);
                                onChangeDate(...args)
                            }}
                            showTimeSelect={ showTimeSelect }
                            customInput={ <DatePickerInput/> }
                            ref="datepicker"
                            minDate={ minDate }
                            dateFormat={ showHour ? 'LLL' : 'MMM DD YYYY' }
                            selected={ moment(date || minDate, DATETIME_FORMAT) }
                            timeIntervals={ 30 }/>

                <Select className="timeZoneSelect"
                        clearable={ false }
                        value={ timeZone }
                        options={ MAPPED_TIMEZONES }
                        onChange={ onChangeTimeZone }/>
            </div>
        )
    }
}
