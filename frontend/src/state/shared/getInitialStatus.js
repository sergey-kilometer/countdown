import { assign } from 'lodash'

export default function getInitialStatus(status) {
    return assign({
        isLoading: false,
        isLoaded: false,
        isSaving: false,
        isSaved: false,
        error: null
    }, status);
}