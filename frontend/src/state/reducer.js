import { combineReducers } from 'redux'

import userReducer from './user/reducer'
import videoReducer from './video/reducer'
import focusItemReducer from './focusItem/reducer'

export default combineReducers({
    user: userReducer,
    video: videoReducer,
    focusItem: focusItemReducer,
});