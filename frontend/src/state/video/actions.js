import axios from 'axios'
import setWith from 'lodash/setWith'
import extend from 'lodash/extend'

import { SET_VIDEO } from './actionTypes.js'
import { setFocusItem } from '../focusItem/actions'
import { GET_DEFAULT_JSON_URL } from '../../constants/urls'
import { DAYS_TIME } from '../../constants/common'
import getInitialStatus from '../shared/getInitialStatus'


export function setVideo(payload) {
    return {
        type: SET_VIDEO ,
        payload: payload
    }
}

export function setItem(item) {
    return (dispatch , getState) => {
        let { focusItem: { path }, video } = getState();
        dispatch(setVideo(setWith(video, path, item)));
    };
}

export function fetchDefaultJson(id = 0, schedule_type = 'continuous') {
    return (dispatch , getState) => {
        let { video } = getState();
        video.status = getInitialStatus({ isLoading: true });
        video.id = id;
        dispatch(setVideo(video));
        axios.get(GET_DEFAULT_JSON_URL + `${id}/?category=${schedule_type}`)
            .then(({ data: { video_json } }) => {
                video_json.status = getInitialStatus();
                video_json.schedule_type = schedule_type;
                // change time if campaign type days
                if (schedule_type === 'days') {
                    let labelToReplace = video_json.frames[0]['labels'].find(({ text }) => text === 'ENDS IN');
                    video_json.time = extend(video_json.time, DAYS_TIME);
                    labelToReplace && (labelToReplace['text'] = 'OFFER ENDING SOON');
                }
                dispatch(setFocusItem(video_json.focusedItem));
                dispatch(setVideo(video_json));
            })
            .catch((response) => {
                let error = 'Oops some error occurred please check your network and try to reset template.';
                video.status = getInitialStatus({ error });
                dispatch(setVideo(video));
            });
    };
}