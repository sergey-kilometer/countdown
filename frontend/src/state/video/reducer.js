import merge from 'lodash/merge'
import initialState from './initialState'
import { SET_IN, SET_VIDEO } from './actionTypes'

export default function reducer(state = initialState, {type, payload} = {}) {

    switch (type) {
        case SET_IN:
            return merge(state, payload);

        case SET_VIDEO:
            return Object.assign({}, payload);

        default:
            return state

    }

}