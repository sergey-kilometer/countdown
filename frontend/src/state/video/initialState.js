import getInitialStatus from '../shared/getInitialStatus'

export default localStorage.getObject('countdown-video') || {status: getInitialStatus()}