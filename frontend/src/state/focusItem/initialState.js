export default localStorage.hasOwnProperty('countdown-video') && localStorage.getObject('countdown-video').focusedItem ?
    localStorage.getObject('countdown-video')['focusedItem']
    :
    {type: null, path:[]}