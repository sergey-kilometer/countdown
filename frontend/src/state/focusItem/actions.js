import { SET_FOCUSED_ITEM } from './actionTypes.js'

export function setFocusItem(payload) {
    return {
		type: SET_FOCUSED_ITEM,
		payload: payload
	}
}