import { SET_USER } from './actionTypes'

export default function reducer(state = {}, {type, payload} = {}) {

    switch (type) {
        case SET_USER:
            return Object.assign({}, payload);

        default:
            return state

    }

}