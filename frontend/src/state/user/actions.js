import axios from 'axios'
import { SET_USER } from './actionTypes.js'
import { POST_COUNTDOWN_USER, POST_CAMPAIGN_URL, GET_CARD_STATUS_URL } from '../../constants/urls'
import getInitialStatus from '../shared/getInitialStatus'


export function setUser(payload) {
    return {
		type: SET_USER,
		payload: payload
	}
}

export function saveUser(newUser) {
    return (dispatch, getState) => {
        let user = getState().user;
        user.status = getInitialStatus({isSaving: true});
        dispatch(setUser(user));
        axios.post(POST_COUNTDOWN_USER, newUser)
            .then((response) => {
                let newUser = response.data;
                newUser.status = getInitialStatus({isSaved: true});
                newUser.settings = JSON.parse(newUser.settings);
                dispatch(setUser({...user, ...newUser}));
            }).catch((error) => {
                if (error.response) {
                    user.status = getInitialStatus({
                        error: 'Oops some error occurred please contact us at support@topvid.com'
                    });
                    dispatch(setUser({...user}));
                }
            });
    };
}

export function pullPaymentStatus(isOneTime=false) {
    return (dispatch, getState) => {
        axios.get(GET_CARD_STATUS_URL)
            .then(({data}) => {
                if (data.status === 'completed') {
                    let user = getState().user;
                    user.countdown_plan = data.plan_name;
                    dispatch(setUser({...user}));
                } else if (!isOneTime) {
                    setTimeout(() => {dispatch(pullPaymentStatus())}, 3000);
                }
            }).catch((error) => {
                if (!isOneTime) {
                    setTimeout(() => {dispatch(pullPaymentStatus())}, 3000);
                }
            });
    };
}

export function saveCampaign(history) {
    return async (dispatch, getState) => {
        let { user, video } = getState();
        user.status = getInitialStatus({isLoading: true});
        dispatch(setUser({...user}));
        axios.post(POST_CAMPAIGN_URL, { settings: user.settings, video_json: video})
            .then(({data}) => {
                if (data.error) {
                    user.status = getInitialStatus({isLoaded: true, error: data.error});
                    dispatch(setUser(user));
                } else {
                    user.status = getInitialStatus({isLoaded: true});
                    user.settings['pending_campaign'] = data.pending_campaign;
                    dispatch(setUser(user));
                    history.push('/my-campaigns');
                }
            })
            .catch(error => {
                if (error.response) {
                    user.status = getInitialStatus({error: error.response.data});
                }
                user.status = getInitialStatus({
                    error: 'Oops some error occurred please contact us at support@topvid.com'
                });
                dispatch(setUser({...user}));
            });
    };
}
