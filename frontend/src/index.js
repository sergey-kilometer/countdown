import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import promise from 'redux-promise'
import thunk from 'redux-thunk'

import './styles/base.css'
import Root from './routes/Root'
import reducers from './state/reducer'

const createStoreWithMiddleware = applyMiddleware(thunk, promise)(createStore);

ReactDOM.render((
    <Provider store={ createStoreWithMiddleware(reducers) }>
        <Root/>
    </Provider>
), document.getElementById('root'));