import React from 'react'
import range from 'lodash/range'

import { pad } from '../../../../../constants/settingsForm'
import { DEFAULT_DIMENSIONS, MARGIN, SELECTED_ARRAY } from '../../../../../constants/timer'


export default function FlipTimer(props) {
    let { time: { start, style, dimensions=DEFAULT_DIMENSIONS }, onFocusItem, type } = props,
        BLOCK_SIZE = dimensions.width / 7,
        BLOCK_HEIGHT = dimensions.height, splitTime,
        blocksNum = 6,
        clockStyle = style;

    if (isNaN(start)) {
        splitTime = start.split(':');
    } else {
        blocksNum = 2;
        splitTime = pad(start);
    }

    return (
        <div id="clockTime"
             className={ 'clockTime' + (SELECTED_ARRAY.indexOf(type) > -1 ? ' selected-div' : '') }
             onClick={ onFocusItem }
             style={{
                zIndex: 2,
                position: 'absolute',
                transform: dimensions.location,
                width: `${dimensions.width}px`,
                height: `${dimensions.height}px`,
            }}>
            {/* Render digits*/}

            { range(0, blocksNum).map(i => {
                let currentTime = blocksNum > 2 ? splitTime[Math.floor(i / 2)][i % 2] : splitTime[i],
                    height = (BLOCK_HEIGHT / 2) - 1,
                    x = (i * BLOCK_SIZE) +
                        ((i === 2 || i === 3) ? BLOCK_SIZE / 2 : 0) +
                        ((i === 4 || i === 5) ? BLOCK_SIZE : 0);

                return (
                    <div key={i}>
                        <div style={{
                                fontFamily: clockStyle['fontFamily'],
                                color: clockStyle['color'],
                                fontSize: clockStyle['fontSize'],
                                backgroundColor: clockStyle['backgroundColor'],
                                borderRadius: '15% 15% 0 0',
                                zIndex: 3,
                                transformOrigin: '50% 100%',
                                textAlign: 'center',
                                position: 'absolute',
                                lineHeight: `${BLOCK_HEIGHT}px`,
                                width: `${BLOCK_SIZE - MARGIN}px`,
                                height: `${height}px`,
                                transform: `matrix(1, 0, 0, 1, ${x}, 0)`
                            }}>
                            { currentTime }
                        </div>

                        <div style={{
                                fontFamily: clockStyle['fontFamily'],
                                color: clockStyle['color'],
                                overflow: 'hidden',
                                fontSize: clockStyle['fontSize'],
                                backgroundColor: clockStyle['backgroundColor'],
                                borderRadius: '0 0 15% 15%',
                                zIndex: 1,
                                textAlign: 'center',
                                position: 'absolute',
                                width: `${BLOCK_SIZE - MARGIN}px`,
                                lineHeight: 0,
                                height: `${height}px`,
                                transform: `matrix(1, 0, 0, 1, ${x}, ${BLOCK_HEIGHT / 2 + 1})`
                            }}>
                            { currentTime }
                        </div>
                    </div>
                );
            })}

            { isNaN(start) &&
                <div>
                    {/* Render separators*/}
                    <div style={{
                            fontFamily: clockStyle['fontFamily'],
                            color: clockStyle['color'],
                            fontSize: '200px',
                            position: 'absolute',
                            width: `${BLOCK_SIZE / 2 - MARGIN}px`,
                            lineHeight: `${BLOCK_HEIGHT}px`,
                            textAlign: 'center',
                            height: `${BLOCK_HEIGHT}px`,
                            transform: `matrix(1, 0, 0, 1, ${BLOCK_SIZE * 2}, 0)`
                        }}>
                        :
                    </div>

                    <div style={{
                            fontFamily: clockStyle['fontFamily'],
                            color: clockStyle['color'],
                            fontSize: '200px',
                            position: 'absolute',
                            width: `${BLOCK_SIZE / 2 - MARGIN}px`,
                            lineHeight: `${BLOCK_HEIGHT}px`,
                            textAlign: 'center',
                            height: `${BLOCK_HEIGHT}px`,
                            transform: `matrix(1, 0, 0, 1, ${BLOCK_SIZE * 4.5}, 0)`
                        }}>
                        :
                    </div>
                </div>
            }
        </div>
    );
}