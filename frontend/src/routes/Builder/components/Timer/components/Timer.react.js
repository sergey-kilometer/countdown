import React from 'react'

import CubeTimer from './CubeTimer.react'
import FlipTimer from './FlipTimer.react'
import BasicTimer from './BasicTimer.react'
import SquareTimer from './SquareTimer.react'
import DigitalTimer from './DigitalTimer.react'

export default function Timer(props) {
    switch (props.time.version) {
        case 2:
            return <DigitalTimer { ...props }/>;
        case 3:
            return <FlipTimer { ...props }/>;
        case 4:
            return <CubeTimer { ...props }/>;
        case 5:
            return <SquareTimer { ...props }/>;
        default:
            return <BasicTimer { ...props }/>;
    }
}