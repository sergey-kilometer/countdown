import React from 'react'
import { isEqual, set } from 'lodash'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Timer from './../../Timer'
import Hideable from './../../Elements/Hideable.react'
import AutoFitInput from './../../Elements/AutoFitInput.react'
import DraggableImage from './../../Elements/DraggableImage.react'
import { setFocusItem } from '../../../../../state/focusItem/actions'

class Frames21 extends React.Component {

    onFocusItem(focusItem, event) {
        let { setFocusItem } = this.props;
        event.preventDefault();
        event.stopPropagation();
        setFocusItem(focusItem);
    }

    handleLabelChange(index, label) {
        let { setVideo, videoJson } = this.props;
        set(videoJson, ['frames', 0, 'labels', index], label);
        setVideo(videoJson);
    }

    render() {
        let { videoJson: { frames, time, bg, schedule_type }, handleChange, focusItem: { path, type } } = this.props,
            { images, labels } = frames[0],
            timeFocusItem = {path: ['time'], type: 'timer'},
            colors = bg.background.colors;

        set(labels, [1, 'style', 'backgroundColor'], colors[0]);

        if (schedule_type === 'limited_stock') {
            timeFocusItem.type = 'stock';
        }

        return (
            <div className="Template21">
                <div id="scene0"
                     className={ type === 'background' ? 'selected-div' : '' }
                     style={{backgroundColor: colors[0]}}
                     onClick={ this.onFocusItem.bind(this, {path: ['bg', 'background'], type: 'background'}) }>

                    <Timer time={ time }
                           type={ type }
                           onFocusItem={ this.onFocusItem.bind(this, timeFocusItem) }/>

                    <div className="middle-block" style={{backgroundColor: colors[1]}}/>

                    <div className="clockDiv" style={{backgroundColor: colors[2]}}/>

                    { time.labels.map((label, idx) =>
                        <AutoFitInput key={ idx }
                                      label={ label }
                                      isFocused={ isEqual(path, ['time', 'labels', idx]) }
                                      onFocusItem={ this.onFocusItem.bind(this, {
                                          path: ['time', 'labels', idx],
                                          type: 'text'
                                      })}
                                      handleChange={ handleChange }/>
                    )}

                    <Hideable style={ labels[1].style }
                              id="label1"
                              paths={ [['frames', 0, 'labels', 1, 'isHidden']] }/>

                    { labels.map((label, idx) =>
                        <AutoFitInput label={ label }
                                      key={ idx }
                                      isFocused={ isEqual(path, ['frames', 0, 'labels', idx]) }
                                      onFocusItem={ this.onFocusItem.bind(this, {
                                          path: ['frames', 0, 'labels', idx],
                                          type: 'text'
                                      })}
                                      handleChange={ this.handleLabelChange.bind(this, idx) }/>
                    )}

                    <Hideable style={ images[0].style }
                              id="image0"
                              paths={ [['frames', 0, 'images', 0, 'isHidden']] }/>

                    <DraggableImage image={ images[0] }
                                    isFocused={ isEqual(path, ['frames', 0, 'images', 0]) }
                                    onFocusItem={ this.onFocusItem.bind(this, {
                                        path: ['frames', 0, 'images', 0],
                                        type: 'image'
                                    })}
                                    handleChange={ handleChange }/>


                    <div className="outlineDiv">
                        <DraggableImage image={ images[1] }
                                        isFocused={ isEqual(path, ['frames', 0, 'images', 1]) }
                                        onFocusItem={ this.onFocusItem.bind(this, {
                                            path: ['frames', 0, 'images', 1],
                                            type: 'image'
                                        })}
                                        handleChange={ handleChange }/>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        focusItem: state.focusItem
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setFocusItem
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Frames21);