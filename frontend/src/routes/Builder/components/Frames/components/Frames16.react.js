import React from 'react'
import set from 'lodash/set'
import isEqual from 'lodash/isEqual'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Timer from './../../Timer'
import AutoFitInput from './../../Elements/AutoFitInput.react'
import DraggableImage from './../../Elements/DraggableImage.react'
import { setFocusItem } from '../../../../../state/focusItem/actions'


class Frames16 extends React.Component {

    onFocusItem(focusItem, event) {
        let { setFocusItem } = this.props;
        event.preventDefault();
        event.stopPropagation();
        setFocusItem(focusItem);
    }

    handleLabelChange(index, label) {
        let { setVideo, videoJson } = this.props;
        set(videoJson, ['frames', 0, 'labels', index], label);
        setVideo(videoJson);
    }

    render() {
        let { videoJson:{ frames, bg, time, schedule_type }, handleChange, focusItem: { path, type } } = this.props,
            frame = frames[0],
            timeFocusItem = {path: ['time'], type: 'timer'},
            bgColors = bg.background.colors;

        if (schedule_type === 'limited_stock') {
            timeFocusItem.type = 'stock';
        }

        return (
            <div className={ 'Template16' + (type === 'background' ? ' selected-div' : '') }>
                <div id="scene0">
                    <svg onClick={ this.onFocusItem.bind(this, {path: ['bg', 'background'], type: 'background'}) }
                         version="1.1" id="BG" xmlns="http://www.w3.org/2000/svg"
                         xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 1080 1080" xmlSpace="preserve">
                        <polygon points="0,1.6 0,434.1 354.6,780.6 320.1,0" fill={ bgColors[0] }/>
                        <polygon points="329.5,0 639.6,0 714.6,570.2 349.2,657.6 320.1,0" fill={ bgColors[1] }/>
                        <polygon points="0,434.1 659.1,1080 0,1080" fill={ bgColors[2] }/>
                        <polygon points="349.2,657.6 750.6,561.6 1080,881.1 1080,1080 659.1,1080 354.6,780.6" fill={ bgColors[3] }/>
                        <polygon points="666.4,202.6 714.6,570.2 750.6,561.6 1080,881.1 1080,442.4" fill={ bgColors[0] }/>
                        <polygon points="639.6,1 666.4,203.6 1080,442.4 1080,1" fill={ bgColors[2] }/>
                    </svg>

                    <div className="mainText"/>

                    <Timer version={ 16 }
                           time={ time }
                           type={ type }
                           onFocusItem={ this.onFocusItem.bind(this, timeFocusItem) }/>

                    { time.labels.map((label, idx) =>
                        <AutoFitInput key={ idx }
                                      label={ label }
                                      isFocused={ isEqual(path, ['time', 'labels', idx]) }
                                      onFocusItem={ this.onFocusItem.bind(this, {path: ['time', 'labels', idx], type: 'text'}) }
                                      handleChange={ handleChange }/>
                    )}

                    { frame.labels.map((label, idx) =>
                        <AutoFitInput key={ idx }
                                      label={ label }
                                      isFocused={ isEqual(path, ['frames', 0, 'labels', idx]) }
                                      onFocusItem={ this.onFocusItem.bind(this, {path: ['frames', 0, 'labels', idx], type: 'text'}) }
                                      handleChange={ this.handleLabelChange.bind(this, idx) }/>
                    )}
                    { frame.images.map((image, idx) =>
                        <DraggableImage image={ image }
                                        key={ idx }
                                        isFocused={ isEqual(path, ['frames', 0, 'images', idx]) }
                                        onFocusItem={ this.onFocusItem.bind(this, {path: ['frames', 0, 'images', idx], type: 'image'}) }
                                        handleChange={ handleChange }/>
                    )}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        focusItem: state.focusItem
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setFocusItem,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Frames16);