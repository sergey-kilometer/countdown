import React from 'react'
import { isEqual, range, merge, get, set } from 'lodash'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Timer from './../../Timer'
import BG from './../../Elements/BG.react'
import Hideable from './../../Elements/Hideable.react'
import AutoFitInput from './../../Elements/AutoFitInput.react'
import DraggableImage from './../../Elements/DraggableImage.react'
import { setFocusItem } from '../../../../../state/focusItem/actions'


class Frames18 extends React.Component {

    onFocusItem(focusItem, event) {
        let { setFocusItem } = this.props;
        event.preventDefault();
        event.stopPropagation();
        setFocusItem(focusItem);
    }

    componentWillReceiveProps(nextProps) {
        let { videoJson, setVideo } = nextProps,
            currentHeight = get(videoJson, ['bg', 'color', 'style', 'height'], '594px'),
            isHidden = get(videoJson, ['frames', 0, 'labels', 1, 'isHidden'], false);

        if (isHidden && currentHeight === '594px') {
            merge(videoJson, {bg: {
                video: {style: {height: '710px'}},
                image: {style: {height: '710px'}},
                color: {style: {height: '710px'}}
            }});
            setVideo(videoJson);
        } else if (!isHidden && currentHeight === '710px') {
            merge(videoJson, {bg: {
                video: {style: {height: '594px'}},
                image: {style: {height: '594px'}},
                color: {style: {height: '594px'}}
            }});
            setVideo(videoJson);
        }
    }

    handleLabelChange(index, label) {
        let { setVideo, videoJson } = this.props;
        set(videoJson, ['frames', 0, 'labels', index], label);
        setVideo(videoJson);
    }

    render() {
        let { videoJson: { frames, time, bg, schedule_type }, handleChange, focusItem: { path, type }, setFocusItem } = this.props,
            frame = frames[0],
            colors = bg.background.colors,
            timeFocusItem = {path: ['time'], type: 'timer'},
            labels = frame.labels;

        set(labels, [0, 'style', 'backgroundColor'], colors[0]);
        set(labels, [2, 'style', 'backgroundColor'], colors[1]);
        set(labels, [3, 'style', 'backgroundColor'], colors[1]);

        if (schedule_type === 'limited_stock') {
            timeFocusItem.type = 'stock';
        }

        return (
            <div className="Template18">
                <div id="scene0"
                     style={{backgroundColor: colors[2]}}>

                    <Timer time={ time }
                           type={ type }
                           onFocusItem={ this.onFocusItem.bind(this, timeFocusItem) }/>

                    <div className={ 'clockDiv ' + ('background' === type ? ' selected-div' : '') }
                         style={{backgroundColor: colors[3]}}
                         onClick={ this.onFocusItem.bind(this, {path: ['bg', 'background'], type: 'background'}) }/>

                    <BG setFocusItem={ setFocusItem }
                        handleChange={ handleChange }
                        path={ path }
                        bg={ bg }/>

                    { time.labels.map((label, idx) =>
                        <AutoFitInput key={ idx }
                                      label={ label }
                                      isFocused={ isEqual(path, ['time', 'labels', idx]) }
                                      onFocusItem={ this.onFocusItem.bind(this, {path: ['time', 'labels', idx], type: 'text'})}
                                      handleChange={ handleChange }/>
                    )}

                    { range(0, 2).map((val) => {
                        let label = labels[val];
                        return (
                            <div key={ val }>
                                <Hideable style={ label.style }
                                          id={ 'label' + val }
                                          paths={ [['frames', 0, 'labels', val, 'isHidden']] }/>

                                <AutoFitInput label={ label }
                                              isFocused={ isEqual(path, ['frames', 0, 'labels', val]) }
                                              onFocusItem={ this.onFocusItem.bind(this, {path: ['frames', 0, 'labels', val], type: 'text'})}
                                              handleChange={ this.handleLabelChange.bind(this, val) }/>
                            </div>
                        );
                    })}

                    <Hideable style={ labels[2].style }
                              id="label2"
                              paths={ [['frames', 0, 'labels', 2, 'isHidden'], ['frames', 0, 'labels', 3, 'isHidden']] }/>

                    <AutoFitInput label={ labels[2] }
                                  isFocused={ isEqual(path, ['frames', 0, 'labels', 2]) }
                                  onFocusItem={ this.onFocusItem.bind(this, {path: ['frames', 0, 'labels', 2], type: 'text'})}
                                  handleChange={ this.handleLabelChange.bind(this, 2) }/>

                    { range(3, 5).map((val) =>
                        <AutoFitInput label={ labels[val] }
                                      key={ val }
                                      isFocused={ isEqual(path, ['frames', 0, 'labels', val]) }
                                      onFocusItem={ this.onFocusItem.bind(this, {path: ['frames', 0, 'labels', val], type: 'text'})}
                                      handleChange={  this.handleLabelChange.bind(this, val) }/>
                    )}

                    { frame.images.map((image, idx) =>
                        <DraggableImage image={ image }
                                        key={ idx }
                                        isFocused={ isEqual(path, ['frames', 0, 'images', idx]) }
                                        onFocusItem={ this.onFocusItem.bind(this, {path: ['frames', 0, 'images', idx], type: 'image'})}
                                        handleChange={ handleChange }/>
                    )}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        focusItem: state.focusItem
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setFocusItem
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Frames18);