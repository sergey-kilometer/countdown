import React from 'react'
import { isEqual, rangeRight, set, cloneDeep } from 'lodash'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import BG from './../../Elements/BG.react'
import AutoFitInput from './../../Elements/AutoFitInput.react'
import { setFocusItem } from '../../../../../state/focusItem/actions'


class Frames26 extends React.Component {

    constructor(props) {
        super(props);
        this.handleTextArea = this.handleTextArea.bind(this);
    }

    onFocusItem(focusItem, event) {
        let { setFocusItem } = this.props;
        event.preventDefault();
        event.stopPropagation();
        setFocusItem(focusItem);
    }

    handleTextArea(event) {
        let { setVideo, videoJson } = this.props;
        videoJson.frames[0].labels[videoJson.frames[0].labels.length - 1].text = event.target.value;
        setVideo(videoJson);
    }

    handleLabelChange(index, label) {
        let { setVideo, videoJson } = this.props;
        set(videoJson, ['frames', 0, 'labels', index], label);
        setVideo(videoJson);
    }

    render() {
        let { videoJson:{ frames, bg, time:{ total } }, handleChange, focusItem:{ path, type }, setFocusItem } = this.props,
            frame = frames[0],
            colors = bg.background.colors,
            color1 = 12, color2 = 255,
            labels = cloneDeep(frame.labels),
            labelsLength = labels.length,
            lastLabel = labels.pop();

        set(labels, [1, 'style', 'backgroundColor'], colors[1]);

        return (
            <div className="Template18">
                <div id="scene0"
                     className={ type === 'background' ? 'selected-div' : '' }
                     style={{backgroundColor: colors[0]}}
                     onClick={ this.onFocusItem.bind(this, {path: ['bg', 'background'], type: 'background'}) }>

                    <div id="stock-container"
                         className={ type === 'stock' ? 'selected-div' : '' }
                         style={{backgroundColor: colors[2]}}
                         onClick={ this.onFocusItem.bind(this, {path: ['time'], type: 'stock'}) }>
                        <div className="stock-num" style={{color: lastLabel.style['color']}}>{ total }</div>
                        <div className="stock-num" style={{color: lastLabel.style['color']}}>0</div>
                        <div id="stock-range">
                            { rangeRight(40).map((i) =>
                                <div key={ i }
                                     className="range-block"
                                     style={{
                                         backgroundColor: `rgb(${color1 <= 255 ? color1+=13 : 255}, ${color1 >= 255 ? color2-=13 : color2}, 0)`
                                     }}/>
                            )}
                        </div>
                    </div>

                    <div id="bubble">
                        <textarea name="bubble" cols="30" rows="2"
                                  onClick={ this.onFocusItem.bind(this, {path: ['frames', 0, 'labels', labelsLength - 1], type: 'text'}) }
                                  style={{...lastLabel.style, backgroundColor: colors[2]}}
                                  value={ lastLabel.text }
                                  onChange={ this.handleTextArea }/>
                    </div>

                    { labels.map((label, labelIdx) =>
                        <AutoFitInput label={ label }
                                      key={ labelIdx }
                                      isFocused={ isEqual(path, ['frames', 0, 'labels', labelIdx]) }
                                      onFocusItem={ this.onFocusItem.bind(this, {path: ['frames', 0, 'labels', labelIdx], type: 'text'}) }
                                      handleChange={ this.handleLabelChange.bind(this, labelIdx) }/>
                    )}

                    <BG setFocusItem={ setFocusItem }
                        handleChange={ handleChange }
                        path={ path }
                        bg={ bg }/>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        focusItem: state.focusItem
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setFocusItem
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Frames26);