import React from 'react'

import Frames16 from './Frames16.react'
import Frames18 from './Frames18.react'
import Frames21 from './Frames21.react'
import Frames26 from './Frames26.react'


export default (props) => {
    switch (props.videoJson.id) {
        case 16:
            return <Frames16 {...props} />;
        case 18:
            return <Frames18 {...props} />;
        case 21:
            return <Frames21 {...props} />;
        case 26:
            return <Frames26 {...props} />;
        default:
            return null;
    }
}