import React from 'react'
import axios from 'axios'
import forOwn from 'lodash/forOwn'
import { Modal, Tabs, Tab, Button } from 'react-bootstrap'

import VoiceOverMenu from './VoiceOverMenu.react'
import { MUSIC_URL } from '../../../../constants/urls'
import { MAX_BYTE_SIZE } from '../../../../constants/common'
import musicNameFilter from '../../../../utils/musicNameFilter'
import { MUSICS, MUTE_MUSIC } from '../../../../constants/defaultMusic'

class MusicPicker extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            userMusicFiles: [],
        };
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.musicUpload = this.musicUpload.bind(this);
        this.handleMuteMusic = this.handleMuteMusic.bind(this);
    }

    componentWillMount() {
        axios.get(MUSIC_URL)
            .then((response) => {
                let data = response.data;
                if (data instanceof Array)
                    this.setState({userMusicFiles: data})
            })
    }

    close() {
        this.setState({showModal: false, playingMusic: null});
    }

    open() {
        this.setState({showModal: true});
    }

    pickMusic(musicSource) {
        let { onChange } = this.props;
        onChange(musicSource);
        this.close();
    }

    musicUpload(event) {
        let file = event.target.files[0];
        this.setState({fileTooLarge: false});
        if (file.size < MAX_BYTE_SIZE) {
            let formData = new FormData();
            formData.append('file', file);
            axios.post(MUSIC_URL, formData, {
                'Content-Type': undefined,
                'X-Requested-With': 'XMLHttpRequest'
            })
            .then((response) => {
                let { userMusicFiles } = this.state;
                this.setState({
                    userMusicFiles: userMusicFiles.unshift(response.data)
                });
            });
        } else {
            this.setState({fileTooLarge: true});
        }
    }

    handleMuteMusic() {
        let { onChange, music } = this.props;
        if (music === MUTE_MUSIC) {
            onChange(MUSICS[0]);
        } else {
            onChange(MUTE_MUSIC);
        }
    }

    playMusic(music) {
        forOwn(this.refs, (ref, key) =>  {
            if (ref.paused && key === music) {
                ref.play();
            } else {
                ref.pause();
            }
        });
        let { paused, src } = this.refs[music];
        this.setState({
            playingMusic: !paused ? decodeURIComponent(src) : null
        });
    }

    renderMusicOptions(musicOptions) {
        let { music } = this.props,
            { playingMusic } = this.state;

        return musicOptions.map((musicOption) =>
            <div key={ musicOption }
                 className={ 'music-option' + (music === musicOption ? ' music-option-selected' : '') }>

                <span className="glyphicon glyphicon-ok"/>

                <div className={ playingMusic === musicOption ? 'musicPlayed' : 'musicPaused' }>
                    <button className="play-btn"
                            onClick={ this.playMusic.bind(this, musicOption) }/>

                    <div className="music-title">
                        { musicNameFilter(musicOption) }
                    </div>

                    <Button className="choose-btn"
                            onClick={ this.pickMusic.bind(this, musicOption) }>
                        Choose
                    </Button>
                </div>

                <audio ref={ musicOption } src={ musicOption }/>
            </div>
        );
    }

    render() {
        let { music, videoMusic ,videoJson , setVideo } = this.props,
            { showModal, userMusicFiles, fileTooLarge } = this.state;

        return (
            <div id="MusicPicker">
                <span className="music-title">
                    { musicNameFilter(music) }
                </span>

                <Button className="btn-change-music"
                        onClick={ this.open }>
                    &#9836; Change Music
                </Button>

                <VoiceOverMenu videoJson={ videoJson } setVideo={ setVideo }/>

                <Modal id="MusicContentModal"
                       show={ showModal }
                       onHide={ this.close }>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Music Selector
                        </Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <Tabs defaultActiveKey={ 1 } id="music-tab">
                            <Tab eventKey={ 1 } title="Library">
                                { this.renderMusicOptions(MUSICS) }
                            </Tab>

                            <Tab eventKey={ 2 } title="Uploads">
                                <div className="music-option music-uploader">
                                    <input className="hidden"
                                           id="customMusicUploader"
                                           onChange={ this.musicUpload }
                                           type="file" accept="audio/*"/>

                                    <label htmlFor="customMusicUploader" id="customMusicUploaderLabel">
                                        <span className="glyphicon glyphicon-open upload-sign"/>
                                        <span className="upload-title">Upload your music</span>
                                        <span className="upload-warning">You must have the appropriate license to use music</span>
                                        <span className={ fileTooLarge ? 'upload-error' : 'hidden' }>
                                            Music file is to large. Please upload a file smaller than 30 MB
                                        </span>
                                    </label>
                                </div>

                                { this.renderMusicOptions(userMusicFiles) }
                            </Tab>
                        </Tabs>

                        { music === MUTE_MUSIC || music === videoMusic ?
                            <Button bsStyle="primary"
                                    onClick={ this.handleMuteMusic }
                                    className="muteMusic">
                                Enable Music
                            </Button>
                            :
                            <Button bsStyle="default"
                                    onClick={ this.handleMuteMusic }
                                    className="muteMusic">
                                Mute Music
                            </Button>
                        }
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

export default MusicPicker;