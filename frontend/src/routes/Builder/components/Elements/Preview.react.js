import React from 'react'
import axios from 'axios'
import Spinner from 'react-spinkit'
import Modal from 'react-bootstrap/lib/Modal'

import { GET_PREVIEW_URL } from '../../../../constants/urls'
import PlayBtnIcon from '../../../../images/icons/play-btn-icon.png'

class Preview extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            loading:false
        };
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state , resolve)
        });
    }

    close() {
        this.setState({showModal: false});
    }

    async open(start_time=0) {
        let { videoJson } = this.props;

        await this.setStateAsync({ showModal: true, loading: true });

        try {
            let response = await axios.post(GET_PREVIEW_URL , {
                video_json: videoJson ,
                template_version: videoJson.id ,
                zoom: 50 ,
                start_time: start_time
            });

            const iFrame = document.getElementById('previewVideo');
            iFrame.contentWindow.document.write(response.data);
        } catch (Exception) {
            alert('ERROR SHOWING PREVIEW');
            this.setState({ loading: false })
        }
    }

    render() {
        let { showModal, loading } = this.state;

        return (
            <div className="inline play-preview">
                <img onClick={ this.open } src={ PlayBtnIcon } alt=""/>

                <span>Play Preview</span>

                { showModal &&
                    <Modal id="PreviewModal" show={ showModal } onHide={ this.close }>
                        <Modal.Header closeButton>
                            <Modal.Title>Video preview</Modal.Title>

                            <span className="sub-title">
                                This is preview mode. The final HD video will offer better quality and smoother motion.
                            </span>
                        </Modal.Header>

                        <Modal.Body>
                            <iframe title="Video Preview" width="100%" height="100%" frameBorder="0" id="previewVideo"/>
                            { loading && <Spinner className="loader" name="three-bounce" color="blue"/> }
                        </Modal.Body>
                    </Modal>
                }
            </div>
        );
    }
}

export default Preview;