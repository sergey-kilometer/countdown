import React from 'react'
import set from 'lodash/set'
import { Textfit } from 'react-textfit'


class AutoFitInput extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleFitText = this.handleFitText.bind(this);
    }

    handleFitText(newFontSize) {
        let { label, handleChange, isFocused } = this.props;
        newFontSize = `${newFontSize}px`;
        if (label.style.fontSize !== newFontSize && isFocused) {
            handleChange(set(label, ['style', 'fontSize'], newFontSize));
        }
    }

    handleChange(event) {
        let { label, handleChange } = this.props;
        handleChange({...label, text: event.target.value});
    }

    render() {
        let { label:{ isManual, style:{ fontSize, ...restStyle }, text, maxFontSize=40 }, readOnly=false,
              onFocusItem=()=>{}, isFocused } = this.props;

        return (
            <div>
                { isFocused && !isManual &&
                    <Textfit className="text-editing selected-div autoFit"
                             mode="single"
                             max={ maxFontSize }
                             style={{...restStyle}}
                             onReady={ this.handleFitText }>
                        { text }
                    </Textfit>
                }

                <input type="text"
                       readOnly={ readOnly }
                       className={ 'text-editing' + (isFocused ? ' selected-div' : '') }
                       onChange={ this.handleChange }
                       onClick={ onFocusItem }
                       style={{...restStyle, fontSize}}
                       value={ text }/>
            </div>
        )
    }
}

export default AutoFitInput;