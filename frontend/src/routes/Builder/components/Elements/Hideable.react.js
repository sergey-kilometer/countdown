import React from 'react'
import { get, set }  from 'lodash'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { setVideo } from '../../../../state/video/actions'

class Hideable extends React.Component {

    handleShowHide() {
        let { paths, videoJson, setVideo } = this.props;
        paths.forEach(path => {
            let isHidden = !get(videoJson, path);
            set(videoJson, path, isHidden);
            path[path.length - 1] = 'style';
            path.push('opacity');
            set(videoJson, path, isHidden ? 0.5 : 1);
        });
        setVideo(videoJson);
    }

    static getStyle(style) {
        return {
            width: style.width,
            height: style.height,
            transform: style.transform
        }
    }

    render() {
        let { id, videoJson, paths, style } = this.props,
            isHidden = get(videoJson, paths[0]);

        return (
            isHidden ?
                <div id={ 'Hideable' + id }>
                    <div className="Hideable" id={ id } data-tooltip="Show">
                        <i className="fa fa-eye-slash fa-5x show-hide-scene fa_with_bg"
                           onClick={ this.handleShowHide.bind(this) }
                           aria-hidden="true"/>
                    </div>

                    <div className="hidden-msg" style={ Hideable.getStyle(style) }/>
                </div>
                :
                <div id={ 'Hideable' + id }>
                    <div className="Hideable" id={ id } data-tooltip="Hide">
                        <i className="fa fa-eye fa-5x show-hide-scene fa_with_bg"
                           onClick={ this.handleShowHide.bind(this) }
                           aria-hidden="true"/>
                    </div>
                </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        videoJson: state.video
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setVideo,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Hideable);