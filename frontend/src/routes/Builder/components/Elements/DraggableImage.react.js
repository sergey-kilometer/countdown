import React from 'react'
import Draggable from 'react-draggable'


class DraggableImage extends React.Component {

    constructor(props) {
        super(props);
        this.handleStopDrag = this.handleStopDrag.bind(this);
    }

    handleStopDrag(event, data) {
        event.preventDefault();
        event.stopPropagation();
        let { handleChange=()=>{}, image } = this.props;

        image.coordinates['x'] = data.x;
        image.coordinates['y'] = data.y;
        handleChange(image);
    }

    render() {
        let { image:{ src, coordinates:{ x, y, zoom }, style }, isFocused, onFocusItem } = this.props;

        return (
            <div style={{...style}}
                 onClick={ onFocusItem }
                 className={ isFocused ? 'selected-div' : ' ' }>
                <Draggable onDrag={ this.handleStopDrag }
                           disabled={ !isFocused }
                           position={{x, y}}>
                    <span>
                        <img src={ src } style={{transform: `matrix(${zoom}, 0, 0, ${zoom}, ${x}, ${y})` }} alt=""/>
                    </span>
                </Draggable>
            </div>
        )
    }
}

export default DraggableImage;