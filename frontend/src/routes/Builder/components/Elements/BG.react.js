import React from 'react'
import { isEqual, set } from 'lodash'

import DraggableImage from './DraggableImage.react'

class BG extends React.Component {

    onFocusItem(focusItem, event) {
        let { setFocusItem } = this.props;
        event.preventDefault();
        event.stopPropagation();
        setFocusItem(focusItem);
    }

    handleBGChange(path, value) {
        let { bg, handleChange } = this.props;
        set(bg, path, value);
        handleChange(bg);
    }

    handleStopDrag(video, event, data) {
        this.handleBGChange(['video', 'coordinates'], {
            x: data.x,
            y: data.y,
            zoom: video.coordinates['zoom']
        })
    }

    render() {
        let { frameIdx=0, path, bg: { type, color, video, image } } = this.props,
            onFocusItem = this.onFocusItem.bind(this, {path: ['bg'], type: 'bg', sceneNum: frameIdx});

        switch (type) {
            case 'color':
                return (
                    <div style={{...color.style}}
                         onClick={ onFocusItem }
                         className={ isEqual(path, ['bg']) ? 'selected-div' : ' ' }>
                        <div style={{...color.pattern}}/>
                    </div>
                );
            case 'video':
                return <DraggableImage image={ video }
                                       isFocused={ isEqual(path, ['bg']) }
                                       onFocusItem={ onFocusItem }
                                       handleChange={ this.handleBGChange.bind(this, ['bg', 'image']) }/>;
            default:
                return <DraggableImage image={ image }
                                       isFocused={ isEqual(path, ['bg']) }
                                       onFocusItem={ onFocusItem }
                                       handleChange={ this.handleBGChange.bind(this, ['bg', 'image']) }/>;
        }
    }
}

export default BG;