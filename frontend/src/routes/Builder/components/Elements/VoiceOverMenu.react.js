import React from 'react'
import axios from 'axios'
import { Modal, Button, Badge } from 'react-bootstrap'
import Select from 'react-select'
import swal from 'sweetalert'

import { CREATE_VOICE_OVER_URL } from '../../../../constants/urls'
import { POLLY_MAPPING, DEFAULT_HOURS_TTS, DEFAULT_DAYS_TTS, DEFAULT_LIMITED_STOCK } from '../../../../constants/amazonPolly'


class VoiceOverMenu extends React.Component {

    constructor(props) {
        super(props);
        let { videoJson: { voiceOver={} } } = this.props;

        this.state = {
            pickedLanguageCode: voiceOver.languageCode || DEFAULT_HOURS_TTS.languageCode,
            pickedVoice: voiceOver.voice || DEFAULT_HOURS_TTS.voice,
            showModal: false ,
            loading: false ,
            musicLoading: false
        };
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
        this.changeVoiceOver = this.changeVoiceOver.bind(this);
        this.playMusic = this.playMusic.bind(this);
        this.handleLanguageCodeChange = this.handleLanguageCodeChange.bind(this);
        this.handleVoiceChange = this.handleVoiceChange.bind(this);
        this.deleteVoiceOver = this.deleteVoiceOver.bind(this)
    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state , resolve)
        });
    }

    deleteVoiceOver() {
        let { videoJson, setVideo } = this.props;
        this.setState({showModal: false});
        delete videoJson['voiceOver'];
        setVideo(videoJson)
    }

    async close() {
        let { voiceOver, videoJson , setVideo } = this.props ,
            { text: unsavedText } = this.lastVoiceOver || {};

        // Show alert , unsaved voice change.
        if (voiceOver && voiceOver.text !== unsavedText && unsavedText && this.inputText.value !== voiceOver.text) {
            let shouldSave = await swal({
                title: 'Unsaved Changes' ,
                type: 'warning' ,
                text: 'Your changes were not saved' ,
                allowOutsideClick: true ,
                buttons: {
                    cancel: "Don't Save" ,
                    confirm: 'Apply Changes' ,
                }
            });

            if (shouldSave) {
                this.audioElement.oncanplaythrough = null;
                videoJson['voiceOver'] = this.lastVoiceOver;
                setVideo(videoJson)
            }
        }

        this.setState({ showModal: false });
    }

    open() {
        this.setState({ showModal: true });
    }

    async playMusic() {
        let { videoJson: { voiceOver = {} } } = this.props,
            { text, voice } = voiceOver,
            { pickedVoice } = this.state,
            currentText = this.inputText.value;
        
        if (text !== currentText || voice !== pickedVoice) {
            await this.setStateAsync({ loading: true });

            try {
                let { data: newVoiceOver } = await axios.post(CREATE_VOICE_OVER_URL, {
                    text: currentText ,
                    voice: pickedVoice
                });

                // Save the last voice over that was played.
                this.lastVoiceOver = newVoiceOver;
                // Change audio element source and play when ready.
                this.audioElement.src = newVoiceOver.src;

                this.audioElement.oncanplaythrough = async () => {
                    await this.setStateAsync({ loading: false });
                    this.audioElement.play();
                }
            } catch (Exception) {
                alert('Failed to fetch voiceover, please try again later');
                this.setState({ loading: false })
            }
        } else {
             // Nothing was changed, hear current voice over.
            this.audioElement.play();
        }
    }


    handleLanguageCodeChange({ code: pickedLanguageCode, voices: [ pickedVoice ] }) {
        this.setState({ pickedLanguageCode, pickedVoice })
    }

    handleVoiceChange({ value: pickedVoice }) {
        this.setState({ pickedVoice })
    }

    async changeVoiceOver() {
        let text = this.inputText.value ,
            { videoJson, setVideo } = this.props ,
            { pickedVoice, pickedLanguageCode } = this.state;

        await this.setStateAsync({ showModal: false });

        try {
            let { data: newVoiceOver } = await axios.post(CREATE_VOICE_OVER_URL, { text, voice: pickedVoice });
            videoJson['voiceOver'] = {...newVoiceOver, languageCode: pickedLanguageCode};
            setVideo(videoJson)
        } catch (Exception) {
            alert('Failed to create voice over, please try again later');
        }
    }

    render() {
        let { showModal, loading, pickedLanguageCode, pickedVoice } = this.state ,
            { videoJson: { voiceOver, schedule_type } } = this.props,
            defaultValue = DEFAULT_HOURS_TTS,
            languages = POLLY_MAPPING.find(({ code: languageCode }) => languageCode === pickedLanguageCode)['voices']
                                     .map(voice => ({ value: voice }));

        if (voiceOver) {
            defaultValue = voiceOver;
        } else if (schedule_type === 'days') {
            defaultValue = DEFAULT_DAYS_TTS;
        } else if (schedule_type === 'limited_stock') {
            defaultValue = DEFAULT_LIMITED_STOCK;
        }

        return (
            <i id="VoiceOverMenu" onClick={ this.open } className="inline fa fa-microphone">
                <Badge>New</Badge>

                { showModal &&
                    <Modal id="VoiceOverModal" show={ showModal } onHide={ this.close }>
                        <Modal.Header closeButton>
                            <Modal.Title>
                                <i className="fa fa-microphone" aria-hidden="true"/>Voice-Over
                            </Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            <div className="annoation-description">
                                Add voice to your video ads with our advanced text to speech technology.
                            </div>
                            { voiceOver &&
                                <Button onClick={ this.deleteVoiceOver } bsStyle="danger" className="delete-button">
                                    Disable Voice Over
                                </Button>
                            }

                            <audio src={ defaultValue.src } ref={ (audioElement) => {this.audioElement = audioElement} }/>

                            <div className="input-wrapper">
                                <textarea id="VoiceOverInput"
                                          placeholder="Enter text here."
                                          ref={ (inputText) => {this.inputText = inputText} }
                                          defaultValue={ defaultValue.text }/>

                                <i id="PlayButton"
                                   className="fa fa-play-circle"
                                   aria-hidden="true"
                                   onClick={ this.playMusic }/>

                                <div className="template-description">
                                    { schedule_type === 'limited_stock' ?
                                        <span>
                                            [x] will be replaced by the number units already sold<br/>
                                            [y] will be replaced by the number of units available
                                        </span>
                                        :
                                        <span>
                                            [x] will be replaced by the number of { schedule_type === 'days' ? 'days' : 'hours' } left
                                        </span>
                                    }
                                </div>
                            </div>

                            <div className="buttons-wrapper">
                                <Button disabled={ loading }
                                        id="VoiceOverSubmit"
                                        onClick={ this.changeVoiceOver }>
                                    { loading ? 'Processing...' : 'Apply' }
                                </Button>

                                <Select clearable={ false }
                                        onChange={ this.handleLanguageCodeChange }
                                        value={ pickedLanguageCode }
                                        labelKey={ 'name' }
                                        valueKey={ 'code' }
                                        options={ POLLY_MAPPING }/>

                                <Select clearable={ false }
                                        value={ pickedVoice }
                                        onChange={ this.handleVoiceChange }
                                        labelKey={ 'value' }
                                        valueKey={ 'value' }
                                        options={ languages }/>
                            </div>
                        </Modal.Body>
                    </Modal>
                }
            </i>
        );
    }
}

export default VoiceOverMenu;
