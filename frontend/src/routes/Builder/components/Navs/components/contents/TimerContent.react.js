import React from 'react'
import Select from 'react-select'
import set from 'lodash/set'
import moment from 'moment'
import Button from 'react-bootstrap/lib/Button'

import ColorBar from './../shared/ColorBar.react'
import TextOptions from './../contents/TextOptions'
import DateTimeZonePicker from '../../../../../../shared/DateTimeZonePicker.react'
import { TIMER_VERSIONS, DURATION_OPTIONS } from '../../../../../../constants/timer'


class TimerContent extends React.Component {

    constructor(props) {
        super(props);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleVersionChange = this.handleVersionChange.bind(this);
        this.handleDurationChange = this.handleDurationChange.bind(this);
        this.handleStyleChange = this.propertyChange.bind(this, 'style');
        this.handleColorChange = this.propertyChange.bind(this, ['style', 'color']);
        this.handleTimeZoneChange = this.propertyChange.bind(this, 'time_zone');
        this.handleBackgroundChange = this.propertyChange.bind(this, ['style', 'backgroundColor']);
    }

    propertyChange(path, value) {
        let { item, handleChange } = this.props;
        set(item, path, value);
        handleChange(item);
    }

    handleDurationChange(option) {
        let { item, handleChange } = this.props;
        item.totalDuration = option.value;
        item.start = `${option.value -1}:10:35`;
        handleChange(item);
    }

    handleVersionChange(event) {
        let { item, handleChange } = this.props;
        item.version = parseInt(event.target.id.replace('timerVersion', ''));
        if (item.version === 2) {
            item.style['color'] = 'rgb(116, 238, 156)';
            item.style['backgroundColor'] = 'black';
        } else {
            item.style['color'] = 'black';
            item.style['backgroundColor'] = 'white';
        }
        handleChange(item);
    }

    handleDateChange(newDate) {
        let { item, handleChange } = this.props,
            zone = item.time_zone.label.substring(4, 10),
            newStart = parseInt(moment.duration(newDate.zone(zone).diff(moment().zone(zone))).asDays());
        if (31 > newStart && newStart > -1) {
            item.start = newStart;
            item.destDate = newDate;
            handleChange(item);
        }
    }

    render() {
        let { item:{ style, totalDuration, time_zone, destDate=moment().add(11, 'days') }, scheduleType } = this.props;

        return (
            <div>
                { scheduleType !== 'days' ?
                    scheduleType !== 'limited_stock' &&
                        <div>
                            <div className="subMenuTitle margin-bottom-20">Campaign Duration</div>

                            <p>72 Hours is the recommended countdown campaign duration</p>

                            <Select onChange={ this.handleDurationChange }
                                    options={ DURATION_OPTIONS }
                                    className="margin-bottom-10"
                                    clearable={ false }
                                    value={ totalDuration }/>
                        </div>
                    :
                    <div>
                        <div className="subMenuTitle margin-bottom-20">Destination Date</div>

                        <DateTimeZonePicker className="timerNav"
                                            date={ destDate }
                                            minDate={ moment().add(1, "days")}
                                            timeZone={ time_zone }
                                            onChangeDate={ this.handleDateChange }
                                            onChangeTimeZone={ this.handleTimeZoneChange }/>
                    </div>
                }

                { ['days', 'limited_stock'].indexOf(scheduleType) === -1 &&
                    <div>
                        <div className="subMenuTitle margin-bottom-20">Type</div>

                        { TIMER_VERSIONS.map(value =>
                            <Button key={ value }
                                    id={ 'timerVersion' + value }
                                    onClick={ this.handleVersionChange }>
                                { value }
                            </Button>
                        )}
                    </div>
                }

                <div className="subMenuTitle margin-bottom-20">Font</div>

                <TextOptions lineThrough={ false } style={ style } handleChange={ this.handleStyleChange }/>

                <div className="text-line"/>

                <ColorBar handleChange={ this.handleColorChange }
                          selectedColor={ style['color'] }/>

                <ColorBar label="Background Color"
                          className="margin-top-0"
                          handleChange={ this.handleBackgroundChange }
                          selectedColor={ style['backgroundColor'] }/>
            </div>
        );
    }
}

export default TimerContent;