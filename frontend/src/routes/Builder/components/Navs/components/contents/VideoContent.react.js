import React from 'react'
import axios from 'axios'
import set from 'lodash/set'
import Spinner from 'react-spinkit'
import { Modal, Tabs, Tab, FormGroup, ControlLabel, Button, FormControl } from 'react-bootstrap'

import FitItem from '../shared/FitItem.react'
import RangeInput from '../shared/RangeInput.react'
import { VIDEO_URL } from '../../../../../../constants/urls'
import { MAX_BYTE_SIZE } from '../../../../../../constants/common'
import musicNameFilter from '../../../../../../utils/musicNameFilter'
import fitSelectedImage from '../../../../../../utils/fitSelectedImage'
import defaultVideos from '../../../../../../constants/defaultVideos.json'

class Video extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userVideoFiles: [],
            videoUploading: false
        };
        this.toggleModal = this.toggleModal.bind(this);
        this.videoUpload = this.videoUpload.bind(this);
        this.handleRangeChange = this.handleRangeChange.bind(this);
    }

    componentDidMount() {
        axios.get(VIDEO_URL + '?filter=.mp4')
            .then(({data}) => {
                if (data instanceof Array)
                    this.setState({userVideoFiles: data})
            })
    }

    handleRangeChange(value) {
        let { item, handleChange } = this.props;
        set(item, ['coordinates', 'zoom'], value);
        handleChange(item);
    }

    videoUpload(event) {
        let file = event.target.files[0];
        this.setState({fileTooLarge: false});
        // check for max file size
        if (file.size < MAX_BYTE_SIZE) {
            let formData = new FormData();
            formData.append('file', file);
            this.setState({videoUploading: true});
            // upload file to s3
            axios.post(VIDEO_URL, formData, {'Content-Type': undefined, 'X-Requested-With': 'XMLHttpRequest'})
                .then(({data}) => {
                    // set countdown duration to video uploaded duration
                    let video = document.createElement('video');
                    video.preload = 'metadata';
                    video.addEventListener('loadedmetadata', (e) => {
                        event.persist();
                        if (typeof video.duration === 'number') {
                            let duration = Math.round(video.duration);
                            this.props.handleDurationChange(Math.min(duration, 30), data.music);
                        }
                    });
                    video.src = data.src;
                    this.handleVideoChoose(data);
                    this.setState({videoUploading: false});
                })
                .catch(() => {
                    this.setState({videoUploading: false});
                });
        } else {
            this.setState({fileTooLarge: true});
        }
    }

    toggleModal() {
        this.setState({showModal: !this.state.showModal});
    }

    handleVideoChoose(video) {
        let { handleChange, item } = this.props,
            img = new Image();

        img.onload = () => {
            handleChange({...item, ...{
                music: video.music,
                coordinates: fitSelectedImage(img),
                videoSrc: video.src,
                src: video.not_scaled_poster,
                size: {width: img.width, height: img.height}
            }});
            this.toggleModal();
        };
        img.src = video.not_scaled_poster;
    }

    renderVideos(videos) {
        return videos.map((video, idx) =>
            <div key={ idx } className="video-option">
                <span className="video-name pull-left">{ musicNameFilter(video.src) }</span>

                <Button className="pull-right"
                        onClick={ this.handleVideoChoose.bind(this, video) }>
                    Choose
                </Button>

                <video width="100%"
                       poster={ video.poster }
                       preload="none"
                       height="90%"
                       id="videoPreview"
                       src={ video.src } controls/>
            </div>
        )
    }

    renderModal() {
        let { showModal, userVideoFiles, videoUploading, fileTooLarge } = this.state;

        return (
            <Modal id="VideoContentModal" show={ showModal } onHide={ this.toggleModal }>
                <Modal.Header closeButton>
                    <Modal.Title>Video Selector</Modal.Title>
                </Modal.Header>

                <Modal.Body id="VideoContentBody">
                    <Tabs defaultActiveKey={1} id="video-modal-tabs">
                        <Tab eventKey={1} title="Library">
                            { this.renderVideos(defaultVideos) }
                        </Tab>

                        <Tab eventKey={2} title="Uploads">
                            <div className="video-option">
                                { videoUploading ?
                                    <div className="customVideoUploaderLabel loading-video">
                                        <Spinner name="three-bounce" color="blue"/>
                                    </div>
                                    :
                                    <FormGroup controlId="customVideoUploader">
                                        <ControlLabel className="customVideoUploaderLabel">
                                            <span className="glyphicon glyphicon-open upload-sign"/>
                                            <span className="upload-title">Upload your video</span>
                                            <span className="upload-warning">You must have the appropriate license.</span>
                                            <span className={ fileTooLarge ? 'upload-error' : 'hidden' }>
                                                Video file is to large. Please upload a file smaller than 30 MB
                                            </span>
                                        </ControlLabel>

                                        <FormControl className="hidden"
                                                     type="file"
                                                     accept="video/mp4"
                                                     onChange={ this.videoUpload }/>
                                    </FormGroup>
                                }
                            </div>

                            { this.renderVideos(userVideoFiles) }
                        </Tab>
                    </Tabs>
                </Modal.Body>
            </Modal>
        );
    }

    render() {
        let { item, handleChange } = this.props;

        return (
            <div id="VideoContent" className="rangeInputs">
                { this.renderModal() }

                <div className="image-upload">
                    <video width="100%"
                           preload="none"
                           height="100%"
                           poster={ item.src }
                           src={ item.videoSrc }
                           controls/>
                </div>

                <div className="btn-repl-wrapper">
                    <FormGroup controlId="customImageUploader" className="margin-bottom-0">
                        <ControlLabel className="btn btn-repl-img" onClick={ this.toggleModal }>
                            Replace Video
                        </ControlLabel>
                    </FormGroup>
                </div>

                <RangeInput value={ item.coordinates.zoom }
                            label="Zoom"
                            max={ 4 }
                            handleChange={ this.handleRangeChange }/>

                <FitItem label="Fit Video" item={ item } handleChange={ handleChange }/>
            </div>
        );
    }
}

export default Video;