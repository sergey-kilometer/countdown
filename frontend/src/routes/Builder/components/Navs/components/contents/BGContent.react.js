import set from 'lodash/set'
import React, { Component } from 'react'

import ColorBar from '../shared/ColorBar.react'
import RangeInput from '../shared/RangeInput.react'
import { initPatterns } from '../../../../../../utils/initFunctions'

class BGContent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            patterns: initPatterns(),
        }
    }

    onChange(path, value) {
        let { item, handleChange } = this.props;
        set(item, path, value);
        handleChange(item);
    }

    static patternClass(pattern, framePattern) {
        return 'backGroundChoice' + (
                pattern.replace(/\.[^/.]+$/, '') === framePattern.replace(/\.[^/.]+$/, '').replace('url(', '')
                ? ' activeFilmPicChoice outline-5' : ''
            )
    }

    render() {
        let { item: { pattern, style: { backgroundColor } } } = this.props,
            { patterns } = this.state;

        return (
            <div id="TextNav">
                <ColorBar selectedColor={ backgroundColor }
                          className="rangeInputs"
                          handleChange={ this.onChange.bind(this, ['style', 'backgroundColor']) }/>

                <span className="subMenuTitle rangeInputs">Patterns</span>

                <div className="rangeInputs">
                    { patterns.map((patrn, idx) =>
                        <div className={ BGContent.patternClass(patrn, pattern.backgroundImage) }
                             key={ idx }
                             onClick={ this.onChange.bind(this, ['pattern', 'backgroundImage'], `url(${patrn.replace('.png', '.svg')})`) }>
                            <img className="backGroundChoiceImg" src={ patrn } alt=""/>
                        </div>
                    )}

                    <RangeInput value={ pattern.backgroundSize }
                                label="Pattern Size"
                                min={ 20 }
                                max={ 500 }
                                step={ 5 }
                                handleChange={ this.onChange.bind(this, ['pattern', 'backgroundSize']) }/>

                    <RangeInput value={ pattern.opacity }
                                label="Pattern Opacity"
                                handleChange={ this.onChange.bind(this, ['pattern', 'opacity']) }/>

                    <button className="btn btn-repl-img"
                            onClick={ this.onChange.bind(this, ['pattern', 'backgroundImage'], null) }>
                        Hide Pattern
                    </button>
                </div>
            </div>
        );
    }
}

export default BGContent;