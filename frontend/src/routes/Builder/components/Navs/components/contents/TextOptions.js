import React, { Component } from 'react'
import Select from 'react-select'
import { ButtonToolbar, Button } from 'react-bootstrap'

import { initFontSizes } from '../../../../../../utils/initFunctions'
import FONT_FAMILIES from '../../../../../../constants/fontFamilies'

class TextOptions extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fontSizes: initFontSizes(40, 445)
        };
        this.handleFontSizeChange = this.handleFontSizeChange.bind(this);
    }

    onChangeBtn(key, value) {
        let { style, handleChange } = this.props;
        if (style.hasOwnProperty(key)) {
            delete style[key]
        } else {
            style[key] = value;
        }
        handleChange(style);
    }

    onChange(key, option) {
        let { style, handleChange } = this.props;
        style[key] = option.value;
        handleChange(style);
    }

    handleFontSizeChange(option) {
        let { style, handleChange } = this.props;
        style['fontSize'] = option.value;
        handleChange(style, true);
    }

    render() {
        let { style: { fontFamily, fontSize, fontWeight, fontStyle, textDecoration }, lineThrough=true } = this.props,
            { fontSizes } = this.state;

        return (
            <div>
                <Select name="form-field-name"
                        className="margin-bottom-20"
                        clearable={ false }
                        value={ fontFamily }
                        options={ FONT_FAMILIES }
                        onChange={ this.onChange.bind(this, 'fontFamily') }/>

                <Select name="form-field-name"
                        clearable={ false }
                        value={ fontSize }
                        options={ fontSizes }
                        onChange={ this.handleFontSizeChange }/>

                <ButtonToolbar className="margin-top-20">
                    <Button className={ (fontWeight === 'bold' ? 'active': '') }
                            onClick={this.onChangeBtn.bind(this, 'fontWeight', 'bold')}>
                        <i className="fa fa-bold" aria-hidden="true"/>
                    </Button>

                    { lineThrough &&
                        <Button className={ (textDecoration === 'line-through' ? 'active': '') }
                                style={{textDecoration: 'line-through'}}
                                onClick={ this.onChangeBtn.bind(this, 'textDecoration', 'line-through') }>
                            ABC
                        </Button>
                    }

                    <Button className={ (fontStyle === 'italic' ? 'active': '') }
                            onClick={ this.onChangeBtn.bind(this, 'fontStyle', 'italic') }>
                        <i className="fa fa-italic" aria-hidden="true"/>
                    </Button>

                    <Button className={ (textDecoration === 'underline' ? ' active': '') }
                            onClick={ this.onChangeBtn.bind(this, 'textDecoration', 'underline') }>
                        <i className="fa fa-underline" aria-hidden="true"/>
                    </Button>
                </ButtonToolbar>
            </div>
        );
    }
}

export default TextOptions;