import React from 'react'
import axios from 'axios'
import set from 'lodash/set'
import { Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap'

import FitItem from '../shared/FitItem.react'
import RangeInput from '../shared/RangeInput.react'
import ShopifyProducts from '../shared/ShopifyProducts.react'
import fitSelectedImage from '../../../../../../utils/fitSelectedImage'
import MyComputerIcon from '../../../../../../images/navs/ImageNav/mycomputer-icon.png'
import ShopifyIconBlue from '../../../../../../images/navs/ImageNav/shopify-icon-blue.png'
import { CLOUDINARY_UPLOAD_URL, GET_PRODUCTS_URL } from '../../../../../../constants/urls'
import { IS_SHOPIFY } from '../../../../../../constants/common'


class ImageContent extends React.Component {

    constructor(props) {
        super(props);
        this.handleOpacity = this.handleRangeChange.bind(this, ['style', 'opacity']);
        this.handleZoom = this.handleRangeChange.bind(this, ['coordinates', 'zoom']);
        this.onImageUpload = this.onImageUpload.bind(this);
        this.handleShopify = this.handleShopify.bind(this);
    }

    componentDidMount() {
        let featherEditor = new window.Aviary.Feather({
            apiKey: '1234567',
            onSave: (imageID, newURL) => {
                let { image, handleChange } = this.props;
                // update image source
                image.src = newURL;
                handleChange(image);
                featherEditor.close();
            }
        });
        document.getElementById('imageEdit').onclick = () => {
            let { image: { src } } = this.props;
            function launchEditor() {
                featherEditor.launch({image: 'uploadImageImg', url: src});
                return false;
            }
            return launchEditor();
        };
    }

    handleShopify(product) {
        let { image, handleChange } = this.props;
        image.src = product.image;
        handleChange(image);
    }

    onImageUpload(event) {
        let reader = new FileReader();

        reader.onload = async (loadEvent) => {
            let src = loadEvent.target.result,
                formData = new FormData();

            formData.append('upload_preset', 'ya8xvjy8');
            formData.append('api_key', '392132768994591');
            formData.append('api_secret', 'DTmFTL90aoz2qQWIgVba25gz2Lw');
            formData.append('file', src);
            formData.append('tags', window.user_json ? window.user_json.username : 'sergey@kilometer.io');

            let response = await axios.post(CLOUDINARY_UPLOAD_URL, formData, { headers: {'Content-Type': undefined, 'X-Requested-With': 'XMLHttpRequest'}});
            let newImg = new Image();
                newImg.onload = () => {
                    let { image, handleChange } = this.props;
                    document.getElementById('customImageUploader').value = '';
                    // update image source
                    image.src = newImg.src;
                    // handleChange(image);
                    // make auto fit
                    image.coordinates = fitSelectedImage(newImg);
                    handleChange(image);
                };
                newImg.src = response.data.secure_url;
        };

        reader.readAsDataURL(event.target.files[0]);
    }

    handleRangeChange(path, value) {
        let { image, handleChange } = this.props;
        set(image, path, value);
        handleChange(image);
    }

    render() {
        let { image, handleChange } = this.props;

        return (
            <div>
                <div className="image-upload">
                    <img id="uploadImageImg" src={ image.src } alt=""/>
                </div>

                <div className="btn-repl-wrapper">
                    { IS_SHOPIFY ?
                        <div>
                            <ShopifyProducts productsUrl={ GET_PRODUCTS_URL }
                                             onChange={ this.handleShopify }
                                             btnClass="btn-repl-img margin-bottom-20"
                                             btnContent={
                                                 <div>
                                                     <img className="margin-right-5" src={ ShopifyIconBlue } alt=""/>
                                                     Store
                                                 </div>
                                             }/>

                            <FormGroup controlId="customImageUploader" className="margin-bottom-0">
                                <ControlLabel className="btn btn-repl-img">
                                    <img className="margin-right-5" src={ MyComputerIcon } alt=""/>
                                    Computer
                                </ControlLabel>

                                <FormControl onChange={ this.onImageUpload }
                                             className="hidden"
                                             accept="image/*"
                                             type="file"/>
                            </FormGroup>
                        </div>
                        :
                        <FormGroup controlId="customImageUploader" className="margin-bottom-0">
                            <ControlLabel className="btn btn-repl-img">
                                Replace Picture
                            </ControlLabel>

                            <FormControl onChange={ this.onImageUpload }
                                         className="hidden"
                                         accept="image/*"
                                         type="file"/>
                        </FormGroup>
                    }

                    <Button className="btn-edit-img margin-top-20 margin-bottom-20" id="imageEdit">
                        <i className="fa fa-pencil" aria-hidden="true"/>
                        Edit Picture
                    </Button>
                </div>


                <div className="rangeInputs">
                    <RangeInput value={ image.coordinates.zoom }
                                label="Zoom"
                                max={ 5 }
                                handleChange={ this.handleZoom }/>

                    <RangeInput value={ image.coordinates.opacity }
                                label="Opacity"
                                handleChange={ this.handleOpacity }/>

                    <FitItem item={ image } handleChange={ handleChange }/>
                </div>
            </div>
        );
    }
}

export default ImageContent;