import React from 'react'
import { FormGroup, ControlLabel } from 'react-bootstrap'


export default (props) => {
    let { label, value, step=0.025, min=0, max=1, handleChange } = props,
        controlId = label + 'Range';

    return (
        <FormGroup controlId={ controlId }>
            <ControlLabel>{ label }</ControlLabel>

            <input type="range"
                   step={ step }
                   min={ min }
                   max={ max }
                   value={ value }
                   onChange={ (event) => { handleChange(parseFloat(event.target.value))} }
                   id={ controlId }
                   className="input-range"/>
        </FormGroup>
    );
}