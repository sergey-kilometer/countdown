import React, { Component } from 'react'
import { SketchPicker } from 'react-color'

const defaultColorArray = ['#000000', '#ffffff', '#2ddbdb', '#ff8373', '#74ee9c'];

class ColorBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showColorPicker: false
        }
    }

    onToggleColorPicker() {
        let { showColorPicker } = this.state;
        this.setState({
            showColorPicker: !showColorPicker
        })
    }

    handleClose() {
        this.setState({showColorPicker: false})
    };

    handleChange(color) {
        let { handleChange } = this.props;
        handleChange(color);
    }

    render() {
        let { showColorPicker } = this.state,
            { selectedColor='#000000', label='Color', className='' } = this.props;

        return (
            <div className={ 'colorBar bgColorBar ' + className }>
                <span className="subMenuTitle">{ label }</span>

                { defaultColorArray.map(color =>
                    <div className="colorBox"
                         key={ color }
                         onClick={ this.handleChange.bind(this, color) }
                         style={{backgroundColor: color}}/>
                )}

                <div onClick={ this.onToggleColorPicker.bind(this) }
                     style={{backgroundColor: selectedColor}}
                     className="Plus">
                    +
                </div>

                <div className="colorPickerSection sketchPicker">
                    { showColorPicker &&
                        <div className="blurWrapper">
                            <div className="blurDiv" onClick={ this.handleClose.bind(this) }/>

                            <SketchPicker color={ selectedColor }
                                          onChange={ (color) => {this.handleChange(color.hex)} }/>
                        </div>
                    }
                </div>
            </div>
        );
    }
}

export default ColorBar;