import React, { Component } from 'react'
import { FormGroup, ControlLabel } from 'react-bootstrap'

import WidthFit from '../../../../../../images/navs/ImageNav/width-fit.png'
import HeightFit from '../../../../../../images/navs/ImageNav/height-fit.png'
import WidthFitSelected from '../../../../../../images/navs/ImageNav/width-fit-seleted.png'
import HeightFitSelected from '../../../../../../images/navs/ImageNav/height-fit-seleted.png'

const $ = window.$;

class FitItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            widthImageFit: WidthFit,
            heightImageFit: HeightFit,
        }
    }

    fitImage(property) {
        let { item, handleChange } = this.props;
        let zoom,
            itemWrapper = $('.selected-div'),
            itemW = itemWrapper.find('img');

        if (property === 'height') {
            zoom = Math.ceil(itemWrapper[0].clientHeight / itemW.height() * 100) / 100;
        } else {
            zoom = Math.ceil(itemWrapper[0].clientWidth / itemW.width() * 100) / 100;
        }

        itemW.css({transform: ` matrix(${zoom}, 0, 0, ${zoom}, 0, 0)`});
        item.coordinates = {x: itemW.position().left * -1, y: itemW.position().top * -1, zoom: zoom};
        handleChange(item);
    }

    handleFitImage(key, value) {
        this.setState({[key]: value,})
    }

    render() {
        let { heightImageFit, widthImageFit } = this.state,
            { label='Fit Image' } = this.props;

        return (
            <FormGroup controlId="fitImage">
                <ControlLabel className="margin-bottom-10">{ label }</ControlLabel>

                <div>
                    <img className="fit-img pull-left"
                         alt=""
                         onMouseOver={ this.handleFitImage.bind(this, 'widthImageFit', WidthFitSelected) }
                         onMouseLeave={ this.handleFitImage.bind(this, 'widthImageFit', WidthFit) }
                         onClick={ this.fitImage.bind(this, 'width') }
                         src={ widthImageFit }/>

                    <img className="fit-img pull-left"
                         alt=""
                         onMouseOver={ this.handleFitImage.bind(this, 'heightImageFit', HeightFitSelected) }
                         onMouseLeave={ this.handleFitImage.bind(this, 'heightImageFit', HeightFit) }
                         onClick={ this.fitImage.bind(this, 'height') }
                         src={ heightImageFit }/>
                </div>
            </FormGroup>
        );
    }
}

export default FitItem;