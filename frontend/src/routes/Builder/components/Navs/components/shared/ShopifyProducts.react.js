import React from 'react'
import Select from 'react-select'
import ReactPaginate from 'react-paginate'
import Spinner from 'react-spinkit'
import axios from 'axios'
import { Modal, Row, Button } from 'react-bootstrap'

const MAX_PRODUCTS_ON_PAGE = 12;


class ShopifyProducts extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            productSearch: '',
            selectedPage: 0,
            category: null,
            products: [],
            categories: []
        };
        this.handleProductSearch = this.handleProductSearch.bind(this);
        this.handleSelectPage = this.handleSelectPage.bind(this);
        this.handleCategory = this.handleCategory.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
    }

    componentDidUpdate(prevProps, prevState) {
        if (!prevState.showModal && this.state.showModal) {
            this.fetchProducts();
        }
    }

    fetchProducts() {
        this.setState({showLoading: true});
        axios.get(this.props.productsUrl)
            .then(({data}) => {
                this.setState({
                    products: data.products,
                    categories: data.categories.map((c) => {return{value: c}}),
                    showLoading: false
                });
            })
            .catch(() => {
                this.setState({showLoading: false});
            })
    }

    toggleModal() {
        this.setState({showModal: !this.state.showModal});
    }

    handleProduct(product) {
        this.props.onChange(product);
        this.setState({showModal: false});
    }

    handleProductSearch(e) {
        this.setState({
            productSearch: e.target.value.toLowerCase(),
            selectedPage: 0
        });
    }

    handleSelectPage(page) {
        this.setState({selectedPage: page.selected});
    }

    handleCategory(option) {
        this.setState({category: option ? option.value : null});
    }

    render() {
        let { products, categories, productSearch, selectedPage, category, showLoading, showModal } = this.state,
            { btnContent='Choose product', btnClass="" } = this.props,
            sliceStart = Math.ceil(selectedPage * MAX_PRODUCTS_ON_PAGE),
            filteredProducts = products.filter(p =>
                (!category || category === p.product_type) &&
                (productSearch === '' || p.title.toLowerCase().includes(productSearch))
            ),
            pageCount = Math.ceil(filteredProducts.length / MAX_PRODUCTS_ON_PAGE);

        return (
            <div>
                <Button className={ btnClass }
                        onClick={ this.toggleModal }>
                    { btnContent }
                </Button>

                { showModal &&
                    <Modal id="ProductModal" bsSize="large" show={ true } onHide={ this.toggleModal }>
                        <Modal.Header closeButton>
                            <Modal.Title>Product Selector</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            <div className="form-group row">
                                <div className="col-md-3">
                                    <div className="form-input-icon form-input-icon-right">
                                        <i className="fa fa-search" aria-hidden="true"/>
                                        <input type="text"
                                               value={ productSearch }
                                               onChange={ this.handleProductSearch }
                                               className="form-control"
                                               placeholder="Search Products"/>
                                    </div>
                                </div>

                                <div className="col-md-3">
                                    <Select value={ category }
                                            labelKey={ 'value' }
                                            valueKey={ 'value' }
                                            options={ categories }
                                            placeholder="Categories"
                                            onChange={ this.handleCategory }/>
                                </div>
                            </div>

                            { showLoading ?
                                <Spinner className="templateLoading" name="three-bounce" color="blue"/>
                                :
                                <div>
                                    <div className="cui-ecommerce--catalog variants-box">
                                        <Row>
                                            { filteredProducts
                                                .slice(sliceStart, sliceStart + MAX_PRODUCTS_ON_PAGE)
                                                .map((product) =>
                                                <div className="col-xl-3 col-lg-4 col-md-6 col-sm-12"
                                                     onClick={ this.handleProduct.bind(this, product) }
                                                     key={ product.variant_id }>
                                                    <div className="cui-ecommerce--catalog--item">
                                                        <div className="cui-ecommerce--catalog--item--img">
                                                            <a href="javascript: void(0);">
                                                                <img src={ product.image } alt=""/>
                                                            </a>
                                                        </div>

                                                        <div className="cui-ecommerce--catalog--item--title">
                                                            <a href="javascript: void(0);">{ product.title }</a>
                                                        </div>

                                                        <div className="cui-ecommerce--catalog--item--descr">
                                                            <p className="pull-left">${ product.price }</p>
                                                            <p className="pull-right">QTY{ product.quantity }</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            )}
                                        </Row>
                                    </div>

                                    <div className="align-center">
                                        <ReactPaginate breakLabel={<a href="">...</a>}
                                                       breakClassName="break-me"
                                                       pageCount={ pageCount }
                                                       marginPagesDisplayed={ 2 }
                                                       pageRangeDisplayed={ 5 }
                                                       onPageChange={ this.handleSelectPage }
                                                       containerClassName="pagination"
                                                       subContainerClassName="pages pagination"
                                                       activeClassName="active"/>
                                    </div>
                                </div>
                            }
                        </Modal.Body>
                    </Modal>
                }
            </div>
        );
    }
}

export default ShopifyProducts;