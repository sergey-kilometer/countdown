import React from 'react'
import { set } from 'lodash'

import ColorBar from './shared/ColorBar.react'
import NavHeader from './shared/NavHeader.react'
import TextOptions from './contents/TextOptions'
import LeftAlign from '../../../../../images/icons/right-align-button.png'
import RightAlign from '../../../../../images/icons/left-align-button.png'
import CenterAlign from '../../../../../images/icons/center-align-button.png'


class TextNav extends React.Component {

    onChange(path, value, isManual=false) {
        let { item, handleChange } = this.props;
        set(item, path, value);
        if (isManual) {
            set(item, 'isManual', true);
        }
        handleChange(item);
    }

    render() {
        let { item: { style } } = this.props;

        return (
            <NavHeader title="Text"
                       icon={ <i className="fa fa-font fa-lg margin-top-5 pull-left"/> }>
                <div className="side-bar-content">
                    <div className="subMenuTitle">Font</div>

                    <div className="text-line font-line"/>

                    <TextOptions style={ style } handleChange={ this.onChange.bind(this, ['style']) }/>

                    <div className="text-line"/>

                    <div className="subMenuTitle margin-bottom-25">Align</div>

                    <div className="margin-top-10">
                        <div className="inline radioButton">
                            <input type="radio"
                                   id="leftAlignRadioButton"
                                   onChange={(event) => {this.onChange(['style', 'textAlign'], event.target.value)}}
                                   checked={ style['textAlign'] === 'left' }
                                   value="left"/>

                            <label htmlFor="leftAlignRadioButton">
                                <img className="textAlignImage" src={ LeftAlign } alt=""/>
                            </label>
                        </div>

                        <div className="inline radioButton">
                            <input type="radio"
                                   id="centerAlignRadioButton"
                                   onChange={ (event) => {this.onChange(['style', 'textAlign'], event.target.value)} }
                                   checked={ style['textAlign'] === 'center' }
                                   value="center"/>

                            <label htmlFor="centerAlignRadioButton">
                                <img className="textAlignImage" src={ CenterAlign } alt=""/>
                            </label>
                        </div>

                        <div className="inline radioButton">
                            <input type="radio"
                                   id="rightAlignRadioButton"
                                   onChange={ (event) => {this.onChange(['style', 'textAlign'], event.target.value)} }
                                   checked={ style['textAlign'] === 'right' }
                                   value="right"/>

                            <label htmlFor="rightAlignRadioButton">
                                <img className="textAlignImage" src={ RightAlign } alt=""/>
                            </label>
                        </div>
                    </div>

                    <div className="text-line font-line"/>

                    <ColorBar handleChange={ this.onChange.bind(this, ['style', 'color']) }
                              selectedColor={ style['color'] }/>
                </div>
            </NavHeader>
        );
    }
}

export default TextNav;