import React from 'react'
import isEqual from 'lodash/isEqual'
import { SketchPicker } from 'react-color'

import NavHeader from './shared/NavHeader.react'
import BackIcon from '../../../../../images/navs/icon-background.png'
import { initSchemaColors } from '../../../../../utils/initFunctions'

class BackgroundNav extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            schemaColors: initSchemaColors(this.props.item.colors)
        };
        this.handleClose = this.handleClose.bind(this);
    }

    onChange(key, value) {
        let { item, handleChange } = this.props;
        item[key] = value;
        handleChange(item);
    }

    onToggleColorPicker(colorIdx, color) {
        let { showColorPicker } = this.props;

        this.setState({
            sketchColor: color,
            sketchColorIdx: colorIdx,
            showColorPicker: !showColorPicker
        })
    }

    handleClose() {
        this.setState({showColorPicker: false})
    };

    onChangeColor(newColor) {
        let { item: { colors } } = this.props,
            { sketchColorIdx } = this.state,
            newColors = colors;

        newColors[sketchColorIdx] = newColor;
        this.setState({sketchColor: newColor});
        this.onChange('colors', newColors)
    }

    render() {
        let { item } = this.props,
            { schemaColors, showColorPicker, sketchColor } = this.state;

        return (
            <NavHeader title="Background"
                       icon={ <img className="pull-left" src={ BackIcon } alt=""/> }>
                <div className="side-bar-content">
                    <div className="pull-left">Color scheme</div>

                    <hr className="margin-top-15 pull-left" width="91%"/>

                    <div className="row">
                        { schemaColors.map((schemaColor, indx) => {
                            let colors = schemaColor.slice(0, item.colors.length);
                            return (
                                <div key={indx}
                                     className={'color-bar-bg pull-left' + (isEqual(colors, item.colors) ? ' theme-selected': '')}
                                     onClick={ this.onChange.bind(this, 'colors', colors) }>
                                    { colors.map((color, idx) =>
                                        <div key={idx} className="colorBox" style={{backgroundColor: color}}/>
                                    )}
                                </div>
                            );
                        })}

                        <div id="patterns-schema" className="pull-left">
                            { item.colors.map((color, colorIdx) =>
                                <div key={colorIdx} className="colorPickerSection pull-left">
                                    <button className="Plus"
                                            type="button"
                                            onClick={ this.onToggleColorPicker.bind(this, colorIdx, color) }
                                            style={{backgroundColor: color}}>
                                        +
                                    </button>
                                </div>
                            )}

                            { showColorPicker &&
                                <div style={{position: 'absolute',zIndex: 2,marginTop: '40px'}}>
                                    <div style={{position: 'fixed',top: '0px',right: '0px', bottom: '0px',left: '0px',}}
                                         onClick={ this.handleClose }/>
                                    <SketchPicker color={ sketchColor }
                                                  onChange={ (color) => {this.onChangeColor(color.hex)} }/>
                                </div>
                            }
                        </div>
                    </div>

                    <hr className="margin-top-15 pull-left" width="91%"/>
                </div>
            </NavHeader>
        );
    }
}

export default BackgroundNav;