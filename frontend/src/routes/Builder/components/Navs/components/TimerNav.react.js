import React from 'react'

import NavHeader from './shared/NavHeader.react'
import BackIcon from '../../../../../images/navs/icon-background.png'
import TimerContent from './contents/TimerContent.react'


export default function Timer(props) {
    return (
        <NavHeader title="Countdown"
                   icon={ <img id="icon-text" className="pull-left" src={ BackIcon } alt=""/> }>
            <div className="side-bar-content">
                <TimerContent {...props}/>
            </div>
        </NavHeader>
    );
}