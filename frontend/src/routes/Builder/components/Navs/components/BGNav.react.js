import React from 'react'
import { ButtonGroup, Button } from 'react-bootstrap'

import NavHeader from './shared/NavHeader.react'
// import BGContent from './contents/BGContent.react'
import ImageContent from './contents/ImageContent.react'
import VideoContent from './contents/VideoContent.react'
import BackIcon from '../../../../../images/navs/icon-background.png'


class BG extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleTypeChange = this.handleTypeChange.bind(this);
    }

    handleChange(newItem) {
        let { item, handleChange } = this.props;
        item[item.type] = newItem;
        handleChange(item);
    }

    handleTypeChange(event) {
        let { item, handleChange } = this.props;
        item['type'] = event.target.id.replace('BG', '');
        handleChange(item);
    }

    render() {
        let { item:{ type, image, video }, handleDurationChange } = this.props;

        return (
            <NavHeader title="Background"
                       icon={ <img className="pull-left" src={ BackIcon } alt=""/> }>
                <div className="BG-btn-wrapper">
                    <ButtonGroup>
                        {/*<Button id="BGcolor"*/}
                                {/*onClick={ this.handleTypeChange }*/}
                                {/*className={ (type === 'color' && 'active') }>*/}
                            {/*Color*/}
                        {/*</Button>*/}

                        <Button id="BGimage"
                                onClick={ this.handleTypeChange }
                                className={ (type === 'image' && 'active') }>
                            Image
                        </Button>

                        <Button id="BGvideo"
                                onClick={ this.handleTypeChange }
                                className={ (type === 'video' && 'active') }>
                            Video
                        </Button>
                    </ButtonGroup>
                </div>

                { type === 'video' ?
                        <VideoContent item={ video }
                                      path={ ['bg'] }
                                      handleChange={ this.handleChange }
                                      handleDurationChange={ handleDurationChange }/>
                        :
                        <ImageContent image={ image }
                                      path={ ['bg'] }
                                      handleChange={ this.handleChange }/>
                }

                {/*{ type === 'color' ?*/}
                    {/*<BGContent item={ color }*/}
                               {/*path={ ['bg'] }*/}
                               {/*handleChange={ this.handleChange }/>*/}
                    {/*:*/}
                    {/*type === 'video' ?*/}
                        {/*<VideoContent item={ video }*/}
                                     {/*path={ ['bg'] }*/}
                                     {/*handleChange={ this.handleChange }*/}
                                     {/*handleDurationChange={ handleDurationChange }/>*/}
                        {/*:*/}
                        {/*<ImageContent image={ image }*/}
                                     {/*path={ ['bg'] }*/}
                                     {/*handleChange={ this.handleChange }/>*/}
                {/*}*/}
            </NavHeader>
        );
    }
}

export default BG;