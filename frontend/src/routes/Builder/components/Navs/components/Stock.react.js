import React from 'react'
import set from 'lodash/set'
import range from 'lodash/range'
import Select from 'react-select'
import { FormGroup, ControlLabel, FormControl, Button, Checkbox } from 'react-bootstrap'

import NavHeader from './shared/NavHeader.react'
import TimerContent from './contents/TimerContent.react'
import ShopifyProducts from './shared/ShopifyProducts.react'
import { GET_PRODUCTS_URL } from '../../../../../constants/urls'
import { IS_SHOPIFY } from '../../../../../constants/common'
import ShopifyIcon from '../../../../../images/icons/shopify-icon.png'

const DAYS = range(1, 8).map((i) => {return {value: i}});


class Stock extends React.Component {

    constructor(props) {
        super(props);
        this.handleTotal = this.onChangeInput.bind(this, ['total']);
        this.handleSold = this.onChangeInput.bind(this, ['sold']);
        this.handleDays = this.onChangeOption.bind(this, ['totalDuration']);
        this.onChangeProduct = this.onChangeProduct.bind(this);
        this.removeProduct = this.onChangeProduct.bind(this, null);
        this.handleAutoDecrease = this.handleAutoDecrease.bind(this);
    }

    static
    linkMyShopify() {
        window.location.href = 'http://www.shopify.com/admin/oauth/authorize?redirect_uri=' +
                            'https://countdown.topvid.com/shopify/login/finalize/&' +
                            'client_id=28b738cedc147fc9a603aa07333d0836&scope=read_products';
    }

    handleAutoDecrease() {
        let { item, handleChange } = this.props;
        item.autoDecrease = !item.autoDecrease;
        handleChange(item);
    }

    onChangeProduct(product) {
        let { item, handleChange } = this.props;
        if (product) {
            delete item.totalDuration;
            item.product = product;
            item.sold = item.total - product.quantity;
        } else {
            delete item.product;
            item.sold = 0;
            item.totalDuration = 7;
        }
        handleChange(item);
    }

    onChangeInput(path, event) {
        let { item, handleChange } = this.props,
            value = event.target.value;
        if (path[0] === 'total' && value > 100) {
            value = 100;
        }
        set(item, path, value);
        if (path[0] !== 'totalDuration') {
            set(item, 'start', item.total - item.sold);
        }
        handleChange(item);
    }

    onChangeOption(path, option) {
        let { item, handleChange } = this.props;
        set(item, path, option.value);
        handleChange(item);
    }

    onChange(path, value) {
        let { item, handleChange } = this.props;
        set(item, path, value);
        handleChange(item);
    }

    render() {
        let { item } = this.props;

        return (
            <NavHeader title="Stock Countdown"
                       icon={ <i className="fa fa-clock-o fa-lg margin-top-5 pull-left"/> }>
                <div className="side-bar-content">
                    <div className="pull-left">Stock settings</div>
                    <hr className="margin-top-15 pull-left" width="91%"/>
                    <br/>

                    <FormGroup controlId="offerUnits">
                        <ControlLabel className="stock-label">Total offer units(y)</ControlLabel>
                        <FormControl className="stock-count-form" type="number" min="1" max="100"
                                     value={ item.total }
                                     onChange={ this.handleTotal }/>
                    </FormGroup>

                    <FormGroup controlId="soldUnits">
                        <ControlLabel className="stock-label">Already sold(x)</ControlLabel>
                        <FormControl className="stock-count-form" min="0" type="number" max="100"
                                     disabled={ item.product }
                                     value={ item.sold }
                                     onChange={ this.handleSold }/>
                    </FormGroup>

                    { IS_SHOPIFY ?
                        <ShopifyProducts productsUrl={ GET_PRODUCTS_URL }
                                         btnClass="btn-shopify"
                                         onChange={ this.onChangeProduct }
                                         btnContent={
                                             <div>
                                                 <img className="sopify-icon" src={ ShopifyIcon } alt=""/>
                                                 Choose product
                                             </div>
                                         }/>
                        :
                        <Button className="btn-shopify"
                                onClick={ Stock.linkMyShopify }>
                            <img className="sopify-icon" src={ ShopifyIcon } alt=""/>
                            Link to shopify
                        </Button>
                    }

                    { item.product ?
                        <div className="cui-ecommerce--catalog--item">
                            <i className="fa fa-times" aria-hidden="true" onClick={ this.removeProduct }/>

                            <div className="cui-ecommerce--catalog--item--img">
                                <a href="javascript: void(0);">
                                    <img src={ item.product.image } alt=""/>
                                </a>
                            </div>

                            <div className="cui-ecommerce--catalog--item--title">
                                <a href="javascript: void(0);">{ item.product.title }</a>
                            </div>

                            <div className="cui-ecommerce--catalog--item--descr">
                                <p className="pull-left">${ item.product.price }</p>
                                <p className="pull-right">QTY{ item.product.quantity }</p>
                            </div>
                        </div>
                        :
                        <div>
                            <Checkbox checked={ item.autoDecrease }
                                      onClick={ this.handleAutoDecrease }>
                                Automatically decrease valuable until all stock is sold.<br/>
                            </Checkbox>

                            { item.autoDecrease &&
                                <FormGroup controlId="decreaseUnits">
                                    <div className="margin-bottom-10">Stock should be sold out in</div>
                                    <Select className="stock-count-form"
                                            clearable={ false }
                                            value={ item.totalDuration }
                                            labelKey={ 'value' }
                                            valueKey={ 'value' }
                                            options={ DAYS }
                                            onChange={ this.handleDays }/>
                                    <span className="daysDuration">days</span>
                                </FormGroup>
                            }
                        </div>
                    }

                    { item.labels &&  <TimerContent {...this.props}/> }
                </div>
            </NavHeader>
        );
    }
}

export default Stock;