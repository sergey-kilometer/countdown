import React from 'react'

import NavHeader from './shared/NavHeader.react'
import ImageContent from './contents/ImageContent.react'

export default (props) => {
    let { item, path, handleChange } = props;

    return (
        <NavHeader title="Image"
                   icon={ <i className="fa fa-picture-o fa-lg margin-top-5 pull-left"/> }>
            <ImageContent image={ item } path={ path } handleChange={ handleChange }/>
        </NavHeader>
    );
}