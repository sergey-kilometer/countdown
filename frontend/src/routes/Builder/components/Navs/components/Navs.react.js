import React from 'react'
import get from 'lodash/get'

import Stock from './Stock.react'
import BGNav from './BGNav.react'
import TextNav from './Text.react'
import ImageNav from './Image.react'
import TimerNav from './TimerNav.react'
import BackgroundNav from './Background.react'


export default function Navs(props) {
    let { focusItem:{ type, path }, videoJson, handleItemChange, handleDurationChange } = props,
        item = get(videoJson, path, {});

    switch (type) {

        case 'background':
            return <BackgroundNav item={ item } handleChange={ handleItemChange }/>;

        case 'text':
            return <TextNav item={ item } handleChange={ handleItemChange }/>;

        case 'timer':
            return <TimerNav scheduleType={ videoJson.schedule_type } item={ item } handleChange={ handleItemChange }/>;

        case 'bg':
            return <BGNav item={ item }
                          path={ path }
                          handleChange={ handleItemChange }
                          handleDurationChange={ handleDurationChange }/>;

        case 'stock':
            return <Stock scheduleType={ videoJson.schedule_type }
                          item={ item }
                          handleChange={ handleItemChange }/>;

        default:
            return <ImageNav item={ item }
                             path={ path }
                             handleChange={ handleItemChange }/>;

    }
}