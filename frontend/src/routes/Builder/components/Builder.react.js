import React from 'react'
import swal from 'sweetalert'
import get from 'lodash/get'
import Spinner from 'react-spinkit'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router-dom'
import { Button } from 'react-bootstrap'

import Navs from './Navs'
import Frames from './Frames'
import Preview from './Elements/Preview.react'
import MusicPicker from './Elements/MusicPicker.react'
import { MUTE_MUSIC } from '../../../constants/defaultMusic'
import { setFocusItem } from '../../../state/focusItem/actions'
import { fetchDefaultJson, setItem, setVideo } from '../../../state/video/actions'


class Builder extends React.Component {

    constructor(props) {
        super(props);
        this.onUnload = this.onUnload.bind(this);
        this.resetTemplate = this.resetTemplate.bind(this);
        this.handleItemChange = this.handleItemChange.bind(this);
        this.handleMusicChange = this.handleMusicChange.bind(this);
        this.handleDurationChange = this.handleDurationChange.bind(this);
    }

    componentDidMount() {
        localStorage.setItem('countdown-state', '/builder');
        window.addEventListener('beforeunload', this.onUnload)
    }

    componentWillUnmount() {
        this.onUnload();
        window.removeEventListener('beforeunload', this.onUnload)
    }

    onUnload() {
        localStorage.setObject('countdown-video', this.props.videoJson);
    }

    resetTemplate() {
        swal({
            title: 'Are you sure?', icon: 'warning',
            dangerMode: true, buttons: ['Cancel', 'OK'],
            text: 'All the changes you have made will be canceled.',
            closeOnClickOutside: true, closeOnEsc: true
        }).then((value) => {
            if (value) {
                let { fetchDefaultJson, videoJson } = this.props;
                fetchDefaultJson(videoJson.id, videoJson.schedule_type);
            }
        });
    }

    handleItemChange(newItem) {
        let { setItem } = this.props;
        setItem(newItem);
    }

    handleMusicChange(newMusic) {
        let { setVideo, videoJson } = this.props,
            videoMusic = get(videoJson, ['bg', 'video', 'music']);
        if (newMusic === MUTE_MUSIC && videoMusic) {
            videoJson.music = videoMusic;
        } else {
            videoJson.music = newMusic;
        }
        setVideo(videoJson);
    }

    handleDurationChange(newDuration, videoMusic) {
        let { setVideo, videoJson } = this.props;
        if (videoJson.music === MUTE_MUSIC && videoMusic) {
            videoJson.music = videoMusic;
        }

        videoJson.time.duration = newDuration;
        setVideo(videoJson);
    }

    render() {
        let { focusItem, videoJson, setVideo, setFocusItem } = this.props,
            isLoading = get(videoJson, ['status', 'isLoading'], false),
            videoMusic = get(videoJson, ['bg', 'video', 'music']),
            error = get(videoJson, ['status', 'error'], false);

        return (
            <section id="Builder">
                { isLoading ?
                    <Spinner className="templateLoading" name="three-bounce" color="blue"/>
                    :
                    error ?
                        <div className="alert alert-danger">{ error }</div>
                        :
                        <div className="framesWrapper">
                            <MusicPicker music={ videoJson.music }
                                         videoJson={videoJson }
                                         setVideo={setVideo}
                                         videoMusic={ videoMusic }
                                         onChange={ this.handleMusicChange }/>

                            <div className="frames-wrapper">
                                <Preview videoJson={ videoJson }/>


                                <Frames videoJson={ videoJson }
                                        setVideo={ setVideo }
                                        focusItem={ focusItem }
                                        setFocusItem={ setFocusItem }
                                        handleChange={ this.handleItemChange }/>
                            </div>
                        </div>
                }

                <div className="nav-section-wrapper no-padding box-shadow">
                    <div id="navigationSectionId" className="inline">
                        { !(isLoading || error) &&
                            <Navs videoJson={ videoJson }
                                  focusItem={ focusItem }
                                  setVideo={ setVideo }
                                  handleItemChange={ this.handleItemChange }
                                  handleDurationChange={ this.handleDurationChange }/>
                        }
                    </div>
                </div>

                <Button className="reset-template" onClick={ this.resetTemplate }>
                    <i className="fa fa-refresh margin-right-5" aria-hidden="true"/>
                    Reset Template
                </Button>
            </section>
        );
    }
}

function mapStateToProps(state) {
    return {
        videoJson: state.video,
        focusItem: state.focusItem
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setFocusItem,
        fetchDefaultJson,
        setItem,
        setVideo,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Builder));