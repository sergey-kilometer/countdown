import React from 'react'
import { Nav, NavItem, Navbar } from 'react-bootstrap'


export default () => {
    return (
        <Navbar id="top-menu" inverse>
            <Navbar.Toggle className="pull-left margin-top-15"/>
            <Navbar.Collapse className="top-menu-collapse">
                <Nav>
                    <NavItem eventKey={1}
                             className="link-top-menu"
                             href="https://video-maker.topvid.com">
                        <i className="fa fa-youtube-play fa-lg" aria-hidden="true"/>
                        <span className="nav-title">Video Ads</span>
                    </NavItem>
                    <NavItem eventKey={2}
                             className="link-top-menu"
                             href="/">
                        <i className="fa fa-clock-o fa-lg" aria-hidden="true"/>
                        <span className="nav-title">Countdown Ads</span>
                    </NavItem>
                    {/*<NavItem eventKey={3}*/}
                             {/*className="link-top-menu"*/}
                             {/*href="https://video-reports.topvid.com">*/}
                        {/*<i className="fa fa-file-video-o fa-lg" aria-hidden="true"/>*/}
                        {/*<span className="nav-title">Video Reports</span>*/}
                    {/*</NavItem>*/}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}