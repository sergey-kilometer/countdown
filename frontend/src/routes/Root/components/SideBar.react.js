import React from 'react'
import axios from 'axios'
import { connect } from 'react-redux'
import { Button } from 'react-bootstrap'
import { withRouter } from 'react-router-dom'

import PaymentPopUp from './PaymentPopUp.react'
import TopVidIcon from '../../../images/icons/topvid-icon.png'
import TopVidLogo from '../../../images/icons/topvid-logo.png'
import { LOGOUT_URL, LOGIN_URL } from '../../../constants/urls'


class SideBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {showPayment: false};
        this.togglePayment = this.togglePayment.bind(this);
    }

    togglePayment() {
        let { showPayment } = this.state;
        this.setState({
            showPayment: !showPayment
        });
    }

    static
    logOut() {
        axios.get(LOGOUT_URL).then(() => {
            window.location.href = LOGIN_URL;
        });
    }

    render() {
        let { plan_name, history: { location: { pathname } } } = this.props,
            { showPayment } = this.state;

        return (
            <div id="sidebar-menu" className="sideBarMenuContainer">
                <nav className="left-menu">
                    <div className="left-menu-head">
                        <div className="margin-top-20">
                            <a href="#/" className="logo">
                                <img src={ TopVidIcon } alt=""/>
                            </a>
                        </div>

                        <img className="TopVidLogo" src={ TopVidLogo } alt=""/>

                        <h3>Countdown Ads</h3>

                        <div className="line-separator-menu"/>
                    </div>

                    <div className="left-menu-inner scroll-pane">
                        <ul className="left-menu-list left-menu-list-root list-unstyled">
                            <li className="left-menu-list-active">
                                <a className={ 'left-menu-link ' + (!pathname.includes('/my-campaigns') && ' active-left-menu') }
                                   href="#/">
                                    <i className="left-menu-link-icon fa fa-arrow-circle-o-right"/>
                                    Templates
                                </a>
                            </li>

                            <li className="left-menu-list-active">
                                <a className={ 'left-menu-link ' + (pathname.includes('/my-campaigns') && ' active-left-menu') }
                                   href="#/my-campaigns">
                                    <i className="left-menu-link-icon fa fa-arrow-circle-o-right"/>
                                    My Campaigns
                                </a>
                            </li>

                            <li className="left-menu-list-active">
                                <a className="left-menu-link"
                                   href="http://help.topvid.com/countdown-ads" target="blank">
                                    <i className="left-menu-link-icon fa fa-question-circle-o"/>
                                    Help
                                </a>
                            </li>

                            <li className="left-menu-list-active">
                                <a className="left-menu-link"
                                   onClick={ SideBar.logOut }>
                                    <i className="left-menu-link-icon fa fa-power-off"/>
                                    Logout
                                </a>
                            </li>
                        </ul>

                        { plan_name !== 'agency' &&
                            <div>
                                <div className="planGroup">
                                    <span>Plan: <b>{ plan_name }</b></span>

                                    <Button onClick={ this.togglePayment }
                                            className="banner-btn">
                                        UPGRADE NOW
                                    </Button>
                                </div>

                                <PaymentPopUp showPayment={ showPayment } onHide={ this.togglePayment }/>
                            </div>
                        }
                    </div>
                </nav>
            </div>
        );
    }
}

function mapStateToProps(state) {
    let { countdown_plan } = state.user;
    return {
        plan_name: countdown_plan,
    };
}

export default withRouter(connect(mapStateToProps, null)(SideBar));