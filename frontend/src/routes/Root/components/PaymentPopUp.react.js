import React from 'react'
import axios from 'axios'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Modal, Tab, Row, Col, Nav, NavItem } from 'react-bootstrap'

import { PLANS } from '../../../constants/payment'
import { GET_CARD_CHECKOUT_URL } from '../../../constants/urls'
import { pullPaymentStatus } from '../../../state/user/actions'


class PaymentPopUp extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            planName: props.currentPlan === 'pro' ? 'agency' : props.currentPlan
        };
        this.onSelectPlan = this.onSelectPlan.bind(this);
        this.checkoutCard = this.checkoutCard.bind(this);
        this.onHideCreditCard = this.onHideCreditCard.bind(this);
    }

    onSelectPlan(planName) {
        this.setState({planName: planName});
    }

    checkoutCard(price) {
        this.setState({showCreditCard: true}, () => {
            document.getElementById('ccPaymentFrame').src = '';
            axios.get(GET_CARD_CHECKOUT_URL, {params: { price, sale_type: 'recurring_sale' }})
                .then((response) => {
                    document.getElementById('ccPaymentFrame').src = response.data;
                    this.props.pullPaymentStatus();
                });
        });
    }

    onHideCreditCard() {
        this.setState({showCreditCard: false}, () => {
            this.props.onHide();
        });
    }

    render() {
        let { username, showPayment, onHide, currentPlan } = this.props,
            { planName, showCreditCard } = this.state,
            plans = PLANS;

        if (currentPlan === 'pro') {
            plans = plans.slice(1, plans.length)
        }

        return (
            <div>
                <Modal id="paymentModal" show={ !showCreditCard && showPayment } onHide={ onHide }>
                    <Modal.Header closeButton>
                        <Modal.Title>Pricing</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <Tab.Container id="left-tabs-example" defaultActiveKey={ planName }>
                            <Row className="clearfix">
                                <Col sm={ 8 }>
                                    <Nav stacked onSelect={ this.onSelectPlan }>
                                        { plans.map(plan =>
                                            <NavItem eventKey={ plan.eventKey } key={ plan.name }>
                                                <Row className={ plan.name }>
                                                    <Col sm={ 8 }>
                                                        <div className="planDesc">
                                                            { planName === plan.name.toLowerCase() ?
                                                                <div className="plan-circle circle-active">
                                                                    <div className="active-circle"/>
                                                                </div>
                                                                :
                                                                <div className="plan-circle"/>
                                                            }
                                                            <span className="plan-name">{ plan.name }</span>
                                                        </div>

                                                        <ul className="planIncludes">
                                                            { plan.includes.map(item => <li key={ item }>{ item }</li> )}
                                                        </ul>
                                                    </Col>
                                                    <Col sm={ 4 }
                                                         className="priceDesc"
                                                         style={{backgroundColor: plan.bgColor}}>
                                                        <span>{ plan.priceText }</span>
                                                        <span className="regular-price">{ plan.regularPrice }</span>
                                                    </Col>
                                                </Row>
                                            </NavItem>
                                        )}
                                    </Nav>
                                </Col>
                                <Col sm={ 4 }>
                                    <Tab.Content>
                                        { plans.map(plan =>
                                            <Tab.Pane eventKey={ plan.eventKey }
                                                      key={ plan.name }
                                                      className="planContent">
                                                <div className="margin-bottom-10">
                                                    Plan: <span className="cont-plan-name">{ plan.name }</span>
                                                </div>
                                                { plan.name === 'Free' ?
                                                    <button className="pay-btn cc-btn"
                                                            onClick={ onHide }
                                                            type="button">
                                                        Choose plan
                                                    </button>
                                                    :
                                                    <div className="pull-left">
                                                        <div className="margin-bottom-10">
                                                            Price: ${ `${plan.name === 'Agency' ? plan.price + 100 : plan.price + 50}` }
                                                        </div>
                                                        <div className="discount margin-bottom-10">
                                                            % Discount: -${ plan.name === 'Agency' ? 100 : 50 }
                                                        </div>
                                                        <div className="margin-bottom-10">
                                                            Total: ${ `${plan.price}` }
                                                        </div>

                                                        <hr width="100%" size="10"/>

                                                        { currentPlan === plan.name.toLowerCase() ?
                                                            <button className="pay-btn cc-btn"
                                                                    onClick={ onHide }
                                                                    type="button">
                                                                Choose plan
                                                            </button>
                                                            :
                                                            <div>
                                                                <form className="pay-btn paypal-btn"
                                                                      action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                                                    <input type="hidden" name="cmd" value="_s-xclick"/>
                                                                    <input type="hidden" name="custom"
                                                                           value={ `${username},${plan.price},${(Date.now() + plan.price).toString()}` }/>
                                                                    <input type="hidden" name="hosted_button_id" value={ plan.hostedButtonId }/>
                                                                    <input className="submit-btn" type="submit" src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribe_SM.gif"
                                                                           name="submit" alt="PayPal - The safer, easier way to pay online!" value="PayPal"/>
                                                                    <img alt="" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1"/>
                                                                </form>

                                                                <button className="pay-btn cc-btn"
                                                                        onClick={ this.checkoutCard.bind(this, plan.price) }
                                                                        type="button">
                                                                    <i className="fa fa-credit-card"/>
                                                                    Credit Card
                                                                </button>
                                                            </div>
                                                        }
                                                    </div>
                                                }
                                            </Tab.Pane>
                                        )}
                                    </Tab.Content>
                                </Col>
                            </Row>
                        </Tab.Container>
                    </Modal.Body>
                </Modal>

                { showCreditCard &&
                    <Modal id="ccPayment" show={ showCreditCard } onHide={ this.onHideCreditCard }>
                        <Modal.Header closeButton/>
                        <Modal.Body>
                            <iframe height="100%" width="100%" id="ccPaymentFrame" title="Credit Card Payment" frameBorder="0"/>
                        </Modal.Body>
                    </Modal>
                }

            </div>
        );
    }
}

function mapStateToProps(state) {
    let { username, countdown_plan:currentPlan } = state.user;
    return {
        username,
        currentPlan
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        pullPaymentStatus
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentPopUp);