import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Route, Switch, HashRouter } from 'react-router-dom'

// import TopBar from './TopBar.react'
import SideBar from './SideBar.react'
import MyCampaigns from '../../MyCampaigns'
import CreateCampaign from '../../CreateCampaign'
import { setUser } from '../../../state/user/actions'


class Root extends React.Component {

    componentWillMount() {
        this.props.setUser(window.user_json);
    }

    render() {
        return (
            <HashRouter>
                <div>
                    <SideBar/>
                    {/*<TopBar/>*/}
                    <Switch>
                        <Route exact path='/my-campaigns' component={ MyCampaigns }/>
                        <Route path='/' component={ CreateCampaign }/>
                    </Switch>
                </div>
            </HashRouter>
        )
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setUser
    }, dispatch);
}

export default connect(null, mapDispatchToProps)(Root);