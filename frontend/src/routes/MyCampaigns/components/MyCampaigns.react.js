import axios from 'axios'
import React from 'react'
import swal from 'sweetalert'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Spinner from 'react-spinkit'
import { withRouter } from 'react-router-dom'
import { Table , Button , ButtonToolbar } from 'react-bootstrap'
import get from 'lodash/get'
import set from 'lodash/set'
import cloneDeep from 'lodash/cloneDeep'

import Banner from './Banner'
import BannerPopUp from './BannerPopUp.react'
import Icon from '../../../images/icons/icon-no-camp.png'
import { saveUser } from '../../../state/user/actions'
import { setVideo } from '../../../state/video/actions'
import { setFocusItem } from '../../../state/focusItem/actions'
import { GET_CAMPAIGNS_URL, UPDATE_CAMPAIGN_URL } from '../../../constants/urls'
import { SWAL_TEXT , SWAL_TITLE , FACEBOOK_CAMPAIGN_URL } from '../../../constants/myCampaigns'


class MyCampaigns extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            campaigns: [],
            loadingData: true,
        };
        this.handleDuplicate = this.handleDuplicate.bind(this);
        this.toggleBanner = this.toggleBanner.bind(this);
        this.saveBanner = this.saveBanner.bind(this);
    }

    handleDuplicate(campaign) {
        let { setVideo, setFocusItem, saveUser, history: { push } } = this.props;
        setVideo(campaign.video_json);
        setFocusItem(campaign.video_json.focusedItem);
        saveUser({settings: campaign.settings});
        push('/builder');
    }

    toggleBanner(campaign) {
        this.setState({
            bannerCamp: campaign,
        });
    }

    componentDidMount() {
        axios.get(GET_CAMPAIGNS_URL)
            .then((response) => {
                let { user, saveUser } = this.props,
                    data = response.data;
                if (data instanceof Array) {
                    let pending_campaign = get(user, ['settings', 'pending_campaign']);
                    this.setState({campaigns: data, loadingData: false});
                    if (pending_campaign) {
                        let camp = data.find(campaign => campaign.id === pending_campaign);
                        if (camp) {
                            swal({
                                title: SWAL_TITLE , type: 'warning' , text: SWAL_TEXT ,
                                confirmButtonColor: '#487eed' , confirmButtonText: 'Go to AdSet' ,
                                closeOnConfirm: true , allowEscapeKey: true , allowOutsideClick: true
                            }, () => {
                                window.open(FACEBOOK_CAMPAIGN_URL + `?act=${camp.account_id}&selected_ad_set_ids=
                                    ${camp.adset_targeting}&selected_campaign_ids=${camp.id}&
                                    editing=level~ad_set|initial_ids~${camp.adset_targeting}|
                                    selected_ids~${camp.adset_targeting}&
                                    open_tray=EDITOR_DRAWER`.replace(/\s/g , ''), '_blank'
                                );
                                set(user, ['settings', 'pending_campaign'], null);
                                saveUser(user);
                            })
                        }
                    }
                } else {
                    this.setState({loadingData: false});
                }
            })
            .catch(() => {
                this.setState({loadingData: false});
            })
    }

    saveBanner(bannerJson) {
        let { campaigns, bannerCamp:{ id } } = this.state,
            campIndex = campaigns.findIndex(camp => camp.id = id);
        campaigns[campIndex]['banner_json'] = bannerJson;
        this.setState({campaigns});
    };

    handleStatus(status, campaignId) {
        axios.post(UPDATE_CAMPAIGN_URL + campaignId, { status })
            .then(({data}) => {
                if (data === 'updated') {
                    let { campaigns } = this.state,
                        campIndex = campaigns.findIndex(camp => camp.id = campaignId);
                    if (campIndex > -1) {
                        if (status === 'DELETED') {
                            this.setState({campaigns: campaigns.filter(c => c.id !== campaignId)});
                        } else {
                            campaigns[campIndex]['status'] = status;
                            this.setState({campaigns});
                        }
                    }
                }
            });
    }

    static renderCampaignStatus(status) {
        if (status === 'Campaign Running') {
            return (
                <div className="campaign-running">
                    Campaign Running <Spinner name="double-bounce" color="green"/>
                </div>
            );
        } else if (status === 'Creating Ads') {
            return (
                <div>
                    Creating Ads
                    <Spinner name="three-bounce" color="orange"/>
                    (takes a few hours)
                </div>
            );
        }
        return status;
    }

    render() {
        let { campaigns, loadingData, bannerCamp } = this.state;

        return (
            <section id="MyCampaigns" className="page-content">
                <h2>My Campaigns</h2>
                <h4>The app will display analytics about the campaign</h4>

                <BannerPopUp/>

                <Table responsive>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>View on Facebook</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    { campaigns.length > 0 || loadingData ?
                        campaigns
                            .sort((a , b) => new Date(b.created_time) - new Date(a.created_time))
                            .map((campaign , idx) =>
                                <tr key={ idx }>
                                    <td>{ campaign.name }</td>
                                    <td>{ new Date(campaign.created_time).toUTCString() }</td>
                                    <td>{ MyCampaigns.renderCampaignStatus(campaign.status) }</td>
                                    <td>
                                        <a className="btn btn-default view-on-facebook-btn"
                                           href={ FACEBOOK_CAMPAIGN_URL + `?act=${campaign.account_id}&selected_campaign_ids=${campaign.id}` }
                                           target="_blank">
                                            Edit Audience
                                        </a>
                                    </td>
                                    <td>
                                        <ButtonToolbar>
                                            { campaign.status === 'Campaign Running' ?
                                                <Button className="camp-btn pause-btn margin-top-5"
                                                        onClick={ this.handleStatus.bind(this, 'PAUSED', campaign.id) }>
                                                    Pause
                                                </Button>
                                                :
                                                campaign.status !== 'Creating Ads' &&
                                                    <Button className="camp-btn start-btn margin-top-5"
                                                            onClick={ this.handleStatus.bind(this, 'ACTIVE', campaign.id) }>
                                                        Start
                                                    </Button>
                                            }

                                            <Button className="camp-btn delete-btn margin-top-5"
                                                    onClick={ this.handleStatus.bind(this, 'DELETED', campaign.id) }>
                                                Delete
                                            </Button>

                                            <Button className="camp-btn banner-small-btn margin-left-5 margin-top-5"
                                                    onClick={ this.toggleBanner.bind(this, campaign) }>
                                                Banner
                                            </Button>

                                            <Button bsStyle="info"
                                                    className="camp-btn margin-top-5"
                                                    onClick={ this.handleDuplicate.bind(this, campaign) }>
                                                Create Similar
                                            </Button>
                                        </ButtonToolbar>
                                    </td>
                                </tr>
                            )
                        :
                        <tr>
                            <td className="no-camp">
                                <img className="margin-right-15" src={ Icon } alt=""/>
                                You don't have any countdown campaigns yet
                            </td>
                        </tr>
                    }
                    </tbody>
                </Table>

                { bannerCamp  &&
                    <Banner bannerJson={ cloneDeep(bannerCamp.banner_json) }
                            bannerUrl={ bannerCamp.banner_url }
                            campaignId={ bannerCamp.id }
                            onHide={ this.toggleBanner.bind(this, null) }
                            onSave={ this.saveBanner }/>
                }
            </section>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user ,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setFocusItem,
        setVideo,
        saveUser
    } , dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MyCampaigns));