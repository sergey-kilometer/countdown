import React from 'react'
import { Button, Modal, Row, Col } from 'react-bootstrap'

import BannerExample from '../../../images/banner-example.jpg'


class BannerPopUp extends React.Component {

    constructor(props) {
        super(props);
        this.state = {showColorPickershowBanner: false}
    }

    toggleBanner() {
        let { showBanner } = this.state;

        this.setState({
            showBanner: !showBanner
        });
    }

    render() {
        let { showBanner } = this.state;

        return (
            <div>
                <Button onClick={ this.toggleBanner.bind(this) }
                        className="pull-right banner-btn">
                    Embed countdown Banner
                </Button>

                <Modal id="bannerModal" show={ showBanner } onHide={ this.toggleBanner.bind(this) }>
                    <Modal.Header closeButton>
                        <Modal.Title>Embed countdown banner</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <Row>
                            <Col lg={ 8 }>
                                <p>
                                    The countdown banner will be displayed in your website to visitors who came through the countdown Facebook ad.
                                    <br/>
                                    The time on the counter will be in sync with the time displayed in the ad's counter.
                                    <br/>
                                    Visitors who didn't come through the countdown Facebook ad will not see the countdown banner.
                                </p>

                                <hr width="100%"/>

                                <h3>JavaScript Code</h3>

                                <div className="code-wrapper">
                                    <span className="s">
                                        &lt;<span className="tag">script</span>
                                        <span className="attr"> type</span>="
                                        <span className="attr-value">text/javascript</span>"
                                        <span className="attr"> src</span>="
                                        <span className="attr-value">//countdown.topvid.com/static/assets/js/countdown-banner.js</span>"&gt;&lt;/
                                        <span className="tag">script</span>&gt;
                                    </span>
                                </div>
                            </Col>

                            <Col lg={ 4 }>
                                <img className="bnr-img" src={ BannerExample } alt=""/>
                            </Col>
                        </Row>

                        <h3>Preview countdown banner</h3>

                        <p>
                            If you wish to preview the countdown banner in your website<b>(This will only work if you have already added the JavaScript code)</b>.
                            <br/>
                            <b>Add the following parameter to your website's URL:</b> ?cd=e1290ea691df4fb284cc4a800d41aa64
                        </p>

                        <div className="banner-example">
                            <b>For example: </b>
                            <span className="banner-example-link">www.mystore.com/?cd=e1290ea691df4fb284cc4a800d41aa64</span>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

export default BannerPopUp;