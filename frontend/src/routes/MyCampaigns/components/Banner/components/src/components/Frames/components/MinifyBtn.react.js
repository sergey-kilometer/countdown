import React from 'react'

import IconV from '../../../images/iconV.png'
import IconX from '../../../images/iconX.png'
import { minifyBtnImg, minifyBtn, minifyBtnImgClose } from '../../../constants/styles/common'


class MinifyBtn extends React.Component {

    handleClose(event) {
        event.stopPropagation();
        let { hideCountdown } = this.props;
        hideCountdown();
    }

    render () {
        let { onMinimize, closeButton } = this.props;

        return (
            <div style={ minifyBtn }
                 onClick={ closeButton ? this.handleClose.bind(this) : onMinimize }>
                <img src={ closeButton ? IconX : IconV }
                     style={ closeButton ? minifyBtnImgClose : minifyBtnImg } alt=""/>
            </div>
        );
    }
}

export default MinifyBtn;