import React from 'react'
import { Textfit } from 'react-textfit'

import Timer from '../../Timer'
import PoweredBy from './PoweredBy.react'
import MinifyBtn from './MinifyBtn.react'
import Minimized from './Minimized.react'
import TimeLabels from './TimeLabels.react'
import { DEFAULT_DIMENSIONS } from '../../../constants/timer'
import { templateStyle, sceneStyle, textEditing, mobileStyle, leftStyle } from '../../../constants/styles/common'
import { timeLabel0, timeLabel1, timeLabel2, label0, label1, label2, mainText,
         middleBlock } from '../../../constants/styles/template21'


export default (props) => {
    let { videoJson: { labels, time, colors, bannerPos, schedule_type }, hideCountdown, onMinimize, isMinimized, isMobile } = props,
        tmpStyle = Object.assign({}, templateStyle, isMobile ? mobileStyle : {});

    time['dimensions'] = { ...DEFAULT_DIMENSIONS, location: 'matrix(1, 0, 0, 1, 25, 175)'};

    if (bannerPos === 'left') {
        tmpStyle = Object.assign({}, tmpStyle, leftStyle);
    }

    labels.forEach(label => {
        label['style']['position'] = 'absolute';
    });

    return (
        <div style={ tmpStyle }>
            <MinifyBtn onMinimize={ onMinimize }/>

            <div style={{ ...sceneStyle, backgroundColor: colors[0] }}>
                <Timer time={ time } hideCountdown={ hideCountdown }/>

                <div style={{ ...middleBlock, backgroundColor: colors[1] }}/>

                <div style={{ ...mainText, backgroundColor: colors[2] }}/>

                { !/^\s*$/.test(labels[0].text) &&
                    <Textfit style={ Object.assign({}, textEditing, labels[0].style, label0) }
                             mode="single"
                             max={ 30 }>
                        { labels[0].text }
                    </Textfit>
                }

                { !/^\s*$/.test(labels[1].text) &&
                    <Textfit style={ Object.assign({}, textEditing, labels[1].style, label1) }
                             mode="single"
                             max={ 50 }>
                        { labels[1].text }
                    </Textfit>
                }

                { !/^\s*$/.test(labels[2].text) &&
                    <Textfit style={
                                Object.assign({}, textEditing, labels[2].style, label2, {backgroundColor: colors[0]}, labels[2].isHidden && {display: 'none'})
                             }
                             mode="single"
                             max={ 20 }>
                        { labels[2].text }
                    </Textfit>
                }

                { schedule_type === 'days' ?
                    !/^\s*$/.test(labels[3].text) &&
                        <Textfit style={
                                    Object.assign(
                                        {},
                                        textEditing,
                                        labels[3].style, { top: '175px', left: '94px', width: '200px', lineHeight: '50px'}
                                    )
                                 }
                                 mode="single"
                                 max={ 40 }>
                            { labels[3].text }
                        </Textfit>
                    :
                    <TimeLabels labels={ time.labels } styles={ [timeLabel0, timeLabel1, timeLabel2] }/>
                }

                <PoweredBy/>
            </div>
        </div>
    )
}