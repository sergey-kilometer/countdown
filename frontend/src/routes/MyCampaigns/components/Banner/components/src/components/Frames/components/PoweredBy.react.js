import React from 'react'

import { POWERED_BY_URL } from '../../../constants/urls'
import { poweredBy } from '../../../constants/styles/common'


export default () => {
    return (
        <div style={ poweredBy } onClick={ () => {window.open(POWERED_BY_URL, '_blank')} }>
            by Topvid
        </div>
    );
}