// Timer styles
export const timeLabel0 = {
    height: '15px',
    lineHeight: '15px',
    width: '65px',
    top: '155px',
    left: '26px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none'
};
export const timeLabel1 = {
    height: '15px',
    lineHeight: '15px',
    width: '65px',
    top: '155px',
    left: '115px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none'
};
export const timeLabel2 = {
    height: '15px',
    lineHeight: '15px',
    width: '65px',
    top: '155px',
    left: '205px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none'
};
// Labels styles
export const label0 = {
    lineHeight: '50px',
    height: '50px',
    width: '225px',
    right: '100px',
    top: '25px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none',
    zIndex: 2
};
export const label1 = {
    lineHeight: '30px',
    height: '30px',
    width: '215px',
    top: '185px',
    left: '35px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none',
    cursor: 'text',
};
export const label2 = {
    lineHeight: '35px',
    height: '35px',
    width: '115px',
    top: '15px',
    left: '195px',
    paddingBottom: '5px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none',
};
export const label3 = {
    lineHeight: '35px',
    height: '35px',
    width: '115px',
    top: '50px',
    left: '195px',
    paddingBottom: '5px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none',
};
// Addition styles
export const defaultColors = ['#454540', '#856BF9', '#ffffff', '#856BF9'];
export const mainText = {
    'backgroundColor': 'rgb(133, 107, 249)',
    'position': 'absolute',
    'overflow': 'hidden',
    'zIndex': 1,
    'height': '175px',
    'width': '100%'
};