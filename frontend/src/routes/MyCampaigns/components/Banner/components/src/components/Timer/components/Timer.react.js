import React from 'react'

// import CubeTimer from './CubeTimer.react'
import FlipTimer from './FlipTimer.react'
// import BasicTimer from './BasicTimer.react'
// import SquareTimer from './SquareTimer.react'
// import DigitalTimer from './DigitalTimer.react'
import { pad, calcDays } from '../../../utils/helpers'

class Timer extends React.Component {

    constructor(props) {
        super(props);
        let start = props.time.start;
        if (props.time.countBy === 'days') {
            start = calcDays(props.time);
        }
        this.state = {start: start}
    }

    componentDidMount() {
        let intervalId;
        if (this.props.time.countBy === 'days') {
            intervalId = setInterval(this.timer.bind(this), 3600000);
        } else {
            intervalId = setInterval(this.timer.bind(this), 1000);
        }
        this.setState({intervalId: intervalId});
    }

    componentWillUnmount() {
       clearInterval(this.state.intervalId);
    }

    setNewTime(timeArray) {
        let { cookies } = this.props,
            newStart = timeArray.join(':');
        this.setState({start: newStart});
        // cookies.set(TIME_ITEM, newStart, {domain: `.${window.location.hostname}`});
    }

    timer() {
        let { hideCountdown, cookies, time } = this.props,
            { start, intervalId } = this.state;

        if (time.countBy !== 'days') {
            let timeArray = start.split(':');
            timeArray.forEach(num => parseInt(num));
            let newSeconds = timeArray[2] - 1;
            if (newSeconds < 0) {
                timeArray[2] = 59;
                let newMinutes = timeArray[1] - 1;
                if (newMinutes < 0) {
                    timeArray[1] = 59;
                    let newHours = timeArray[0] - 1;
                    if (newHours < 0) {
                        clearInterval(intervalId);
                        hideCountdown();
                    } else {
                        timeArray[0] = pad(timeArray[0] - 1);
                        this.setNewTime(timeArray);
                    }
                } else {
                    timeArray[1] = pad(newMinutes);
                    this.setNewTime(timeArray);
                }
            } else {
                timeArray[2] = pad(newSeconds);
                this.setNewTime(timeArray);
            }
        } else {
            let newStart = calcDays(time);
            if (newStart > 0) {
                this.setNewTime([newStart]);
            } else {
                clearInterval(intervalId);
                hideCountdown();
            }
        }
    }

    render() {
        let { time: { version, style, dimensions } } = this.props,
            { start } = this.state;

        return <FlipTimer start={ start } style={ style } dimensions={ dimensions }/>;

        // switch (version) {
        //     case 1:
        //         return <BasicTimer start={ start } style={ style } dimensions={ dimensions }/>;
        //     case 2:
        //         return <DigitalTimer start={ start } style={ style } dimensions={ dimensions }/>;
        //     case 3:
        //         return <FlipTimer start={ start } style={ style } dimensions={ dimensions }/>;
        //     case 4:
        //         return <CubeTimer start={ start } style={ style } dimensions={ dimensions }/>;
        //     case 5:
        //         return <SquareTimer start={ start } style={ style } dimensions={ dimensions }/>;
        //     default:
        //         return null;
        // }
    }
}

export default Timer;