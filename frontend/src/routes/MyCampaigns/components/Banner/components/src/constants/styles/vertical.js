// TopBottom Styles
import IconX from '../../images/iconX.png'

export const bottomStyle = {
    position: 'absolute',
    bottom: '77px',
    right: 0,
    zIndex: 1051,
    width: '97%',
    height: '45px',
    boxShadow: 'none',
    MozBoxShadow: 'none',
    WebkitBoxShadow: 'none',
    textAlign: 'center',
    display: 'block',
    marginRight: '10px'
};
export const daysLabelStyle = {
    width: '20%',
    display: 'inline-block',
    lineHeight: '45px',
    marginRight: '5px'
    // marginLeft: '45px',
    // marginTop: '-4px'
};
export const imageClose = {
    float: 'right',
    marginTop: '17px',
    marginRight: '1%',
    width: '8px',
    height: '8px',
    backgroundImage: `url(${IconX})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    cursor: 'pointer'
};
// Timer wrapper div style
export const timerWrapper = {
    display: 'inline-block',
    width: '195px',
    marginTop: '3px',
    marginRight: '1%',
    marginLeft: '1%',
    height: '15px',
    direction: 'ltr'
};