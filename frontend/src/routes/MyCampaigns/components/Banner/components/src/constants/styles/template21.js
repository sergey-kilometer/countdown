// Timer styles
export const timeLabel0 = {
    height: '15px',
    lineHeight: '15px',
    width: '65px',
    top: '228px',
    left: '26px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none'
};
export const timeLabel1 = {
    height: '15px',
    lineHeight: '15px',
    width: '65px',
    top: '228px',
    left: '115px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none'
};
export const timeLabel2 = {
    height: '15px',
    lineHeight: '15px',
    width: '65px',
    top: '228px',
    left: '205px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none'
};
// Labels styles
export const label0 = {
    top: '40px',
    left: '30px',
    lineHeight: '40px',
    height: '40px',
    width: '235px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none',
    zIndex: 2
};
export const label1 = {
    top: '80px',
    left: '30px',
    lineHeight: '45px',
    height: '45px',
    width: '235px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none',
    zIndex: 2
};
export const label2 = {
    top: '130px',
    left: '20px',
    lineHeight: '25px',
    height: '25px',
    width: '250px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none',
    zIndex: 2,
    cursor: 'text'
};
// Addition styles
export const middleBlock = {
    position: 'absolute',
    zIndex: 2,
    top: '35px',
    left: '25px',
    borderRadius: '5px',
    height: '100px',
    width: '245px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none'
};
export const mainText = {
    'position': 'absolute',
    'overflow': 'hidden',
    'zIndex': '1',
    top: '100px',
    'height': '160px',
    'width': '100%',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none'
};
