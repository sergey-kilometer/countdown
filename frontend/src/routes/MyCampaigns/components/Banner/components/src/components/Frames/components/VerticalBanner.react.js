import React from 'react'
import { Textfit } from 'react-textfit'

import Timer from '../../Timer'
import TimeLabels from './TimeLabels.react'
import { editTimeLabelStyle } from '../../../utils/topBottom'
import { bottomStyle, daysLabelStyle, imageClose, timerWrapper } from './../../../constants/styles/vertical'


export default function VerticalBanner(props) {
    let { videoJson:{ time, schedule_type, labels, colors, bannerPos, id }, hideCountdown, isMinimized } = props,
        timeWrapp = timerWrapper,
        imgClose = imageClose,
        daysStyle = schedule_type === 'days' ? {...labels[3].style, ...daysLabelStyle} : {},
        editTime = editTimeLabelStyle.bind(null, {top: 34, margin: 72, height: 10, width: 52}),
        tmpStyle = {...bottomStyle, backgroundColor: colors[colors.length - 1]};

    if (bannerPos === 'top') {
        delete tmpStyle['bottom'];
        tmpStyle = {...tmpStyle, top: '67px'}
    }

    time.dimensions = { height: 32, width: 200 };
    if (schedule_type === 'days') {
        timeWrapp['width'] = '50px';
        timeWrapp['height'] = '25px';
    }
    labels.forEach((label) => {label.style = {...label.style, ...{
        width: '20%',
        display: 'inline-block',
        lineHeight: '45px',
        verticalAlign: 'top',
        marginRight: '10px'
    }}});
    labels[1].style = {...labels[1].style, lineHeight: '35px', marginTop: '4px'};
    labels[2].style = {...labels[2].style, lineHeight: '35px', marginTop: '2px'};

    labels[1]['style']['backgroundColor'] = colors[0];
    labels[2]['style']['backgroundColor'] = colors[1];
    if (labels.length > 3) {
        labels[3]['style']['backgroundColor'] = colors[2];
    }

    return (
        <div style={ tmpStyle }>
            {/*<Textfit style={ labels[0].style }*/}
                     {/*mode="single"*/}
                     {/*max={ 45 }>*/}
                {/*{ labels[0].text }*/}
            {/*</Textfit>*/}

            { !/^\s*$/.test(labels[1].text) &&
                <Textfit style={ labels[1].style }
                         mode="single"
                         max={ 45 }>
                    { labels[1].text }
                </Textfit>
            }

            { isMinimized ?
                <div style={{width: '100%', textAlign: 'center'}}>
                    <div style={ timeWrapp }>
                        <Timer time={ time } hideCountdown={ hideCountdown }/>

                        { schedule_type === 'days' ?
                            !/^\s*$/.test(labels[3].text) &&
                                <Textfit style={ daysStyle }
                                         mode="single"
                                         max={ 45 }>
                                    { labels[3].text }
                                </Textfit>
                            :
                            <TimeLabels labels={ time.labels.map(editTime) }
                                        styles={ [{}, {}, {}] }/>
                        }
                    </div>
                </div>
                :
                <div style={ timeWrapp }>
                    <Timer time={ time } hideCountdown={ hideCountdown }/>

                    { schedule_type !== 'days' &&
                        <TimeLabels labels={ time.labels.map(editTime) } styles={ [{}, {}, {}] }/>
                    }
                </div>
            }

            { schedule_type === 'days' && !/^\s*$/.test(labels[3].text) &&
                <Textfit style={ daysStyle }
                         mode="single"
                         max={ 45 }>
                    { labels[3].text }
                </Textfit>
            }

            { !/^\s*$/.test(labels[2].text) &&
                <Textfit style={ labels[2].style }
                         mode="single"
                         max={ 45 }>
                    { labels[2].text }
                </Textfit>
            }

            <div style={ imgClose } onClick={ hideCountdown }/>
        </div>
    )
}