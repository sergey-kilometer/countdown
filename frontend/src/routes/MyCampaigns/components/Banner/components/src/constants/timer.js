export const DEFAULT_DIMENSIONS = {width: 255, height: 50, location: 'matrix(1, 0, 0, 1, 25, 180)'};
export const MARGIN = 7;
export const OFF_COLOR = '#22221e';

export const ITERATE_ARRAY = [0, 1, 2, 3, 4, 5];

export const DIGITAL_MAP = [
    [true, true, true, false, true, true, true],
    [false, false, true, false, false, false, true],
    [false, true, true, true, true, true, false],
    [false, true, true, true, false, true, true],
    [true, false, true, true, false, false, true],
    [true, true, false, true, false, true, true],
    [true, true, false, true, true, true, true],
    [false, true, true, false, false, false, true],
    [true, true, true, true, true, true, true],
    [true, true, true, true, false, true, true],
];