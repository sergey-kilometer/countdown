export function editTimeLabelStyle(config, label, idx) {
    delete label.style['transform'];
    label.style = {...label.style,
        height: `${config.height}px`,
        lineHeight: `${config.height}px`,
        width: `${config.width}px`,
        top: `${config.top}px`
    };
    if (idx > 0) {
        label.style.marginLeft = `${idx * config.margin}px`
    }
    return label;
}