import React from 'react'
import { Textfit } from 'react-textfit'

import LimitedStock from './elements/LimitedStock.react'
import { bottomStyle, imageClose } from './../../../constants/styles/vertical'


export default function Vertical26(props) {
    let { videoJson:{ time, labels, colors }, hideCountdown, bannerPos='top' } = props,
        imgClose = imageClose,
        stockLabel = {
            "backgroundColor": "transparent",
            "left": "20px",
            "height": "26%",
            width: '186px',
            "position": "absolute",
            "top": "-8px",
            "fontSize": "9px",
        },
        limitedStock = {
            display: 'inline-block',
            position: 'relative',
            top: 0,
            width: '239px',
            marginRight: '5px',
            height: '22px'
        },
        tmpStyle = {...bottomStyle,
            backgroundColor: colors[colors.length - 1],
            boxShadow: 'rgba(0, 0, 0, 0.75) 0px 0px 10px 1.5px',
            MozBoxShadow: 'rgba(0, 0, 0, 0.75) 0px 0px 10px 1.5px',
            WebkitBoxShadow: 'rgba(0, 0, 0, 0.75) 0px 0px 10px 1.5px',
        };

    if (bannerPos === 'top') {
        delete tmpStyle['bottom'];
        tmpStyle = {...tmpStyle, top: 0}
    }

    labels.forEach((label) => {label.style = {...label.style, ...{
        width: '100px',
        display: 'inline-block',
        lineHeight: '35px',
        verticalAlign: 'top',
        marginRight: '10px',
        marginTop: '5px'
    }}});
    labels[1].style = {...labels[1].style, backgroundColor: colors[0]};
    labels[2].style = {...labels[2].style, backgroundColor: colors[1]};
    labels[3].style = {...labels[3].style, marginTop: '1px'};
    labels[4].style = {...labels[1].style, display: 'block', lineHeight: 'none', marginTop: '0', backgroundColor: colors[2]};

    return (
        <div style={ tmpStyle }>
            { !/^\s*$/.test(labels[1].text) &&
                <Textfit style={ labels[1].style }
                         mode="single"
                         max={ 45 }>
                    { labels[1].text }
                </Textfit>
            }

            { !/^\s*$/.test(labels[2].text) &&
                <Textfit style={ labels[2].style }
                         mode="single"
                         max={ 45 }>
                    { labels[2].text }
                </Textfit>
            }

            <LimitedStock label={ labels[4] }
                          style={ limitedStock }
                          labelStyle={ stockLabel }
                          sold={ time.sold }
                          total={ time.total }/>

            { !/^\s*$/.test(labels[3].text) &&
                <Textfit style={ labels[3].style }
                         mode="single"
                         max={ 45 }>
                    { labels[3].text }
                </Textfit>
            }

            <div style={ imgClose } onClick={ hideCountdown }/>
        </div>
    )
}