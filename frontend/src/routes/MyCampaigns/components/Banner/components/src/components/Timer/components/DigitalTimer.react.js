import React from 'react'

import { DEFAULT_DIMENSIONS, MARGIN, DIGITAL_MAP, OFF_COLOR, ITERATE_ARRAY } from '../../../constants/timer'

export default (props) => {
    let { start, style, dimensions=DEFAULT_DIMENSIONS } = props,
        BLOCK_SIZE = dimensions.width / 7,
        BLOCK_HEIGHT = dimensions.height,
        resizeH = BLOCK_HEIGHT / 400.39,
        splitTime = start.split(':'),
        clockStyle = style,
        onColor = clockStyle['color'];

    let dotsStyle = {
        fontFamily: clockStyle['fontFamily'],
        color: clockStyle['color'],
        fontSize: '200px',
        position: 'absolute',
        width: `${BLOCK_SIZE / 2 - MARGIN}px`,
        lineHeight: `${BLOCK_HEIGHT}px`,
        textAlign: 'center',
        height: `${BLOCK_HEIGHT}px`,
        transform: `matrix(1, 0, 0, 1, ${BLOCK_SIZE * 2}, 0)`
    };

    return (
        <div id="clockTime"
             className="clockTime"
             style={{
                cursor: 'default',
                zIndex: 2,
                position: 'absolute',
                transform: style['transform'] || dimensions.location,
                width: `${dimensions.width}px`,
                height: `${dimensions.height}px`,
            }}>
            {/* Render digits*/}
            { ITERATE_ARRAY.map(i => {
                let currentDigit = splitTime[Math.floor(i / 2)][i % 2],
                    colorArray = DIGITAL_MAP[parseInt(currentDigit)];

                return (
                    <div key={ i }
                        className="time"
                        style={{
                            backgroundColor: clockStyle['backgroundColor'] || 'white',
                            fontSize: clockStyle['fontSize'] || '200px',
                            overflow: 'hidden',
                            textAlign: 'center',
                            position: 'absolute',
                            width: `${BLOCK_SIZE - MARGIN}px`,
                            lineHeight: `${BLOCK_HEIGHT}px`,
                            height: `${BLOCK_HEIGHT}px`,
                            transform: `matrix(1, 0, 0, 1, ${(i * BLOCK_SIZE) +
                                ((i === 2 || i === 3) ? BLOCK_SIZE / 2 : 0) +
                                ((i === 4 || i === 5) ? BLOCK_SIZE : 0) }, 0)`
                        }}>

                        <svg xmlns="http://www.w3.org/2000/svg"
                             style={{
                                 transformOrigin: 'left top',
                                 transform: `matrix(${resizeH}, 0, 0, ${resizeH}, 0, -10)`
                             }}
                             height="400" id="time4svg"
                             xmlnsXlink="http://www.w3.org/1999/xlink">
                            <polygon points="40,80 60,60 80,80 80,160 60,180 40,160"
                                     style={{fill: colorArray[0] ? onColor : OFF_COLOR}}/>
                            <polygon points="63,57 83,37 147,37 167,57 147,77 83,77"
                                     style={{fill: colorArray[1] ? onColor : OFF_COLOR}}/>
                            <polygon points="150,80 170,60 190,80 190,160 170,180 150,160"
                                     style={{fill: colorArray[2] ? onColor : OFF_COLOR}}/>
                            <polygon points="63,183 83,163 147,163 167,183 147,204 83,204"
                                     style={{fill: colorArray[3] ? onColor : OFF_COLOR}}/>
                            <polygon points="40,206 60,187 80,207 80,287  60,307 40,287"
                                     style={{fill: colorArray[4] ? onColor : OFF_COLOR}}/>
                            <polygon points="63,310 83,290 147,290 167,310 147,330 83,330"
                                     style={{fill: colorArray[5] ? onColor : OFF_COLOR}}/>
                            <polygon points="150,206 169,187 190,207 190,287  170,307 150,287"
                                     style={{fill: colorArray[6] ? onColor : OFF_COLOR}}/>
                        </svg>
                    </div>
                );
            })}
            {/* Render separators*/}
            <div style={ dotsStyle }>:</div>

            <div style={ Object.assign(dotsStyle, {transform: `matrix(1, 0, 0, 1, ${BLOCK_SIZE * 4.5}, 0)`}) }>
                :
            </div>
        </div>
    );
}