import React from 'react'
import axios from 'axios'
import get from 'lodash/get'
import set from 'lodash/set'
import cloneDeep from 'lodash/cloneDeep'
import { SketchPicker } from 'react-color'
import Spinner from 'react-spinkit'
import { Button, Modal, Row, Col, ButtonToolbar, FormGroup, HelpBlock } from 'react-bootstrap'

import BannerPreview from './src/components/Frames'
import BannerBack from '../../../../../images/banner-back.png'
import { UPDATE_CAMPAIGN_URL } from '../../../../../constants/urls'

const POSITIONS = [
    {label: 'Left Box', value: 'left'},
    {label: 'Right Box', value: 'right'},
    {label: 'Top Bar', value: 'top'},
    {label: 'Bottom Bar', value: 'bottom'},
];


class Banner extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            bannerJson: props.bannerJson,
            showColorPicker: false,
            pickerPath: [],
            isSaving: false,
            isChanged: false
        };
        this.handleSave = this.handleSave.bind(this);
        this.onCopy = this.onCopy.bind(this);
        this.handleColor = this.handleColor.bind(this);
        this.handleBannerPosition = this.handleBannerPosition.bind(this);
    }

    onCopy() {
        this.setState({ copied: true });
    }

    handleSave() {
        let { bannerJson } = this.state;
        this.setState({isSaving: true});
        axios.post(UPDATE_CAMPAIGN_URL + this.props.campaignId, { bannerJson })
            .then(({data}) => {
                if (data === 'updated') {
                    this.setState({isSaving: false, isChanged: false});
                    this.props.onSave(bannerJson);
                }
            });
    }

    toggleColorPicker(path) {
        let { showColorPicker } = this.state;
        this.setState({
            showColorPicker: !showColorPicker,
            pickerPath: path
        });
    }

    handleColor(newColor) {
        let { pickerPath, bannerJson } = this.state;
        set(bannerJson, pickerPath, newColor.hex);
        this.setState({ bannerJson, isChanged: true });
    }

    handleText(index, e) {
        let { bannerJson } = this.state;
        bannerJson.labels[index]['text'] = e.target.value;
        this.setState({ bannerJson, isChanged: true });
    }

    handleBannerPosition(event) {
        let { bannerJson } = this.state,
            { id, bannerPos, labels } = bannerJson,
            newPosition = event.target.value;
        if (id === 18) {
            if (bannerPos.match(/^(left|right)$/) && newPosition.match(/^(top|bottom)$/)) {
                // change from vertical to horizontal
                labels[1].text = labels[1].text + ' ' + labels[2].text;
                labels.splice(2, 1);
                bannerJson.labels = labels;
            }  else if (bannerPos.match(/^(top|bottom)$/) && newPosition.match(/^(left|right)$/)) {
                // change from horizontal to vertical
                let splitArray = labels[1].text.split(' ');
                labels[1].text = splitArray.length > 0 ? splitArray[0] : '';
                labels.splice(2, 0, {...labels[1], text: splitArray.length > 1 ? splitArray[1] : ''});
                bannerJson.labels = labels;
            }
        }
        bannerJson.bannerPos = newPosition;
        this.setState({ bannerJson, isChanged: true });
    }

    render() {
        let { bannerJson, showColorPicker, pickerPath, copied, isSaving, isChanged } = this.state;

        return (
            <Modal id="bannerModal" className="bannerEdit" show={ true } onHide={ this.props.onHide }>
                <Modal.Header closeButton>
                    <Modal.Title componentClass="h3">
                        <b>Embed countdown banner</b>
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Row>
                        <Col lg={ 7 }>
                            <p>
                                The countdown banner will be displayed in your website to visitors who came through the countdown Facebook ad.
                                The time on the counter will be in sync with time displayed in the ads counter
                            </p>

                            <h3 className="margin-bottom-25 margin-top-25">JavaScript Code</h3>

                            <div className="code-wrapper">
                                <span className="s">
                                    &lt;<span className="tag">script</span>
                                    <span className="attr"> type</span>="
                                    <span className="attr-value">text/javascript</span>"
                                    <span className="attr"> src</span>="
                                    <span className="attr-value">//countdown.topvid.com/static/assets/js/countdown-banner.js</span>"&gt;&lt;/
                                    <span className="tag">script</span>&gt;
                                </span>
                            </div>
                            <br/>

                            { copied && <span style={{color: 'red'}}>Copied.</span> }

                            <hr className="pull-left" width="95%"/>

                            <FormGroup className="margin-bottom-5">
                                <h3 className="pull-left margin-top-3">Choose Banner style:</h3>

                                { POSITIONS.map((pos, idx) =>
                                    <div className="radio inline-block" key={ pos.value }>
                                        <input id={`radio-${idx}`}
                                               value={ pos.value }
                                               name="bannerPos" type="radio"
                                               onChange={ this.handleBannerPosition }
                                               checked={ bannerJson.bannerPos === pos.value }/>
                                        <label htmlFor={`radio-${idx}`} className="radio-label">
                                            { pos.label }
                                        </label>
                                    </div>
                                )}
                            </FormGroup>

                            <hr className="pull-left" width="95%"/>

                            <h3>Customize Banner:</h3>

                            { bannerJson.labels.map((label, idx) => {
                                if ((bannerJson.bannerPos.match(/^(top|bottom)$/) && idx === 0)
                                    || (bannerJson.id === 26 && idx === 4)) {
                                    return <div key={ idx }/>;
                                }
                                return (
                                    <div className="inline-block" key={ idx }>
                                        <input className="bannerLabel"
                                               type="text"
                                               onChange={ this.handleText.bind(this, idx) }
                                               value={ label.text }/>

                                        <div className="Plus"
                                             onClick={ this.toggleColorPicker.bind(this, ['labels', idx, 'style', 'color']) }
                                             style={{backgroundColor: label.style.color}}/>
                                    </div>
                                );
                            })}

                            <div className="sketchPicker">
                                { showColorPicker && pickerPath[0] === 'labels' &&
                                    <div className="blurWrapper">
                                        <div className="blurDiv" onClick={ this.toggleColorPicker.bind(this, []) }/>

                                        <SketchPicker color={ get(bannerJson, pickerPath, '#fffff') || '#fffff' }
                                                      onChange={ this.handleColor }/>
                                    </div>
                                }
                            </div>
                        </Col>

                        <Col lg={ 5 } style={{padding: '0px'}}>
                            <img className="bnr-img" src={ BannerBack } alt=""/>

                            <BannerPreview videoJson={ cloneDeep(bannerJson) }
                                           isMobile={ false }
                                           isMinimized={ false }
                                           onMinimize={ () => {} }
                                           hideCountdown={ () => {} }/>

                            <div id="patterns-schema" className="pull-left">
                                <h4 className="pull-left"><b>Color Schema</b></h4>

                                <div className="pull-left colorBarWrapper margin-left-10">
                                    { bannerJson.colors.map((color, colorIdx) =>
                                        <div key={ colorIdx } className="colorPickerSection pull-left">
                                            <div style={{backgroundColor: color}}
                                                 className="colorBox"
                                                 onClick={ this.toggleColorPicker.bind(this, ['colors', colorIdx]) }/>
                                        </div>
                                    )}
                                </div>

                                <div className="sketchPicker colorSchemaPicker">
                                    { showColorPicker && pickerPath[0] !== 'labels' &&
                                        <div className="blurWrapper">
                                            <div className="blurDiv" onClick={ this.toggleColorPicker.bind(this, []) }/>

                                            <SketchPicker color={ get(bannerJson, pickerPath, '#fffff') || '#fffff' }
                                                          onChange={ this.handleColor }/>
                                        </div>
                                    }
                                </div>
                            </div>
                        </Col>
                    </Row>

                    <ButtonToolbar>
                        <Button className="btn-save-banner" onClick={ this.handleSave } disabled={ isSaving }>Save</Button>

                        { isSaving ?
                            <Spinner className="loader margin-top-10" name="three-bounce" color="blue"/>
                            :
                            !isChanged &&
                                <Button bsStyle="info"
                                        onClick={ ()=>{window.open(this.props.bannerUrl, '_blank')}  }>
                                    Show Preview
                                </Button>
                        }
                    </ButtonToolbar>

                    <HelpBlock>Preview will only work if you have added the JS code to your web page.</HelpBlock>
                </Modal.Body>
            </Modal>
        );
    }
}

export default Banner;