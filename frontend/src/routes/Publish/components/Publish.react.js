import React from 'react'
import get from 'lodash/get'
import Spinner from 'react-spinkit'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import SettingsForm from './SettingsForm'
import LinkFacebook from './LinkFacebook.react'
import { setUser } from '../../../state/user/actions'
import { saveCampaign, saveUser } from '../../../state/user/actions'
import { setVideo } from '../../../state/video/actions'


class Publish extends React.Component {

    componentWillMount() {
        window.localStorage.setItem('countdown-state', '/publish');
    }

    render() {
        let { user, saveUser, setUser, video, saveCampaign, setVideo } = this.props,
            isSaving = get(user, ['status', 'isSaving'] , false);

        return (
            <section id="Publish">
                { isSaving ?
                    <Spinner className="templateLoading" name="three-bounce" color="blue"/>
                    :
                    user.isConnected ?
                        <SettingsForm user={ user }
                                      video={ video }
                                      setUser={ setUser }
                                      setVideo={ setVideo }
                                      saveCampaign={ saveCampaign }/>
                        :
                        <LinkFacebook user={ user } saveUser={ saveUser }/>
                }
            </section>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        video: state.video,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        saveUser,
        setUser,
        saveCampaign,
        setVideo
    } , dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Publish);
