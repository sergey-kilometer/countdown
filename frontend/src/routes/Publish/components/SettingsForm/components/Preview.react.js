import axios from 'axios'
import React from 'react'
import { Button } from 'react-bootstrap'
import Textarea from 'react-textarea-autosize'

import { GET_PREVIEW_URL } from '../../../../../constants/urls'


class Preview extends React.Component {

    componentWillReceiveProps(nextProps) {
        if (nextProps.video.time.start !== this.props.video.time.start) {
            Preview.getVideoPreview(nextProps.video);
        }
    }

    componentDidMount() {
        Preview.getVideoPreview(this.props.video);
    }

    static
    getVideoPreview(video) {
        document.getElementById('publishPreview').contentWindow.document.location.reload(true);
        axios.post(GET_PREVIEW_URL, {
            video_json: video,
            template_version: video.id,
            zoom: 40,
            is_music: false,
            start_time: 0
        }).then((response) => {
            document.getElementById('publishPreview').contentWindow.document.write(response.data);
        });
    }

    render() {
        let { ad_message, ad_headline, ad_link_desc, call_to_action='NO_BUTTON', ad_link_display,
              call_to_action_label='' } = this.props;

        return (
            <div>
                <h3>Preview</h3>
                <div className="previewWrapper">
                    <div>
                        <Textarea className="previewTextArea"
                                  value={ ad_message === '' ? 'Your text here' : ad_message } readOnly/>
                    </div>

                    <iframe id="publishPreview" title="Preview"/>

                    <div className="previewSpanWrapper">
                        <span>{ ad_headline === '' ? 'Your headline here' : ad_headline}</span>
                        <span>{ ad_link_desc === '' ? 'Your link description here' : ad_link_desc }</span>
                        <span>{ ad_link_display === '' ? 'Your display link here' : ad_link_display }</span>
                    </div>

                    <Button className={ 'callToAction-btn' + (call_to_action === 'NO_BUTTON' ? ' hidden' : '')}>
                        { call_to_action_label }
                    </Button>
                </div>
            </div>
        );
    }
}

export default Preview;