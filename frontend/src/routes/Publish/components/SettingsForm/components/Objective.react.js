import React from 'react'
import axios from 'axios'
import Select from 'react-select'
import { FormGroup, Radio, FormControl } from 'react-bootstrap'

import { GET_CONVERSIONS_URL } from '../../../../../constants/urls'

const OBJECTIVES = [
    {value: 'LINK_CLICKS', label: 'Traffic'},
    {value: 'REACH', label: 'Reach'},
    {value: 'CONVERSIONS', label: 'Conversions'},
];

function transformOptions(options) {
    const option = (value, label, render, disabled = false) => ({value, label, render, disabled});
    return options.reduce((acc, o) => {
        const parent = option(o.value, o.label, (<strong style={{color: '#000'}}>{o.label}</strong>), true);
        const children = o.children.map(c => option(c.value, c.label, <div style={{paddingLeft: 10}}>{c.label}</div>));
        return acc.concat(parent).concat(children);
    }, []);
}

class Objective extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            customConversions: [],
        };
        this.handleFrequency = this.handleChange.bind(this, 'max_frequency');
        this.handleObjective = this.handleChange.bind(this, 'objective');
        this.handleIntervalDays = this.handleChange.bind(this, 'interval_days');
        this.handleConversion = this.handleChangeConversion.bind(this);
    }

    componentDidMount() {
        let { settings } = this.props;
        this.getCustomConversions(settings.ad_account);
    }

    componentWillReceiveProps(nextProps) {
        let { settings } = nextProps;
        if (settings.ad_account !== this.props.settings.ad_account) {
            this.getCustomConversions(settings.ad_account);
        }
    }

    getCustomConversions(ad_account) {
        if (ad_account) {
            axios.get(GET_CONVERSIONS_URL + `?account=${ad_account}`)
                .then(({ data }) => {
                    this.setState({ customConversions: data }, () => {
                        this.handleChangeConversion(data.length > 0 ? data[0].children[0] : {});
                    });
                });
        }
    }

    handleChange(key, event) {
        let { setSettings, settings } = this.props;
        settings[key] = event.target.value;
        setSettings(settings);
    }

    handleChangeConversion(customConversion) {
        let { setSettings, settings } = this.props;
        setSettings({ ...settings, customConversion});
    }

    render() {
        let { settings:{ objective='LINK_CLICKS', max_frequency=1, interval_days=90, customConversion={} } } = this.props,
            { customConversions } = this.state;

        return (
            <div>
                <h3>Objective</h3>

                <FormGroup>
                    { OBJECTIVES.map(obj =>
                        <Radio name="radioGroup"
                               key={ obj.value }
                               value={ obj.value }
                               checked={ objective === obj.value }
                               onChange={ this.handleObjective }
                               inline>
                            { obj.label }
                        </Radio>
                    )}
                </FormGroup>

                { objective === 'REACH' ?
                    <div>
                        <FormGroup>
                            <p>Frequency cup</p>

                            <FormControl className="frequency-form inline-block"
                                         value={ max_frequency || 1 }
                                         onChange={ this.handleFrequency }
                                         min="3"
                                         type="number"/>

                            <span> impression every </span>

                            <FormControl className="frequency-form inline-block"
                                         value={ interval_days || 90 }
                                         onChange={ this.handleIntervalDays }
                                         min="3"
                                         type="number"/>

                            <span> days </span>
                        </FormGroup>
                    </div>
                    :
                    objective === 'CONVERSIONS' &&
                        <Select className="pub-select"
                                clearable={ false }
                                value={ customConversion.value }
                                options={ transformOptions(customConversions) }
                                optionRenderer={ option => option.render }
                                onChange={ this.handleConversion }/>
                }
            </div>
        );
    }
}

export default Objective;
