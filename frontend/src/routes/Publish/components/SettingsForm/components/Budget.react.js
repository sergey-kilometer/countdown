import React from 'react'
import { FormControl, FormGroup, ControlLabel, HelpBlock, InputGroup } from 'react-bootstrap'

export default ({ budgetValid, settings:{ currency, adset_budget }, onChange }) => {
    return (
        <div>
            <h3>Budget</h3>

            <FormGroup controlId="formAdSetBudget">
                <ControlLabel>Daily budget</ControlLabel>

                <InputGroup className="budget-form">
                    <InputGroup.Addon>{ currency }</InputGroup.Addon>

                    <FormControl className="budget-form"
                                 value={ adset_budget || 50 }
                                 onChange={ onChange }
                                 min="5"
                                 type="number"/>

                    <InputGroup.Addon>.00</InputGroup.Addon>
                </InputGroup>

                { !budgetValid &&
                    <HelpBlock className="error-text">
                        The budget for this campaign must be at least { currency }5.00.
                    </HelpBlock>
                }

                <HelpBlock>Minimum Budget $5</HelpBlock>
            </FormGroup>
        </div>
    );
}
