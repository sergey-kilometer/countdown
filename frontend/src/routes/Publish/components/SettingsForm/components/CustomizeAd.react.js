import React from 'react'
import Select from 'react-select'
import { FormControl, FormGroup, ControlLabel, HelpBlock } from 'react-bootstrap'

import FieldGroup from './FieldGroup.react'
import { CALL_TO_ACTIONS } from '../../../../../constants/settingsForm'

class CustomizeAd extends React.Component {

    constructor(props) {
        super(props);
        this.handleMessage = this.handleChange.bind(this , 'ad_message');
        this.handleHeadline = this.handleChange.bind(this, 'ad_headline');
        this.handleLinkDesc = this.handleChange.bind(this, 'ad_link_desc');
        this.handleLinkDisp = this.handleChange.bind(this , 'ad_link_display');
        this.handleWebsiteUrl = this.handleChange.bind(this , 'website_url');
        this.handleCallToAction = this.handelCallToActionChange.bind(this);
    }

    handleChange(key, event) {
        let { setSettings , settings } = this.props;
        settings[key] = event.target.value;
        setSettings(settings);
    }

    handelCallToActionChange(obj) {
        let { setSettings , settings } = this.props;
        settings['call_to_action_label'] = obj.label;
        settings['call_to_action'] = obj.value;
        setSettings(settings);
    }

    render() {
        let { settings={}, urlValid, linkDisplayValid } = this.props;

        return (
            <div>
                <h3>Customize Ad</h3>

                <FormGroup controlId="formAdText">
                    <ControlLabel>Text</ControlLabel>

                    <textarea type="text"
                              value={ settings.ad_message}
                              placeholder="Some text here"
                              onChange={ this.handleMessage }
                              className="form-control pub-select"/>

                    <HelpBlock>Insert the text that will appear in your ad.</HelpBlock>
                </FormGroup>

                <FieldGroup id="formHeadline"
                            type="text"
                            label="Headline"
                            value={ settings.ad_headline }
                            onChange={ this.handleHeadline }
                            placeholder="Your headline here"
                            className="pub-select"/>

                <FieldGroup id="formLinkDesc"
                            type="text"
                            label="Link description"
                            value={ settings.ad_link_desc }
                            onChange={ this.handleLinkDesc }
                            placeholder="Your link description here"
                            className="pub-select"/>

                <FormGroup controlId="formCallToAction">
                    <ControlLabel>Call To Action</ControlLabel>

                    <Select className="budget-form"
                            clearable={ false }
                            value={ settings.call_to_action || 'NO_BUTTON' }
                            options={ CALL_TO_ACTIONS }
                            onChange={ this.handleCallToAction }/>
                </FormGroup>

                <FormGroup controlId="formWebsiteURL">
                    <ControlLabel>Website URL</ControlLabel>

                    <FormControl value={ settings.website_url }
                                 placeholder="https://www.topvid.com/"
                                 onChange={ this.handleWebsiteUrl }
                                 type="text"
                                 className="pub-select"/>

                    { !urlValid &&
                        <HelpBlock className="error-text">
                            Link must me an URL starting with http:// or https://
                        </HelpBlock>
                    }
                </FormGroup>

                <FormGroup controlId="formDisplayLink">
                    <ControlLabel>Display link</ControlLabel>

                    <FormControl value={ settings.ad_link_display }
                                 placeholder="Your display link here"
                                 onChange={ this.handleLinkDisp }
                                 type="text"
                                 className="pub-select"/>

                    { !linkDisplayValid &&
                        <HelpBlock className="error-text">Display link field must be an URL.</HelpBlock>
                    }
                </FormGroup>
            </div>
        );
    }
}

export default CustomizeAd;
