import React from 'react'
import moment from 'moment'
import Select from 'react-select'
import set from 'lodash/set'
import cloneDeep from 'lodash/cloneDeep'
import withRouter from 'react-router-dom/withRouter'
import { FormGroup, Col, Row, HelpBlock, Button, Radio } from 'react-bootstrap'

import Preview from './Preview.react'
import Budget from './Budget.react'
import Objective from './Objective.react'
import Destination from './Destination.react'
import CustomizeAd from './CustomizeAd.react'
import Thumbnail from './Thumbnail.react'
import validateUrl from '../../../../../utils/validateUrl'
import PaymentPopUp from '../../../../Root/components/PaymentPopUp.react'
import DateTimeZonePicker from '../../../../../shared/DateTimeZonePicker.react'
import { DURATION_OPTIONS } from '../../../../../constants/timer'
import { DATETIME_FORMAT } from '../../../../../constants/settingsForm'


class SettingsForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showPayment: false,
            isSaving: false,
            urlValid: true ,
            budgetValid: true ,
            linkDisplayValid: true
        };
        this.handleSave = this.handleSave.bind(this);
        this.setSettings = this.setSettings.bind(this);
        this.togglePayment = this.togglePayment.bind(this);
        this.handleBudget = this.handleChange.bind(this, 'adset_budget');
        this.handleTimeZoneChange = this.handleTimeZoneChange.bind(this);
        this.handleChangeDateTime = this.handleChangeDateTime.bind(this);
        this.handleStartChange = this.handleChange.bind(this, 'start_time');
        this.handleDuration = this.handleDuration.bind(this);
        this.handleScheduleType = this.handleScheduleType.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleTimeZone = this.propertyChange.bind(this, ['time', 'time_zone']);
    }

    componentDidMount() {
        let { history: { location: { search } } } = this.props;
        if (search.indexOf('paymentSuccess') !== -1) {
            this.handleSave();
        }
    }

    componentWillReceiveProps(nextProps) {
        let { user:{ status={} }, user, setUser } = nextProps;
        if (status.error && status.error === 'countdown_plan') {
            this.setState({ showPayment: true, isSaving: false }, () => {
                user.status.error = null;
                setUser({ ...user });
            });
        }
    }

    togglePayment() {
        let { showPayment } = this.state;
        this.setState({
            showPayment: !showPayment
        });
    }

    propertyChange(path, value) {
        let { video, setVideo } = this.props;
        set(video, path, value);
        setVideo(video);
    }

    handleTimeZoneChange(option) {
        let { setUser, user } = this.props;
        user['settings']['time_zone'] = option.value;
        setUser({ ...user });
    }

    handleDateChange(newDate) {
        let { video, setVideo } = this.props,
            zone = video.time.time_zone.label.substring(4, 10),
            newStart = parseInt(moment.duration(newDate.utcOffset(zone).diff(moment().utcOffset(zone))).asDays());
        if (31 > newStart && newStart > -1) {
            let newVideo = cloneDeep(video);
            newVideo.time.start = newStart;
            newVideo.time.destDate = newDate;
            setVideo(newVideo);
        }
    }

    handleDuration(opt) {
        let { video, setVideo } = this.props;
        set(video, ['time', 'totalDuration'], opt.value);
        setVideo({...video});
    }

    handleScheduleType(event) {
        let { video, setVideo } = this.props;
        video.schedule_type = event.target.value;
        setVideo({...video});
    }

    handleChange(key , event) {
        let { setUser , user } = this.props;
        user['settings'][key] = event.target.value;
        setUser({ ...user });
    }

    handleSave() {
        let { user: { settings: { website_url , adset_budget , ad_account , ad_link_display } } , saveCampaign , history } = this.props ,
            urlValid = validateUrl(website_url),
            budgetValid = parseFloat(adset_budget) > 4,
            linkDisplayValid = ad_link_display === '' || ad_link_display.split('.').length > 1;

        if (urlValid && budgetValid && ad_account && linkDisplayValid) {
            this.setState({isSaving: true});
            saveCampaign(history);
        } else {
            this.setState({ urlValid, budgetValid, linkDisplayValid });
        }
    }

    handleChangeDateTime(val) {
        let { setUser, user } = this.props;
        user['settings']['start_datetime'] = val.format(DATETIME_FORMAT);
        setUser({ ...user });
    }

    setSettings(newSettings) {
        let { setUser, user } = this.props;
        user.settings = newSettings;
        setUser({ ...user });
    }

    render() {
        let { user: { settings, status = {} }, video, user, setUser, setVideo } = this.props,
            { isSaving, urlValid, budgetValid, linkDisplayValid, showPayment } = this.state;

        return (
            <div>
                <Row>
                    <Col lg={4}>
                        <Destination user={ user }
                                     setUser={ setUser }/>

                        { video.schedule_type !== 'limited_stock' ?
                            <div>
                                <h3>Campaign type:</h3>

                                <FormGroup>
                                    <Radio name="scheduleType"
                                           value="continuous"
                                           onChange={ this.handleScheduleType }
                                           checked={ video.schedule_type === 'continuous' }>
                                        Continuous
                                    </Radio>

                                    <Radio name="scheduleType"
                                           value="onetime"
                                           onChange={ this.handleScheduleType }
                                           checked={ video.schedule_type === 'onetime' }>
                                        Onetime
                                    </Radio>
                                </FormGroup>

                                <h3>Duration:</h3>
                                <p>72 Hours is the recommended countdown campaign duration</p>
                                <Select onChange={ this.handleDuration }
                                        options={ DURATION_OPTIONS }
                                        className="margin-bottom-10 budget-form"
                                        clearable={ false }
                                        value={ video.time.totalDuration }/>
                            </div>
                            :
                            video.schedule_type === 'days' &&
                                <div>
                                    <span>Days to go</span>
                                    <h3>Destination Date:</h3>
                                    <DateTimeZonePicker className="timerNav"
                                                        date={ video.time.destDate }
                                                        minDate={ moment().add(1, 'days') }
                                                        timeZone={ video.time.time_zone }
                                                        onChangeDate={ this.handleDateChange }
                                                        onChangeTimeZone={ this.handleTimeZone }/>
                                </div>
                        }

                        { video.schedule_type !== 'continuous' &&
                            <div>
                                <h3>Campaign Start Time</h3>
                                <FormGroup>
                                    <Radio name="campaignStart"
                                           value="immediately"
                                           onChange={ this.handleStartChange }
                                           checked={ settings.start_time === 'immediately' } inline>
                                        Start Campaign Immediately
                                    </Radio>

                                    <Radio name="campaignStart"
                                           value={ "specific" }
                                           onChange={ this.handleStartChange }
                                           checked={ settings.start_time === 'specific' } inline>
                                        Start Campaign at a specific date and time
                                    </Radio>

                                    { settings.start_time === 'specific' &&
                                        <DateTimeZonePicker showTimeSelect
                                                            showHour
                                                            className="margin-top-10"
                                                            minDate={ moment() }
                                                            date={ settings.start_datetime }
                                                            timeZone={ settings.time_zone }
                                                            onChangeDate={ this.handleChangeDateTime }
                                                            onChangeTimeZone={ this.handleTimeZoneChange }/>
                                    }
                                </FormGroup>

                                <Objective settings={ settings }
                                           setSettings={ this.setSettings }/>
                            </div>
                        }

                        <CustomizeAd settings={ settings }
                                     setSettings={ this.setSettings }
                                     urlValid={ urlValid }
                                     linkDisplayValid={ linkDisplayValid }/>

                        <hr className="seperator" width="100%"/>

                        <Budget settings={ settings }
                                onChange={ this.handleBudget }
                                budgetValid={ budgetValid }/>

                        <hr className="seperator sep-after-budget" width="100%"/>

                        { status.error && <HelpBlock className="error-text">{ status.error }</HelpBlock> }

                        { showPayment &&
                            <PaymentPopUp showPayment={ showPayment } onHide={ this.togglePayment }/>
                        }
                    </Col>

                    <Col lg={ 4 }>
                        <Preview { ...settings } video={ video }/>

                        <Thumbnail video={ video } setVideo={ setVideo }/>
                    </Col>
                </Row>

                <Button className="save-btn margin-bottom-25"
                        disabled={ isSaving }
                        onClick={ this.handleSave }>
                    Publish
                </Button>
            </div>
        )
    }
}

export default withRouter(SettingsForm);