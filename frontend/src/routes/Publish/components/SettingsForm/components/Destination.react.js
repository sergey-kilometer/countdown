import React from 'react'
import axios from 'axios'
import find from 'lodash/find'
import cloneDeep from 'lodash/cloneDeep'
import Select from 'react-select'
import getSymbolFromCurrency from 'currency-symbol-map'
import { FormGroup, ControlLabel, HelpBlock } from 'react-bootstrap'

import { GET_ACCOUNTS_URL } from '../../../../../constants/urls'


class Destination extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            pages: [],
            accounts: [],
            accountValid: true,
        };
        this.handlePage = this.handleChangeSelect.bind(this , 'page');
        this.handleAccount = this.handleChangeSelect.bind(this , 'account');
    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    async componentDidMount() {
        let { setUser, user } = this.props,
            newUser = cloneDeep(user);

        try {
            let { data={} } = await axios.get(GET_ACCOUNTS_URL);
            let { pages, accounts } = data;
            newUser.settings['currency'] = '';

            if (pages.length > 0 && !user.settings['page_id']) {
                let page = pages[0];
                newUser.settings['page_id'] = page['value'];
                newUser.settings['page_name'] = page['label'];
            }
            if (accounts.length > 0 && !newUser.settings['ad_account']) {
                let acc = accounts[0];
                newUser.settings['currency'] = getSymbolFromCurrency(acc['currency']) || '';
                newUser.settings['ad_account'] = acc['value'];
                newUser.settings['ad_account_name'] = acc['label'];
            }

            setUser({...newUser});
            await this.setStateAsync({ pages: pages, accounts: accounts });
        } catch (TypeError) {
            newUser.isConnected = false;
            setUser({...newUser});
        } finally {
            this.setState({ isLoading: false });
        }
    }

    handleChangeSelect(type , obj) {
        let { setUser, user } = this.props,
            newUser = cloneDeep(user);
        if (type === 'page') {
            newUser.settings['page_id'] = obj.value;
            newUser.settings['page_name'] = obj.label;
        } else {
            let account = find(this.state.accounts, { value: obj.value });
            this.setState({ accountValid: account && account.funding_source });
            newUser.settings['ad_account'] = obj.value;
            newUser.settings['ad_account_name'] = obj.label;
            newUser.settings['currency'] = getSymbolFromCurrency(obj.currency) || '';
        }
        setUser(newUser);
    }

    render() {
        let { accounts, pages, isLoading, accountValid } = this.state,
            { user:{ settings={} } } = this.props;

        return (
            <div>
                <h3>Destination</h3>

                <FormGroup controlId="formAdAccount">
                    <ControlLabel>Ad account</ControlLabel>

                    <Select className="pub-select"
                            clearable={ false }
                            value={ settings.ad_account }
                            options={ accounts }
                            onChange={ this.handleAccount }/>

                    { accounts.length > 0 ?
                        !accountValid &&
                            <HelpBlock className="error-text">
                                    <span>
                                        The selected ad account doesn't have a payment method associated with it.
                                        <br/>
                                        Please add a payment method on Facebook, or choose a different ad account.
                                    </span>
                            </HelpBlock>
                        :
                        !isLoading &&
                            <HelpBlock className="error-text">
                                    <span>
                                        Before we can begin you need to claim an ad account…
                                        <br/>
                                        ( <a href="https://www.facebook.com/business/help/1421678934721028"
                                             style={{ color: '#0088ff' }}
                                             rel="noopener noreferrer"
                                             target="_blank">
                                            Learn how to claim an ad account
                                        </a> )
                                    </span>
                            </HelpBlock>
                    }
                </FormGroup>

                <FormGroup controlId="formPage">
                    <ControlLabel>Page</ControlLabel>

                    <Select className="pub-select"
                            clearable={ false }
                            value={ settings.page_id }
                            options={ pages }
                            onChange={ this.handlePage }/>
                </FormGroup>
            </div>
        );
    }
}

export default Destination;
