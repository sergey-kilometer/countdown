import axios from 'axios'
import React from 'react'
import { FormGroup, FormControl, ControlLabel } from 'react-bootstrap'

import { GET_THUMBNAIL_URL, CLOUDINARY_UPLOAD_URL } from '../../../../../constants/urls'


class Thumbnail extends React.Component {

    constructor(props) {
        super(props);
        this.onImageUpload = this.onImageUpload.bind(this);
    }

    componentDidMount() {
        let { video } = this.props;
        if (!video.thumbnail_url) {
            Thumbnail.getVideoPreview(this.props.video);
        }
    }

    onImageUpload(event) {
        let reader = new FileReader();

        reader.onload = async (loadEvent) => {
            let src = loadEvent.target.result,
                formData = new FormData();

            formData.append('upload_preset', 'ya8xvjy8');
            formData.append('api_key', '392132768994591');
            formData.append('api_secret', 'DTmFTL90aoz2qQWIgVba25gz2Lw');
            formData.append('file', src);
            formData.append('tags', window.user_json ? window.user_json.username : 'sergey@kilometer.io');

            axios.post(CLOUDINARY_UPLOAD_URL, formData, {
                    headers: {'Content-Type': undefined, 'X-Requested-With': 'XMLHttpRequest'}
                })
                .then((response) => {
                    let { video, setVideo } = this.props,
                        newVideo = {...video, thumbnail_url: response.data.secure_url};
                    setVideo(newVideo);
                    localStorage.setObject('countdown-video', newVideo);
                });
        };

        reader.readAsDataURL(event.target.files[0]);
    }

    static
    getVideoPreview(video) {
        document.getElementById('thumbnailPreview').contentWindow.document.location.reload(true);
        axios.post(GET_THUMBNAIL_URL, {...video, isPreview: true})
            .then((response) => {
                document.getElementById('thumbnailPreview').contentWindow.document.write(response.data);
            });
    }

    render() {
        let { video:{ thumbnail_url } } = this.props,
            previewStyle = {};

        if (thumbnail_url) {
            previewStyle = {
                backgroundImage: `url(${thumbnail_url})`,
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'cover',
            };
        }

        return (
            <div className="thumbnailUpload">
                <h5>Thumbnail</h5>

                <div className="thumbnailPreview" style={ previewStyle }>
                    { !thumbnail_url &&
                        <iframe width="100%" height="100%" id="thumbnailPreview" title="Preview"/>
                    }
                </div>

                <FormGroup controlId="customThumbnailUploader">
                    <ControlLabel className="btn margin-top-25 uploadLabel">
                        Upload custom thumbnail
                    </ControlLabel>

                    <FormControl onChange={ this.onImageUpload }
                                 className="hidden"
                                 accept="image/*"
                                 type="file"/>
                </FormGroup>

                <p>
                    Your image should include minimal text.
                    <a href="https://www.facebook.com/business/help/980593475366490"> See how the amount of text </a>
                    in your ad image will impact the reach of your ad.
                </p>
            </div>
        );
    }
}

export default Thumbnail;