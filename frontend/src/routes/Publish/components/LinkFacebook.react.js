import React from 'react'
import get from 'lodash/get'
import moment from 'moment'
import Button from 'react-bootstrap/lib/Button'

import { DATETIME_FORMAT, DEFAULT_TIMEZONE } from '../../../constants/settingsForm'


class LinkFacebook extends React.PureComponent {

    constructor(props) {
        super(props);
        this.onLogin = this.onLogin.bind(this);
    }

    componentDidMount() {
        window.FB.init({
            appId: '1007349092734783',
            status: true,
            cookie: true,
            version: 'v2.10'
        });
    }

    onLogin() {
        window.FB.login((response) => {
            let accessToken = get(response, ['authResponse', 'accessToken'], null);
            if (accessToken) {
                let { saveUser } = this.props;
                saveUser({
                    isConnected: true,
                    access_token: accessToken,
                    objective: 'LINK_CLICKS',
                    user_id: get(response, ['authResponse', 'userID']),
                    settings: {
                        start_datetime: moment().format(DATETIME_FORMAT),
                        start_time: 'immediately',
                        // FIXME Deafault timezone not working =(
                        time_zone: (DEFAULT_TIMEZONE && DEFAULT_TIMEZONE.value) || 'Europe/Athens',
                        adset_budget: 5,
                        website_url: '', user_id: null, page_id: null, page_name: '',
                        pixel_id: null, pixel_name: '', ad_account: null, ad_account_name: '',
                        call_to_action: 'SHOP_NOW', call_to_action_label: 'Shop Now',
                        ad_link_display: '', ad_link_desc: '', ad_message: '', ad_headline: '',
                    }
                });
            }
        }, {scope: 'ads_management, manage_pages, pages_show_list, pages_messaging',});
    }

    render() {

        return (
            <div>
                <div className="description link-desc">
                    The countdown campaign will be created on your Facebook ads account.
                    <br/>
                    Please click on the button to provide access.
                </div>

                <div>
                    <Button className="btn-login" onClick={ this.onLogin }>
                        <i className="fa fa-facebook" aria-hidden="true"/>
                        Link Facebook Ad Account
                    </Button>
                </div>

                <hr className="link-fb-separator"/>
            </div>
        )
    }
}

export default LinkFacebook;
