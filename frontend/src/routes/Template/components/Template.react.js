import React from 'react'
import extend from 'lodash/extend'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router-dom'
import { Modal, Button, Tab, Tabs } from 'react-bootstrap'
import { DefaultPlayer as Video } from 'react-html5video'

import MobileModal from './MobileModal.react'
import isUsingMobile from '../../../utils/isUsingMobile'
import { TEMPLATE_VIDEOS, TYPE_TABS } from '../../../constants/template'
import { fetchDefaultJson, setVideo } from '../../../state/video/actions'
import { DAYS_TIME, HOURS_TIME } from '../../../constants/common'


class Template extends React.Component {

    constructor(props) {
        super(props);
        let { videoJson: { schedule_type } } = this.props, typeKey = 0;
        if (schedule_type) {
            let scheduleIndex = TYPE_TABS.findIndex(tab => tab.schedule_type === schedule_type);
            if (scheduleIndex > -1) {
                typeKey = scheduleIndex;
            }
        }
        this.state = {
            typeKey: typeKey,
            showMobileDialog: false,
            showModal: false,
            selectedTemplate: TEMPLATE_VIDEOS[0]
        };
        this.close = this.close.bind(this);
        this.closeMobile = this.closeMobile.bind(this);
        this.handleSelectType = this.handleSelectType.bind(this);
    }

    chooseTemplate(num) {
        let { history, fetchDefaultJson, setVideo } = this.props,
            video = localStorage.hasOwnProperty('countdown-video') && localStorage.getObject('countdown-video').frames ?
                localStorage.getObject('countdown-video') : {id: -1};

        if (isUsingMobile()) {
            this.close();
            this.setState({showMobileDialog: true});
        } else {
            let schedule_type = TYPE_TABS[this.state.typeKey].schedule_type;
            if (num !== parseInt(video.id, 10)) {
                fetchDefaultJson(num, schedule_type);
            } else if (schedule_type !== video.schedule_type) {
                video.schedule_type = schedule_type;
                if (schedule_type === 'days') {
                    video.time = extend(video.time, DAYS_TIME);
                    let labelToReplace = video.frames[0]['labels'].find(({text}) => text === 'ENDS IN');
                    labelToReplace && (labelToReplace['text'] = 'OFFER ENDING SOON');
                } else if (schedule_type === 'limited_stock') {
                    fetchDefaultJson(num, schedule_type);
                } else {
                    video.time = extend(video.time, HOURS_TIME);
                }
                setVideo(video);
            }
            history.push('/builder');
        }
    }

    close() {
        this.setState({showModal: false});
    }

    closeMobile() {
        this.setState({showMobileDialog: false});
    }

    handleSelectType(key) {
        this.setState({typeKey: key});
    }

    handlePlayTemplate(video) {
        this.setState({
            showModal: true,
            selectedTemplate: video
        });
    }

    renderVideoModal() {
        let { selectedTemplate, showModal, typeKey } = this.state,
            src = selectedTemplate.src,
            poster = selectedTemplate.poster;

        if (typeKey === 2) {
            poster = poster.replace('.png', '-days.png');
            src = src.replace('.mp4', '-days.mp4');
        } else if (typeKey === 3) {
            poster = poster.replace('.png', '-limited-stock.jpg');
            src = src.replace('.mp4', '-limited-stock.mp4');
        }

        return (
            <Modal show={ showModal } onHide={ this.close }>
                <Modal.Header closeButton>
                    <Modal.Title>{ selectedTemplate.name }</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Video autoPlay loop
                           controls={ ['PlayPause', 'Seek', 'Time', 'Volume', 'Fullscreen'] }
                           poster={ poster }>
                        <source src={ src } type="video/mp4" />
                    </Video>

                    <Button bsClass="btn btn-success"
                            onClick={ this.chooseTemplate.bind(this, selectedTemplate.id) }>
                        Choose Template
                    </Button>
                </Modal.Body>
            </Modal>
        );
    }

    render() {
        let { showMobileDialog, typeKey } = this.state;

        return (
            <section id="Template">
                <div className="template-content">
                    <div className="mainLabel">Choose Campaign Type</div>

                    <Tabs id="typeTabs" activeKey={ typeKey } onSelect={ this.handleSelectType }>
                        { TYPE_TABS.map((tab, idx) =>
                            <Tab key={ `tab${idx}` }
                                 eventKey={ idx }
                                 title={ <div className="tabTitle">{ tab.title }</div> }>
                                <span><b>{ tab.contentTitle }</b></span>
                                <span className="pull-right">
                                    <a href={ tab.learnMore } target="blank">Learn more</a>
                                </span>
                                <br/>
                                <span>{ tab.content }</span><br/>
                                <span className="exampleUse">Example use case: </span>
                                <span>{ tab.useCase }</span>
                            </Tab>
                        )}
                    </Tabs>

                    <div className="mainLabel">Choose Template</div>

                    <div className="row">
                        { TEMPLATE_VIDEOS.map((template, index) => {
                            let poster = template.poster;
                            if (typeKey === 2) {
                                poster = template.poster.replace('.png', '-days.png');
                            } else if (typeKey === 3) {
                                poster = template.poster.replace('.png', '-limited-stock.jpg');
                            }
                            if (template.id === 26 && typeKey !== 3) {
                                return null;
                            }
                            return (
                                <div key={ index } className="template-wrapper">
                                    <button className="template-button"
                                            onClick={ this.handlePlayTemplate.bind(this, template) }
                                            style={{
                                                backgroundImage: `url(${poster})`,
                                                backgroundRepeat: 'no-repeat',
                                                backgroundSize: '100% 100%'
                                            }}>
                                        <div className="hover-template-button">
                                            <div className="play-image"/>
                                        </div>
                                    </button>

                                    <div className="template-title"
                                         onClick={ this.chooseTemplate.bind(this, template.id) }>
                                        <span>{ template.name }</span>
                                    </div>
                                </div>
                            );
                        })}
                    </div>

                    { showMobileDialog && <MobileModal closeMobile={ this.closeMobile }/> }

                    { this.renderVideoModal() }
                </div>
            </section>
        );
    }
}

function mapStateToProps(state) {
    return {
        videoJson: state.video,
    };
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchDefaultJson,
        setVideo
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Template));