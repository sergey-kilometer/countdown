import React from 'react'
import Modal from 'react-bootstrap/lib/Modal'

import MobilePage from '../../../images/mobile-page.jpg'

export default (props) => {
    return (
        <Modal id="mobile-user-dialog" show={ true } onHide={ props.closeMobile }>
            <div className="img-wrapper">
                <img src={ MobilePage } alt=""/>
                <button id="mobile-dialog-confirm"
                        onClick={ props.closeMobile }
                        className="btn got-it">
                    Got it
                </button>
            </div>
        </Modal>
    );
}