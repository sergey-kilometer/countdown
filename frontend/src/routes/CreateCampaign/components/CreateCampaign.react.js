import React from 'react'
import { Route, Switch } from 'react-router-dom'

import Builder from '../../Builder'
import Publish from '../../Publish'
import Template from '../../Template'
import OnBoardingSteps from './OnBoardingSteps.react'


export default () => {
    return (
        <div>
            <OnBoardingSteps/>
            <Switch>
                <Route exact path="/" component={ Template }/>
                <Route exact path="/builder" component={ Builder }/>
                <Route exact path="/publish" component={ Publish }/>
            </Switch>
        </div>
    );
}