import React from 'react'
import { get } from 'lodash'
import Steps from 'rc-steps'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router-dom'
import { ButtonToolbar, Button } from 'react-bootstrap'

import validateUrl from '../../../utils/validateUrl'
import { saveCampaign } from '../../../state/user/actions'

class OnBoardingSteps extends React.Component {

    componentDidMount() {
        let { history: { push, location: { search } } } = this.props;
        if (search.indexOf('paymentSuccess') !== -1) {
            let nextPath = window.localStorage.getItem('countdown-state') || '/';
            if (nextPath === '/publish') {
                nextPath += search;
            }
            push(nextPath);
        }
    }

    static
    currentStep(pathname) {
        if ('/' === pathname) {
            return 0;
        } else if ('/builder' === pathname) {
            return 1;
        }
        return 2;
    }

    historyPush(path) {
        let { history: { push, location: { pathname } } } = this.props;
        if (path !== pathname) {
            if ('/builder' === path  && localStorage.hasOwnProperty('countdown-video')) {
                push(path);
            } else {
                push(path);
            }
        }
    }

    handleSave() {
        let { website_url, saveCampaign, history } = this.props;
        if (validateUrl(website_url)) {
            saveCampaign(history);
        }
    }

    render() {
        let { history: { location: { pathname } }, isConnected } = this.props;

        return (
            <div id="onBoardMenu" className={ '/builder' === pathname && 'margin-right-300' }>
                <Steps className="rcSteps"
                       labelPlacement="vertical"
                       current={ OnBoardingSteps.currentStep(pathname) }>
                    <Steps.Step title="Template" onClick={ this.historyPush.bind(this, '/') }/>
                    <Steps.Step title="Customize" onClick={ this.historyPush.bind(this, '/builder') }/>
                    <Steps.Step title="Publish" onClick={ this.historyPush.bind(this, '/publish') }/>
                </Steps>

                { '/' !== pathname &&
                    <ButtonToolbar className={ 'pull-right' + ('/builder' !== pathname && ' margin-right-300') }>
                        <Button bsStyle="default"
                                className="btn-back"
                                onClick={ this.historyPush.bind(this, ('/builder' === pathname ? '/' : '/builder')) }>
                            <span className="next-arrow"/>
                            Back
                        </Button>

                        { isConnected && '/publish' === pathname ?
                            <Button bsStyle="success"
                                    className="btn-next"
                                    onClick={ this.handleSave.bind(this) }>
                                Publish
                                <span className="next-arrow"/>
                            </Button>
                            :
                            <Button bsStyle="success"
                                    className={ 'btn-next' + ('/publish' === pathname ? ' hidden' : '') }
                                    onClick={ this.historyPush.bind(this, '/publish') }>
                                Next
                                <span className="next-arrow"/>
                            </Button>
                        }
                    </ButtonToolbar>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        website_url: get(state.user, ['settings', 'website_url'], ''),
        isConnected: state.user.isConnected,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        saveCampaign
    }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(OnBoardingSteps));