export default (pathName) => {
    let newPath = pathName.replace(/^.*[\\\/]/, '')
        .replace('.mp3','')
        .replace('.mp4','')
        .replace(/(^[a-z])|(\s+[a-z])/g, txt => {return txt.toUpperCase()});
    if (pathName.startsWith('https://s3.')) {
        return newPath.replace(/[-]/g,' ')
            .split('_')
            .slice(2)
            .join('');
    }
    return newPath.replace(/[-_]/g,' ');
}