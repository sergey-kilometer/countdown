import { VIDEO_MAKER_DOMAIN } from '../constants/urls';

export function initFontSizes(start = 40, end = 185, interval = 5) {
    let fontSizes = [];
    for (let i = start; i < end; i = i + interval) {
        fontSizes.push({
            value: i + 'px',
            label: i
        });
    }
    return fontSizes;
}

export function initSchemaColors(defaultSchema) {
    return [
        defaultSchema, ['#F33FA4', '#D9E8FB', '#051DCC', '#F6F63E', '#4149D7'],
        ['#C8F95D', '#FFBDE3', '#F752E0', '#582DFF', '#CDBA96'], ['#9D7DFF', '#FFB5DB', '#03FFD5', '#000000', '#132045'],
        ['#2D2D2D', '#FFF292', '#41E5E5', '#FFD833', '#D2B48C'], ['#45FFC5', '#FFC7EA', '#261F0B', '#FF45B4', '#CDB5CD']
    ];
}

export function initPatterns() {
    let patterns = [];
    for (let i = 0; i < 6; i++) {
        patterns.push(`${VIDEO_MAKER_DOMAIN}/static/assets/img/shopify-video-maker/patterns/${i}.png`);
    }
    return patterns;
}