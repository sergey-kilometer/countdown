const $ = window.$;
// The function returns new image coordinates
export default (img) => {
    let imageWrapper = $('.selected-div'),
        imageElem = imageWrapper.find('img'),
        x, y, zoom = Math.ceil(imageWrapper[0].clientHeight / img.height * 100) / 100;

    imageElem.css({transform: ` matrix(${zoom}, 0, 0, ${zoom}, 0, 0)`});
    x = (imageElem.position().left * -1);
    y = (imageElem.position().top * -1);
    imageElem.css({transform: ` matrix(${zoom}, 0, 0, ${zoom}, ${x}, ${y})`});

    if (imageElem[0].clientWidth < imageWrapper[0].clientWidth) {
        zoom =  Math.ceil(imageWrapper[0].clientWidth / img.width * 100) / 100 + 0.02;
        imageElem.css({transform: ` matrix(${zoom}, 0, 0, ${zoom}, 0, 0)`});
        x = (imageElem.position().left * -1);
        y = (imageElem.position().top * -1);
        imageElem.css({transform: ` matrix(${zoom}, 0, 0, ${zoom}, ${x}, ${y})`});
    }

    return {
        x: Math.floor(x),
        y: Math.floor(y),
        zoom: zoom
    }
}