export default function(element, pseudoElem) {
    let fontSize = parseFloat(pseudoElem[0].style.fontSize.replace('px', '')),
        elemHeight = element.height(), elemWidth = element.width() - 40;
    while (pseudoElem.width() > elemWidth && fontSize > 20) {
        fontSize -= 1;
        pseudoElem.css('fontSize', fontSize);
    }
    while (elemWidth > pseudoElem.width() && elemHeight > pseudoElem.height() && fontSize < 600) {
        fontSize += 1;
        pseudoElem.css('fontSize', fontSize);
    }
    return fontSize;
}