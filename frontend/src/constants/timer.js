export const DEFAULT_DIMENSIONS = {width: 1000, height: 170, location: 'matrix(1, 0, 0, 1, 40, 845.5)'};

export const MARGIN = 15;

export const OFF_COLOR = '#22221e';

export const DIGITAL_MAP = [
    [true, true, true, false, true, true, true],
    [false, false, true, false, false, false, true],
    [false, true, true, true, true, true, false],
    [false, true, true, true, false, true, true],
    [true, false, true, true, false, false, true],
    [true, true, false, true, false, true, true],
    [true, true, false, true, true, true, true],
    [false, true, true, false, false, false, true],
    [true, true, true, true, true, true, true],
    [true, true, true, true, false, true, true],
];

export const TIMER_VERSIONS = [1, 2, 3, 4, 5];

export const DURATION_OPTIONS = [
    {label: '72 Hours', value: 72},
    {label: '48 Hours', value: 48},
    {label: '24 Hours', value: 24}
];

export const SELECTED_ARRAY = ['timer', 'stock'];
