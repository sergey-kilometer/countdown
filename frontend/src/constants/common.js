import { DEFAULT_TIMEZONE } from './settingsForm'

export const IS_SHOPIFY = window.user_json && window.user_json.myshopify_domain;

export const MAX_BYTE_SIZE = 31457280;

export const DAYS_TIME = {
    version: 3,
    countBy: 'days',
    start: 10,
    time_zone: DEFAULT_TIMEZONE,
    labels: [{
        text: 'DAYS TO GO',
        maxFontSize: 90,
        style: {
            lineHeight: '158.9px' , width: '612px' , zIndex: 2 ,
            transform: 'matrix(1, 0, 0, 1, 351.714, 849)' , height: '159px' , position: 'absolute' ,
            fontFamily: 'Montserrat, sans-serif' , fontWeight: 'bold' , textAlign: 'left' , fontSize: '90px'
        }
    }]
};

export const HOURS_TIME = {
    version: 3,
    start: '71:10:35',
    time_zone: DEFAULT_TIMEZONE,
    labels: [
        {
            'text': 'Hours',
            'style': {
                'position': 'absolute',
                'lineHeight': '69.9px',
                'width': '232px',
                'height': '70px',
                'zIndex': 3,
                'transform': 'matrix(1, 0, 0, 1, 115, 1010)',
                'fontSize': '38px',
                'fontFamily': 'Abril Fatface',
                'textAlign': 'center'
            }
        },
        {
            'text': 'Minutes',
            'style': {
                'position': 'absolute',
                'lineHeight': '69.9px',
                'width': '232px',
                'height': '70px',
                'zIndex': 3,
                'transform': 'matrix(1, 0, 0, 1, 424.143, 1008)',
                'fontSize': '38px',
                'fontFamily': 'Abril Fatface',
                'textAlign': 'center'
            }
        },
        {
            'text': 'Seconds',
            'style': {
                'position': 'absolute',
                'lineHeight': '69.9px',
                'zIndex': 3,
                'width': '232px',
                'height': '70px',
                'transform': 'matrix(1, 0, 0, 1, 733.286, 1008)',
                'fontSize': '38px',
                'fontFamily': 'Abril Fatface',
                'textAlign': 'center'
            }
        },
    ]
};