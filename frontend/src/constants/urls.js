export const VIDEO_MAKER_DOMAIN = 'https://video-maker.topvid.com';
export const COUNTDOWN_DOMAIN = 'https://countdown.topvid.com';
// video-maker project urls
export const GET_PREVIEW_URL = VIDEO_MAKER_DOMAIN + '/video/preview/';

export const GET_THUMBNAIL_URL = VIDEO_MAKER_DOMAIN + '/countdown/thumbnail/';

export const GET_DEFAULT_JSON_URL = VIDEO_MAKER_DOMAIN + '/get-default-json/';
export const LOGOUT_URL = COUNTDOWN_DOMAIN + '/logout/';
export const LOGIN_URL = COUNTDOWN_DOMAIN + '/login/';
export const GET_PRODUCTS_URL = COUNTDOWN_DOMAIN + '/shopify/products/';
// countdown urls
export const GET_CARD_CHECKOUT_URL = COUNTDOWN_DOMAIN + '/card/checkout/';
export const GET_CARD_STATUS_URL = COUNTDOWN_DOMAIN + '/card/status/';

export const MUSIC_URL = COUNTDOWN_DOMAIN + '/music/';
export const VIDEO_URL = COUNTDOWN_DOMAIN + '/music/';
export const UPDATE_CAMPAIGN_URL = COUNTDOWN_DOMAIN + '/campaigns/';
export const GET_ACCOUNTS_URL = COUNTDOWN_DOMAIN + '/accounts/';
export const GET_CAMPAIGNS_URL = COUNTDOWN_DOMAIN + '/campaigns/';


export const GET_CONVERSIONS_URL = COUNTDOWN_DOMAIN + '/custom/conversions/';

export const POST_CAMPAIGN_URL = COUNTDOWN_DOMAIN + '/campaign/create/';
export const POST_COUNTDOWN_USER = COUNTDOWN_DOMAIN + '/countdown/user/save/';

// Voice OVer
export const CREATE_VOICE_OVER_URL = COUNTDOWN_DOMAIN + '/generate-voice-over';

export const CLOUDINARY_UPLOAD_URL = 'https://api.cloudinary.com/v1_1/dzorggfln/image/upload';