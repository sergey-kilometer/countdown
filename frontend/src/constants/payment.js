export const PLANS = [
    {
        eventKey: 'free',
        name: 'Free',
        includes: [
            'One countdown campaign',
            'Topvid credit on ads'
        ],
        priceText: 'FREE',
        bgColor: '#000000',
        price: 'free'
    },
    {
        eventKey: 'pro',
        name: 'Pro',
        includes: [
            'Up to 3 countdown campaigns',
            'No credit on ads'
        ],
        priceText: 'Only $49 /month',
        bgColor: '#0073FA',
        regularPrice: 'Regular price $99',
        price: 49,
        hostedButtonId: '7QWDTVBNK9RTN'
    },
    {
        name: 'Agency',
        eventKey: 'agency',
        includes: [
            'Unlimited countdown campaigns',
            'No credit on ads',
            'Multiple ad accounts supported',
            '"Limited quantity ads" included (coming soon)'
        ],
        priceText: 'Only $99 /month',
        bgColor: '#FF647A',
        regularPrice: 'Regular price $199',
        price: 99,
        hostedButtonId: 'HAV3KUKWP55DY'
    },
];