import { VIDEO_MAKER_DOMAIN } from './urls'

const MUSIC_URL = VIDEO_MAKER_DOMAIN + '/static/assets/music/';

export const MUTE_MUSIC = MUSIC_URL + '1-minute-of-silence.mp3';

export const MUSICS = [
    MUSIC_URL + 'claps_guitar.mp3', MUSIC_URL + 'fooling-the-night.mp3',
    MUSIC_URL + 'cuban_party.mp3', MUSIC_URL + 'dance_elevation.mp3',
    MUSIC_URL + 'industry_news.mp3', MUSIC_URL + 'oh_yeah.mp3',
    MUSIC_URL + 'old_party.mp3', MUSIC_URL + 'ukulele.mp3',
    MUSIC_URL + 'yolo.mp3', MUSIC_URL + 'happy_days.mp3',
    MUSIC_URL + 'live_your_life.mp3', MUSIC_URL + 'astral_rhythm.mp3',
    MUSIC_URL + 'lay-it-down.mp3', MUSIC_URL + 'stomp-music.mp3',
    MUSIC_URL + 'Doug-Hoyer-Walks-with-the-Tender.mp3',
    MUSIC_URL + 'magic-of-christmas.mp3', MUSIC_URL + 'hooray-for-christmas.mp3',
    MUSIC_URL + 'christmas-is-coming.mp3', MUSIC_URL + 'miami-christmas.mp3',
    MUSIC_URL + 'christmas-shopping.mp3'
];