export const SWAL_TITLE = 'Choose Campaign Audience';
export const SWAL_TEXT = 'Before the campaign will go live, please be sure to \n choose the audience you would like to ' +
    'target using the \n Facebook audience options.\n The button below will send you to the adset on \n' +
    'Facebook which need to be configured with the desired \n audience.';

export const FACEBOOK_CAMPAIGN_URL = 'https://business.facebook.com/ads/manager/account/adsets/';