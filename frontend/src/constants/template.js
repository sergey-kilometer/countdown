import { COUNTDOWN_DOMAIN } from './urls'

export const TEMPLATE_VIDEOS = [
    {
        id: 26,
        status: 'ready',
        name: 'Bar',
        src: COUNTDOWN_DOMAIN + '/static/assets/video/Bar.mp4',
        poster: COUNTDOWN_DOMAIN + '/static/assets/img/Bar.png'
    },
    {
        id: 18,
        status: 'ready',
        name: 'Stock Video Footage',
        src: COUNTDOWN_DOMAIN + '/static/assets/video/Stock-Video-Footage.mp4',
        poster: COUNTDOWN_DOMAIN + '/static/assets/img/Stock-Video-Footage.png'
    },
    {
        id: 16,
        status: 'ready',
        name: 'Multi-color',
        src: COUNTDOWN_DOMAIN + '/static/assets/video/Multi-color.mp4',
        poster: COUNTDOWN_DOMAIN + '/static/assets/img/Multi-color.png'
    },
    {
        id: 21,
        status: 'ready',
        name: 'Circle',
        src: COUNTDOWN_DOMAIN + '/static/assets/video/Circle.mp4',
        poster: COUNTDOWN_DOMAIN + '/static/assets/img/Circle.png',
    },
];

export const TYPE_TABS = [
    {
        schedule_type: 'continuous',
        learnMore: 'http://help.topvid.com/countdown-ads/continuous-countdown-campaigns-how-they-work',
        title: 'Continuous countdown',
        useCase: 'Retargeting - visitors of your website who didn’t complete a purchase will be retargeted with a ' +
            'special offer within 72 hours of their visit.',
        contentTitle: 'Always on 24-72 hour countdown campaign.',
        content: 'Continuous countdown campaign will restart every 72 hours, however users who saw the old campaign ' +
            'will be excluded from the new one.',
    },
    {
        schedule_type: 'onetime',
        learnMore: 'http://help.topvid.com/countdown-ads/one-time-countdown-campaigns-how-they-work',
        title: 'One time countdown',
        useCase: 'Flash sale - countdown ads are perfect way to showcase a limited time offer around a specific date ' +
            '(weekend sale, Black Friday/ Cyber Monday etc)',
        contentTitle: '24-72 hour countdown campaign.',
        content: 'One time countdown campaign that will stop once the time is over.',
    },
    {
        schedule_type: 'days',
        learnMore: 'http://help.topvid.com/countdown-ads/days-to-go-countdown-campaigns-how-they-work',
        title: 'Days to go countdown',
        useCase: 'Event - the countdown ad will display the number of days left until an event or special occasion will start.',
        contentTitle: '3-31 days countdown campaign.',
        content: 'The campaign will countdown the number of days left until a specific date.',
    },
    {
        schedule_type: 'limited_stock',
        learnMore: 'http://help.topvid.com/countdown-ads/limited-stock-countdown-campaigns-how-they-work',
        title: 'Limited Stock countdown',
        useCase: 'Offering limited amount of products at a discounted price.',
        contentTitle: 'Limited Stock countdown',
        content: 'The ad will display the number of offers/products left in a specific sale.',
    },
];