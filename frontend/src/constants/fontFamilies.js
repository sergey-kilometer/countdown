export default [
    {value: 'Lato, sans-serif', label: 'Lato', style: {fontFamily: 'Lato, sans-serif'}},
    {value: 'Oswald, sans-serif', label: 'Oswald', style: {fontFamily: 'Oswald, sans-serif'}},
    {value: 'Lobster, cursive', label: 'Lobster', style: {fontFamily: 'Lobster, cursive'}},
    {value: 'Merriweather, serif', label: 'Merriweather', style: {fontFamily: 'Merriweather, serif'}},
    {value: 'Abril Fatface, cursive', label: 'Abril Fatface', style: {fontFamily: 'Abril Fatface, cursive'}},
    {value: 'Alfa Slab One, cursive', label: 'Alfa Slab One', style: {fontFamily: 'Alfa Slab One, cursive'}},
    {value: 'Arvo, serif', label: 'Arvo, serif', style: {fontFamily: 'Arvo, serif'}},
    {value: 'Fira Sans, sans-serif', label: 'Fira Sans', style: {fontFamily: 'Fira Sans, sans-serif'}},
    {value: 'Playfair Display, serif', label: 'Playfair Display', style: {fontFamily: 'Playfair Display, serif'}},
    {value: 'Raleway, sans-serif', label: 'Raleway', style: {fontFamily: 'Raleway, sans-serif'}},
    {value: 'Roboto, sans-serif', label: 'Roboto', style: {fontFamily: 'Roboto, sans-serif'}},
    {value: 'Montserrat, sans-serif', label: 'Montserrat', style: {fontFamily: 'Montserrat, sans-serif'}},
];