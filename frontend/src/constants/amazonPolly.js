export const POLLY_MAPPING = [
    {code:'da-DK', name: 'Danish', voices: ['Mads', 'Naja']},
    {code:'nl-NL', name: 'Dutch', voices: ['Lotte', 'Ruben']},
    {code:'en-AU', name: 'English (Australian)', voices: ['Nicole', 'Russel']},
    {code:'en-GB', name: 'English (British)', voices: ['Amy', 'Brian', 'Emma']},
    {code:'en-US', name: 'English (US)', voices: ['Ivy', 'Joanna', 'Joey', 'Justin', 'Kendra', 'Kimberly', 'Salli']},
    {code:'fr-FR', name: 'French', voices: ['Celine', 'Mathieu']},
    {code:'fr-CA', name: 'French (Canadian)', voices: ['Chantal']},
    {code:'de-DE', name: 'German', voices: ['Hans', 'Marlene', 'Vicky']},
    {code: 'is-IS',name: 'Icelandic', voices: ['Dora', 'Karl']},
    {code:'it-IT', name: 'Italian', voices: ['Carla', 'Giorgio']},
    {code:'ja-JP', name: 'Japanese', voices: ['Mizuki']},
    {code:'nb-NO', name: 'Norwegian', voices: ['Liv']},
    {code:'pl-PL', name: 'Polish', voices: ['Jacek', 'Jan', 'Ewa', 'Maja']},
    {code:'pt-BR', name: 'Portuguese (Brazilian)', voices: ['Ricardo', 'Vitoria']},
    {code:'pt-PT', name: 'Portuguese (European)', voices: ['Cristiano', 'Ines']},
    {code:'ro-RO', name: 'Romanian', voices: ['Carmen']},
    {code:'ru-RU', name: 'Russian', voices: ['Maxim', 'Tatyana']},
    {code:'es-ES', name: 'Spanish (Castilian)', voices: ['Conchita', 'Enrique']},
    {code:'es-US', name: 'Spanish (Latin American)', voices: ['Miguel', 'Penelope']},
    {code:'sv-SE', name: 'Swedish', voices: ['Astrid']},
    {code:'tr-TR', name: 'Turkish', voices: ['Fliz']},
    {code:'cy-GB', name: 'Welsh', voices: ['Gwyneth']}
];

export const DEFAULT_HOURS_TTS = {
    text: 'Enjoy our special weekend sale.\n20% off on all items.\nOnly [x] hours left.',
    src: "https://s3.amazonaws.com/topvid-user-music/d6693d2b-a024-4e98-977b-90c3bcec28a5.mp3",
    length: '7.42',
    voice: 'Brian',
    languageCode: 'en-GB'
};

export const DEFAULT_DAYS_TTS = {
    text: 'Enjoy our special sale.\n20% off on all items.\nOnly [x] days left.',
    src: 'https://s3.amazonaws.com/topvid-user-music/6f840d67-5a33-4c82-9978-c509ec791fc3.mp3',
    length: '6.84',
    voice: 'Brian',
    languageCode: 'en-GB'
};

export const DEFAULT_LIMITED_STOCK = {
    voice: 'Brian',
    languageCode: 'en-GB',
    text: 'Enjoy our special weekend sale.\nOnly on the first [y] orders.\nHurry up! Only [x] orders left.',
    src: 'https://s3.amazonaws.com/topvid-user-music/f5bbd76f-eaae-49ee-975d-02ccb342d674.mp3',
    length: '8.80'
};