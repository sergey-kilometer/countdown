#!/usr/bin/env bash

source ../venv/bin/activate

sudo rm -R ../static/js/
sudo rm -R ../static/css/

python3 ../manage.py collectstatic --noinput