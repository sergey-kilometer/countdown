from django.http.response import HttpResponse
from countdown_app.models import (CountdownUser,CountdownCampaigns, CountdownAds)
from django.views.generic import ListView
from datetime import datetime




def health_check(_):
    res = HttpResponse('Hello countdown app')
    res.status_code = 200
    res['Content-Length'] = 10
    return res



class CampaignListView(ListView):
    ordering = '-create_date'
    model = CountdownUser
    context_object_name = 'users'
    template_name = 'user_view.html'


    def get_context_data(self, **kwargs):
        context = super(CampaignListView, self).get_context_data(**kwargs)

        for user in context['users']:
            setattr(user,'active_ads',0)
            campaigns = CountdownCampaigns.objects.filter(username=user.username)
            for campaign in campaigns:
                ads = CountdownAds.objects.filter(campaign_id=campaign.campaign_id).order_by('-hour')
                active_ads = len([ad for ad in ads if ad.status == 'ACTIVE'])

                setattr(campaign, 'ads', ads)
                setattr(campaign,'active_ads',active_ads)
                user.active_ads = user.active_ads + active_ads

            setattr(user,'campaigns', campaigns)
            setattr(user,'trial_left', 14 - (datetime.now() - user.trial_start_date).days if user.trial_start_date else 'NO TRIAL')

        return context



    def get_ordering(self):
        self.ordering = self.request.GET.get('ordering',self.ordering )
        # validate ordering here
        return self.ordering


# @login_required
# def admin_panel(request):
#     if '@kilometer.io' in request.user.username:
#         campaigns = CountdownCampaigns.objects.all()
#         return render(request,'admin_panel.html',{'campaigns':campaigns})
#
# @login_required
# def admin_panel_campaign(request,campaign_id):
#     if '@kilometer.io' in request.user.username:
#         ads =  CountdownAds.objects.filter(campaign_id=campaign_id)
#
#         return render(request, 'campaign_view.html',{'ads':ads})
