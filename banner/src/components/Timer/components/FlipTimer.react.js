import React from 'react'
import { Textfit } from 'react-textfit'

import { DEFAULT_DIMENSIONS, MARGIN, ITERATE_ARRAY } from '../../../constants/timer'

export default (props) => {
    let { start, style, dimensions=DEFAULT_DIMENSIONS } = props,
        BLOCK_SIZE = dimensions.width / 7,
        BLOCK_HEIGHT = dimensions.height,
        splitTime,
        iterateArray = ITERATE_ARRAY,
        clockStyle = style;

    if (isNaN(start)) {
        splitTime = start.split(':');
    } else {
        iterateArray = [0, 1];
        splitTime = (start < 10) ? '0' + start.toString() : start.toString();
    }

    return (
        <div style={{
                cursor: 'default',
                direction: 'ltr',
                zIndex: 2,
                position: 'absolute',
                transform: dimensions.location || style['transform'],
                width: `${dimensions.width}px`,
                height: `${dimensions.height}px`,
            }}>
            {/* Render digits*/}
            { iterateArray.map(i => {
                let currentTime = iterateArray.length > 2 ? splitTime[Math.floor(i / 2)][i % 2] : splitTime[i],
                    height = (BLOCK_HEIGHT / 2) - 1,
                    x = (i * BLOCK_SIZE) +
                        ((i === 2 || i === 3) ? BLOCK_SIZE / 2 : 0) +
                        ((i === 4 || i === 5) ? BLOCK_SIZE : 0);
                return (
                    <div key={i}>
                        <Textfit style={{
                                fontFamily: clockStyle['fontFamily'],
                                color: clockStyle['color'],
                                backgroundColor: clockStyle['backgroundColor'] || 'white',
                                borderRadius: '15% 15% 0 0',
                                zIndex: 3,
                                overflow: 'hidden',
                                transformOrigin: '50% 100%',
                                textAlign: 'center',
                                position: 'absolute',
                                lineHeight: `${BLOCK_HEIGHT}px`,
                                width: `${BLOCK_SIZE - MARGIN}px`,
                                height: `${height}px`,
                                transform: `matrix(1, 0, 0, 1, ${x}, 0)`
                            }} mode="single" max={ 55 }>
                            { currentTime }
                        </Textfit>

                        <Textfit style={{
                                fontFamily: clockStyle['fontFamily'],
                                color: clockStyle['color'],
                                overflow: 'hidden',
                                backgroundColor: clockStyle['backgroundColor'] || 'white',
                                borderRadius: '0 0 15% 15%',
                                zIndex: 1,
                                textAlign: 'center',
                                position: 'absolute',
                                width: `${BLOCK_SIZE - MARGIN}px`,
                                lineHeight: 0,
                                height: `${height}px`,
                                transform: `matrix(1, 0, 0, 1, ${x}, ${BLOCK_HEIGHT / 2})`
                            }} mode="single" max={ 55 }>
                            { currentTime }
                        </Textfit>
                    </div>
                );
            })}
            {/* Render separators*/}
            { isNaN(start) &&
                <div>
                    <Textfit style={{
                                fontFamily: clockStyle['fontFamily'],
                                color: clockStyle['color'],
                                position: 'absolute',
                                width: `${BLOCK_SIZE / 2 - MARGIN}px`,
                                lineHeight: `${BLOCK_HEIGHT}px`,
                                textAlign: 'center',
                                height: `${BLOCK_HEIGHT}px`,
                                transform: `matrix(1, 0, 0, 1, ${BLOCK_SIZE * 2}, 0)`
                             }}
                             mode="single">
                        :
                    </Textfit>

                    <Textfit style={{
                                fontFamily: clockStyle['fontFamily'],
                                color: clockStyle['color'],
                                position: 'absolute',
                                width: `${BLOCK_SIZE / 2 - MARGIN}px`,
                                lineHeight: `${BLOCK_HEIGHT}px`,
                                textAlign: 'center',
                                height: `${BLOCK_HEIGHT}px`,
                                transform: `matrix(1, 0, 0, 1, ${BLOCK_SIZE * 4.5}, 0)`
                            }} mode="single">
                        :
                    </Textfit>
                </div>
            }
        </div>
    );
}