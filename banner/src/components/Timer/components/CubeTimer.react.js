import React from 'react'

import { DEFAULT_DIMENSIONS, MARGIN, ITERATE_ARRAY } from '../../../constants/timer'

export default (props) => {
    let { start, style, dimensions=DEFAULT_DIMENSIONS } = props,
        BLOCK_SIZE = dimensions.width / 7,
        BLOCK_HEIGHT = dimensions.height,
        splitTime = start.split(':'),
        clockStyle = style;

    return (
        <div id="clockTime"
             className="clockTime"
             style={{
                cursor: 'default',
                zIndex: 2,
                position: 'absolute',
                transform: style['transform'] || dimensions.location,
                width: `${dimensions.width}px`,
                height: `${dimensions.height}px`,
            }}>
            {/* Render digits*/}
            { ITERATE_ARRAY.map(i =>
                <div key={i}
                     style={{
                        fontFamily: clockStyle['fontFamily'],
                        color: clockStyle['color'],
                        fontSize: clockStyle['fontSize'] || '200px',
                        backgroundColor: clockStyle['backgroundColor'] || 'white',
                        transformStyle: 'preserve-3d',
                        transformOrigin: '50% 50%',
                        transformPerspective: 1000,
                        textAlign: 'center',
                        position: 'absolute',
                        width: `${BLOCK_SIZE - MARGIN}px`,
                        lineHeight: `${BLOCK_HEIGHT}px`,
                        height: `${BLOCK_HEIGHT}px`,
                        transform: `matrix(1, 0, 0, 1, ${(i * BLOCK_SIZE) +
                                    ((i === 2 || i === 3) ? BLOCK_SIZE / 2 : 0) +
                                    ((i === 4 || i === 5) ? BLOCK_SIZE : 0) }, 0)`
                    }}>
                     { splitTime[Math.floor(i / 2)][i % 2] }
                </div>
            )}
            {/* Render separators*/}
            <div style={{
                    fontFamily: clockStyle['fontFamily'],
                    color: clockStyle['color'],
                    fontSize: clockStyle['fontSize'] || '200px',
                    position: 'absolute',
                    width: `${BLOCK_SIZE / 2 - MARGIN}px`,
                    lineHeight: `${BLOCK_HEIGHT}px`,
                    textAlign: 'center',
                    height: `${BLOCK_HEIGHT}px`,
                    transform: `matrix(1, 0, 0, 1, ${BLOCK_SIZE * 2}, 0)`
                }}>
                :
            </div>

            <div style={{
                    fontFamily: clockStyle['fontFamily'],
                    color: clockStyle['color'],
                    fontSize: clockStyle['fontSize'] || '200px',
                    position: 'absolute',
                    width: `${BLOCK_SIZE / 2 - MARGIN}px`,
                    lineHeight: `${BLOCK_HEIGHT}px`,
                    textAlign: 'center',
                    height: `${BLOCK_HEIGHT}px`,
                    transform: `matrix(1, 0, 0, 1, ${BLOCK_SIZE * 4.5}, 0)`
                }}>
                :
            </div>
        </div>
    );
}