import React from 'react'
import { withCookies } from 'react-cookie'

import Frames from './Frames'
import { GET_VIDEO_JSON } from '../constants/urls'
import { isMobile, GET, getParameter, calcDays } from '../utils/helpers'
import { TIME_ITEM, HASH_ITEM, IS_MINIMIZED_ITEM, HASH_REMOVED } from '../constants/localStorageItems'


class Root extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            videoJson: {id: -1},
            isMinimized: (props.cookies.get(IS_MINIMIZED_ITEM) === 'true')
        }
    }

    toggleMinimize() {
        let { isMinimized } = this.state,
            { cookies } = this.props;
        this.setState({isMinimized: !isMinimized});
        cookies.set(IS_MINIMIZED_ITEM, !isMinimized, { domain: `.${window.location.hostname}` });
    }

    deactivateCountdown() {
        this.setState({videoJson: {id: -1}});
    }

    componentWillMount() {
        let { cookies } = this.props,
            urlHash = getParameter('cd'),
            hash =  cookies.get(HASH_ITEM);

        if (urlHash && urlHash !== hash) {
            cookies.set(HASH_ITEM, urlHash, {domain: `.${window.location.hostname}`});
            cookies.remove(TIME_ITEM);
            hash = urlHash
        }

        if (hash && hash !== cookies.get(HASH_REMOVED)) {
            GET(GET_VIDEO_JSON + `${hash}/`).then(data => {
                let jsonTime = data.time.start,
                    localTime = cookies.get(TIME_ITEM);

                if (localTime) {
                    if (data.time.countBy !== 'days' && !data.time.total) {
                        let localTimeArray = localTime.split(':'),
                            timeArray = jsonTime.split(':');

                        localTimeArray.forEach(num => parseInt(num));
                        timeArray.forEach(num => parseInt(num));

                        if (localTimeArray[0] < timeArray[0]
                            || (timeArray[0] === localTimeArray[0] && localTimeArray[1] < timeArray[1])
                            || (timeArray[1] === localTimeArray[1] && localTimeArray[2] < timeArray[2])) {
                            data.time.start = localTime;
                        }
                    } else {
                        let newStart = calcDays(data.time);
                        if (newStart < data.time.start) {
                            data.time.start = newStart > 0 ? newStart : 0;
                        }
                    }
                }

                if (data.time.total) {
                    let total = data.time.total,
                        sold = data.time.sold;
                    data.labels.forEach(label => {
                        label.text = label.text.replace('[Y]', total).replace('[X]', sold);
                    });
                }

                if (data.time.start !== '00:00:00' && data.time.start !== 0) {
                    cookies.set(TIME_ITEM, data.time.start, { domain: `.${window.location.hostname}` });
                    this.setState({videoJson: data});
                }
            });
        }
    }

    render() {
        let { videoJson, isMinimized } = this.state;

        return (
            videoJson.id !== -1 &&
                <Frames videoJson={ videoJson }
                        isMobile={ isMobile() }
                        isMinimized={ isMinimized }
                        onMinimize={ this.toggleMinimize.bind(this) }
                        hideCountdown={ this.deactivateCountdown.bind(this) }/>
        )
    }
}

export default withCookies(Root);