import React from 'react'
import { Textfit } from 'react-textfit'

import Timer from '../../Timer'
import TimeLabels from './TimeLabels.react'
import { getWindowSize } from '../../../utils/helpers'
import { editTimeLabelStyle } from '../../../utils/topBottom'
import { bottomStyle, daysLabelStyle, imageClose, timerWrapper } from './../../../constants/styles/vertical'

const STYLE_1010 = {
    label: {
        width: '225px',
        display: 'inline-block',
        lineHeight: '70px',
        verticalAlign: 'top',
        marginRight: '1%'
    },
    timeDim: { height: 50, width: 255 },
    editTime: editTimeLabelStyle.bind(null, {top: 58, margin: 90, height: 11, width: 66})
};

const STYLE_800 = {
    label: {
        width: '147px',
        display: 'inline-block',
        lineHeight: '70px',
        verticalAlign: 'top',
        marginRight: '10px'
    }
};

class VerticalBanner extends React.Component {

    constructor(props) {
        super(props);
        this.state = { windowSize: getWindowSize() };
        this.onResize = this.onResize.bind(this);
    }

    componentDidMount() {
        if (window.attachEvent) {
            window.attachEvent('onresize', this.onResize);
        } else if(window.addEventListener) {
            window.addEventListener('resize', this.onResize, true);
        }
    }

    componentWillUnmount() {
        if (window.detachEvent) {
            window.detachEvent('onresize', this.onResize);
        } else if(window.removeEventListener) {
            window.removeEventListener('resize', this.onResize);
        }
    }

    onResize() {
        this.setState({ windowSize: getWindowSize() });
    }

    render() {
        let { videoJson:{ time, schedule_type, labels, colors }, hideCountdown, bannerPos='top', isMinimized, onMinimize } = this.props,
            { windowSize } = this.state,
            timeWrapp = timerWrapper,
            imgClose = imageClose,
            daysStyle = ['days', 'limited_stock'].indexOf(schedule_type) > -1 ?
                {...labels[3].style, ...daysLabelStyle} : {},
            editTime = STYLE_1010.editTime,
            tmpStyle = {...bottomStyle, backgroundColor: colors[colors.length - 1]};

        if (bannerPos === 'top') {
            tmpStyle = {...tmpStyle, top: 0}
        }

        time.dimensions = STYLE_1010.timeDim;
        // window width gereter then 800 styling
        if (windowSize > 800) {
            if (windowSize > 1010) {
                labels.forEach((label) => {label.style = {...label.style, ...STYLE_1010.label}});
            } else {
                labels.forEach((label) => {label.style = {...label.style, ...STYLE_800.label}});
            }
            labels[1].style = {...labels[1].style, lineHeight: '55px', marginTop: '5px'};
            labels[2].style = {...labels[2].style, lineHeight: '55px', marginTop: '5px'};
        } else if (isMinimized) {
            // window width less then 800 and not minimized !!!!
            timeWrapp = {...timerWrapper, ...{marginTop: '22px', height: '70px', width: '300px'}};
            tmpStyle = {...tmpStyle, height: '200px'};
            labels.forEach((label) => {label.style = {...label.style,
                width: '95%',
                display: 'inline-block',
                lineHeight: '45px',
                verticalAlign: 'top'
            }});
            imgClose = {...imgClose,
                marginTop: '0', position: 'absolute', top: '5px', right: '5px', width: '10px', height: '10px'
            };
            editTime = editTimeLabelStyle.bind(null, {top: 120, margin: 104, height: 15, width: 82});
            time.dimensions = { height: 45, width: 300 };
            // labels[0].style = {...labels[0].style, width: '45%', maxWidth: '175px'};
            labels[1].style = {...labels[1].style, width: '45%', maxWidth: '175px'};
        } else if (windowSize <= 800 ) {
            // window width less then 800 styling
            daysStyle = {...daysStyle, width: '124px', marginLeft: '40px'};
            timeWrapp = {...timerWrapper, ...{width: '180px', marginTop: '12px', height: '40px', marginLeft: '2%'}};
            labels.forEach((label) => {label.style = {...label.style, display: 'none'}});
            labels[1].style = {...labels[1].style,
                display: 'inline-block',
                lineHeight: '60px',
                width: '45%',
                maxWidth: '150px',
                verticalAlign: 'top',
                marginTop: '5px',
            };
            imgClose = {...imgClose, width: '10px', height: '10px'};
            editTime = editTimeLabelStyle.bind(null, {top: 55, margin: 68, height: 12, width: 45});
            time.dimensions = { height: 45, width: 180 };
        }

        labels[1]['style']['backgroundColor'] = colors[0];
        labels[2]['style']['backgroundColor'] = colors[1];
        if (labels.length > 3) {
            labels[3]['style']['backgroundColor'] = colors[2];
        }

        return (
            <div style={ tmpStyle } onClick={ windowSize < 800 && onMinimize }>
                {/*<Textfit style={ labels[0].style }*/}
                         {/*mode="single"*/}
                         {/*max={ 45 }>*/}
                    {/*{ labels[0].text }*/}
                {/*</Textfit>*/}

                { !/^\s*$/.test(labels[1].text) &&
                    <Textfit style={ labels[1].style }
                             mode="single"
                             max={ 45 }>
                        { labels[1].text }
                    </Textfit>
                }

                { isMinimized ?
                    <div style={{width: '100%', textAlign: 'center'}}>
                        <div style={ timeWrapp }>
                            <Timer time={ time } hideCountdown={ hideCountdown }/>

                            { ['days', 'limited_stock'].indexOf(schedule_type) > -1 ?
                                !/^\s*$/.test(labels[3].text) &&
                                    <Textfit style={ daysStyle }
                                             mode="single"
                                             max={ 45 }>
                                        { labels[3].text }
                                    </Textfit>
                                :
                                <TimeLabels labels={ time.labels.map(editTime) }
                                            styles={ [{}, {}, {}] }/>
                            }
                        </div>
                    </div>
                    :
                    <div style={ timeWrapp }>
                        <Timer time={ time } hideCountdown={ hideCountdown }/>

                        { ['days', 'limited_stock'].indexOf(schedule_type) > -1 ?
                            !/^\s*$/.test(labels[3].text) &&
                                <Textfit style={ daysStyle }
                                         mode="single"
                                         max={ 45 }>
                                    { labels[3].text }
                                </Textfit>
                            :
                            <TimeLabels labels={ time.labels.map(editTime) }
                                        styles={ [{}, {}, {}] }/>
                        }
                    </div>
                }

                { !/^\s*$/.test(labels[2].text) &&
                    <Textfit style={ labels[2].style }
                             mode="single"
                             max={ 45 }>
                        { labels[2].text }
                    </Textfit>
                }

                <div style={ imgClose } onClick={ hideCountdown }/>
            </div>
        )
    }
}

export default VerticalBanner;