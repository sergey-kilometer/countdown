import React from 'react'
import { Textfit } from 'react-textfit'

import Timer from '../../Timer'
import PoweredBy from './PoweredBy.react'
import MinifyBtn from './MinifyBtn.react'
import TimeLabels from './TimeLabels.react'
import { templateStyle, textEditing, minimizedLabel, miniTmplStyle,
         miniTimeLabel0, miniTimeLabel1, miniTimeLabel2 } from './../../../constants/styles/common'

export default (props) => {
    let { style, text, hideCountdown, onMinimize, time, wrapperStyle, timeLabel } = props,
        tmpTime = { ...time };

    tmpTime.dimensions = {width: 225, height: 36, location: 'matrix(1, 0, 0, 1, 14, 39)'};

    return (
        <div onClick={ onMinimize }
             style={{ ...templateStyle, ... miniTmplStyle, ...wrapperStyle, direction: 'ltr'}}>
            <MinifyBtn closeButton hideCountdown={ hideCountdown }/>

            { !/^\s*$/.test(text) &&
                <Textfit style={ Object.assign({}, textEditing, style, minimizedLabel) }
                         mode="single"
                         max={ 34 }>
                    { text }
                </Textfit>
            }

            <Timer time={ tmpTime } hideCountdown={ hideCountdown }/>

            { time.countBy === 'days' ?
                !/^\s*$/.test(timeLabel.text) &&
                    <Textfit style={
                                Object.assign(
                                    {},
                                    textEditing,
                                    timeLabel.style, { top: '35px', left: '80px', width: '165px', lineHeight: '50px'}
                                )
                             }
                             mode="single"
                             max={ 40 }>
                        { timeLabel.text }
                    </Textfit>
                :
                <TimeLabels labels={ tmpTime.labels } styles={ [miniTimeLabel0, miniTimeLabel1, miniTimeLabel2] }/>
            }

            <PoweredBy/>
        </div>
    );
}