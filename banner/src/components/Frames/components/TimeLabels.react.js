import React from 'react'
import { Textfit } from 'react-textfit'

import { textEditing } from '../../../constants/styles/common'


export default (props) => {
    let { labels, styles, max=16 } = props;

    return (
        labels.map((label, idx) =>
            <Textfit key={ idx }
                     style={ Object.assign({}, textEditing, label.style, styles[idx]) }
                     mode="single"
                     max={ max }>
                { label.text }
            </Textfit>
        )
    );
}