import React from 'react'
import { Textfit } from 'react-textfit'

import { getWindowSize } from '../../../utils/helpers'
import LimitedStock from './elements/LimitedStock.react'
import { bottomStyle, imageClose } from './../../../constants/styles/vertical'

const STYLE_1010 = {
    label: {
        width: '225px',
        display: 'inline-block',
        lineHeight: '70px',
        verticalAlign: 'top',
        marginRight: '1%'
    }
};

const STYLE_800 = {
    label: {
        width: '147px',
        display: 'inline-block',
        lineHeight: '70px',
        verticalAlign: 'top',
        marginRight: '10px'
    }
};

class VerticalBanner extends React.Component {

    constructor(props) {
        super(props);
        this.state = { windowSize: getWindowSize() };
        this.onResize = this.onResize.bind(this);
    }

    componentDidMount() {
        if (window.attachEvent) {
            window.attachEvent('onresize', this.onResize);
        } else if(window.addEventListener) {
            window.addEventListener('resize', this.onResize, true);
        }
    }

    componentWillUnmount() {
        if (window.detachEvent) {
            window.detachEvent('onresize', this.onResize);
        } else if(window.removeEventListener) {
            window.removeEventListener('resize', this.onResize);
        }
    }

    onResize() {
        this.setState({ windowSize: getWindowSize() });
    }

    render() {
        let { videoJson:{ time, labels, colors }, hideCountdown, bannerPos='top', isMinimized, onMinimize } = this.props,
            { windowSize } = this.state,
            imgClose = imageClose,
            stockLabel = {},
            limitedStock = {display: 'inline-block', position: 'relative', top: 0, marginRight: '5px'},
            tmpStyle = {...bottomStyle,
                backgroundColor: colors[colors.length - 1],
                boxShadow: 'rgba(0, 0, 0, 0.75) 0px 0px 10px 1.5px',
                MozBoxShadow: 'rgba(0, 0, 0, 0.75) 0px 0px 10px 1.5px',
                WebkitBoxShadow: 'rgba(0, 0, 0, 0.75) 0px 0px 10px 1.5px',
            };

        if (bannerPos === 'top') {
            tmpStyle = {...tmpStyle, top: 0}
        }

        // window width gereter then 800 styling
        if (windowSize > 800) {
            if (windowSize > 1010) {
                labels.forEach((label) => {label.style = {...label.style, ...STYLE_1010.label}});
            } else {
                labels.forEach((label) => {label.style = {...label.style, ...STYLE_800.label}});
            }
            labels[1].style = {...labels[1].style, lineHeight: '60px', marginTop: '5px'};
            labels[2].style = {...labels[2].style, lineHeight: '60px', marginTop: '5px'};
            labels[3].style = {...labels[3].style, lineHeight: '60px', marginTop: '1px'};
            labels[4].style = {...labels[1].style, display: 'block', lineHeight: 'none', marginTop: '-2px', backgroundColor: 'transparent'};
        } else if (isMinimized) {
            // window width less then 800 and not minimized !!!!
            tmpStyle = {...tmpStyle, height: '200px'};
            labels.forEach((label) => {label.style = {...label.style,
                width: '95%',
                display: 'inline-block',
                lineHeight: '50px',
                verticalAlign: 'top'
            }});
            stockLabel = {left: 0, lineHeight: '25px'};
            limitedStock = {...limitedStock, height: '80px'};
            imgClose = {...imgClose,
                marginTop: '0', position: 'absolute', top: '5px', right: '5px', width: '10px', height: '10px'
            };
            labels[1].style = {...labels[1].style, width: '45%', maxWidth: '175px'};
        } else if (windowSize <= 800 ) {
            // window width less then 800 styling
            labels.forEach((label) => {label.style = {...label.style, display: 'none'}});
            labels[1].style = {...labels[1].style,
                display: 'inline-block',
                lineHeight: '60px',
                width: '45%',
                maxWidth: '150px',
                verticalAlign: 'top',
                marginTop: '5px',
                marginRight: '5px',
            };
            labels[2].style = {...labels[1].style,
                display: 'inline-block',
                lineHeight: '60px',
                width: '45%',
                maxWidth: '150px',
                verticalAlign: 'top',
                marginTop: '5px',
            };
            imgClose = {...imgClose, width: '10px', height: '10px'};
        }

        labels[1]['style']['backgroundColor'] = colors[0];
        labels[2]['style']['backgroundColor'] = colors[1];
        if (labels.length > 3) {
            labels[3]['style']['backgroundColor'] = colors[2];
        }

        return (
            <div style={ tmpStyle } onClick={ windowSize < 800 ? onMinimize : ()=>{} }>
                { !/^\s*$/.test(labels[1].text) &&
                    <Textfit style={ labels[1].style }
                             mode="single"
                             max={ 45 }>
                        { labels[1].text }
                    </Textfit>
                }

                { !/^\s*$/.test(labels[2].text) &&
                    <Textfit style={ labels[2].style }
                             mode="single"
                             max={ 45 }>
                        { labels[2].text }
                    </Textfit>
                }

                { (isMinimized || windowSize > 800) &&
                    <LimitedStock label={ labels[4] }
                                  style={ limitedStock }
                                  labelStyle={ stockLabel }
                                  sold={ time.sold }
                                  total={ time.total }/>
                }

                { !/^\s*$/.test(labels[3].text) &&
                    <Textfit style={ labels[3].style }
                             mode="single"
                             max={ 45 }>
                        { labels[3].text }
                    </Textfit>
                }

                <div style={ imgClose } onClick={ hideCountdown }/>
            </div>
        )
    }
}

export default VerticalBanner;