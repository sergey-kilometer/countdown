import React from 'react'
import { withCookies } from 'react-cookie'

import IconV from '../../../images/iconV.png'
import IconX from '../../../images/iconX.png'
import { minifyBtnImg, minifyBtn, minifyBtnImgClose } from '../../../constants/styles/common'
import { TIME_ITEM, HASH_ITEM, IS_MINIMIZED_ITEM, HASH_REMOVED } from '../../../constants/localStorageItems'


class MinifyBtn extends React.Component {

    handleClose(event) {
        event.stopPropagation();
        let { cookies, hideCountdown } = this.props;
        cookies.set(HASH_REMOVED, cookies.get(HASH_ITEM), {domain: `.${window.location.hostname}`});
        cookies.remove(HASH_ITEM);
        cookies.remove(TIME_ITEM);
        cookies.remove(IS_MINIMIZED_ITEM);
        hideCountdown();
    }

    render () {
        let { onMinimize, closeButton } = this.props;

        return (
            <div style={ minifyBtn }
                 onClick={ closeButton ? this.handleClose.bind(this) : onMinimize }>
                <img src={ closeButton ? IconX : IconV }
                     style={ closeButton ? minifyBtnImgClose : minifyBtnImg } alt=""/>
            </div>
        );
    }
}

export default withCookies(MinifyBtn);