import React from 'react'
import { Textfit } from 'react-textfit'

import PoweredBy from './PoweredBy.react'
import MinifyBtn from './MinifyBtn.react'
import LimitedStock from './elements/LimitedStock.react'
import { templateStyle, textEditing, miniTmplStyle, mobileStyle, leftStyle,
         sceneStyle } from './../../../constants/styles/common'
import { label0, label1, label2, label3 } from '../../../constants/styles/template26'


export default (props) => {
    let { videoJson: { labels, time, colors, bannerPos }, hideCountdown, onMinimize, isMinimized, isMobile } = props,
        tmpStyle = Object.assign({}, templateStyle, isMobile ? mobileStyle : {}),
        miniStyle = {...miniTmplStyle};

    delete miniStyle['WebkitBoxShadow'];
    delete miniStyle['MozBoxShadow'];
    delete miniStyle['boxShadow'];

    if (bannerPos === 'left') {
        tmpStyle = Object.assign({}, tmpStyle, leftStyle);
    }

    labels.forEach(label => {label['style']['position'] = 'absolute'});

    return (
        isMinimized ?
            <div onClick={ onMinimize }
                 style={{ ...templateStyle, ... miniStyle, direction: 'ltr', backgroundColor: colors[0]}}>
                <MinifyBtn closeButton hideCountdown={ hideCountdown }/>

                { !/^\s*$/.test(labels[0].text) &&
                    <Textfit style={
                                Object.assign({}, textEditing, labels[0].style, label0, {
                                    "width": "106px",
                                    "top":"10px",
                                    "left":"5px"
                                })
                            }
                             mode="single"
                             max={ 30 }>
                        { labels[0].text }
                    </Textfit>
                }

                { !/^\s*$/.test(labels[1].text) &&
                    <Textfit style={{...textEditing, ...labels[1].style, ...label1, backgroundColor: colors[1], left: '110px'}}
                             mode="single"
                             max={ 30 }>
                        { labels[1].text }
                    </Textfit>
                }

                { !/^\s*$/.test(labels[2].text) &&
                    <Textfit style={{...textEditing, ...labels[2].style, ...label2, ...{
                                "width":"240px","top":"44px","height":"47px","lineHeight":"47px", left: '5px'
                            }}}
                             mode="single"
                             max={ 35 }>
                        { labels[2].text }
                    </Textfit>
                }

                <PoweredBy/>
            </div>
            :
            <div style={{...tmpStyle, backgroundColor: colors[0] }}>
                <MinifyBtn onMinimize={ onMinimize }/>

                <div style={ sceneStyle }>
                    { !/^\s*$/.test(labels[0].text) &&
                        <Textfit style={ Object.assign({}, textEditing, labels[0].style, label0) }
                                 mode="single"
                                 max={ 30 }>
                            { labels[0].text }
                        </Textfit>
                    }

                    { !/^\s*$/.test(labels[1].text) &&
                        <Textfit style={{...textEditing, ...labels[1].style, ...label1, backgroundColor: colors[1]}}
                                 mode="single"
                                 max={ 30 }>
                            { labels[1].text }
                        </Textfit>
                    }

                    { !/^\s*$/.test(labels[2].text) &&
                        <Textfit style={{...textEditing, ...labels[2].style, ...label2}}
                                 mode="single"
                                 max={ 70 }>
                            { labels[2].text }
                        </Textfit>
                    }

                    { !/^\s*$/.test(labels[3].text) &&
                        <Textfit style={{...textEditing, ...labels[3].style, ...label3}}
                                 mode="single"
                                 max={ 25 }>
                            { labels[3].text }
                        </Textfit>
                    }

                    <LimitedStock label={ labels[4] } sold={ time.sold } total={ time.total }/>

                    <PoweredBy/>
                </div>
            </div>
    )
}