import React from 'react'
import { Textfit } from 'react-textfit'

import Timer from '../../Timer'
import PoweredBy from './PoweredBy.react'
import MinifyBtn from './MinifyBtn.react'
import Minimized from './Minimized.react'
import TimeLabels from './TimeLabels.react'
import { templateStyle, textEditing, sceneStyle, mobileStyle, leftStyle } from './../../../constants/styles/common'
import { mainText, label0, label1, label2, svgStyle, timeLabel0, timeLabel1, timeLabel2 } from './../../../constants/styles/template16'


export default (props) => {
    let { videoJson: { labels, colors, time, bannerPos, schedule_type }, hideCountdown, onMinimize, isMinimized, isMobile } = props,
        tmpStyle = Object.assign({}, templateStyle, isMobile ? mobileStyle : {}, );

    if (bannerPos === 'left') {
        tmpStyle = Object.assign({}, tmpStyle, leftStyle);
    }

    labels.forEach(label => {
        label['style']['position'] = 'absolute';
        label['style']['backgroundColor'] = 'transparent';
    });

    return (
        isMinimized ?
            <Minimized onMinimize={ onMinimize }
                       hideCountdown={ hideCountdown }
                       wrapperStyle={{ backgroundColor: colors[1] }}
                       time={ time }
                       timeLabel={ labels[3] }
                       style={{ ...labels[1].style, ...label0 }}
                       text={ labels[1].text }/>
            :
            <div style={ tmpStyle }>
                <MinifyBtn onMinimize={ onMinimize }/>

                <div style={ sceneStyle }>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" style={ svgStyle }
                         xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 1080 1080" xmlSpace="preserve">
                        <polygon points="0,1.6 0,434.1 354.6,780.6 320.1,0" fill={ colors[0] }/>
                        <polygon points="329.5,0 639.6,0 714.6,570.2 349.2,657.6 320.1,0" fill={ colors[1] }/>
                        <polygon points="0,434.1 659.1,1080 0,1080" fill={ colors[2] }/>
                        <polygon points="349.2,657.6 750.6,561.6 1080,881.1 1080,1080 659.1,1080 354.6,780.6" fill={ colors[3] }/>
                        <polygon points="666.4,202.6 714.6,570.2 750.6,561.6 1080,881.1 1080,442.4" fill={ colors[0] }/>
                        <polygon points="639.6,1 666.4,203.6 1080,442.4 1080,1" fill={ colors[2] }/>
                    </svg>

                    <div style={ mainText }/>

                    { !/^\s*$/.test(labels[1].text) &&
                        <Textfit style={ Object.assign({}, textEditing, labels[1].style, label0) }
                                 mode="single"
                                 max={ 200 }>
                            { labels[1].text }
                        </Textfit>
                    }

                    { !/^\s*$/.test(labels[2].text) &&
                        <Textfit style={ Object.assign({}, textEditing, labels[2].style, label1) }
                                 mode="single"
                                 max={ 75 }>
                            { labels[2].text }
                        </Textfit>
                    }

                    { !/^\s*$/.test(labels[0].text) &&
                        <Textfit style={ Object.assign({}, textEditing, labels[0].style, label2) }
                                 mode="single"
                                 max={ 25 }>
                            { labels[0].text }
                        </Textfit>
                    }

                    <Timer time={ time } hideCountdown={ hideCountdown }/>

                    { ['days', 'limited_stock'].indexOf(schedule_type) > -1 ?
                        !/^\s*$/.test(labels[0].text) &&
                            <Textfit style={
                                        Object.assign(
                                            {},
                                            textEditing,
                                            labels[3].style, {top: '175px', left: '94px', width: '200px', lineHeight: '50px'}
                                        )
                                     }
                                     mode="single"
                                     max={ 40 }>
                                { labels[3].text }
                            </Textfit>
                        :
                        <TimeLabels labels={ time.labels } styles={ [timeLabel0, timeLabel1, timeLabel2] }/>
                    }

                    <PoweredBy/>
                </div>
            </div>
    )
}