import React from 'react'

import Frames16 from './Frames16.react'
import Frames18 from './Frames18.react'
import Frames21 from './Frames21.react'
import Frames26 from './Frames26.react'
import Vertical26 from './Vertical26.react'
import VerticalBanner from './VerticalBanner.react'

export default (props) => {
    let { videoJson: { id, bannerPos } } = props;

    if (['top', 'bottom'].indexOf(bannerPos) === -1) {
        switch (id) {
            case 16:
                return <Frames16 {...props} />;
            case 18:
                return <Frames18 {...props} />;
            case 21:
                return <Frames21 {...props} />;
            case 26:
                return <Frames26 {...props} />;
            default:
                return null;
        }
    } else {
        if (id === 26) {
            return <Vertical26 {...props}/>
        }
        return <VerticalBanner {...props}/>
    }
}