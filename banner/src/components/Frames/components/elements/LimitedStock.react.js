import React from 'react'
import { Textfit } from 'react-textfit'
import range from 'lodash/range'


import { stockContainer, rangeBlock, stockRange, stockWrapper, stockNum,
         stockNumTotal, stockLabel } from '../../../../constants/styles/stock'


export default function LimitedStock(props) {
    let { sold, total, label, style={}, labelStyle={} } = props,
        percentage = 20 * (sold/total),
        color1 = 12, color2 = 255;

    return (
        <div style={{...stockWrapper, ...style}}>
            { !/^\s*$/.test(label.text) &&
                <Textfit style={{...label.style, ...stockLabel, ...labelStyle}}
                         mode="single"
                         max={ 20 }>
                    { label.text }
                </Textfit>
            }

            <div style={ stockContainer }>
                <div style={{...stockNum, ...stockNumTotal, color: label.style['color']}}>{ total }</div>
                <div style={{...stockNum, color: label.style['color']}}>0</div>
                <div style={ stockRange }>
                    { range(20).map((i) => {
                        let backgroundColor = '#D6D6D6';
                        if (i < percentage) {
                            backgroundColor = `rgb(${color1 >= 255 ? color2-=26 : color2}, ${color1 <= 255 ? color1+=26 : 255}, 0)`;
                        }
                        return (
                            <div key={ i } style={{ ...rangeBlock, backgroundColor }}/>
                        );
                    })}
                </div>
            </div>
        </div>
    );
}