import React from 'react'
import { Textfit } from 'react-textfit'

import Timer from '../../Timer'
import PoweredBy from './PoweredBy.react'
import MinifyBtn from './MinifyBtn.react'
import Minimized from './Minimized.react'
import TimeLabels from './TimeLabels.react';
import { DEFAULT_DIMENSIONS } from '../../../constants/timer'
import { templateStyle, textEditing, sceneStyle, mobileStyle, leftStyle } from './../../../constants/styles/common'
import { mainText, label0, label1, label2, label3, timeLabel0, timeLabel1,
         timeLabel2 } from './../../../constants/styles/template18'

export default (props) => {
    let { videoJson: { labels, time, colors, bannerPos, schedule_type }, hideCountdown, onMinimize,
          isMinimized, isMobile } = props,
        tmpStyle = Object.assign({}, templateStyle, isMobile ? mobileStyle : {}),
        labelOStyle = Object.assign({}, labels[0].style, label0);

    labels[0]['style']['backgroundColor'] = colors[0];
    labels[2]['style']['backgroundColor'] = colors[1];
    labels[3]['style']['backgroundColor'] = colors[2];
    time.dimensions = {...DEFAULT_DIMENSIONS, location: 'matrix(1, 0, 0, 1, 25, 100)'};

    if (bannerPos === 'left') {
        tmpStyle = Object.assign({}, tmpStyle, leftStyle);
    }

    labels.forEach(label => {
        label['style']['position'] = 'absolute';
    });

    return (
        isMinimized ?
            <Minimized onMinimize={ onMinimize }
                       hideCountdown={ hideCountdown }
                       wrapperStyle={{ backgroundColor: colors[3] }}
                       time={ time }
                       timeLabel={ labels[4] }
                       style={ labelOStyle }
                       text={ labels[0].text }/>
            :
            <div style={ tmpStyle }>
                <MinifyBtn onMinimize={ onMinimize }/>

                <div style={ Object.assign({}, sceneStyle, {overflow: 'visible', backgroundColor: colors[2]})}>
                    <Timer time={ time } hideCountdown={ hideCountdown }/>

                    <div style={ Object.assign({}, mainText, {backgroundColor: colors[3]})}/>

                    { !/^\s*$/.test(labels[0].text) &&
                        <Textfit style={
                                    Object.assign({}, textEditing, labels[0].style, label0)
                                 }
                                 mode="single"
                                 max={ 106 }>
                            { labels[0].text }
                        </Textfit>
                    }

                    { !/^\s*$/.test(labels[3].text) &&
                        <Textfit style={
                                    Object.assign({}, textEditing, labels[3].style, label1)
                                 }
                                 mode="single"
                                 max={ 150 }>
                            { labels[3].text }
                        </Textfit>
                    }

                    { !/^\s*$/.test(labels[1].text) &&
                        <Textfit style={
                                    Object.assign({}, textEditing, labels[1].style, label2)
                                 }
                                 mode="single"
                                 max={ 38 }>
                            { labels[1].text }
                        </Textfit>
                    }

                    { !/^\s*$/.test(labels[2].text) &&
                        <Textfit style={
                                    Object.assign({}, textEditing, labels[2].style, label3)
                                 }
                                 mode="single"
                                 max={ 38 }>
                            { labels[2].text }
                        </Textfit>
                    }

                    { ['days', 'limited_stock'].indexOf(schedule_type) > -1 ?
                        !/^\s*$/.test(labels[4].text) &&
                            <Textfit style={
                                        Object.assign(
                                            {},
                                            textEditing,
                                            labels[4].style, {top: '100px', left: '94px', width: '200px', lineHeight: '50px'}
                                        )
                                     }
                                     mode="single"
                                     max={ 40 }>
                                { labels[4].text }
                            </Textfit>
                        :
                        <TimeLabels labels={ time.labels } styles={ [timeLabel0, timeLabel1, timeLabel2] }/>
                    }

                    <PoweredBy/>
                </div>
            </div>
    )
}