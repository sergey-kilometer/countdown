export const TIME_ITEM = 'topvid-countdown-time';
export const IS_MINIMIZED_ITEM = 'topvid-countdown-is-minimized';
export const HASH_REMOVED = 'topvid-countdown-hash-removed';
export const HASH_ITEM = 'topvid-countdown-hash';