export const COUNTDOWN_DOMAIN = 'https://countdown.topvid.com';
export const GET_VIDEO_JSON = COUNTDOWN_DOMAIN + '/video-json/';
export const POWERED_BY_URL = 'https://www.topvid.com/countdown';