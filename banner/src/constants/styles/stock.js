export const stockWrapper = {
    position: 'absolute',
    top: '180px',
    height: '65px',
    width: '295px',
};
export const stockLabel = {
    width: '249px',
    left: '20px',
    textAlign: 'center',
    height: '30%',
    position: 'relative'
};
export const stockContainer = {
    position: 'absolute',
    top: '30%',
    height: '70%',
    width: '100%',
};
export const stockRange = {
    width: '235px',
    padding: '6px',
    background:'#f5f5f5',
    position: 'relative',
    left: '20px',
    height: '35px',
};
export const rangeBlock = {
    marginBottom: '5px',
    marginLeft: '5px',
    height: '100%',
    overflow: 'hidden',
    width: '2.8%',
    float: 'left'
};
export const stockNum = {
    'width': '20px',
    'position': 'absolute',
    'color': 'rgb(0, 0, 0)',
    'top': '30%',
    'textAlign': 'center'
};
export const stockNumTotal = {
    'width': '30px',
    left: 'calc(100% - 30px)'
};