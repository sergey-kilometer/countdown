// Timer styles
export const timeLabel0 = {
    height: '15px',
    lineHeight: '15px',
    width: '65px',
    top: '234px',
    left: '26px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none'
};
export const timeLabel1 = {
    height: '15px',
    lineHeight: '15px',
    width: '65px',
    top: '234px',
    left: '115px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none'
};
export const timeLabel2 = {
    height: '15px',
    lineHeight: '15px',
    width: '65px',
    top: '234px',
    left: '205px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none'
};
// Labels styles
export const label0 = {
    top: '35px',
    left: '30px',
    height: '45px',
    lineHeight: '45px',
    width: '235px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none'
};
export const label1 = {
    top: '85px',
    left: '30px',
    height: '25px',
    lineHeight: '25px',
    width: '235px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none',
    cursor: 'text',
    border: 'dashed 1px black',
};
export const label2 = {
    top: '150px',
    left: '20px',
    height: '25px',
    lineHeight: '25px',
    width: '255px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none',
};
// Addition styles
export const mainText = {
    'position': 'absolute',
    'overflow': 'hidden',
    'zIndex': '0',
    'height': '95px',
    'width': '255px',
    'WebkitBoxShadow': 'rgb(0, 0, 0) 3px 3px 10px 0',
    'boxShadow': 'rgb(0, 0, 0) 3px 3px 10px 0',
    'background': 'white',
    'top': '30px',
    'right': '20px'
};
export const svgStyle = {
    'position': 'absolute',
    'zIndex': 0
};