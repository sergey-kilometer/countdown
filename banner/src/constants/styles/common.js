export const poweredBy = {
    'position': 'absolute',
    'bottom': 0,
    'right': '5px',
    'fontSize': '14px',
    'zIndex': 3,
    'cursor': 'pointer'
};
export const textEditing = {
    'overflow': 'hidden',
    'background': 'transparent',
    'border': 'hidden',
    'zIndex': 2,
    'cursor': 'default',
};
export const mobileStyle = {
    'zoom': 0.90,
    'MsZoom': 0.90,
    'WebkitZoom': 0.90,
    'MozTransformOrigin': 'bottom right',
    'MozTransform': 'scale(0.90)'
};
export const templateStyle = {
    'position': 'fixed',
    'bottom': '30px',
    'right': '30px',
    'zIndex': 1051,
    'width': '295px',
    'height': '260px',
    'WebkitBoxShadow': '0 0 10px 1.5px rgba(0,0,0,0.75)',
    'MozBoxShadow': '0 0 10px 1.5px rgba(0,0,0,0.75)',
    'boxShadow': '0 0 10px 1.5px rgba(0,0,0,0.75)'
};
export const leftStyle = {
    left: '35px',
    right: 0
};
export const sceneStyle = {
    position: 'relative',
    width: '100%',
    height: '100%',
    overflow: 'hidden',
    direction: 'ltr'
};
// Minimized style
export const miniTmplStyle = {
    height: '105px',
    width: '250px',
    WebkitBoxShadow: 'none',
    MozBoxShadow: 'none',
    boxShadow: 'none',
    cursor: 'pointer'
};
export const minifyBtn = {
    'zIndex': 5,
    'position': 'absolute',
    'top': '-14px',
    'right': '-8px',
    'width': '25px',
    'borderRadius': '50%',
    'backgroundColor': 'white',
    'height': '25px',
    'cursor': 'pointer',
};
export const minifyBtnImg = {
    'position': 'absolute',
    'top': '10px',
    'right': '6px',
};
export const minifyBtnImgClose = {
    'position': 'absolute',
    'top': '10px',
    'right': '8px',
};
export const minimizedLabel = {
    'width': '100%',
    'height': '35px',
    lineHeight: '35px',
    left: 0,
    top: 0,
    cursor: 'pointer'
};
// mini timer labels
// Timer styles
export const miniTimeLabel0 = {
    height: '12px',
    lineHeight: '12px',
    width: '65px',
    top: '78px',
    left: '10px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none',
    cursor: 'pointer'
};
export const miniTimeLabel1 = {
    height: '12px',
    lineHeight: '12px',
    width: '65px',
    top: '78px',
    left: '90px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none',
    cursor: 'pointer'
};
export const miniTimeLabel2 = {
    height: '12px',
    lineHeight: '12px',
    width: '65px',
    top: '78px',
    left: '170px',
    MsTransform: 'none',
    WebkitTransform: 'none',
    transform: 'none',
    cursor: 'pointer'
};