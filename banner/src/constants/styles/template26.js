export const label0 = {
    width: '117px',
    top: '10px',
    left: '20px',
};
export const label1 = {
    width: '135px',
    top: '10px',
    left: '135px',
};
export const label2 = {
    width: '249px',
    top: '50px',
    left: '23px',
    height: '75px',
    lineHeight: '75px',
    overflowWrap: 'break-word',
    textAlign: 'center'
};
export const label3 = {
    width: '243px',
    top: '130px',
    left: '21px',
    height: '30px',
    lineHeight: '30px',
};