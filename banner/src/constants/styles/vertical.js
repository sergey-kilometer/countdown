// TopBottom Styles
import IconX from '../../images/iconX.png'

export const bottomStyle = {
    position: 'fixed',
    bottom: 0,
    right: 0,
    zIndex: 1051,
    width: '100%',
    height: '70px',
    boxShadow: 'none',
    MozBoxShadow: 'none',
    WebkitBoxShadow: 'none',
    textAlign: 'center',
    display: 'table-cell'
};
export const label0Style = {
    width: '225px',
    backgroundColor: 'transparent',
};
export const label1Style = {
    width: '225px',
};
export const label1Mobile = {
    width: '130px',
    lineHeight: '42px',
    marginTop: '4px',
};
export const label2Style = {
    width: '225px',
    height: '60px',
    marginTop: '2px'
};
export const daysLabelStyle = {
    width: '200px',
    display: 'inline-block',
    lineHeight: '70px',
    marginLeft: '70px',
    marginTop: '-10px'
};
export const imageClose = {
    float: 'right',
    marginTop: '30px',
    marginRight: '1%',
    width: '13px',
    height: '13px',
    backgroundImage: `url(${IconX})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    cursor: 'pointer'
};
// Timer wrapper div style
export const timerWrapper = {
    display: 'inline-block',
    width: '255px',
    marginTop: '8px',
    marginRight: '1%',
    height: '10px',
    direction: 'ltr'
};
export const miniTimeLabel = {
    width: '55px',
    textAlign: 'center',
    position: 'absolute',
    lineHeight: '16px',
    height: '16px',
    top: '45px',
};
export const miniLabel = {
    // width: '95%',
    height: '62px',
    lineHeight: '62px'
};
export const miniClock = {
    width: '300px',
    position: 'relative',
    margin: '5px auto 20px'
};