export function editLabelStyle(label) {
    delete label.style['position'];
    delete label.style['transform'];
    delete label.style['height'];
    label['style'] = {...label['style'],
        lineHeight: '70px',
        zIndex: 2,
        cursor: 'default',
        marginLeft: '10px',
        marginRight: '10px',
        verticalAlign: 'top',
        display: 'inline-block'
    };
    return label;
}

export function editTimeLabelStyle(config, label, idx) {
    delete label.style['transform'];
    label.style = {...label.style,
        height: `${config.height}px`,
        lineHeight: `${config.height}px`,
        width: `${config.width}px`,
        top: `${config.top}px`
    };
    if (idx > 0) {
        label.style.marginLeft = `${idx * config.margin}px`
    }
    return label;
}

export function bannerJson(time, scheduleType, colors, labels) {

}