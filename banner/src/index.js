import './utils/documentReady'

import React from 'react'
import ReactDOM from 'react-dom'
import { CookiesProvider } from 'react-cookie'

import Root from './components/Root.react'

window.docReady(() => {
    let elemDiv = document.createElement('div');
    elemDiv.setAttribute('id', 'TOPVIDCountdownWidget');
    document.body.appendChild(elemDiv);

    ReactDOM.render((
        <CookiesProvider>
            <Root/>
        </CookiesProvider>
    ), document.getElementById('TOPVIDCountdownWidget'));
});