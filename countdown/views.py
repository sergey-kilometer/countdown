import shopify
from django.http.response import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required


def health_check(_):
    res = HttpResponse('Hello countdown app')
    res.status_code = 200
    res['Content-Length'] = 10
    return res


@login_required
def shopify_products(request):
    products_data = []
    categories = set()
    product_fields = ['title', 'id', 'images', 'variants', 'product_type']
    with request.user.session:
        page = 1
        while True:
            products = shopify.Product.find(page=page, limit=250, fields=product_fields)
            if products:
                for product in products:
                    if product.variants:
                        default_image_src = product.images[0].src if product.images else None
                        product_type = product.product_type
                        product_id = product.id

                        product_title = product.title
                        if product_type:
                            categories.add(product_type)

                        for variant in product.variants:
                            variant_id = variant.id
                            for image in product.images:
                                if variant_id in image.variant_ids:
                                    image_src = image.src
                                    break
                            else:
                                image_src = default_image_src

                            products_data.append({
                                'image': image_src,
                                'quantity': variant.inventory_quantity,
                                'price': variant.price,
                                'variant_id': variant.id,
                                'title': '{} {}'.format(product_title, variant.title),
                                'product_type': product_type,
                                'product_id': product_id
                            })
                page += 1
            else:
                break
    return JsonResponse({'categories': list(categories), 'products': products_data}, safe=False)
