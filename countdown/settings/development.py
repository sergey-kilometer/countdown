from countdown.settings.base import *

DOMAIN_PATH = 'http://127.0.0.1:8000'
# Set the login redirect URL to the "home" page for your app (where to go after logging on).
LOGIN_REDIRECT_URL = DOMAIN_PATH + '/'
LOGIN_URL = 'https://video-maker.topvid.com/login/'

STATICFILES_DIRS = [
    '/home/sergo/projects/topvid/video-maker/static/',
]

MIDDLEWARE.append('countdown.middleware.dev_cors_middleware')

# Isracard Global payment config https://isracard-global.com/system/#!/documentation
ISRACARD_SERVICE_URL = 'https://preprod.paymeservice.com/api/'
ISRACARD_API_KEY = 'MPL15010-55390BDZ-FGTNGEWR-0ZT91XAE'
ISRACARD_TEST_CARD = {
    'CARD_NUMBER': '4580458045804580',
    'EXP': '',
    'CVV': '123'
}

SESSION_COOKIE_DOMAIN = '.topvid.com'
SESSION_COOKIE_NAME = 'topvidesessionid'
