from countdown.settings.base import *

DOMAIN_PATH = 'https://countdown.topvid.com'
# Set the login redirect URL to the "home" page for your app (where to go after logging on).
LOGIN_REDIRECT_URL = DOMAIN_PATH + '/'
LOGIN_URL = DOMAIN_PATH + '/login/'

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

REACT_APP_DIR = os.path.join(BASE_DIR, 'frontend')
STATICFILES_DIRS = [
    os.path.join(REACT_APP_DIR, 'build', 'static'),
]

ISRACARD_SERVICE_URL = 'https://ng.paymeservice.com'
ISRACARD_API_KEY = 'MPL15011-49242SHT-4SCGPHLJ-W9ZY23QO'

# Isracard Global payment config https://isracard-global.com/system/#!/documentation
# ISRACARD_SERVICE_URL = 'https://preprod.paymeservice.com/api/'
# ISRACARD_API_KEY = 'MPL15010-55390BDZ-FGTNGEWR-0ZT91XAE'
ISRACARD_TEST_CARD = {
    'CARD_NUMBER': '4580458045804580',
    'EXP': '',
    'CVV': '123'
}

SESSION_COOKIE_DOMAIN = '.topvid.com'
SESSION_COOKIE_NAME = 'topvidesessionid'
