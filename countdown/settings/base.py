import os
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '24-y1!c#lf1!vc7xttgnfp&(mffaup4lujy((n4x%c(tyz)3km'
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# Application definition
INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'shopify_auth',
    'corsheaders',
    'paypal.standard.ipn',
    'authentication',
    'admin',
    'countdown_app',
    'payment',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'countdown.urls'

X_FRAME_OPTIONS = 'ALLOWALL'
CORS_ORIGIN_ALLOW_ALL = True

CORS_ALLOW_HEADERS = (
    'x-requested-with',
    'content-type',
    'accept',
    'origin',
    'authorization',
    'x-csrftoken',
    'Access-Control-Allow-Origin',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'shopify_auth.context_processors.shopify_auth',
            ],
        },
    },
]

WSGI_APPLICATION = 'countdown.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'video_maker',
        'USER': 'usr_enrich',
        'PASSWORD': '&WF5bOZlCqL5',
        'HOST': 'prod-kilometer-enrichmentservicedb.ckc1qsvzxlnk.us-east-1.rds.amazonaws.com',
        'PORT': '5432',
    }
}

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# FILE_UPLOAD_HANDLERS = ['django.core.files.uploadhandler.TemporaryFileUploadHandler']

# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

AUTHENTICATION_BACKENDS = [
    'authentication.backends.ShopUserBackend',
    'authentication.backends.RegularUserBackend',
    # 'authentication.backends.APIUserBackend',
    'authentication.backends.SharingUserBackend',
    'authentication.backends.FacebookUserBackend'
]

# Auth user model.
AUTH_USER_MODEL = 'countdown_app.VideoMakerUser'

# # Configure Shopify Application settings
# SHOPIFY_APP_NAME = 'Countdown Ads Builder'
# SHOPIFY_APP_API_KEY = '87b985f6f7910d4cad9a06fee1147b57'
# SHOPIFY_APP_API_SECRET = '4e4caddbd77e004a5d47e33ed03dc42f'
# SHOPIFY_APP_API_SCOPE = ['read_products']
# SHOPIFY_APP_IS_EMBEDDED = False
# SHOPIFY_APP_DEV_MODE = True

# Configure Shopify Application settings
SHOPIFY_APP_NAME = 'Facebook Video Maker'
SHOPIFY_APP_API_KEY = '28b738cedc147fc9a603aa07333d0836'
SHOPIFY_APP_API_SECRET = 'd056301da47336eea276f4dce66c5a4c'
SHOPIFY_APP_API_SCOPE = ['read_products']
SHOPIFY_APP_IS_EMBEDDED = False
SHOPIFY_APP_DEV_MODE = False

FILE_UPLOAD_HANDLERS = ['django.core.files.uploadhandler.TemporaryFileUploadHandler']

# Amazon config
AWS_ACCESS_KEY_ID = 'AKIAIP7UPUBF2UYTAAGA'
AWS_SECRET_ACCESS_KEY = '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'
AWS_BASE_PATH = 'https://s3.amazonaws.com/'
AWS_S3_USER_MUSIC_BUCKET = 'topvid-user-music'

# Intercom access token
INTERCOM_TOKEN = 'dG9rOjllODY4MTY2XzI1NDZfNDZmMl9hMzE3XzA4MzRiMzIwNzM3YzoxOjA='
