from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static

from . import views
from admin import urls as admin_urls
from payment import urls as payment_urls
from authentication import urls as auth_urls
from countdown_app import urls as countdown_app_urls

urlpatterns = [
    url(r'^healthcheck/$', views.health_check),
    url(r'^shopify/products/$', views.shopify_products),
    url(r'^admin', include(admin_urls)),
    url(r'^', include(payment_urls)),
    url(r'^', include(auth_urls)),
    url(r'^', include(countdown_app_urls)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
