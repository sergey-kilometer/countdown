import json
from django.conf import settings
from django.http import JsonResponse
from django.contrib.auth.decorators import user_passes_test


def inject_post_data(view_func):
    """
    View decorator. Injects post data

    :param view_func: View function
    :type view_func: callable

    :return: Wrapped view function.
    :rtype: callable
    """
    def view_func_wrapper(request, *args, **kwargs):
        post_data = json.loads(request.body.decode('utf-8'))
        return view_func(request, post_data, *args, **kwargs)

    return view_func_wrapper


def json_response(view_func):
    """
    View decorator. Wraps returned data into JSON response. Wrapped view should return JSON encodable dictionary.

    :param view_func: View function to wrap.
    :type view_func: callable

    :return: Wrapped view function.
    :rtype: callable
    """
    def view_func_wrapper(request, *args, **kwargs):
        return JsonResponse(view_func(request, *args, **kwargs))
    return view_func_wrapper


def anonymous_required(function=None, redirect_url=None):
    """
    Decorator requiring the current user to be anonymous (not logged in).
    """
    if not redirect_url:
        redirect_url = settings.LOGIN_REDIRECT_URL

    actual_decorator = user_passes_test(
        lambda u: u.is_anonymous() or u.api_owner,
        login_url=redirect_url,
        redirect_field_name=None
    )

    if function:
        return actual_decorator(function)
    return actual_decorator
