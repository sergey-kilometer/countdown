from django.conf.urls import url, include
from paypal.standard.ipn import urls as paypal_urls

from . import views


urlpatterns = [
    url(r'^success/recurring/$', views.paypal_recurring_success),
    url(r'^$', include(paypal_urls)),
]
