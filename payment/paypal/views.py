from django.http import HttpResponseRedirect
from paypal.standard.ipn.signals import valid_ipn_received
from paypal.standard.models import ST_PP_COMPLETED, ST_PP_ACTIVE

from .. import utils
from ..models import OrderDetail
from ..constants import PRICE_TO_PLAN, REDIRECT_URL_FAILURE, REDIRECT_URL_SUCCESS, BillingTypes


def paypal_notify(sender, **kwargs):
    """
    After purchase PayPal sends ipn signal about payment completed
    :param sender:
    :param kwargs:
    :return:
    """
    ipn_obj = sender
    custom = ipn_obj.custom.split(',')
    if ipn_obj.payment_status == ST_PP_COMPLETED \
            or ipn_obj.payment_status == ST_PP_ACTIVE \
                    and len(custom) == 3:
        username, price, order_id = utils.extract_custom_attr(custom)
        plan_name = PRICE_TO_PLAN[price]
        # if order in database credits already updated
        try:
            utils.update_order_status(order_id, 'ipn_accepted', ipn_obj)
        except OrderDetail.DoesNotExist:
            # if order not exist we create one and updaing user credits
            utils.create_order(
                username,
                order_id,
                payment_type=BillingTypes.pay_pal,
                status='ipn_accepted',
                details=ipn_obj
            )
            user = utils.update_user_plan(username, plan_name)
            if user:
                utils.intercom_subscribed_plan(user.email, plan_name, price, BillingTypes.pay_pal)

valid_ipn_received.connect(paypal_notify)


def paypal_recurring_success(request):
    """
    View user redirects to after PayPal subscribe success
    :param request:
    :return:
    """
    custom = request.GET.get('cm', request.POST.get('cm', '')).split(',')
    if len(custom) == 3:
        username, price, order_id = utils.extract_custom_attr(custom)
        plan_name = PRICE_TO_PLAN[price]
        try:
            OrderDetail.objects.get(order_id=order_id)
        except OrderDetail.DoesNotExist:
            utils.create_order(username, order_id, payment_type=BillingTypes.pay_pal)
            user = utils.update_user_plan(username, plan_name)
            if user:
                utils.intercom_subscribed_plan(user.email, plan_name, price, BillingTypes.pay_pal)

        return HttpResponseRedirect(REDIRECT_URL_SUCCESS)
    return HttpResponseRedirect(REDIRECT_URL_FAILURE)
