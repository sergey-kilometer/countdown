from django.conf import settings


REDIRECT_URL_SUCCESS = settings.DOMAIN_PATH + '/#/?paymentSuccess=1'
REDIRECT_URL_FAILURE = settings.DOMAIN_PATH + '/#/'
SHOPIFY_RETURN_URL = settings.DOMAIN_PATH + '/shopify/charge/'
PRICE_TO_PLAN = {49: 'pro', 99: 'agency'}


class BillingTypes:
    pay_pal = 'PayPal'
    shopify = 'Shopify'
    credit_card = 'Credit Card'
