import time
import calendar
from contextlib import suppress
from django.conf import settings
from intercom.client import Client
from .models import OrderDetail
from countdown_app.models import VideoMakerUser

intercom = Client(personal_access_token=settings.INTERCOM_TOKEN)


def extract_custom_attr(custom):
    """
    Extracts custom attributes from list ['username', 'price', 'order_id']
    """
    return custom[0], float(custom[1]), custom[2]


def update_user_plan(username, plan_name):
    """
    Updates user credits in data base
    """
    try:
        user = VideoMakerUser.objects.get(username=username)
        user.countdown_plan = plan_name
        user.save(update_fields=['countdown_plan'])
        return user
    except VideoMakerUser.DoesNotExist:
        return None


def create_order(username, order_id, payment_type='Shopify', status='success', details={}):
    """
    Creating new Order row in data base
    """
    VideoMakerUser.objects.get(username=username).orderdetail_set.create(
        username=username,
        credits=0,
        order_id=order_id,
        status=status,
        details=details,
        type=payment_type
    )


def update_order_status(order_id, status='success', details={}):
    """
    Updates Order status in data base
    """
    order = OrderDetail.objects.get(order_id=order_id)
    order.status = status
    order.details = details
    order.save(update_fields=['status', 'details'])
    return order


def intercom_send_event(event_name, email, metadata):
    """
    Sends event to Intercom
    """
    intercom.events.create(
        event_name=event_name,
        created_at=calendar.timegm(time.gmtime()),
        email=email,
        metadata=metadata
    )


def intercom_set_custom_attributes(email, attributes):
    """
    Updates intercom user custom attributes
    """
    intercom_user = intercom.users.find(email=email)
    for key, value in attributes.items():
        intercom_user.custom_attributes[key] = value
    intercom.users.save(intercom_user)


def intercom_subscribed_plan(email, plan_name, price, billing_type):
    """
    Updates intercom user custom attr and sends event 'Credits purchased'
    """
    with suppress(Exception):
        intercom_send_event('Countdown subscribed to plan', email, {
            'price': price,
            'plan_name': plan_name,
            'billing_type': billing_type
        })
        intercom_set_custom_attributes(email, {
            'COUNTDOWN_APP_PLAN': plan_name,
            'COUNTDOWN_APP_PLAN_PRICE': price,
        })
