from django.conf.urls import url, include

from .paypal import urls as paypal_urls
from .card import urls as card_urls

urlpatterns = [
    url(r'^paypal/', include(paypal_urls)),
    url(r'^card/', include(card_urls)),
]
