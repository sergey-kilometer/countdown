from django.db import models
from django.conf import settings
from jsonfield import JSONField


class OrderDetail(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    order_id = models.CharField(max_length=255, primary_key=True)
    username = models.CharField(max_length=255)
    status = models.CharField(max_length=100, default='success')
    credits = models.CharField(max_length=20, default=0)
    type = models.CharField(max_length=100, default=None)
    details = JSONField(default=None)
    create_date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'video_maker_orders_details'
