from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^checkout/$', views.checkout),
    url(r'^status/$', views.pull_status),
]
