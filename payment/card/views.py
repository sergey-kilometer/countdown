from django.http import HttpResponse, JsonResponse
from shopify_auth.decorators import login_required

from .. import utils
from ..models import OrderDetail
from .utils import get_sale, recurring_sale
from ..constants import REDIRECT_URL_FAILURE, BillingTypes, PRICE_TO_PLAN


@login_required
def checkout(request):
    params = request.GET
    price = float(params.get('price'))
    plan_name = PRICE_TO_PLAN[price]
    sale, transaction_id = recurring_sale(price, plan_name)
    res_url = sale.get('sale_url', sale.get('sub_url', REDIRECT_URL_FAILURE))

    if res_url != REDIRECT_URL_FAILURE:
        utils.create_order(
            request.user.username,
            transaction_id,
            payment_type=BillingTypes.credit_card,
            status='sale_generated',
            details=sale
        )
    return HttpResponse(res_url)


@login_required
def pull_status(request):
    order = OrderDetail\
        .objects\
        .filter(username=request.user.username, status='sale_generated')\
        .latest('create_date')
    sale_type = 'subscription' if order.details.get('sub_payme_id') else 'single'
    item, status = get_sale(sale_type, order.details)
    if item and status == 1:
        order.status = 'completed'
        order.details = item
        order.save(update_fields=['status', 'details'])
        price = int(item.get('sale_price', item.get('price', item.get('sub_price')))) / 100
        plan_name = PRICE_TO_PLAN[price]
        user = utils.update_user_plan(request.user.username, plan_name)
        if user:
            utils.intercom_subscribed_plan(user.email, plan_name, price, BillingTypes.credit_card)
        return JsonResponse({
            'status': 'completed',
            'plan_name': plan_name
        })
    return HttpResponse()
