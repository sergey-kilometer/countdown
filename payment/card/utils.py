import requests
import datetime
from django.conf import settings
from django.utils.crypto import get_random_string


def generate_sale(sale_type, price, plan_name):
    return globals().get(sale_type)(price, plan_name)


def regular_sale(price=0, plan_name='pro'):
    """
    Generates single credit card charge
    """
    transaction_id = get_random_string()
    data = {
        'seller_payme_id': settings.ISRACARD_API_KEY,
        'sale_price': price * 100,
        'product_name': 'Countdown {} plan.'.format(plan_name),
        'currency': 'USD',
        'transaction_id': transaction_id,
        'installments': 0,
        'capture_buyer': 1,
        'language': 'en',
        # 'sale_callback_url': settings.DOMAIN_PATH + '/card/callback/'
    }
    res_json = requests\
        .post(settings.ISRACARD_SERVICE_URL + 'generate-sale', json=data)\
        .json()
    return res_json, transaction_id


def recurring_sale(price=0, plan_name='pro'):
    """
    Generates recurring credit card charge
    """
    data = {
        'seller_payme_id': settings.ISRACARD_API_KEY,
        'sub_price': price * 100,
        'sub_description': 'Countdown {} plan.'.format(plan_name),
        'sub_currency': 'USD',
        'sub_iteration_type': '3',
        'sub_start_date': datetime.date.today().strftime('%d/%m/%Y'),
        'language': 'en'
    }
    res = requests.post(settings.ISRACARD_SERVICE_URL + 'generate-subscription', json=data)
    res.raise_for_status()
    res_json = res.json()
    return res_json, res_json.get('sub_payme_id')


def get_sale(sale_type, details={}):
    return globals().get('get_sale_{}'.format(sale_type))(details)


def get_sale_single(details):
    data = {
        'seller_payme_id': settings.ISRACARD_API_KEY,
        'sale_payme_id': details.get('payme_sale_id'),
        'transaction_id': details.get('transaction_id')
    }
    res = requests.post(settings.ISRACARD_SERVICE_URL + 'get-sales', json=data)
    items = res.json().get('items', [])
    if items:
        item = items[0]
        status = 1 if item.get('sale_status') == 'completed' else 0
        return item, status
    else:
        return None, 0


def get_sale_subscription(details):
    data = {
        'seller_payme_id': settings.ISRACARD_API_KEY,
        'sub_payme_code': details.get('sub_payme_code'),
        'sub_payme_id': details.get('sub_payme_id')
    }
    res = requests.post(settings.ISRACARD_SERVICE_URL + 'get-subscriptions', json=data)
    items = res.json().get('items', [])
    if items:
        item = items[0]
        status = 1 if item.get('sub_status') == 2 else 0
        return item, status
    else:
        return None, 0
