from django.conf.urls import url, include

from . import views
from . import amazon_polly_views

from .music import urls as music_urls

urlpatterns = [
    url(r'^$', views.index),
    url(r'^music/', include(music_urls)),
    url(r'^campaigns/(?P<campaign_id>-?[0-9]+)$', views.update_campaign),
    url(r'^campaigns/$', views.get_campaigns),
    url(r'^video-json/(?P<ad_hash>[^/]+)/$', views.get_video_json),
    url(r'^accounts/$', views.get_accounts),
    url(r'^countdown/user/save/$', views.save_countdown_user),
    url(r'^campaign/create/$', views.create_campaign),
    url(r'^generate-voice-over$', amazon_polly_views.get_speech),
    url(r'^custom/conversions/$', views.get_custom_conversions),
]
