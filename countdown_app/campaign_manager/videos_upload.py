#!/opt/kilometer/shopify-marko/venv/bin/python3.4

import boto3
import shopify
from datetime import datetime
from dateutil.relativedelta import relativedelta
from facebookads.api import FacebookAdsApi
from facebookads.objects import (AdAccount, AdVideo, Campaign)

from helpers import notify
from helpers.db import DBHelper
from constants.facebook import API_VERSION
from helpers.utils import update_campaign_status
from constants.status import Status
from constants.html import HTML_BEFORE_EXPIRE, HTML_AFTER_EXPIRE
from constants.data_base import CountdownCampaigns, CountdownAds, VideoMakerVideos, CountdownRetargetingUser

PLAN_TO_PRICE = {
    'professional':  149,
    'custom': 19,
    'business': 49,
    'dormant':  19,
    'basic': 19,
    'unlimited': 299,
    'affiliate': 19,
    'trial': 19,
    'staff':  19,
    'npo_lite': 19,
    'shopify_plus': 299
}

if __name__ == '__main__':
    db_helper = DBHelper()
    # Select ready videos from video_maker_videos where countdown_app_ads.status pending video
    query = "\
        SELECT\
          countdown_app_campaigns.username,\
          countdown_app_campaigns.settings,\
          countdown_app_campaigns.facebook_token,\
          video_maker_videos.id AS video_maker_id,\
          video_maker_videos.uploaded_urls,\
          countdown_app_ads.campaign_id,\
          countdown_app_ads.hour,\
          countdown_app_ads.id\
        FROM countdown_app_ads\
          INNER JOIN video_maker_videos ON (countdown_app_ads.video_maker_id = video_maker_videos.id)\
          INNER JOIN countdown_app_campaigns ON (countdown_app_ads.campaign_id = countdown_app_campaigns.campaign_id)\
        WHERE video_maker_videos.status = 'done' AND countdown_app_ads.status = 'pending' " \
            "AND countdown_app_campaigns.status = 'videos_created'\
    "
    ready_videos = db_helper.execute(query, None, 'dict_fetchall')
    delete_objects = []
    deleted_campaigns = []

    for video in ready_videos:
        username = video.get(CountdownCampaigns.USERNAME)
        campaign_id = video.get(CountdownAds.CAMPAIGN_ID)
        try:
            # extract properties from video
            account_id = video.get(CountdownCampaigns.SETTINGS, {}).get('ad_account')
            hour = video.get(CountdownAds.HOUR)
            facebook_token = video.get(CountdownCampaigns.FACEBOOK_TOKEN)
            uploaded_urls = video.get(VideoMakerVideos.UPLOADED_URLS)
            # upload video to facebook
            # check if video belongs to already deleted campaign
            if not any(camp_id == campaign_id for camp_id in deleted_campaigns):
                FacebookAdsApi.init(access_token=facebook_token, api_version=API_VERSION)
                # check if campaign deleted
                facebook_campaign = Campaign(fbid=campaign_id).remote_read(fields=[Campaign.Field.status])
                if facebook_campaign.get(Campaign.Field.status) in [Campaign.Status.deleted, Campaign.Status.archived]:
                    deleted_campaigns.append(campaign_id)
                    # remove not created videos
                    update_campaign_status(db_helper, campaign_id, Status.CAMPAIGN_DELETED)
                    query = "DELETE FROM video_maker_videos " \
                            "WHERE video_maker_videos.status='pending' AND video_maker_videos.id IN " \
                            "(SELECT countdown_app_ads.video_maker_id FROM countdown_app_ads " \
                            "WHERE countdown_app_ads.campaign_id = '{campaign_id}')".format(campaign_id=campaign_id)
                    db_helper.execute(query, None, None)
                    error_status = 'campaign_id: {} {}'.format(campaign_id, Status.CAMPAIGN_DELETED)
                    msg = 'Campaign deleted by user from facebook before ads created'
                    notify.write_to_slack(username, error_status, msg)
                else:
                    facebook_video = AdAccount(fbid=account_id).create_ad_video(
                        params={
                            'manual_privacy': False,
                            'is_crosspost_video': True,
                            'is_explicit_share': True,
                            'allow_bm_crossposting': True,
                            'description': 'Countdown App Video {}:00:00'.format(hour),
                            'name': 'Countdown App Video {}:00:00'.format(hour),
                            'file_url': uploaded_urls.get('mp4_url')
                        }
                    )
                    # save video to CountdownAds and update status
                    db_helper.build_update(
                        tb_name=CountdownAds.TABLE_NAME,
                        set_columns=[CountdownAds.STATUS, CountdownAds.FACEBOOK_VIDEO_ID],
                        where=[CountdownAds.ID],
                        values=['done', facebook_video.get(AdVideo.Field.id), video.get(CountdownAds.ID)]
                    )
                    # Remove video maker video
                    # db_helper.build_delete(
                    #     tb_name=VideoMakerVideos.TABLE_NAME,
                    #     where_values=[VideoMakerVideos.ID],
                    #     values=[video.get('video_maker_id')]
                    # )

                    for key, value in uploaded_urls.items():
                        if 'mp4' not in value:
                            delete_objects.append({'Key': value.replace('http://static.topvid.com/', '')})
                    # keys = [{'Key': value.replace('http://static.topvid.com/', '')} for key, value in uploaded_urls.items()]
                    # delete_objects = delete_objects + keys

        except Exception as e:
            error_status = 'campaign_id: {} {}'.format(campaign_id, 'FACEBOOK_UPLOAD_ERROR')
            notify.write_to_slack(username, error_status, str(e))

    # delete uploaded_urls
    if delete_objects:
        s3_bucket = boto3.resource(
            's3',
            aws_access_key_id='AKIAJPZ4JVR4BCQNOMGA',
            aws_secret_access_key='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'
        ).Bucket('static.topvid.com')
        s3_bucket.delete_objects(Delete={'Objects': delete_objects})

    # send mails to users of countdown retargeting
    query = "SELECT * FROM countdown_retargeting_user " \
            "WHERE trial_start_date IS NOT NULL " \
            "AND charge_id IS NULL " \
            "AND myshopify_domain NOT LIKE '%unsubscribe' " \
            "AND mail_trial_expired = FALSE"
    cdr_users = db_helper.execute(query, None, 'dict_fetchall')
    for user in cdr_users:
        myshopify_domain = user.get('myshopify_domain', '')
        try:
            datetime_delta = relativedelta(datetime.now(), user.get('trial_start_date'))
            if (datetime_delta.days > 14 or datetime_delta.months > 0) or datetime_delta.days == 13:
                mail_subject = 'Your countdown ads trial expired.'
                html_file = HTML_AFTER_EXPIRE
                update_field = 'mail_trial_expired'

                if datetime_delta.days == 13:
                    mail_subject = 'Your trial is about to expire.'
                    html_file = HTML_BEFORE_EXPIRE
                    update_field = 'mail_before_expired'

                if not user.get(update_field):
                    with shopify.Session.temp(myshopify_domain, user.get('token')):
                        plan_name = shopify.Shop.current().to_dict().get('plan_name', 'basic')
                        price = PLAN_TO_PRICE.get(plan_name, 49)
                        charge = shopify.RecurringApplicationCharge().create({
                            'price': price,
                            'test': False,
                            'name': 'Payment for Facebook Countdown Retargeting every months.',
                            'return_url': 'https://countdown-retargeting.topvid.com/shopify/charge/recurring/'
                        })
                    notify.send_mail(user.get('email'), user.get('name'), charge.confirmation_url, mail_subject, html_file)
                    db_helper.build_update(
                        tb_name=CountdownRetargetingUser.TABLE_NAME,
                        set_columns=[update_field],
                        where=[CountdownRetargetingUser.SHOPIFY_DOMAIN],
                        values=[True, myshopify_domain]
                    )
                    notify.write_to_slack(myshopify_domain, 'MAIL_SENT', mail_subject)
        except Exception as e:
            notify.write_to_slack(myshopify_domain, 'SEND_MAIL_ERROR', str(e))

    db_helper.close()
