#!/opt/kilometer/shopify-marko/venv/bin/python3.4
from helpers.db import DBHelper
from objects.VideoCreator import VideoCreator


if __name__ == '__main__':
    # open DB connection
    db_helper = DBHelper()
    # select campaigns with status
    campaigns = VideoCreator.select_campaigns(db_helper)

    for camp in campaigns:
        video_creator = VideoCreator(db_helper, camp)
        video_creator.create_videos()

    # force close connection
    db_helper.close()
