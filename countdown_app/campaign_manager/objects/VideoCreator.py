import json
import math
import uuid
import tempfile
import urllib.request
import subprocess
import boto3
from copy import deepcopy
from random import randint

from constants.status import Status
from helpers.utils import update_campaign_status
from decorators.exception import exception_handler
from constants.data_base import CountdownCampaigns, CountdownAds, VideoMakerVideos

AMAZON_ID = 'AKIAJKXQABOHNY6G36FQ'
AMAZON_SECRET = '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'
AWS_S3_USER_MUSIC_BUCKET = 'topvid-user-music'


class VideoCreator(object):
    AUTO_DECREASE_HOURS = 6
    SCHEDULE_TYPE = 'schedule_type'
    TIME = 'time'
    DURATION = 'totalDuration'
    VOICE_OVER_PARAMS = ['[x]', '[y]']
    MEDIA_TYPES = json.dumps({'gif': False, 'mp4': True, 'countdown': True})
    MULTI_INSERT_ADS_QUERY = 'INSERT INTO countdown_app_ads (campaign_id, hour, status, ad_type, time, hash, ' \
                             'video_maker_id) VALUES {}'
    MULTI_INSERT_VIDEOS_QUERY = 'INSERT INTO video_maker_videos (username, template_version, video_json, status, ' \
                                'media_types) VALUES {} RETURNING id'

    def __init__(self, db_helper, campaign):
        self.db_helper = db_helper
        self.maker_videos = []
        self.countdown_ads = []
        self.campaign_id = campaign.get(CountdownCampaigns.CAMPAIGN_ID)
        self.video_json = campaign.get(CountdownCampaigns.VIDEO_JSON, {})
        self.username = campaign.get(CountdownCampaigns.USERNAME)
        self.schedule_type = self.video_json.get(self.SCHEDULE_TYPE)
        self.time = self.video_json.get(self.TIME)
        self.duration = self.time.get(self.DURATION, 72)
        self.embedVoice = False

    @staticmethod
    def select_campaigns(db_helper):
        campaigns = db_helper.build_select(
            tb_name=CountdownCampaigns.TABLE_NAME,
            select_value=[
                CountdownCampaigns.USERNAME,
                CountdownCampaigns.VIDEO_JSON,
                CountdownCampaigns.CAMPAIGN_ID,
            ],
            where=[CountdownCampaigns.STATUS],
            values=[Status.VIDEOS_CREATE]
        )
        return campaigns

    @exception_handler(error_status=Status.VIDEOS_CREATE_ERROR)
    def create_videos(self):
        update_campaign_status(self.db_helper, self.campaign_id, Status.VIDEOS_CREATING)
        # create voice over if no params
        voice_over_text = self.video_json.get('voiceOver', {}).get('text')
        if voice_over_text:
            if any(p in voice_over_text for p in self.VOICE_OVER_PARAMS):
                self.embedVoice = True
            else:
                self.video_json['music'] = VideoCreator.embed_voice_over_in_music(self.video_json, False)

        if self.schedule_type == 'limited_stock':
            self._create_limited_stock()
        elif self.schedule_type == 'days':
            self._create_days()
        else:
            self._create_hours()

        self._insert_videos()

        update_campaign_status(self.db_helper, self.campaign_id, Status.VIDEOS_CREATED)

    def _create_limited_stock(self):
        product = self.time.get('product')
        total = int(self.time.get('total', 100))
        sold = int(self.time.get('sold', 1))
        auto_decrease = self.time.get('autoDecrease', True)
        if product or not auto_decrease:
            self._create_video(sold, 'targeting')
        else:
            delta = int(math.ceil((total - sold - 1) / (self.duration * 24 / self.AUTO_DECREASE_HOURS)))
            for i in range(sold, total, delta):
                self._create_video(i, 'targeting')

    def _create_days(self):
        for i in range(1, self.time.get('start') + 1):
            self._create_video(i, 'targeting')

    def _create_hours(self):
        # Creating videos for targeting
        for i in range(1, self.duration, 3):
            self._create_video(i, 'targeting')
        # Creating videos for retargeting
        for i in range(0, self.duration, 3):
            self._create_video(i, 'retargeting')

    def _create_video(self, hours, ad_type):
        # Set time with random minutes and seconds
        new_video_json = deepcopy(self.video_json)
        if self.schedule_type == 'days':
            new_video_json['time']['start'] = hours
        elif self.schedule_type == 'limited_stock':
            new_video_json['time']['sold'] = hours
            new_video_json['time']['start'] = new_video_json['time'].get('total', 100) - hours
        else:
            new_video_json['time']['start'] = '{hours:02d}:{minutes}:{seconds}'\
                .format(hours=hours, minutes=randint(10, 40), seconds=randint(17, 49))
        # Embed voice over if needed.
        if self.embedVoice:
            new_video_json['music'] = VideoCreator.embed_voice_over_in_music(new_video_json)
        # Insert row into video maker videos to create video with status pending
        self.maker_videos.append(tuple((
            self.username,
            new_video_json.get('id'),
            json.dumps(new_video_json),
            'pending',
            self.MEDIA_TYPES
        )))
        self.countdown_ads.append(tuple((
            self.campaign_id, hours,
            'pending', ad_type,
            new_video_json['time']['start'], uuid.uuid4().hex
        )))

    def _insert_videos(self):
        """
        Inserts video_maker videos to DB and retrieves ids,
        after that inserts countdown_ads with video_maker_id
        """
        query = self.MULTI_INSERT_VIDEOS_QUERY.format(','.join(['%s'] * len(self.maker_videos)))
        result = self.db_helper.execute(query, self.maker_videos, 'dict_fetchall')
        # extract video_maker_videos_ids
        temp_ads = []
        for i, e in enumerate(self.countdown_ads):
            temp_ads.append(e + (result[i].get('id'), ))

        query = self.MULTI_INSERT_ADS_QUERY.format(','.join(['%s'] * len(temp_ads)))
        self.db_helper.execute(query, temp_ads, 'dict_fetchall')

    @staticmethod
    def embed_voice_over_in_music(video_json, add_params=True):
        file_name = str(uuid.uuid4()) + '.mp3'
        voice_over = video_json.get('voiceOver', {})
        voice_over_text = voice_over.get('text')
        # Get voice.
        if add_params:
            schedule_type = video_json.get('schedule_type')
            time = video_json.get('time', {})
            if schedule_type == 'limited_stock':
                voice_over_text = voice_over_text\
                    .replace('[x]', str(time.get('sold', 0)))\
                    .replace('[y]', str(time.get('total', 100)))
            elif schedule_type == 'days':
                voice_over_text = voice_over_text.replace('[x]', str(time.get('start', ' ')))
            else:
                voice_over_text = voice_over_text.replace('[x]', time.get('start', ' ').split(':')[0].lstrip('0'))

        polly = boto3.client(
            'polly',
            aws_access_key_id=AMAZON_ID,
            aws_secret_access_key=AMAZON_SECRET,
            region_name='us-east-1'
        )
        polly_response = polly.synthesize_speech(Text=voice_over_text, VoiceId=voice_over['voice'], OutputFormat='mp3')

        main_music_binary = tempfile.NamedTemporaryFile(suffix='.mp3', delete=False)
        main_music_binary.write(urllib.request.urlopen(video_json['music']).read())

        voice_over_binary = tempfile.NamedTemporaryFile(suffix='.mp3', delete=False)
        voice_over_binary.write(polly_response.get('AudioStream').read())

        output_binary = tempfile.NamedTemporaryFile(suffix='.mp3', delete=False)
        # "ffmpeg -i music.mp3 -af "volume=.25:enable='between(t,1,4)" out.mp3"
        # Merges 2 audio files into 1
        command = 'ffmpeg -i {voice_over} -i {main_music} -shortest -filter_complex "[0:a]adelay=1000|1000,volume=2' \
                  '[a0];[1:a]volume=.5[a1];[a0][a1]amix=inputs=2[out]" -map "[out]" -ac 2 -c:a libmp3lame {output} ' \
                  '-y'.format(
            main_music=main_music_binary.name,
            voice_over=voice_over_binary.name,
            output=output_binary.name
        )
        subprocess.call(command, shell=True)

        # Uploads to S3
        s3_client = boto3.client('s3', aws_access_key_id=AMAZON_ID, aws_secret_access_key=AMAZON_SECRET)
        s3_client.upload_file(
            output_binary.name,
            AWS_S3_USER_MUSIC_BUCKET,
            file_name,
            ExtraArgs={'Metadata': {'type': 'voiceover'}}
        )

        return 'https://s3.amazonaws.com/{bucket}/{file_name}'.format(
            bucket=AWS_S3_USER_MUSIC_BUCKET,
            file_name=file_name
        )
