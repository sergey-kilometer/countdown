from facebookads.objects import Campaign
from decorators.exception import exception_handler


class CDCampaign(object):

    class CountdownCampaigns:
        RETARGETING_USER_ID = 'retargeting_id'
        TABLE_NAME = 'countdown_app_campaigns'
        USERNAME = 'username'
        TARGETING_START_TIME = 'targeting_start_time'
        RETARGETING_START_TIME = 'retargeting_start_time'
        SETTINGS = 'settings'
        VIDEO_JSON = 'video_json'
        FACEBOOK_TOKEN = 'facebook_token'
        STATUS = 'status'
        CAMPAIGN_ID = 'campaign_id'
        ADSET_TARGETING = 'adset_targeting'
        ADSET_RETARGETING_DAY_1 = 'adset_retargeting_day_1'
        ADSET_RETARGETING_DAY_2 = 'adset_retargeting_day_2'
        ADSET_RETARGETING_DAY_3 = 'adset_retargeting_day_3'
        ID = 'id'

    def __init__(self, db_helper=None, campaign_id=None, video_json=None, status=None, username=None,
                 facebook_token=None):
        self.db_helper = db_helper
        self.campaign_id = campaign_id
        self.video_json = video_json
        self.status = status
        self.username = username
        self.facebook_token = facebook_token
        # super(Campaign, self).__init__(campaign_id)

    @classmethod
    def update_status(cls, db_helper, campaign_id, status):
        db_helper.build_update(
            tb_name=cls.CountdownCampaigns.TABLE_NAME,
            set_columns=[cls.CountdownCampaigns.STATUS],
            where=[cls.CountdownCampaigns.CAMPAIGN_ID],
            values=[status, campaign_id]
        )

    @classmethod
    def select_by_status(cls, db_helper,  status):
        campaigns = db_helper.build_select(
            tb_name=cls.CountdownCampaigns.TABLE_NAME,
            select_value=[
                cls.CountdownCampaigns.ID,
                cls.CountdownCampaigns.USERNAME,
                cls.CountdownCampaigns.VIDEO_JSON,
                cls.CountdownCampaigns.CAMPAIGN_ID,
                cls.CountdownCampaigns.FACEBOOK_TOKEN
            ],
            where=[cls.CountdownCampaigns.STATUS],
            values=[status]
        )
        return campaigns

    @exception_handler(error_status='Some error status')
    def create_campaign(self, campaign):
        raise Exception('test 123')

