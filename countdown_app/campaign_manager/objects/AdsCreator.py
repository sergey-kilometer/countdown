import json
from facebookads.api import FacebookAdsApi

from helpers import notify
from helpers.dynamic_ad_generator import DynamicAdGenerator
from constants.status import Status
from constants.facebook import API_VERSION
from decorators.exception import exception_handler
from helpers.utils import update_campaign_status, create_thumbnail, _add_cd_param_to_url
from constants.data_base import CountdownCampaigns, CountdownAds, VideoMakerVideos, SELECT_ALL


class AdsCreator(object):

    def __init__(self, db_helper, campaign):
        self.db_helper = db_helper
        self.campaign_id = campaign.get(CountdownCampaigns.CAMPAIGN_ID)
        self.username = campaign.get(CountdownCampaigns.USERNAME)
        self.video_json = campaign.get(CountdownCampaigns.VIDEO_JSON)
        self.settings = campaign.get(CountdownCampaigns.SETTINGS)
        self.facebook_token = campaign.get(CountdownCampaigns.FACEBOOK_TOKEN)
        self.ad_account = self.settings.get('ad_account')
        self.schedule_type = self.video_json.get('schedule_type')
        self.thumbnail_url = self.video_json.get('thumbnail_url', self.settings.get('thumbnail_url'))

    @staticmethod
    def select_campaigns(db_helper):
        campaigns = db_helper.build_select(
            tb_name=CountdownCampaigns.TABLE_NAME,
            select_value=SELECT_ALL,
            where=[CountdownCampaigns.STATUS],
            values=[Status.VIDEOS_CREATED]
        )
        return campaigns

    def _select_ads(self):
        return self.db_helper.build_select(
            tb_name=CountdownAds.TABLE_NAME,
            select_value=SELECT_ALL,
            where=[CountdownAds.CAMPAIGN_ID],
            values=[self.campaign_id],
            order_by=CountdownAds.HOUR
        )

    @exception_handler(error_status=Status.ADS_CREATE_ERROR)
    def create_ads(self):
        countdown_ads = self._select_ads()
        # check that all videos ready
        if len(countdown_ads) == sum(video.get('status') == 'done' for video in countdown_ads):
            update_campaign_status(self.db_helper, self.campaign_id, Status.CAMPAIGN_CREATING)
            FacebookAdsApi.init(access_token=self.facebook_token, api_version=API_VERSION)
            ad_gen = DynamicAdGenerator(ad_account=self.ad_account, page_id=self.settings.get('page_id'))
            ad_gen.set_pixel_id(self.settings.get('pixel_id'))

            if not self.thumbnail_url:
                # Create thumbnail without texts and save in settings
                self.settings['thumbnail_url'] = create_thumbnail(self.video_json)
                self.thumbnail_url = self.settings['thumbnail_url']
                # update campaign settings
                self.db_helper.build_update(
                    tb_name=CountdownCampaigns.TABLE_NAME,
                    set_columns=[CountdownCampaigns.SETTINGS],
                    where=[CountdownCampaigns.CAMPAIGN_ID],
                    values=[json.dumps(self.settings), self.campaign_id]
                )


    def create_ad(self):
        ad_id = video.get(CountdownAds.AD_ID)
        if not ad_id:
            hour = video.get(CountdownAds.HOUR)
            time = 'TIME_{num:02d}:00:00'.format(num=hour)
            self.settings['website_url'] = _add_cd_param_to_url(
                self.settings.get('website_url'),
                video.get(CountdownAds.HASH)
            )
            self.settings['hour'] = hour
            # Create Ad Creative
            ad_gen.create_ad_creative(
                video_id=video.get(CountdownAds.FACEBOOK_VIDEO_ID),
                thumbnail_url=video.get(CountdownAds.THUMBNAIL_URL),
                name=campaign_name + ' Ad Creative {}'.format(time),
                settings=self.settings
            )
            # Create Ad
            ad_id = ad_gen.create_ad(ad_set_id=adset_id, name=campaign_name + ' Ad {}'.format(time))
            # Get ad creative video id
            video_id = Ad(fbid=ad_id).remote_read(fields=['adcreatives{video_id}']) \
                .get('adcreatives', {}).get('data', [{}])[0].get('video_id')
            # Update row in database with ad_id
            self.db_helper.build_update(
                tb_name=CountdownAds.TABLE_NAME,
                set_columns=[CountdownAds.AD_ID, CountdownAds.CREATIVE_VIDEO_ID],
                where=[CountdownAds.ID],
                values=[ad_id, video_id, video.get(CountdownAds.ID)]
            )
            video[CountdownAds.CREATIVE_VIDEO_ID] = video_id
        else:
            video_id = Ad(fbid=ad_id).remote_read(fields=['adcreatives{video_id}']) \
                .get('adcreatives', {}).get('data', [{}])[0].get('video_id')
            self.db_helper.build_update(
                tb_name=CountdownAds.TABLE_NAME,
                set_columns=[CountdownAds.CREATIVE_VIDEO_ID],
                where=[CountdownAds.ID],
                values=[video_id, video.get(CountdownAds.ID)]
            )
            video[CountdownAds.CREATIVE_VIDEO_ID] = video_id
