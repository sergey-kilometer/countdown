import traceback

from helpers import notify
from helpers.utils import update_campaign_status


def exception_handler(error_status):
    """
    Class decorator sends exception to slack and updates status
    :param error_status: Error status to update
    :type error_status: str

    :return: Wrapped function.
    :rtype: callable
    """
    def wrapper(func):
        def wrapped(self, *args, **kwargs):
            try:
                return func(self, *args, **kwargs)
            except Exception:
                status = 'campaign_id: {} {}'.format(self.campaign_id, error_status)
                notify.write_to_slack(self.username, status, str(traceback.format_exc()))
                update_campaign_status(self.db_helper, self.campaign_id, error_status)
        return wrapped
    return wrapper
