#!/opt/kilometer/shopify-marko/venv/bin/python3.4
import json
from facebookads.adobjects.targeting import Targeting
from facebookads.adobjects.adcreativelinkdatacalltoactionvalue import AdCreativeLinkDataCallToActionValue
from facebookads.adobjects.adcreativelinkdatacalltoaction import AdCreativeLinkDataCallToAction
from facebookads.adobjects.adcreativevideodata import AdCreativeVideoData
from facebookads.adobjects.adcreativeobjectstoryspec import AdCreativeObjectStorySpec
from facebookads.objects import (Campaign, AdSet, Ad, AdCreative, CustomAudience, AdAccount, AdsPixel)


class DynamicAdGenerator:
    name_template = 'TOPVID Countdown'

    def __init__(self, ad_account, page_id=None):
        self.ad_account = ad_account
        self.page_id = page_id
        self.ad_set_id = None
        self.campaign_id = None
        self.lead_gen_form_id = None
        self.pixel_id = None

    def set_pixel_id(self, pixel_id):
        """
        Pixel setter
        """
        if pixel_id:
            self.pixel_id = pixel_id
        else:
            pixels = AdAccount(self.ad_account).get_ads_pixels(fields=[AdsPixel.Field.owner_ad_account])
            for pixel in pixels:
                if self.ad_account == pixel.get(AdsPixel.Field.owner_ad_account, {}).get('id'):
                    self.pixel_id = pixel.get(AdsPixel.Field.id)
                    break

    def create_campaign(self, campaign_name, objective=Campaign.Objective.link_clicks):
        campaign = Campaign(parent_id=self.ad_account)
        campaign.update({
            Campaign.Field.name: campaign_name,
            Campaign.Field.can_use_spend_cap: True,
            Campaign.Field.objective: objective,
        })
        campaign.remote_create(params={campaign.Field.status: Campaign.Status.active,})
        return campaign.get(campaign.Field.id)

    def create_ad_set(self, budget, name, campaign_id, custom_audience=None, status=AdSet.Status.active):
        adset = AdSet(parent_id=self.ad_account)
        adset[AdSet.Field.name] = name
        adset[AdSet.Field.is_autobid] = True
        adset[AdSet.Field.billing_event] = AdSet.BillingEvent.impressions
        adset[AdSet.Field.optimization_goal] = AdSet.OptimizationGoal.reach
        adset[AdSet.Field.daily_budget] = budget
        adset[AdSet.Field.campaign_id] = campaign_id
        adset[AdSet.Field.status] = status
        adset[AdSet.Field.promoted_object] = {
            'page_id': self.page_id,
        }
        adset[AdSet.Field.targeting] = {
            Targeting.Field.publisher_platforms: ['facebook'],
            Targeting.Field.custom_audiences: [{'id': custom_audience}]
        }

        adset.remote_create()
        return adset.get(AdSet.Field.id)

    def create_ad(self, video_id, thumbnail_url, name, settings, ad_set_id):
        # create video data
        video_data = AdCreativeVideoData()
        video_data.update({
            AdCreativeVideoData.Field.image_url: thumbnail_url,
            AdCreativeVideoData.Field.video_id: video_id,
            AdCreativeVideoData.Field.title: settings.get('ad_headline', ''),
            'message': settings.get('ad_message', '').format(int(settings.get('hour', '')) + 1),
            'link_description': settings.get('ad_link_desc', ''),
            AdCreativeVideoData.Field.call_to_action: {
                AdCreativeLinkDataCallToAction.Field.type: settings.get('call_to_action', 'NO_BUTTON'),
                AdCreativeLinkDataCallToAction.Field.value: {
                    AdCreativeLinkDataCallToActionValue.Field.link: settings.get('website_url', ''),
                    AdCreativeLinkDataCallToActionValue.Field.link_caption: settings.get('ad_link_display', '')
                }
            }
        })
        # create object story specs
        object_story_spec = AdCreativeObjectStorySpec()
        object_story_spec.update({
            AdCreativeObjectStorySpec.Field.page_id: self.page_id,
            AdCreativeObjectStorySpec.Field.video_data: video_data
        })
        # create ad creative
        creative = AdCreative(parent_id=self.ad_account)
        creative.update({
            AdCreative.Field.name: name,
            AdCreative.Field.object_story_spec: object_story_spec,
            AdCreative.Field.object_type: AdCreative.ObjectType.video,
            AdCreative.Field.video_id: video_id,
            AdCreative.Field.title: settings.get('ad_headline', '')
        })
        creative.remote_create()
        # create ad
        ad = Ad(parent_id=self.ad_account)
        ad.update({
            Ad.Field.name: name.replace('Ad Creative', 'Ad'),
            Ad.Field.adset_id: ad_set_id,
            Ad.Field.creative: {'creative_id': creative.get(creative.Field.id)}
        })
        if self.pixel_id:
            ad[Ad.Field.tracking_specs] = [{'action.type': ['offsite_conversion'], 'fb_pixel': [self.pixel_id]}]
        ad.remote_create(params={Ad.Field.status: Ad.Status.paused})
        return ad.get(Ad.Field.id)

    def create_custom_audience(self, campaign_name, rule, retention_days=2):
        custom_audience = AdAccount(fbid=self.ad_account).create_custom_audience(params={
            CustomAudience.Fields.subtype: CustomAudience.Subtype.engagement,
            CustomAudience.Fields.name: campaign_name + ' audience 3 seconds video view day',
            CustomAudience.Fields.retention_days: retention_days,
            CustomAudience.Fields.rule: json.dumps(rule),
        })
        return custom_audience.get(CustomAudience.Fields.id)
