import boto3
import uuid
import requests
from time import sleep
from contextlib import suppress
from datetime import datetime
from selenium import webdriver
import urllib.parse as urlparse
from urllib.parse import urlencode
from selenium.webdriver.chrome.options import Options
from facebookads.objects import (Ad, AdCreative)

from constants import aws
from constants.data_base import CountdownCampaigns, CountdownAds, CountdownRetargetingUser, VideoMakerUser


def update_campaign_status(db_helper, campaign_id, status):
    db_helper.build_update(
        tb_name=CountdownCampaigns.TABLE_NAME,
        set_columns=[CountdownCampaigns.STATUS],
        where=[CountdownCampaigns.CAMPAIGN_ID],
        values=[status, campaign_id]
    )


def _add_cd_param_to_url(url, cd_hash):
    params = {'cd': cd_hash}
    url_parts = list(urlparse.urlparse(url))
    query = dict(urlparse.parse_qsl(url_parts[4]))
    query.update(params)
    url_parts[4] = urlencode(query)
    return urlparse.urlunparse(url_parts)


def get_user(db_helper, retargeting_id, username):
    if retargeting_id:
        users = db_helper.build_select(
            tb_name=CountdownRetargetingUser.TABLE_NAME,
            select_value=[
                CountdownRetargetingUser.EMAIL, CountdownRetargetingUser.NAME,
                CountdownRetargetingUser.TOKEN, CountdownRetargetingUser.SHOPIFY_DOMAIN
            ],
            where=[CountdownRetargetingUser.ID],
            values=[retargeting_id]
        )
    else:
        users = db_helper.build_select(
            tb_name=VideoMakerUser.TABLE_NAME,
            select_value=[
                VideoMakerUser.EMAIL, VideoMakerUser.NAME,
                VideoMakerUser.TOKEN, VideoMakerUser.SHOPIFY_DOMAIN
            ],
            where=[VideoMakerUser.USERNAME],
            values=[username]
        )
    return next(iter(users or []), None)


def create_thumbnail(video_json):
    """
    Create thumbnail for countdown and uploads to s3
    """
    res = requests.post('https://video-maker.topvid.com/countdown/thumbnail/', json=video_json)

    if res.status_code == 200:
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--start-fullscreen')
        chrome_options.add_argument('--window-size=1080,1080')
        chrome_options.binary_location = '/usr/bin/google-chrome-stable'
        # take browser screen shot
        browser = webdriver.Chrome(executable_path=aws.CHROME_DRIVER_PATH, chrome_options=chrome_options)
        browser.get('data:text/html;charset=utf-8,' + res.text)
        sleep(6)
        browser.execute_script('{timelinemax_id}.pause({time})'.format(timelinemax_id='globals.timeline', time=10))
        # Create a custom thumbnail
        if video_json.get('id') == 18 \
                and video_json.get('bg', {}).get('type') in ('image', 'video'):
            for element_id in ['middle_upper_text_block_ZeroAlpha_', 'coupon_clock_block', 'right_up_text_block']:
                with suppress(Exception):
                    browser.find_element_by_id(element_id).clear()
        sleep(5)
        screen_shot = browser.get_screenshot_as_png()
        browser.quit()
        # upload to s3
        s3_resource = boto3.resource(
            's3',
            aws_access_key_id=aws.ACCESS_KEY_ID,
            aws_secret_access_key=aws.SECRET_ACCESS_KEY
        )
        now = datetime.now()
        server_file_path = 'countdown-thumbnails/{year}-{month:02d}/{hash}.png'.format(
            year=now.year,
            month=now.month,
            hash=uuid.uuid4().hex
        )
        s3_resource.Bucket(aws.S3_USER_MUSIC_BUCKET).put_object(Key=server_file_path, Body=screen_shot)
        return aws.BASE_PATH + aws.S3_USER_MUSIC_BUCKET + '/' + server_file_path
    else:
        raise Exception('thumbnail create error: ' + res.status_code)


def create_ad(ad_gen, db_helper, video, campaign_name, adset_id, settings):
    ad_id = video.get(CountdownAds.AD_ID)
    if not ad_id:
        hour = video.get(CountdownAds.HOUR)
        time = 'TIME_{num:02d}:00:00'.format(num=hour)
        settings['website_url'] = _add_cd_param_to_url(
            settings.get('website_url'),
            video.get(CountdownAds.HASH)
        )
        settings['hour'] = hour
        # Create Ad Creative
        ad_id = ad_gen.create_ad(
            video_id=video.get(CountdownAds.FACEBOOK_VIDEO_ID),
            thumbnail_url=video.get(CountdownAds.THUMBNAIL_URL),
            name=campaign_name + ' Ad Creative {}'.format(time),
            settings=settings,
            ad_set_id=adset_id
        )
        # Get ad creative video id
        video_id = Ad(fbid=ad_id).remote_read(fields=['adcreatives{video_id}']) \
            .get('adcreatives', {}).get('data', [{}])[0].get('video_id')
        # Update row in database with ad_id
        db_helper.build_update(
            tb_name=CountdownAds.TABLE_NAME,
            set_columns=[CountdownAds.AD_ID, CountdownAds.CREATIVE_VIDEO_ID],
            where=[CountdownAds.ID],
            values=[ad_id, video_id, video.get(CountdownAds.ID)]
        )
        video[CountdownAds.CREATIVE_VIDEO_ID] = video_id
    else:
        video_id = Ad(fbid=ad_id).remote_read(fields=['adcreatives{video_id}'])\
            .get('adcreatives', {}).get('data', [{}])[0].get('video_id')
        db_helper.build_update(
            tb_name=CountdownAds.TABLE_NAME,
            set_columns=[CountdownAds.CREATIVE_VIDEO_ID],
            where=[CountdownAds.ID],
            values=[video_id, video.get(CountdownAds.ID)]
        )
        video[CountdownAds.CREATIVE_VIDEO_ID] = video_id
