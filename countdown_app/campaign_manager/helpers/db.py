#!/opt/kilometer/shopify-marko/venv/bin/python3.4

import psycopg2


class DBHelper:
    NAME = 'video_maker',
    USER = 'usr_enrich',
    PASSWORD = '&WF5bOZlCqL5',
    HOST = 'prod-kilometer-enrichmentservicedb.ckc1qsvzxlnk.us-east-1.rds.amazonaws.com',
    PORT = '5432',

    def __init__(self):
        self.db_connection = psycopg2.connect(
            host=self.HOST[0], port=self.PORT[0], database=self.NAME[0],
            user=self.USER[0], password=self.PASSWORD[0]
        )
        self.db_cursor = self.db_connection.cursor()

    def __del__(self):
        self.db_cursor.close()
        self.db_connection.close()

    def close(self):
        self.db_cursor.close()
        self.db_connection.close()

    def build_select(self, tb_name, select_value,  where=list(), values=None, limit=None, fetch_type="dict_fetchall",
                     order_by=None, order='DESC', **args):

        query = "SELECT " + (",".join(select_value)) + " FROM " + tb_name
        query = self.build_where(query, where)
        if order_by:
            query += " ORDER BY " + order_by + " " + order
        if limit:
            query += " LIMIT " + str(limit)
        return self.execute(query, values, fetch_type, **args)

    def build_insert(self, tb_name, column_names, values=None, fetch_type=None, **args):
        query = "INSERT INTO " + tb_name + " "
        if len(column_names) > 0:
            if len(column_names) == 1:
                query += " (" + ''.join(column_names) + ") VALUES(%s) "
            else:
                query += " (" + (",".join(column_names)) + ") "
                vals = ['%s' for _ in range(len(column_names))]
                query += " VALUES " + " (" + (",".join(vals)) + ") "
        else:
            vals = ['%s' for _ in range(len(values))]
            query += " VALUES " + " (" + (",".join(vals)) + ")"

        query += ' RETURNING id'

        return self.execute(query, values, fetch_type, **args)

    def build_update(self, tb_name, set_columns, where, values=None, fetch_type="rowcount", **args):
        query = "UPDATE " + tb_name + " "
        if len(set_columns) > 0:
            if len(set_columns) == 1:
                query += " SET " + ''.join(set_columns) + "=%s "
            else:
                set_cols = " SET "
                for val in set_columns:
                    set_cols += val + "=%s, "
                query += set_cols[:-2]
            query = self.build_where(query, where)
        else:
            return None
        return self.execute(query, values, fetch_type, **args)

    def build_delete(self, tb_name, where_values, values=None, return_type=None, **args):
        query = "DELETE FROM " + tb_name + " "
        if len(where_values) > 0:
            query += " WHERE "
            if len(where_values) == 1:
                query += ''.join(where_values) + "=%s "
            else:
                where = ""
                for val in where_values:
                    where += val + "=%s AND "
                query += where[:-4]

        return self.execute(query, values, return_type, **args)

    @staticmethod
    def build_where(query, where_values):
        if len(where_values) > 0:
            query += " WHERE "
            if len(where_values) == 1:
                query += ''.join(where_values) + "=%s "
            else:
                where = ""
                for val in where_values:
                    where += val + "=%s AND "
                query += where[:-4]
        return query

    def execute(self, query, values, fetch_type, **args):
        self.db_cursor.execute(query, values)
        self.db_connection.commit()
        return globals()[fetch_type](self.db_cursor) if fetch_type else True


def fetchone(cursor):
    return cursor.fetchone()


def fetchall(cursor):
    return cursor.fetchall()


def rowcount(cursor):
    return cursor.rowcount


def dict_fetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
