import pytz
import math
import json
import shopify
from datetime import datetime
from dateutil.relativedelta import relativedelta
from facebookads.api import FacebookAdsApi
from facebookads.objects import (Ad, Campaign)

from constants.status import Status
from constants.facebook import API_VERSION
from constants.data_base import CountdownCampaigns, CountdownAds


class Scheduler:

    def __init__(self, campaign_id, facebook_token, db_helper, targeting_adsets, retargeting_adsets, schedule_type='continuous'):
        FacebookAdsApi.init(access_token=facebook_token, api_version=API_VERSION)
        self.campaign_id = campaign_id
        self.db_helper = db_helper
        self.targeting_adsets = targeting_adsets
        self.retargeting_adsets = retargeting_adsets
        self.targeting_diff_hours = None
        self.schedule_type = schedule_type

    def start_limited_stock(self, targeting_start_time, video_json, user=None):
        datetime_now = datetime.now()
        if not targeting_start_time:
            self._start_next_add(-1, order='ASC')
            self._start_next_add(-1, 'retargeting', order='ASC')
        else:
            diff_hours = relativedelta(datetime_now, targeting_start_time).hours
            if diff_hours >= 6 or video_json.get('time', {}).get('product'):
                time = video_json.get('time', {})
                total = int(time.get('total', 100))
                sold = int(time.get('sold', 1))
                left = total - sold
                product = time.get('product', {})
                if product:
                    myshopify_domain = user.get('myshopify_domain')
                    shopify_token = user.get('token')
                    with shopify.Session.temp(myshopify_domain, shopify_token):
                        variant = shopify.Variant.find(product.get('variant_id'))
                    new_quantity = int(variant.inventory_quantity)
                    if 0 < new_quantity < total and new_quantity != left:
                        new_sold = total - new_quantity
                        # check if add already exist
                        ads = self.db_helper.build_select(
                            tb_name=CountdownAds.TABLE_NAME,
                            select_value=[CountdownAds.HOUR, CountdownAds.AD_ID, CountdownAds.AD_TYPE],
                            where=[CountdownAds.HOUR],
                            values=[new_sold]
                        )
                        if ads:
                            if ads[0].get('status') != Ad.Status.active:
                                self._stop_add()
                                for ad in ads:
                                    ad_id = ad.get(CountdownAds.AD_ID)
                                    Ad(fbid=ad_id).remote_update(params={Ad.Field.status: Ad.Status.active})
                                    self.db_helper.build_update(
                                        tb_name=CountdownAds.TABLE_NAME,
                                        set_columns=[CountdownAds.STATUS],
                                        where=[CountdownAds.AD_ID],
                                        values=[Ad.Status.active, ad_id]
                                    )
                        else:
                            video_json['time']['sold'] = new_sold
                            video_json['time']['start'] = total - new_sold
                            self.db_helper.build_update(
                                tb_name=CountdownCampaigns.TABLE_NAME,
                                set_columns=[CountdownCampaigns.STATUS, CountdownCampaigns.VIDEO_JSON],
                                where=[CountdownCampaigns.CAMPAIGN_ID],
                                values=[Status.VIDEOS_CREATE, json.dumps(video_json), self.campaign_id]
                            )
                    else:
                        self._stop_add()
                        self._start_next_add(sold)
                        self._start_next_add(sold, 'retargeting')
                else:
                    self._stop_add()
                    next_hour = sold + int(math.ceil((left - 1) / (int(time.get('totalDuration', 7)) * 24 / 6)))
                    if next_hour < total:
                        self._start_next_add(next_hour)
                        self._start_next_add(next_hour, 'retargeting')

    def start_days(self, targeting_start_time, time_zone):
        if not targeting_start_time:
            self._start_next_add(-1)
            self._start_next_add(-1, 'retargeting')
        else:
            time_zone = pytz.timezone(time_zone)
            localized_start_time = time_zone.localize(targeting_start_time)
            targeting_diff = relativedelta(datetime.now(time_zone), localized_start_time)
            if targeting_diff.hours >= 24 or targeting_diff.days >= 1:
                live_add = self._stop_add()
                self._stop_add('retargeting')
                if live_add:
                    next_hour = live_add.get(CountdownAds.HOUR) - 1
                    if next_hour > 0:
                        self._start_next_add(next_hour)
                        self._start_next_add(next_hour, 'retargeting')

    def start_targeting(self, targeting_start_time):
        datetime_now = datetime.now()
        if not targeting_start_time:
            # start first ad order by hour
            self.targeting_diff_hours = relativedelta(datetime_now, datetime_now).hours
            self._start_next_add(-1)
        else:
            # check delta how many hours pass if more then 3 stop previous and start next add
            self.targeting_diff_hours = relativedelta(datetime_now, targeting_start_time).hours
            if self.targeting_diff_hours >= 3:
                live_add = self._stop_add()
                # next add hour => prev ad hour - 3
                if live_add:
                    next_hour = live_add.get(CountdownAds.HOUR) - 3
                    # next add start time < -1 and is one_time_campaign don't start next
                    if next_hour > -1 or next_hour < -1 and not self.schedule_type == 'onetime':
                        self._start_next_add(next_hour)

    def start_retargeting(self, retargeting_start_time):
        # If retargeting started
        datetime_now = datetime.now()
        if retargeting_start_time:
            retargeting_diff_hours = relativedelta(datetime_now, retargeting_start_time).hours
            # check delta how many hours pass if more then 3 stop previous and start next add
            if retargeting_diff_hours >= 3:
                live_add = self._stop_add('retargeting')
                if live_add:
                    next_hour = live_add.get(CountdownAds.HOUR) - 3
                    # next add start time < -1 and is one_time_campaign don't start next
                    if next_hour > -1 or next_hour < -1 and not self.schedule_type == 'onetime':
                        self._start_next_add(next_hour, 'retargeting')
        elif self.targeting_diff_hours >= 1:
            # If targeting start delta >= 1 start first retargeting add
            self._start_next_add(-1, add_type='retargeting')

    def _stop_add(self, add_type='targeting'):
        """
        Stops all ads of specific type (make sures that no 2 ads running from same type)
        """
        if add_type == 'targeting':
            adsets_filter = self.targeting_adsets
        else:
            adsets_filter = self.retargeting_adsets
        # find all active ads in adsets_filter and pause them
        ads = Campaign(fbid=self.campaign_id).get_ads(
            fields=[Ad.Field.status, Ad.Field.id],
            params={
                'filtering': [
                    {'field': 'adset.id', 'operator': 'IN', 'value': [adset for adset in adsets_filter if adset]}
                ],
                'effective_status': [Ad.EffectiveStatus.active]
            },
        )
        # pause them in database and get last active
        ad_id = None
        for ad in ads:
            ad_id = ad.get(Ad.Field.id)
            Ad(fbid=ad_id).remote_update(params={Ad.Field.status: Ad.Status.paused})
            self.db_helper.build_update(
                tb_name=CountdownAds.TABLE_NAME,
                set_columns=[CountdownAds.STATUS],
                where=[CountdownAds.AD_ID],
                values=[Ad.Status.paused, ad_id]
            )
        if ad_id:
            live_adds = self.db_helper.build_select(
                tb_name=CountdownAds.TABLE_NAME,
                select_value=[CountdownAds.HOUR],
                where=[CountdownAds.AD_ID],
                values=[ad_id]
            )
            if live_adds:
                return live_adds[0]
        return {CountdownAds.HOUR: 0}

    def _start_next_add(self, next_hour, add_type='targeting', order='DESC'):
        values = [self.campaign_id, add_type]
        where = [CountdownAds.CAMPAIGN_ID, CountdownAds.AD_TYPE]
        # if next_hour == -1 start the first add
        if next_hour > -1:
            values.append(next_hour)
            where.append(CountdownAds.HOUR)

        next_add = self.db_helper.build_select(
            tb_name=CountdownAds.TABLE_NAME,
            select_value=[CountdownAds.HOUR, CountdownAds.AD_ID, CountdownAds.AD_TYPE],
            where=where,
            values=values,
            order_by=CountdownAds.HOUR,
            order=order,
            limit=1
        )[0]
        # Start add
        next_add_id = next_add.get(CountdownAds.AD_ID)
        facebook_ad = Ad(fbid=next_add_id).remote_read(fields=[Ad.Field.effective_status])
        effective_status = facebook_ad.get(Ad.Field.effective_status)
        if effective_status != Ad.EffectiveStatus.pending_review \
                and effective_status != Ad.EffectiveStatus.active:
            facebook_ad.remote_update(params={Ad.Field.status: Ad.Status.active})
            # Update add status in database
            self.db_helper.build_update(
                tb_name=CountdownAds.TABLE_NAME,
                set_columns=[CountdownAds.STATUS],
                where=[CountdownAds.AD_ID],
                values=[Ad.Status.active, next_add_id]
            )

            # Update start time
            if add_type == 'targeting':
                column_name = CountdownCampaigns.TARGETING_START_TIME
            else:
                column_name = CountdownCampaigns.RETARGETING_START_TIME

            self.db_helper.build_update(
                tb_name=CountdownCampaigns.TABLE_NAME,
                set_columns=[column_name],
                where=[CountdownCampaigns.CAMPAIGN_ID],
                values=[datetime.now(), self.campaign_id]
            )
