from contextlib import suppress
from mailsnake import MailSnake
from intercom.client import Client
from slackclient import SlackClient

from constants.html import HTML
from constants.slack import Slack


def write_to_slack(username, status, description, channel=Slack.ERROR_CHANNEL):
    slack = SlackClient(Slack.CLIENT_SECRET)
    slack.api_call(
        'chat.postMessage',
        channel=channel,
        text=Slack.MESSAGE.format(username=username, status=status, description=description)
    )


def intercom_set_custom_attributes(email, attributes):
    """
    Updates intercom user custom attributes
    """
    with suppress(Exception):
        intercom = Client(personal_access_token='dG9rOjllODY4MTY2XzI1NDZfNDZmMl9hMzE3XzA4MzRiMzIwNzM3YzoxOjA=')
        intercom_user = intercom.users.find(email=email)
        for key, value in attributes.items():
            intercom_user.custom_attributes[key] = value
        intercom.users.save(intercom_user)


def send_mail(email, name, campaign, subject='Your countdown campaign is ready', html=HTML):
    mail_snake = MailSnake('c61bHLwX5O5ZhYJMK8xJoA', api='mandrill')
    if not name:
        name = ''
    mail_snake.messages.send(message={
        'html': html.format(name=name, campaign=campaign),
        'subject': subject,
        'from_email': 'support@topvid.com',
        'from_name': 'Topvid.com',
        'to': [{'email': email, 'name': name}]
    })
