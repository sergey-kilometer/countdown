#!/opt/kilometer/shopify-marko/venv/bin/python3.4
import pytz
import shopify
import traceback
from datetime import datetime
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta

from helpers import notify
from helpers.db import DBHelper
from helpers.utils import update_campaign_status, get_user
from constants.status import Status
from constants.data_base import CountdownCampaigns, CountdownAds, SELECT_ALL, CountdownRetargetingUser
from helpers.scheduler import Scheduler


def _is_valid_campaign(campaign):
    retargeting_id = campaign.get(CountdownCampaigns.RETARGETING_USER_ID)
    if retargeting_id:
        retargeting_users = db_helper.build_select(
            tb_name=CountdownRetargetingUser.TABLE_NAME,
            select_value=[
                CountdownRetargetingUser.TRIAL_START_DATE,
                CountdownRetargetingUser.CHARGE_ID,
                CountdownRetargetingUser.SHOPIFY_DOMAIN,
                CountdownRetargetingUser.TOKEN
            ],
            where=[CountdownRetargetingUser.ID],
            values=[retargeting_id]
        )
        if retargeting_users:
            retargeting_user = retargeting_users[0]
            trial_start_date = retargeting_user.get(CountdownRetargetingUser.TRIAL_START_DATE)

            if trial_start_date:
                days_pass = relativedelta(datetime.now(), trial_start_date).days
                if days_pass > 14:
                    charge_id = retargeting_user.get(CountdownRetargetingUser.CHARGE_ID)
                    if charge_id:
                        myshopify_domain = retargeting_user.get(CountdownRetargetingUser.SHOPIFY_DOMAIN)
                        token = retargeting_user.get(CountdownRetargetingUser.TOKEN)
                        with shopify.Session.temp(myshopify_domain, token):
                            charge = shopify.RecurringApplicationCharge().find_one(
                                '/admin/recurring_application_charges/{}.json'.format(charge_id)
                            )
                        return charge.status == 'active'
                    return False
    return True

if __name__ == '__main__':
    db_helper = DBHelper()
    # Selecting campaign with status 'campaign_created'
    campaigns = db_helper.build_select(
        tb_name=CountdownCampaigns.TABLE_NAME,
        select_value=SELECT_ALL,
        where=[CountdownCampaigns.STATUS],
        values=[Status.CAMPAIGN_CREATED]
    )

    for campaign in campaigns:
        username = campaign.get(CountdownCampaigns.USERNAME)
        campaign_id = campaign.get(CountdownCampaigns.CAMPAIGN_ID)
        video_json = campaign.get(CountdownCampaigns.VIDEO_JSON)
        retargeting_id = campaign.get(CountdownCampaigns.RETARGETING_USER_ID)
        schedule_type = video_json.get('schedule_type', 'continuous')

        try:
            # if _is_valid_campaign(campaign):
            settings = campaign.get(CountdownCampaigns.SETTINGS)
            is_schedule = False

            # check if to schedule ads
            start_time = settings.get('start_time', 'immediately')
            if start_time == 'immediately':
                is_schedule = True
            else:
                time_zone = pytz.timezone(settings.get('time_zone', pytz.utc))
                start_date = time_zone.localize(parse(settings.get('start_datetime')))
                delta_seconds = relativedelta(start_date, datetime.now(time_zone)).seconds
                is_schedule = delta_seconds < 0

            if is_schedule:
                targeting_adsets = [campaign.get(CountdownCampaigns.ADSET_TARGETING)]
                retargeting_adsets = []
                adset_retargeting_day_1 = campaign.get(CountdownCampaigns.ADSET_RETARGETING_DAY_1)
                adset_retargeting_day_2 = campaign.get(CountdownCampaigns.ADSET_RETARGETING_DAY_2)
                adset_retargeting_day_3 = campaign.get(CountdownCampaigns.ADSET_RETARGETING_DAY_3)

                if adset_retargeting_day_1:
                    retargeting_adsets.append(adset_retargeting_day_1)
                if adset_retargeting_day_2:
                    retargeting_adsets.append(adset_retargeting_day_2)
                if adset_retargeting_day_3:
                    retargeting_adsets.append(adset_retargeting_day_3)

                scheduler_obj = Scheduler(
                    campaign_id=campaign_id,
                    facebook_token=campaign.get(CountdownCampaigns.FACEBOOK_TOKEN),
                    db_helper=db_helper,
                    targeting_adsets=targeting_adsets,
                    retargeting_adsets=retargeting_adsets,
                    schedule_type=schedule_type
                )

                if schedule_type == 'days':
                    time_zone = video_json.get('time', {}).get('time_zone', {}).get('value', pytz.utc)
                    scheduler_obj.start_days(campaign.get(CountdownCampaigns.TARGETING_START_TIME), time_zone)
                elif schedule_type == 'limited_stock':
                    user = None
                    if video_json.get('time', {}).get('product'):
                        user = get_user(db_helper, retargeting_id, username)
                    scheduler_obj.start_limited_stock(
                        campaign.get(CountdownCampaigns.TARGETING_START_TIME),
                        video_json,
                        user
                    )
                else:
                    scheduler_obj.start_targeting(campaign.get(CountdownCampaigns.TARGETING_START_TIME))
                    scheduler_obj.start_retargeting(campaign.get(CountdownCampaigns.RETARGETING_START_TIME))

        except Exception:
            error_status = 'campaign_id: {} {}'.format(campaign_id, Status.CAMPAIGN_SCHEDULER_ERROR)
            notify.write_to_slack(username, error_status, str(traceback.format_exc()))
            update_campaign_status(db_helper, campaign_id, Status.CAMPAIGN_SCHEDULER_ERROR)

    db_helper.close()
