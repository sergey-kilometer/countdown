#!/opt/kilometer/shopify-marko/venv/bin/python3.4

import json
from facebookads.api import FacebookAdsApi

from helpers import notify
from helpers.db import DBHelper
from helpers.dynamic_ad_generator import DynamicAdGenerator
from helpers.utils import update_campaign_status, create_thumbnail, create_ad, get_user
from constants.slack import Slack
from constants.status import Status
from constants.facebook import API_VERSION
from constants.data_base import CountdownCampaigns, CountdownAds, SELECT_ALL, CountdownRetargetingUser, VideoMakerUser


def _create_retargeting_adset(column_name, adset_retargeting_id, retargeting_videos, rule, settings):
    # If retargeting_adset_id create custom audience and retargeting AdSet
    if not adset_retargeting_id or adset_retargeting_id == '':
        # Create custom audience
        custom_audience_id = ad_gen.create_custom_audience(campaign_name, rule)
        # Create retargeting adset
        adset_retargeting_id = ad_gen.create_ad_set(
            budget=adset_budget,
            name=campaign_name + ' {}'.format(column_name),
            campaign_id=campaign_id,
            custom_audience=custom_audience_id
        )
        # Save retargeting_adset_id
        db_helper.build_update(
            tb_name=CountdownCampaigns.TABLE_NAME,
            set_columns=[column_name],
            where=[CountdownCampaigns.CAMPAIGN_ID],
            values=[adset_retargeting_id, campaign_id]
        )
    # Create retargeting Ads
    for ca_video in retargeting_videos:
        create_ad(ad_gen=ad_gen, db_helper=db_helper, video=ca_video, campaign_name=campaign_name,
                  adset_id=adset_retargeting_id, settings=settings)


if __name__ == '__main__':
    db_helper = DBHelper()
    # Selecting campaign with status 'videos_create'
    campaigns = db_helper.build_select(
        tb_name=CountdownCampaigns.TABLE_NAME,
        select_value=SELECT_ALL,
        where=[CountdownCampaigns.STATUS],
        values=[Status.VIDEOS_CREATED],
    )

    for campaign in campaigns:
        campaign_id = campaign.get(CountdownCampaigns.CAMPAIGN_ID)
        username = campaign.get(CountdownCampaigns.USERNAME)

        try:
            # select all ads for specific campaign
            ad_videos = db_helper.build_select(
                tb_name=CountdownAds.TABLE_NAME,
                select_value=SELECT_ALL,
                where=[CountdownAds.CAMPAIGN_ID],
                values=[campaign_id],
                order_by=CountdownAds.HOUR
            )

            videos_length = len(ad_videos)
            ready_videos_length = sum(video.get('status') == 'done' for video in ad_videos)
            #  if all videos ready
            if videos_length == ready_videos_length:
                video_json = campaign.get(CountdownCampaigns.VIDEO_JSON)
                settings = campaign.get(CountdownCampaigns.SETTINGS)
                schedule_type = video_json.get('schedule_type')
                duration = video_json.get('time', {}).get('totalDuration', 72)
                time_start = video_json.get('time', {}).get('start')
                # update camapign status
                update_campaign_status(db_helper, campaign_id, Status.CAMPAIGN_CREATING)
                # extracting variables
                facebook_token = campaign.get(CountdownCampaigns.FACEBOOK_TOKEN)
                targeting_adset_id = campaign.get(CountdownCampaigns.ADSET_TARGETING)
                adset_retargeting_day_1 = campaign.get(CountdownCampaigns.ADSET_RETARGETING_DAY_1)
                adset_retargeting_day_2 = campaign.get(CountdownCampaigns.ADSET_RETARGETING_DAY_2)
                adset_retargeting_day_3 = campaign.get(CountdownCampaigns.ADSET_RETARGETING_DAY_3)
                retargeting_id = campaign.get(CountdownCampaigns.RETARGETING_USER_ID)
                ad_account = settings.get('ad_account')
                campaign_name = settings.get('campaign_name')
                page_id = settings.get('page_id')
                thumbnail_url = video_json.get('thumbnail_url', settings.get('thumbnail_url'))
                # retargeting adset budget is 30% of all budget
                adset_budget = int(settings.get('adset_budget')) * 30
                # Initializing facebook api and DynamicAdGenerator
                FacebookAdsApi.init(access_token=facebook_token, api_version=API_VERSION)
                ad_gen = DynamicAdGenerator(ad_account=ad_account, page_id=page_id)
                # get default pixel if not set
                ad_gen.set_pixel_id(settings.get('pixel_id'))

                if not thumbnail_url:
                    # Create thumbnail without texts and save in settings
                    settings['thumbnail_url'] = create_thumbnail(video_json)
                    thumbnail_url = settings['thumbnail_url']
                    # update campaign settings
                    db_helper.build_update(
                        tb_name=CountdownCampaigns.TABLE_NAME,
                        set_columns=[CountdownCampaigns.SETTINGS],
                        where=[CountdownCampaigns.CAMPAIGN_ID],
                        values=[json.dumps(settings), campaign_id]
                    )

                for ad_video in ad_videos:
                    # Add thumbnail to all ads
                    ad_video[CountdownAds.THUMBNAIL_URL] = thumbnail_url
                    if ad_video.get(CountdownAds.AD_TYPE) == 'targeting':
                        create_ad(ad_gen=ad_gen, db_helper=db_helper, video=ad_video, campaign_name=campaign_name,
                                  adset_id=targeting_adset_id, settings=settings)

                if schedule_type == 'days' or schedule_type == 'limited_stock':
                    rule = []
                    retargeting_videos = []
                    for ad_video in ad_videos:
                        if ad_video.get(CountdownAds.AD_TYPE) == 'targeting':
                            rule.append({
                                'event_name': 'video_view_25_percent',
                                'object_id': ad_video.get(CountdownAds.CREATIVE_VIDEO_ID)
                            })
                        else:
                            retargeting_videos.append(ad_video)
                    _create_retargeting_adset(
                        column_name=CountdownCampaigns.ADSET_RETARGETING_DAY_1,
                        adset_retargeting_id=adset_retargeting_day_1,
                        retargeting_videos=retargeting_videos,
                        rule=rule,
                        settings=settings
                    )
                else:
                    if duration >= 72:
                        # Create retargeting adset for first day
                        rule = []
                        retargeting_videos = []
                        for ad_video in ad_videos:
                            hour = ad_video.get(CountdownAds.HOUR)
                            if hour >= 48:
                                ad_type = ad_video.get(CountdownAds.AD_TYPE)
                                if ad_type == 'targeting':
                                    rule.append({
                                        'event_name': 'video_view_25_percent',
                                        'object_id': ad_video.get(CountdownAds.CREATIVE_VIDEO_ID)
                                    })
                                else:
                                    retargeting_videos.append(ad_video)

                        _create_retargeting_adset(
                            column_name=CountdownCampaigns.ADSET_RETARGETING_DAY_1,
                            adset_retargeting_id=adset_retargeting_day_1,
                            retargeting_videos=retargeting_videos,
                            rule=rule,
                            settings=settings
                        )

                    if duration >= 48:
                        # Create retargeting adset for second day
                        rule = []
                        retargeting_videos = []
                        for ad_video in ad_videos:
                            hour = ad_video.get(CountdownAds.HOUR)
                            if hour >= 24:
                                ad_type = ad_video.get(CountdownAds.AD_TYPE)
                                if ad_type == 'targeting':
                                    rule.append({
                                        'event_name': 'video_view_25_percent',
                                        'object_id': ad_video.get(CountdownAds.CREATIVE_VIDEO_ID)
                                    })
                                elif hour < 48:
                                    retargeting_videos.append(ad_video)

                        _create_retargeting_adset(
                            column_name=CountdownCampaigns.ADSET_RETARGETING_DAY_2,
                            adset_retargeting_id=adset_retargeting_day_2,
                            retargeting_videos=retargeting_videos,
                            rule=rule,
                            settings=settings
                        )

                    # Create retargeting adset for third day
                    rule = []
                    retargeting_videos = []
                    for ad_video in ad_videos:
                        if ad_video.get(CountdownAds.CREATIVE_VIDEO_ID):
                            rule.append({
                                'event_name': 'video_view_25_percent',
                                'object_id': ad_video.get(CountdownAds.CREATIVE_VIDEO_ID)
                            })
                        if ad_video.get(CountdownAds.HOUR) < 24 \
                                and ad_video.get(CountdownAds.AD_TYPE) == 'retargeting':
                            retargeting_videos.append(ad_video)

                    _create_retargeting_adset(
                        column_name=CountdownCampaigns.ADSET_RETARGETING_DAY_3,
                        adset_retargeting_id=adset_retargeting_day_3,
                        retargeting_videos=retargeting_videos,
                        rule=rule,
                        settings=settings
                    )

                update_campaign_status(db_helper, campaign_id, Status.CAMPAIGN_CREATED)
                user = get_user(db_helper, retargeting_id, username)

                if user:
                    if retargeting_id:
                        query = "SELECT campaign_id FROM countdown_app_campaigns WHERE retargeting_id = '{}' " \
                                "AND status IN ('campaign_created', 'campaign_scheduler_error')".format(retargeting_id)
                    else:
                        query = "SELECT campaign_id FROM countdown_app_campaigns WHERE username = '{}' " \
                                "AND status IN ('campaign_created', 'campaign_scheduler_error')".format(username)
                    campaigns_count = db_helper.execute(query, None, 'rowcount')
                    campaign = 'https://business.facebook.com/ads/manager/account/adsets/?act={act}&' \
                               'selected_campaign_ids={camp_id}' \
                        .format(
                            act=ad_account.replace('act_', ''),
                            camp_id=campaign_id
                        )
                    email = user.get('email', '')
                    notify.send_mail(email, user.get('name'), campaign)
                    camp_status = 'campaign_id: {} {}'.format(campaign_id, Status.CAMPAIGN_CREATED)
                    notify.write_to_slack(username, camp_status, email, channel=Slack.SUCCESS_CHANNEL)
                    notify.intercom_set_custom_attributes(email, {'COUNTDOWN_APP_CAMPAIGNS_COUNT': campaigns_count})

        except Exception as e:
            error_status = 'campaign_id: {} {}'.format(campaign_id, Status.ADS_CREATE_ERROR)
            notify.write_to_slack(username, error_status, str(e))
            update_campaign_status(db_helper, campaign_id, Status.ADS_CREATE_ERROR)

    db_helper.close()
