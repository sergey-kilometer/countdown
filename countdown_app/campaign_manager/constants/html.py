HTML = '<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="viewport" content="width=device-width"></head><body style="margin: 0; padding: 0;"><table style="font-family: \'Open Sans\', Arial, Helvetica, sans-serif; width: 98%; font-size: 16px; color: #20160f" bgcolor="#f8f8f8" align="center" cellpadding="0" cellspacing="0"><tr><td style="padding: 20px 25px 0 25px;"><table align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse;width: 600px; "><tr><td align="left" style="border-radius: 5px;background-color: white; padding: 15px">Hello, {name}<br><br>We just wanted to let you know that your countdown campaign is ready.<br>You can view the campaign in (<a href="{campaign}" title="Campaign">Facebook Ads Manager </a>).<br>If you any questions, please don\'t hesitate to reply to this email.<br><br>Best regard, The TopVid team<br><br><br><br><br></td></tr></table></td></tr><tr><td><table align="center" cellpadding="0" cellspacing="0" height="40" style="border-collapse: collapse;"><tr><td></td></tr></table></td></tr></table></body></html>'

HTML_BEFORE_EXPIRE = '<!DOCTYPE html>' \
        '<html xmlns="http://www.w3.org/1999/xhtml">' \
        '<head>' \
        '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' \
        '<meta name="viewport" content="width=device-width">' \
        '</head>' \
        '<body style="margin: 0; padding: 0;">' \
        '<table style="font-family: \'Open Sans\', Arial, Helvetica, sans-serif; width: 98%; font-size: 16px; color: #20160f" bgcolor="#f8f8f8" align="center" cellpadding="0" cellspacing="0">' \
        '<tr>' \
        '<td style="padding: 20px 25px 0 25px;">' \
        '<table align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse;width: 600px; ">' \
        '<tr>' \
        '<td align="left" style="border-radius: 5px;background-color: white; padding: 15px">' \
        'Hello, {name}<br><br>Your trial <a href="https://apps.shopify.com/facebook-carousel-retargeting" title="Campaign">' \
                     'Facebook Countdown Retargeting</a> is going to expire tomorrow.' \
        '<br>Please subscribe to a paid plan in order to keep your countdown campaign active.<br><br>' \
        '<a href="{campaign}" title="Campaign" style="display: inline-block;padding: 6px 12px;margin-bottom: 0;' \
                     'font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;' \
                     'white-space: nowrap;vertical-align: middle;-ms-touch-action: manipulation;' \
                     'touch-action: manipulation;cursor: pointer;-webkit-user-select: none;' \
                     '-moz-user-select: none;-ms-user-select: none;user-select: none;background-image: none;' \
                     'border: 1px solid black;border-radius: 4px;-webkit-appearance: button;' \
                     '-moz-appearance: button;appearance: button;text-decoration: none;">Upgrade now </a><br><br>'\
        'If you any questions, please don\'t hesitate to reply to this email.<br>' \
        '<br>Best regard, The TopVid team<br><br><br><br><br></td></tr>' \
        '</table></td></tr><tr><td>' \
        '<table align="center" cellpadding="0" cellspacing="0" height="40" style="border-collapse: collapse;"><tr>' \
                     '<td></td></tr></table></td></tr></table></body></html>'

HTML_AFTER_EXPIRE = '<!DOCTYPE html>' \
        '<html xmlns="http://www.w3.org/1999/xhtml">' \
        '<head>' \
        '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' \
        '<meta name="viewport" content="width=device-width">' \
        '</head>' \
        '<body style="margin: 0; padding: 0;">' \
        '<table style="font-family: \'Open Sans\', Arial, Helvetica, sans-serif; width: 98%; font-size: 16px; color: #20160f" bgcolor="#f8f8f8" align="center" cellpadding="0" cellspacing="0">' \
        '<tr>' \
        '<td style="padding: 20px 25px 0 25px;">' \
        '<table align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse;width: 600px; ">' \
        '<tr>' \
        '<td align="left" style="border-radius: 5px;background-color: white; padding: 15px">' \
        'Hello, {name}<br><br>Your trial of <a href="https://apps.shopify.com/facebook-carousel-retargeting" title="Campaign">' \
                     'Facebook Countdown Retargeting</a> has expired.' \
        '<br>Please subscribe to a paid plan in order to keep your countdown campaign active.<br><br>' \
        '<a href="{campaign}" title="Campaign" style="display: inline-block;padding: 6px 12px;margin-bottom: ' \
                    '0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;' \
                    'white-space: nowrap;vertical-align: middle;-ms-touch-action: manipulation;' \
                    'touch-action: manipulation;cursor: pointer;-webkit-user-select: none;' \
                    '-moz-user-select: none;-ms-user-select: none;user-select: none;background-image: none;' \
                    'border: 1px solid black;border-radius: 4px;-webkit-appearance: button;' \
                    '-moz-appearance: button;appearance: button;text-decoration: none;">Upgrade now </a><br><br>'\
        'If you any questions, please don\'t hesitate to reply to this email.<br>' \
        '<br>Best regard, The TopVid team<br><br><br><br><br></td></tr>' \
        '</table></td></tr><tr><td>' \
        '<table align="center" cellpadding="0" cellspacing="0" height="40" style="border-collapse: collapse;">' \
                    '<tr><td></td></tr></table></td></tr></table></body></html>'