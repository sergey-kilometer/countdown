class CountdownAds:
    TABLE_NAME = 'countdown_app_ads'
    CAMPAIGN_ID = 'campaign_id'
    VIDEO_MAKER_ID = 'video_maker_id'
    FACEBOOK_VIDEO_ID = 'facebook_video_id'
    AD_ID = 'ad_id'
    STATUS = 'status'
    AD_TYPE = 'ad_type'
    HOUR = 'hour'
    ID = 'id'
    TIME = 'time'
    HASH = 'hash'
    CREATIVE_VIDEO_ID = 'creative_video_id'
    THUMBNAIL_URL = 'thumbnail_url'


class VideoMakerUser:
    TABLE_NAME = 'video_maker_app_videomakeruser'
    EMAIL = 'email'
    NAME = 'name'
    USERNAME = 'username'
    TOKEN = 'token'
    SHOPIFY_DOMAIN = 'myshopify_domain'


class CountdownRetargetingUser:
    TABLE_NAME = 'countdown_retargeting_user'
    EMAIL = 'email'
    ID = 'id'
    CHARGE_ID = 'charge_id'
    TRIAL_START_DATE = 'trial_start_date'
    NAME = 'name'
    TOKEN = 'token'
    SHOPIFY_DOMAIN = 'myshopify_domain'


class CountdownCampaigns:
    RETARGETING_USER_ID = 'retargeting_id'
    TABLE_NAME = 'countdown_app_campaigns'
    USERNAME = 'username'
    TARGETING_START_TIME = 'targeting_start_time'
    RETARGETING_START_TIME = 'retargeting_start_time'
    SETTINGS = 'settings'
    VIDEO_JSON = 'video_json'
    FACEBOOK_TOKEN = 'facebook_token'
    STATUS = 'status'
    CAMPAIGN_ID = 'campaign_id'
    ADSET_TARGETING = 'adset_targeting'
    ADSET_RETARGETING_DAY_1 = 'adset_retargeting_day_1'
    ADSET_RETARGETING_DAY_2 = 'adset_retargeting_day_2'
    ADSET_RETARGETING_DAY_3 = 'adset_retargeting_day_3'
    ID = 'id'


class VideoMakerVideos:
    TABLE_NAME = 'video_maker_videos'
    ID = 'id'
    TEMPLATE_VERSION = 'template_version'
    VIDEO_JSON = 'video_json'
    STATUS = 'status'
    STATUS_COMMENT = 'status_comment'
    DATE_CHANGED = 'date_changed'
    USERNAME = 'username'
    WORKER_ID = 'worker_id'
    CREATION_TIME = 'creation_time'
    INSTANCE_NAME = 'instance_name'
    UPLOADED_URLS = 'uploaded_urls'
    MEDIA_TYPES = 'media_types'

SELECT_ALL = ['*']
