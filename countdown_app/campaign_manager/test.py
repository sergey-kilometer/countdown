from helpers.db import DBHelper
import uuid
import json
import requests
import math
from constants.data_base import VideoMakerVideos, CountdownAds, CountdownCampaigns, SELECT_ALL
from constants.facebook import API_VERSION
from facebookads.api import FacebookAdsApi
from facebookads.objects import (Ad, Campaign, AdSet, AdAccount, AdsPixel, AdCreative)
from facebookads.adobjects.adpromotedobject import AdPromotedObject
import boto3
from datetime import datetime
from facebookads.adobjects.targeting import Targeting
from objects.MyCampaign import CDCampaign
from facebookads.adobjects.adcreativelinkdatacalltoactionvalue import AdCreativeLinkDataCallToActionValue
from facebookads.adobjects.adcreativelinkdatacalltoaction import AdCreativeLinkDataCallToAction
from facebookads.adobjects.adcreativevideodata import AdCreativeVideoData
from facebookads.adobjects.adcreativeobjectstoryspec import AdCreativeObjectStorySpec

if __name__ == '__main__':
    FacebookAdsApi.init(access_token=self.facebook_token, api_version=API_VERSION)

    object_story_spec = AdCreativeObjectStorySpec()
    object_story_spec.update({
        AdCreativeObjectStorySpec.Field.page_id: '',
        # AdCreativeObjectStorySpec.Field.: video_data
    })

    creative = AdCreative(parent_id=self.ad_account)
    creative.update({
        AdCreative.Field.name: 'Test ad creative',
        AdCreative.Field.object_story_spec: object_story_spec,
        AdCreative.Field.object_type: AdCreative.ObjectType.video,
        # AdCreative.Field.video_id: video_id,
        # AdCreative.Field.title: settings.get('ad_headline', '')
    })
    creative.remote_create(params={'fields': ['']})
    # db_helper = DBHelper()

    # campaign = db_helper.build_select(
    #     tb_name=CountdownCampaigns.TABLE_NAME,
    #     select_value=[
    #         CountdownCampaigns.ID,
    #         CountdownCampaigns.USERNAME,
    #         CountdownCampaigns.VIDEO_JSON,
    #         CountdownCampaigns.CAMPAIGN_ID,
    #         CountdownCampaigns.FACEBOOK_TOKEN
    #     ],
    #     where=[CountdownCampaigns.CAMPAIGN_ID],
    #     values=['23842738400470019'],
    #     limit=1
    # )[0]
    #
    # username = campaign.get(CountdownCampaigns.USERNAME)
    # video_json = campaign.get(CountdownCampaigns.VIDEO_JSON)
    # campaign_id = campaign.get(CountdownCampaigns.CAMPAIGN_ID)
    # time = video_json.get('time', {})
    # schedule_type = video_json.get('schedule_type')
    # duration = time.get('totalDuration', 72)
    #
    # if schedule_type == 'limited_stock':
    #     total = int(time.get('total', 100))
    #     sold = int(time.get('sold', 0))
    #     delta = int(math.ceil((total - sold - 1) / (duration * 24 / 6)))
    #     for i in range(sold, total, delta):
    #         video_json['time']['sold'] = i
    #         video_json['time']['start'] = total - i
    #
    #         print(video_json['time']['sold'])

    # video_maker_video_tuples = [
    #     (
    #         'sergey@kilometer.io',
    #         12,
    #         json.dumps({'gif': True}),
    #         'test3',
    #         json.dumps({'gif': False, 'mp4': True, 'countdown': True})
    #     ),
    #     (
    #         'sergey@kilometer.io',
    #         12,
    #         json.dumps({'gif': True}),
    #         'test4',
    #         json.dumps({'gif': False, 'mp4': True, 'countdown': True})
    #     )
    # ]
    #
    # records_list_template = ','.join(['%s'] * len(video_maker_video_tuples))
    # query = 'INSERT INTO video_maker_videos (username, template_version, video_json, status, media_types) ' \
    #         'VALUES {} RETURNING id'.format(records_list_template)
    # print(query)
    #
    # result = db_helper.execute(query, video_maker_video_tuples, 'dict_fetchall')
    # print(result)
    # db_helper.close()
