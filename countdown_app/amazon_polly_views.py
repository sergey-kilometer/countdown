from countdown.settings import base as SETTINGS
import boto3
import uuid
import tempfile
from mutagen.mp3 import MP3
import json
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

AMAZON_ID = 'AKIAJKXQABOHNY6G36FQ'
AMAZON_SECRET = "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"


@csrf_exempt
def get_speech(request):
    # text_data = json.loads(request.body.decode("utf-8"))['text']

    data = json.loads(request.body.decode('utf-8'))
    text =  data.get('text')
    voice = data.get('voice')

    file_name = str(uuid.uuid4()) + '.mp3'

    polly = boto3.client(
        'polly',
        aws_access_key_id=AMAZON_ID,
        aws_secret_access_key=AMAZON_SECRET,
        region_name='us-east-1'

    )
    s3_client = boto3.resource(
        's3',
        aws_access_key_id=AMAZON_ID,
        aws_secret_access_key=AMAZON_SECRET,
        region_name='us-east-1'

    )

    # Get speech
    response = polly.synthesize_speech(Text=text, VoiceId=voice, OutputFormat='mp3')

    # Get meta-data of the file
    try:
        speech_file = response.get('AudioStream').read()
        temp_speech_file = tempfile.NamedTemporaryFile()
        temp_speech_file.write(speech_file)
        speech_meta = MP3(temp_speech_file.name)
    except:
        speech_meta = False


    # Upload to amazon
    s3_client.Bucket(SETTINGS.AWS_S3_USER_MUSIC_BUCKET).put_object(Key=file_name,Body=speech_file)
    full_path = 'https://s3.amazonaws.com/{bucket}/{file_name}'.format(bucket=SETTINGS.AWS_S3_USER_MUSIC_BUCKET,file_name=file_name)

    return JsonResponse({
        'src': full_path,
        'text': text,
        'voice': voice,
        'length': '{:.2f}'.format(speech_meta.info.length) if speech_meta else 'UKNOWN'
    })
