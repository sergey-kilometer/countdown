import os
import json
import uuid
import requests
from datetime import datetime
from contextlib import suppress
from django.conf import settings
from django.core import serializers
from django.shortcuts import get_object_or_404
from django.template import RequestContext, Template
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse

from facebookads.api import FacebookAdsApi
from facebookads.objects import (AdAccount, Campaign, AdUser, AdsPixel)

from . import utils
from .models import CountdownCampaigns, CountdownUser, CountdownAds
from countdown.decorators import inject_post_data
from .constants import FACEBOOK_PARAMS, FACEBOOK_TOKEN_URI, FACEBOOK_PIXEL_EVENTS, API_VERSION


@login_required
def index(request):
    """
    This URL is only used when you have built the production
    version of the app. Visit http://localhost:3000/ instead, or
    run `yarn run build` to test the production version.
    """
    try:
        with open(os.path.join(settings.REACT_APP_DIR, 'build', 'index.html')) as f:
            fields = ['email', 'username', 'myshopify_domain', 'name', 'register_source', 'countdown_plan']
            context = json.loads(serializers.serialize('json', [request.user], fields=fields))[0].get('fields')

            with suppress(CountdownUser.DoesNotExist):
                countdown_user = CountdownUser.objects.get(username=context.get('username'))
                if countdown_user.facebook_token:
                    context['isConnected'] = True
                else:
                    context['isConnected'] = False
                context['settings'] = countdown_user.settings

            c = RequestContext(request, {'user_json': json.dumps(context)})
            t = Template(f.read())
            return HttpResponse(t.render(c))
    except FileNotFoundError:
        return HttpResponse(status=501)


@csrf_exempt
@login_required
@inject_post_data
def create_campaign(request, post_data):
    plan_name = request.user.countdown_plan
    is_create_campaign = True
    if plan_name != 'agency':
        campaigns_count = CountdownCampaigns.objects.filter(username=request.user.username).count()
        if campaigns_count > 0:
            if plan_name == 'free':
                is_create_campaign = False
            elif campaigns_count >= 7:
                is_create_campaign = False

    if plan_name == 'agency' or is_create_campaign:
        countdown_user = get_object_or_404(CountdownUser, username=request.user.username)
        # extract post data
        facebook_settings = post_data.get('settings', {})
        ad_account = facebook_settings.get('ad_account', '')
        facebook_settings['campaign_name'] = 'Countdown TOPVID {}'\
            .format(datetime.now().strftime('%Y-%m-%d %H:%M'))
        video_json = post_data.get('video_json')
        # Init facebook session
        FacebookAdsApi.init(
            account_id=ad_account,
            access_token=countdown_user.facebook_token,
            api_version=API_VERSION
        )
        # Creating campaign on facebook
        schedule_type = video_json.get('schedule_type')
        campaign_id = utils.create_facebook_campaign(
            ad_account,
            facebook_settings,
            schedule_type,
        )
        # Creating targeting AdSet on facebook
        targeting_adset_id = utils.create_targeting_adset(
            facebook_settings,
            campaign_id,
            schedule_type,
        )
        # Adding campaign to Database
        countdown_campaign = CountdownCampaigns(
            username=countdown_user.username,
            video_json=video_json,
            facebook_token=countdown_user.facebook_token,
            campaign_id=campaign_id,
            settings=facebook_settings,
            adset_targeting=targeting_adset_id,
            hash=uuid.uuid4().hex,
            banner_json=utils.create_banner_json(video_json)
        )
        countdown_campaign.save()
        facebook_settings['pending_campaign'] = campaign_id
        countdown_user.settings = facebook_settings
        countdown_user.save(update_fields=['settings'])
        return JsonResponse({'pending_campaign': campaign_id})
    return JsonResponse({'error': 'countdown_plan'})


@login_required
def get_campaigns(request):
    fields = [Campaign.Field.status, Campaign.Field.name, Campaign.Field.created_time, Campaign.Field.account_id]
    campaigns = CountdownCampaigns.objects.filter(username=request.user.username)
    campaign_data = []
    if campaigns:
        for camp in campaigns:
            with suppress(Exception):
                FacebookAdsApi.init(access_token=camp.facebook_token, api_version=API_VERSION)
                facebook_campaign = Campaign(fbid=camp.campaign_id).remote_read(fields=fields).export_all_data()
                facebook_campaign_status = facebook_campaign.get(Campaign.Field.status)
                if facebook_campaign_status not in [Campaign.Status.deleted, Campaign.Status.archived]:
                    facebook_campaign.update({
                        'video_json': camp.video_json,
                        'settings': camp.settings,
                        'adset_targeting': camp.adset_targeting,
                        'banner_json': camp.banner_json,
                        'banner_url': utils.add_cd_param_to_url(camp.settings.get('website_url', None), camp.hash)
                    })
                    if camp.status == 'campaign_created':
                        if facebook_campaign_status == Campaign.Status.active:
                            facebook_campaign['status'] = 'Campaign Running'
                        else:
                            facebook_campaign['status'] = facebook_campaign_status
                    else:
                        facebook_campaign['status'] = 'Creating Ads'

                    campaign_data.append(facebook_campaign)
    return JsonResponse(campaign_data, safe=False)


@csrf_exempt
@login_required
def update_campaign(request, campaign_id):
    post_data = json.loads(request.body.decode('utf-8'))
    new_status = post_data.get('status')
    banner_json = post_data.get('bannerJson')
    camp = get_object_or_404(CountdownCampaigns, campaign_id=campaign_id, username=request.user.username)
    try:
        if new_status:
            FacebookAdsApi.init(access_token=camp.facebook_token, api_version=API_VERSION)
            Campaign(fbid=campaign_id).remote_update(params={Campaign.Field.status: new_status})
        if banner_json:
            camp.banner_json = banner_json
            camp.save(update_fields=['banner_json'])
        return HttpResponse('updated')
    except Exception:
        return HttpResponse('Oops some error occurred')


def get_video_json(_, ad_hash):
    try:
        countdown_ad = CountdownAds.objects.get(hash=ad_hash)
        countdown_campaign = get_object_or_404(CountdownCampaigns, campaign_id=countdown_ad.campaign_id)
        if utils.countdown_targeting_valid(countdown_campaign):
            # video_json = countdown_campaign.video_json
            banner_json = countdown_campaign.banner_json
            banner_json['time']['start'] = countdown_ad.time
            # video_json['time']['start'] = countdown_ad.time
            return JsonResponse(banner_json)
        else:
            return JsonResponse({}, status=400)
    except CountdownAds.DoesNotExist:
        countdown_campaign = get_object_or_404(CountdownCampaigns, hash=ad_hash)
        if utils.countdown_targeting_valid(countdown_campaign):
            return JsonResponse(countdown_campaign.banner_json)
        else:
            return JsonResponse({}, status=400)


@login_required
def get_accounts(request):
    countdown_user = CountdownUser.objects.get(username=request.user.username)
    resp = {}
    FacebookAdsApi.init(access_token=countdown_user.facebook_token, api_version=API_VERSION)
    pages = AdUser(fbid='me').get_pages(params={'limit': 500})
    accounts = AdUser(fbid='me').get_ad_accounts(fields=[
        AdAccount.Field.account_status,
        AdAccount.Field.name,
        AdAccount.Field.currency,
        AdAccount.Field.funding_source
    ], params={'limit': 500})
    resp['pages'] = [{
        'value': page.get('id'),
        'label': page.get('name')
    } for page in pages if 'CREATE_ADS' in page.get('perms', [])]
    resp['accounts'] = [{
        'currency': acc.get(AdAccount.Field.currency, 'USD'),
        'value': acc.get(AdAccount.Field.id),
        'label': '{} - {}'.format(acc.get(AdAccount.Field.name), acc.get(AdAccount.Field.id)),
        'funding_source': acc.get(AdAccount.Field.funding_source, None)
    } for acc in accounts if 1 == acc.get(AdAccount.Field.account_status, 0)]
    return JsonResponse(resp)


@csrf_exempt
@login_required
def save_countdown_user(request):
    """
    This URL is used for change temp facebook token to long lived and save facebook token and settings
    """
    try:
        countdown_user = CountdownUser.objects.get(username=request.user.username)
    except CountdownUser.DoesNotExist:
        countdown_user = CountdownUser(username=request.user.username)

    post_data = json.loads(request.body.decode('utf-8'))
    access_token = post_data.get('access_token')
    if access_token:
        FACEBOOK_PARAMS['fb_exchange_token'] = access_token
        res = requests.get(FACEBOOK_TOKEN_URI, params=FACEBOOK_PARAMS)
        if res.status_code == 200:
            countdown_user.facebook_token = res.json().get('access_token')

    countdown_user.settings = post_data.get('settings', countdown_user.settings)
    countdown_user.save()
    data = utils.serialize_user(countdown_user)
    return JsonResponse(data)


@login_required
def get_custom_conversions(request):
    account_id = request.GET.get('account')
    countdown_user = get_object_or_404(CountdownUser, username=request.user.username)
    data = []
    params = (
        ('fields', 'name,pixel{name,id},custom_event_type,rule'),
        ('access_token', countdown_user.facebook_token),
        ('limit', 500)
    )
    res = requests.get(
        url='https://graph.facebook.com/{}/{}/customconversions'.format(API_VERSION, account_id),
        params=params
    )
    if res.status_code == 200:
        custom_conv = []
        for conv in res.json().get('data', []):
            pixel_id = conv.get('pixel', {}).get('id')
            if pixel_id:
                custom_conv.append({
                    'label': '{} ({})'.format(conv.get('name', ''), conv.get('pixel', {}).get('name')),
                    'value': conv.get('id', ''),
                    'pixel_rule': conv.get('rule'),
                    'pixel_id': pixel_id,
                    'custom_event_type': conv.get('custom_event_type'),
                })
        if custom_conv:
            data.append({
                'value': 'Custom conversions',
                'label': 'Custom conversions',
                'children': custom_conv
            })

    FacebookAdsApi.init(access_token=countdown_user.facebook_token, api_version=API_VERSION)
    ads_pixels = AdAccount(fbid=account_id).get_ads_pixels(fields=[AdsPixel.Field.id, AdsPixel.Field.name])
    if ads_pixels:
        not_custom_conv = []
        for pixel in ads_pixels:
            pixel_id = pixel.get(AdsPixel.Field.id)
            pixel_name = pixel.get(AdsPixel.Field.name)
            for event in FACEBOOK_PIXEL_EVENTS:
                label = '{} ({})'.format(event, pixel_name)
                not_custom_conv.append({
                    'label': label,
                    'value': label,
                    'pixel_id': pixel_id,
                    'custom_event_type': event,
                })
        if not_custom_conv:
            data.append({
                'value': 'Facebook conversion events',
                'label': 'Facebook conversion events',
                'children': not_custom_conv
            })
    return JsonResponse(data, safe=False)
