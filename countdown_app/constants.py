FACEBOOK_TOKEN_URI = 'https://graph.facebook.com/v2.8/oauth/access_token'
FACEBOOK_PARAMS = {
    'grant_type': 'fb_exchange_token',
    'client_id': '1007349092734783',
    'client_secret': 'b32a769b64401744913b11fd1d37655e',
}
SERIALIZER_FIELDS = ['username', 'facebook_token', 'settings']
API_VERSION = 'v2.11'
FACEBOOK_PIXEL_EVENTS = [
    'COMPLETE_REGISTRATION', 'CONTENT_VIEW', 'SEARCH', 'RATE', 'TUTORIAL_COMPLETION', 'ADD_TO_CART', 'ADD_TO_WISHLIST',
    'INITIATED_CHECKOUT', 'ADD_PAYMENT_INFO', 'PURCHASE', 'LEAD', 'LEVEL_ACHIEVED', 'ACHIEVEMENT_UNLOCKED',
    'SPENT_CREDITS', 'OTHER'
]
