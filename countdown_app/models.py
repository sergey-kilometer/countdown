import shopify
from django.db import models
from shopify_auth.models import ShopUserManager
from shopify_auth.models import AbstractShopUser
from django.contrib.auth.models import AbstractBaseUser
from jsonfield import JSONField


class VideoMakerUser(AbstractBaseUser):
    class Meta:
        db_table = 'video_maker_app_videomakeruser'

    WEBSITE = 'website'
    APP_SHOPIFY = 'app.shopify.com'
    REGISTER_CHOICES = (
        (WEBSITE, 'Website register'),
        (APP_SHOPIFY, 'Shopify register')
    )

    myshopify_domain = models.CharField(max_length=255, unique=True, editable=False, default=None)
    token = models.CharField(max_length=255, editable=True, default='00000000000000000000000000000000')
    pending_video = models.CharField(max_length=255, default=None)
    credits = models.IntegerField(default=0)
    name = models.CharField(max_length=120, null=True, default=None)
    username = models.CharField(max_length=255, unique=True, null=True, default=None)
    email = models.CharField(max_length=255, unique=True, null=True, default=None)
    register_source = models.CharField(max_length=100, choices=REGISTER_CHOICES, default=WEBSITE)
    api_owner = models.IntegerField(default=None)
    watermark = models.BooleanField(default=True)
    outro = models.BooleanField(default=True)
    countdown_plan = models.CharField(max_length=20, default='free')

    objects = ShopUserManager()

    USERNAME_FIELD = 'username'

    @property
    def session(self):
        return shopify.Session.temp(self.myshopify_domain, self.token)

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    def __str__(self):
        return self.get_full_name()


class CountdownUser(models.Model):
    username = models.CharField(max_length=120, default=None, unique=True)
    facebook_token = models.CharField(max_length=255, default=None)
    settings = JSONField(default={})
    create_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.username

    class Meta:
        db_table = 'countdown_app_users'


class CountdownAds(models.Model):
    campaign_id = models.CharField(max_length=150, default=None)
    status=models.CharField(max_length=150, default=None)
    hour=models.CharField(max_length=150, default=None)
    video_maker_id = models.CharField(max_length=150, default=None)
    ad_type = models.CharField(max_length=150, default=None)
    time = models.CharField(max_length=8, default=None)
    hash = models.CharField(max_length=50, default=None)

    class Meta:
        db_table = 'countdown_app_ads'


class CountdownCampaigns(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=120, default=None)
    facebook_token = models.CharField(max_length=255, default=None)
    campaign_id = models.CharField(max_length=150, default=None)
    retargeting_id = models.IntegerField(default=None)
    adset_targeting = models.CharField(max_length=150, default=None)
    create_date = models.DateTimeField(auto_now_add=True)
    video_json = JSONField(default={})
    hash = models.CharField(max_length=50, default=None)
    status = models.CharField(max_length=40, default='videos_create')
    settings = JSONField(default={})
    banner_json = JSONField(default={})

    def __str__(self):
        return self.username + ' ' + self.campaign_id

    class Meta:
        db_table = 'countdown_app_campaigns'


class CountdownRetargetingUser(AbstractShopUser):
    email = models.CharField(max_length=255, null=True, default=None)
    name = models.CharField(max_length=120, null=True, default=None)
    facebook_token = models.CharField(max_length=255, null=True, default=None)
    campaign = models.CharField(max_length=150, null=True, default=None)
    charge_id = models.CharField(max_length=50, null=True, default=None)
    settings = JSONField(default={})
    trial_start_date = models.DateTimeField(default=None)
    create_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'countdown_retargeting_user'
