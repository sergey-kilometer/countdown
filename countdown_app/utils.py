import json
import shopify
from urllib.parse import urlencode
from datetime import datetime
import urllib.parse as urlparse

from django.core import serializers
from contextlib import suppress
from django.conf import settings
from intercom.client import Client
from dateutil.relativedelta import relativedelta
from facebookads.adobjects.targeting import Targeting
from facebookads.objects import (Campaign, AdSet)
from facebookads.adobjects.campaign import Campaign
from facebookads.adobjects.adset import AdSet


from .models import CountdownRetargetingUser
from .constants import SERIALIZER_FIELDS

intercom = Client(personal_access_token=settings.INTERCOM_TOKEN)
STYLE_PROPERTIES = ['fontFamily', 'color', 'zIndex', 'textAlign', 'fontWeight', 'fontStyle', 'fontDecoration', 'position']


def add_cd_param_to_url(url, cd_hash):
    params = {'cd': cd_hash}
    url_parts = list(urlparse.urlparse(url))
    query = dict(urlparse.parse_qsl(url_parts[4]))
    query.update(params)
    url_parts[4] = urlencode(query)
    return urlparse.urlunparse(url_parts)


def serialize_user(user):
    data = json.loads(serializers.serialize('json', [user], fields=SERIALIZER_FIELDS))[0].get('fields')

    if data.pop('facebook_token', None):
        data['isConnected'] = True
    else:
        data['isConnected'] = False

    return data


def intercom_set_custom_attributes(email, attributes):
    """
    Updates intercom user custom attributes
    """
    with suppress(Exception):
        intercom_user = intercom.users.find(email=email)
        for key, value in attributes.items():
            intercom_user.custom_attributes[key] = value
        intercom.users.save(intercom_user)


def _get_charge_status(myshopify_domain, token, charge_id):
    if charge_id:
        with shopify.Session.temp(myshopify_domain, token):
            charge = shopify.RecurringApplicationCharge().find_one(
                '/admin/recurring_application_charges/{}.json'.format(charge_id)
            )
        return charge.status
    return None


def countdown_targeting_valid(campaign):
    is_disabled = campaign.settings.get('isBannerDisable')
    if not is_disabled and campaign.retargeting_id:
        with suppress(CountdownRetargetingUser.DoesNotExist):
            retargeting_user = CountdownRetargetingUser.objects.get(pk=campaign.retargeting_id)
            trial_start_date = retargeting_user.trial_start_date
            if trial_start_date:
                days_pass = relativedelta(datetime.now(), trial_start_date).days
                if days_pass > 14:
                    charge_status = _get_charge_status(
                        retargeting_user.myshopify_domain,
                        retargeting_user.token,
                        retargeting_user.charge_id,
                    )
                    return charge_status == 'active'
    return True


def create_facebook_campaign(ad_account, settings, schedule_type):
    """
    Creates campaign on facebook
    """
    if schedule_type == 'continuous':
        campaign_objective = Campaign.Objective.reach
    else:
        campaign_objective = settings.get('objective', Campaign.Objective.link_clicks)

    campaign = Campaign(parent_id=ad_account)
    campaign.update({
        Campaign.Field.name: settings['campaign_name'],
        Campaign.Field.can_use_spend_cap: True,
        Campaign.Field.objective: campaign_objective,
    })
    campaign.remote_create(params={Campaign.Field.status: Campaign.Status.active})
    return campaign.get(campaign.Field.id)


def create_targeting_adset(facebook_settings, campaign_id, schedule_type):
    """
    Creates Custom Audience and AdSet on facebook
    """
    adset = AdSet(parent_id=facebook_settings['ad_account'])
    adset.update({
        AdSet.Field.name: facebook_settings['campaign_name'] + ' Edit Audience',
        AdSet.Field.is_autobid: True,
        AdSet.Field.billing_event: AdSet.BillingEvent.impressions,
        AdSet.Field.optimization_goal: AdSet.OptimizationGoal.reach,
        AdSet.Field.daily_budget: int(facebook_settings.get('adset_budget')) * 70,
        AdSet.Field.campaign_id: campaign_id,
        AdSet.Field.status: AdSet.Status.active,
        AdSet.Field.targeting: {
            Targeting.Field.geo_locations: {
                Targeting.Field.country_groups: ['worldwide'],
            },
            Targeting.Field.publisher_platforms: ['facebook'],
        }
    })

    objective = facebook_settings.get('objective')
    if schedule_type == 'continuous' or objective == Campaign.Objective.reach:
        adset.update({
            AdSet.Field.frequency_control_specs: {
                'event': 'IMPRESSIONS',
                'interval_days': facebook_settings.get('interval_days', 90),
                'max_frequency': facebook_settings.get('max_frequency', 1)
            }
        })
    else:
        if objective == Campaign.Objective.conversions:
            adset.update({AdSet.Field.optimization_goal: AdSet.OptimizationGoal.offsite_conversions})
            custom_conv = facebook_settings.get('customConversion')
            if custom_conv:
                promoted_object = {
                    'pixel_id': custom_conv.get('pixel_id'),
                    'custom_event_type': custom_conv.get('custom_event_type'),
                }
                if 'pixel_rule' in custom_conv:
                    promoted_object['pixel_rule'] = custom_conv.get('pixel_rule')
                adset.update({AdSet.Field.promoted_object: promoted_object})
        else:
            adset.update({AdSet.Field.optimization_goal: AdSet.OptimizationGoal.link_clicks})

    adset.remote_create(params={AdSet.Field.status: AdSet.Status.active})
    return adset.get(AdSet.Field.id)


def create_banner_json(video_json):
    banner_labels = []
    schedule_type = video_json.get('schedule_type')
    video_id = video_json.get('id')
    labels = video_json.get('frames', [{}])[0].get('labels', [])
    bg = video_json.get('bg', {})
    time = video_json.get('time', {})
    colors = bg.get('background', {}).get('colors', ['#454540', '#856BF9', '#ffffff', '#856BF9'])

    try:
        # add first label
        if video_id == 26:
            for label in labels:
                label_style = label['style']
                banner_labels.append({
                    'text': '' if label.get('isHidden') else label['text'],
                    'style': {
                        'fontFamily': label_style.get('fontFamily'),
                        'color': label_style.get('color', '#000000'),
                        'zIndex': label_style.get('zIndex'),
                        'border': label_style.get('border'),
                        'textAlign': label_style.get('textAlign'),
                        'fontWeight': label_style.get('fontWeight'),
                        'fontStyle': label_style.get('fontStyle'),
                        'fontDecoration': label_style.get('fontDecoration'),
                    }
                })
        else:
            first_label = labels[0]
            banner_labels.append({
                'text': '' if first_label.get('isHidden') else first_label['text'],
                'style': {
                    'fontFamily': first_label['style'].get('fontFamily'),
                    'color': first_label['style'].get('color', '#000000'),
                    'zIndex': first_label['style'].get('zIndex'),
                    'border': first_label['style'].get('border'),
                    'textAlign': first_label['style'].get('textAlign'),
                    'fontWeight': first_label['style'].get('fontWeight'),
                    'fontStyle': first_label['style'].get('fontStyle'),
                    'fontDecoration': first_label['style'].get('fontDecoration'),
                }
            })
            # add second label
            second_label = labels[2]
            if video_id == 18:
                banner_labels.append({
                    'text': '' if second_label.get('isHidden') else second_label['text'],
                    'style': {
                        'fontFamily': second_label['style'].get('fontFamily'),
                        'color': second_label['style'].get('color', '#000000'),
                        'zIndex': second_label['style'].get('zIndex'),
                        'border': second_label['style'].get('border'),
                        'textAlign': second_label['style'].get('textAlign'),
                        'fontWeight': second_label['style'].get('fontWeight'),
                        'fontStyle': second_label['style'].get('fontStyle'),
                        'fontDecoration': second_label['style'].get('fontDecoration'),
                        'backgroundColor': colors[1]
                    }
                }),
                banner_labels.append({
                    'text': '' if second_label.get('isHidden') else labels[3]['text'],
                    'style': {
                        'fontFamily': second_label['style'].get('fontFamily'),
                        'color': second_label['style'].get('color', '#000000'),
                        'zIndex': second_label['style'].get('zIndex'),
                        'border': second_label['style'].get('border'),
                        'textAlign': second_label['style'].get('textAlign'),
                        'fontWeight': second_label['style'].get('fontWeight'),
                        'fontStyle': second_label['style'].get('fontStyle'),
                        'fontDecoration': second_label['style'].get('fontDecoration'),
                        'backgroundColor': colors[1]
                    }
                })
            else:
                second_label_text = ''
                if not second_label.get('isHidden'):
                    second_label_text = second_label['text']
                    if video_id != 16:
                        second_label_text = second_label_text + ' {}'.format(labels[3]['text'])
                banner_labels.append({
                    'text': second_label_text,
                    'style': {
                        'fontFamily': second_label['style'].get('fontFamily'),
                        'color': second_label['style'].get('color', '#000000'),
                        'zIndex': second_label['style'].get('zIndex'),
                        'border': second_label['style'].get('border'),
                        'textAlign': second_label['style'].get('textAlign'),
                        'fontWeight': second_label['style'].get('fontWeight'),
                        'fontStyle': second_label['style'].get('fontStyle'),
                        'fontDecoration': second_label['style'].get('fontDecoration'),
                        'backgroundColor': colors[1]
                    }
                })
            # add third label
            coupon_label = labels[1]
            banner_labels.append({
                'text': '' if coupon_label.get('isHidden') else coupon_label['text'],
                'id': 'coupon_label',
                'style': {
                    'fontFamily': coupon_label['style'].get('fontFamily'),
                    'color': coupon_label['style'].get('color', '#000000'),
                    'zIndex': coupon_label['style'].get('zIndex'),
                    'border': coupon_label['style'].get('border'),
                    'textAlign': coupon_label['style'].get('textAlign'),
                    'fontWeight': coupon_label['style'].get('fontWeight'),
                    'fontStyle': coupon_label['style'].get('fontStyle'),
                    'fontDecoration': coupon_label['style'].get('fontDecoration'),
                    'backgroundColor': colors[0] if video_id != 18 else colors[2]
                }
            })
            # add days label
            if schedule_type == 'days':
                time_label = time.get('labels', [{}])[0]
                time_label_style = time_label.get('style', {})
                banner_labels.append({
                    'text': time_label.get('text', ''),
                    'id': 'days_label',
                    'style': {
                        'fontFamily': time_label_style.get('fontFamily'),
                        'color': time_label_style.get('color', '#000000'),
                        'zIndex': time_label_style.get('zIndex'),
                        'textAlign': time_label_style.get('textAlign'),
                        'fontWeight': time_label_style.get('fontWeight'),
                        'fontStyle': time_label_style.get('fontStyle'),
                        'fontDecoration': time_label_style.get('fontDecoration'),
                    }
                })

        return {
            'schedule_type': schedule_type,
            'labels': banner_labels,
            'colors': colors,
            'time': time,
            'bannerPos': 'right',
            'id': video_id
        }
    except Exception as e:
        return {'error': str(e)}
