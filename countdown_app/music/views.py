import os
import boto3
import shutil
from urllib.parse import quote
from ffmpy import FFmpeg
from datetime import datetime
from django.views import View
from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.utils.crypto import get_random_string
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.http.response import JsonResponse, HttpResponse


@method_decorator(login_required, name='dispatch')
@method_decorator(csrf_exempt, name='dispatch')
class UploadView(View):
    video_formats = ['.mpg', '.mpeg', '.avi', '.wmv', '.mov', '.rm', '.ram', '.swf', '.flv', '.ogg', '.webm', '.mp4',
                     '.ogg', '.ogv']
    tmp_directory = os.path.dirname(os.path.realpath(__file__)) + '/tmp/'
    s3_client = boto3.client(
        's3',
        aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY
    )
    """
    Upload and Get music file
    """
    @staticmethod
    def _get_file_path(filename):
        return 'https://s3.amazonaws.com/{bucket_name}/{filename}'.format(
            bucket_name=settings.AWS_S3_USER_MUSIC_BUCKET,
            filename=quote(filename)
        )

    @staticmethod
    def _get_file_key(username, random_string, name):
        now = datetime.now()
        return '{year}/{month:02d}/{username}/{random}_{name}'.format(
            year=now.year,
            month=now.month,
            username=username,
            random=random_string,
            name=name
        )

    def get(self, request):
        key_filter = request.GET.get('filter', '.mp3')
        file_paths = []
        music = self.s3_client.list_objects(
            Bucket=settings.AWS_S3_USER_MUSIC_BUCKET,
            Prefix=request.user.username
        )
        keys = [self._get_file_path(i.get('Key')) for i in music.get('Contents', []) if request.user.username in i.get('Key')]
        if key_filter == '.mp4':
            for key in keys:
                video_format = [v_f for v_f in self.video_formats if v_f in key]
                if video_format:
                    file_paths.append({
                        'src': key,
                        'music': key.replace(video_format[0], '.mp3'),
                        'not_scaled_poster': key.replace(video_format[0], '.jpg'),
                        'poster': key.replace(video_format[0], '.jpg')
                    })
        else:
            file_paths = [key for key in keys if key_filter in key]
        return JsonResponse(file_paths, safe=False)

    def post(self, request):
        username = request.user.username
        random_string = get_random_string(length=10).replace('_', '')
        temp_file = request.FILES.get('file')
        file_key = self._get_file_key(username, random_string, temp_file.name)
        temporary_file_path = temp_file.temporary_file_path()
        video_format = [v_f for v_f in self.video_formats if v_f in temp_file.name]
        if video_format:
            video_format = video_format[0]
            path = default_storage.save(self.tmp_directory + temp_file.name, ContentFile(temp_file.read()))
            image_path = path.replace(video_format, '.jpg')
            image_file_key = self._get_file_key(
                username,
                random_string,
                temp_file.name.replace(video_format, '.jpg')
            )
            try:
                FFmpeg(inputs={path: None}, outputs={image_path: ['-ss', '00:00:4', '-q:v', '1', '-vframes', '1']}).run()
                #  upload thumbnail to s3
                self.s3_client.upload_file(
                    image_path,
                    settings.AWS_S3_USER_MUSIC_BUCKET,
                    image_file_key,
                    ExtraArgs={'Metadata': {'user': username}}
                )
                # create and upload mp3 to s3
                music_path = path.replace(video_format, '.mp3')
                music_file_key = self._get_file_key(
                    username,
                    random_string,
                    temp_file.name.replace(video_format, '.mp3')
                )
                FFmpeg(inputs={path: None}, outputs={music_path: {}}).run()
                self.s3_client.upload_file(
                    music_path,
                    settings.AWS_S3_USER_MUSIC_BUCKET,
                    music_file_key,
                    ExtraArgs={'Metadata': {'user': username}}
                )
                music = self._get_file_path(music_file_key)
            except Exception:
                music = 'https://video-maker.topvid.com/static/assets/music/1-minute-of-silence.mp3'
            shutil.rmtree(self.tmp_directory)
            #  upload mp4 to s3
            self.s3_client.upload_file(
                temporary_file_path,
                settings.AWS_S3_USER_MUSIC_BUCKET,
                file_key,
                ExtraArgs={'Metadata': {'user': username}}
            )
            return JsonResponse({
                'src': self._get_file_path(file_key),
                'music': music,
                'poster': self._get_file_path(image_file_key),
                'not_scaled_poster': self._get_file_path(image_file_key),
            }, safe=False)
        else:
            self.s3_client.upload_file(
                temporary_file_path,
                settings.AWS_S3_USER_MUSIC_BUCKET,
                file_key,
                ExtraArgs={'Metadata': {'user': username}}
            )
            file_path = self._get_file_path(file_key)
            return HttpResponse(file_path)
