SHELL = /bin/bash

start-frontend:
	cd frontend && yarn start

build-frontend:
	cd frontend && yarn build

start-banner:
	cd countdown-widget && yarn start

build-banner:
	cd countdown-widget && yarn build

videos_create:
	python3 countdown_app/campaign_manager/videos_create.py

videos_upload:
	python3 countdown_app/campaign_manager/videos_upload.py

ads_create:
	python3 countdown_app/campaign_manager/ads_create.py