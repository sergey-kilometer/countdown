var userAgent = navigator.userAgent;
if (navigator.appName === 'Microsoft Internet Explorer'
    || !!(userAgent.match(/Trident/)
    || userAgent.match(/rv:11/))
    || userAgent.indexOf('Edge') > -1) {
    window.location = 'https://video-maker.topvid.com/control-panel/not-supported/';
}

Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
};

Storage.prototype.getObject = function(key) {
    var value = this.getItem(key);
    return value ? JSON.parse(value) : value;
};