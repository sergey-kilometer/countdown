FB.init({appId: '1007349092734783', status: true, cookie: true, version: 'v2.10'});

function loginWithShopify() {
    window.location.href = '/shopify/login/';
}

function loginWithFacebook() {
    FB.login(function(response) {
        if (response && !response.error && response.status === 'connected') {
            $.post('https://countdown.topvid.com/facebook/login/', {
                access_token: response['authResponse']['accessToken']
            }, function (response) {
                window.location.href = '/';
            });
        }
    }, {scope: 'email'});
}

jQuery(document).ready(function($) {
    $("#logo-wrapper").click(function(event) {
        window.location.href = $(this).attr("url");
        event.preventDefault();
    });

	$("#reset-password-trigger").click(function() {
		$(".forgotten-password").slideToggle("slow");
	});

  	// Validation Login
  	$("#login-form").submit(function() {
	    var value_login = $("#login-username").val();
	    var value_password = $("#login-password").val();
	    // If login is email
	    var email_values = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	    // Email format validation
       	if (!email_values.test(value_login)) {
			$("#status-username").removeClass("icon-check");
      		$("#status-username").addClass("icon-close");
      		$(".login-username").addClass("error-placeholder");
      		$("#login-username").addClass("error-input");

        	return false;
    	}
	    // Everything is all right
	    if (value_login != "" && value_password != "") {
	    	$("#status-username").addClass("icon-check");
	    	$("#status-username").switchClass("icon-close", "icon-check");
	    	$("#status-password").addClass("icon-check");
	    	$("#status-password").switchClass("icon-close", "icon-check");

	    	return true;
	    }

	    // If its not ok
	    else {
	    	// If login isn't ok
	      	if (value_login == "") {
	      		$("#status-username").removeClass("icon-check");
	      		$("#status-username").addClass("icon-close");
	      		$(".login-username").addClass("error-placeholder");
	      		$("#login-username").addClass("error-input");
	      	}

	      	// If login is ok but password not
	      	else if (value_login != "") {
	 			$("#status-username").removeClass("icon-close");
	 			$("#status-username").addClass("icon-check");
	 			$(".login-username").removeClass("error-placeholder");
	      		$("#login-username").removeClass("error-input");
	      	}

	      	// If password isn't ok
	      	if (value_password == "") {
	      		$("#status-password").removeClass("icon-check");
	      		$("#status-password").addClass("icon-close");
	      		$(".login-password").addClass("error-placeholder");
	      		$("#login-password").addClass("error-input");
	      	}

	      	// If password is ok but login not
	      	else if (value_password != "") {
	  			$("#status-password").removeClass("icon-close");
	 			$("#status-password").addClass("icon-check");
	 			$(".login-password").removeClass("error-placeholder");
	      		$("#login-password").removeClass("error-input");
	      	}

	      return false;
	    }

   	});

   	// Validation Email in Forgotten password
  	$("#forgotten-password-form").submit(function() {
        var value_forgotten_password_email = $("#login-forgotten-password").val(),
            email_values = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	    // Email format validation
       	if (!email_values.test(value_forgotten_password_email) || value_forgotten_password_email === "") {
       		$("#status-forgotten-password").removeClass("icon-check");
      		$("#status-forgotten-password").addClass("icon-close");
      		$(".login-forgotten-password").addClass("error-placeholder");
      		$("#login-forgotten-password").addClass("error-input");

        	return false;
    	} else {
    		$("#status-forgotten-password").removeClass("icon-close");
 			$("#status-forgotten-password").addClass("icon-check");
 			$(".login-forgotten-password").removeClass("error-placeholder");
      		$("#login-forgotten-password").removeClass("error-input");

      		// Send password reset
      		$.post("/password/reset/",{email: value_forgotten_password_email});
      		$("#forgotten-password-form").slideUp("slow");
      		$(".forgotten-password").addClass("forgotten-password-sent");
      		$(".forgotten-password p").text("Password reset succesfully sent");

    		return false;
    	}
   	});

   	// Validation Sign up
  	$("#signup-form").submit(function() {
	    var value_name = $("#signup-name").val();
	    var value_email = $("#signup-email").val();
	    var value_password = $("#signup-password").val();
	    var value_repassword = $("#signup-repassword").val();
	    // Email format validation
		var email_values = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

	    if (!email_values.test(value_email)) {
	    	$("#status-email").removeClass("icon-check");
      		$("#status-email").addClass("icon-close");
      		$(".signup-email").addClass("error-placeholder");
      		$("#signup-email").addClass("error-input");
	    	return false;
	    }

	    // Everything is all right
	    if (value_name != ""
            && value_email != ""
            && value_password != ""
            && value_repassword == value_password
            && ($('#signup-agree:checked').val() !== undefined)) {
			return true;
	    } else {
			// If login isn't ok
			if (value_name == "") {
				$("#status-name").removeClass("icon-check");
	      		$("#status-name").addClass("icon-close");
	      		$(".signup-name").addClass("error-placeholder");
	      		$("#signup-name").addClass("error-input");
			} else if (value_name != "") {
				$("#status-name").addClass("icon-check");
	      		$("#status-name").removeClass("icon-close");
	      		$(".signup-name").removeClass("error-placeholder");
	      		$("#signup-name").removeClass("error-input");
			}

			if (value_email == "") {
				$("#status-email").removeClass("icon-check");
	      		$("#status-email").addClass("icon-close");
	      		$(".signup-email").addClass("error-placeholder");
	      		$("#signup-email").addClass("error-input");
	      	} else if (value_email != "") {
				$("#status-email").addClass("icon-check");
	      		$("#status-email").removeClass("icon-close");
	      		$(".signup-email").removeClass("error-placeholder");
	      		$("#signup-email").removeClass("error-input");
			}

			if (value_password == "") {
				$("#status-password").removeClass("icon-check");
	      		$("#status-password").addClass("icon-close");
	      		$(".signup-password").addClass("error-placeholder");
	      		$("#signup-password").addClass("error-input");
			} else if (value_password != "") {
				$("#status-password").addClass("icon-check");
	      		$("#status-password").removeClass("icon-close");
	      		$(".signup-password").removeClass("error-placeholder");
	      		$("#signup-password").removeClass("error-input");
			}

			if (value_password != value_repassword || value_repassword == "") {
				$("#status-repassword").removeClass("icon-check");
	      		$("#status-repassword").addClass("icon-close");
	      		$(".signup-repassword").addClass("error-placeholder");
	      		$("#signup-repassword").addClass("error-input");
			} else if (value_password == value_repassword && value_repassword != "") {
				$("#status-repassword").addClass("icon-check");
	      		$("#status-repassword").removeClass("icon-close");
	      		$(".signup-repassword").removeClass("error-placeholder");
	      		$("#signup-repassword").removeClass("error-input");
			}

			var statusAgree = $("#status-agree"),
                signUpAgreeVal = $('#signup-agree:checked').val();
			if (signUpAgreeVal === undefined) {
                statusAgree.removeClass("hidden");
				statusAgree.addClass("icon-close");
	      		statusAgree.removeClass("icon-check");
	      		statusAgree.attr('data-original-title', 'Incorrect Email');
			} else if (signUpAgreeVal !== undefined) {
			    statusAgree.addClass("hidden");
				statusAgree.removeClass("icon-close");
	      		statusAgree.addClass("icon-check");
	      		statusAgree.prop('title', '');
			}
	      	return false;
	    }
	});

	// Validation Reset password
  	$("#reset-password-form").submit(function() {
	    var value_password = $("#reset-password").val(),
            value_repassword = $("#reset-repassword").val();
	    // Everything is all right
	    if (value_password !== "" && value_repassword === value_password) {
            $.post("/password/check/",{
                'reset-password': value_password,
                token: $("#token").val()
            }, function(data, status) {
                window.location.href = '/';
            });
            return false;
	    } else {
			if (value_password === "") {
				$("#status-password").removeClass("icon-check");
	      		$("#status-password").addClass("icon-close");
	      		$(".signup-password").addClass("error-placeholder");
	      		$("#signup-password").addClass("error-input");
			} else if (value_password !== "") {
				$("#status-password").addClass("icon-check");
	      		$("#status-password").removeClass("icon-close");
	      		$(".signup-password").removeClass("error-placeholder");
	      		$("#signup-password").removeClass("error-input");
			}

			if (value_password !== value_repassword || value_repassword === "") {
				$("#status-repassword").removeClass("icon-check");
	      		$("#status-repassword").addClass("icon-close");
	      		$(".signup-repassword").addClass("error-placeholder");
	      		$("#signup-repassword").addClass("error-input");
			} else if (value_password === value_repassword && value_repassword !== "") {
				$("#status-repassword").addClass("icon-check");
	      		$("#status-repassword").removeClass("icon-close");
	      		$(".signup-repassword").removeClass("error-placeholder");
	      		$("#signup-repassword").removeClass("error-input");
			}

	      	return false;
	    }
	});
});