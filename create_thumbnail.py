import os
import json
from ffmpy import FFmpeg

dir_path = os.path.dirname(os.path.realpath(__file__))
ff_path = dir_path + '/static/assets/bg-video/'
json_path = dir_path + '/frontend/src/constants/defaultVideos.json'

print(os.listdir(ff_path))

# Create Thumbnails
for file in os.listdir(ff_path):
    if '.mp4' in file:
        try:
            FFmpeg(
                inputs={ff_path + file: None},
                outputs={ff_path + file.replace('.mp4', '_not_scaled.jpg'): ['-ss', '00:00:4', '-q:v', '1', '-vframes', '1']}
            ).run()
            FFmpeg(
                inputs={ff_path + file: None},
                outputs={ff_path + file.replace('.mp4', '.jpg'): ['-ss', '00:00:4', '-q:v',  '1', '-vframes', '1', '-s', '490x280']}
            ).run()
        except Exception:
            pass

# write video paths to json file
domain = 'https://video-maker.topvid.com/static/assets/bg-video/'
videos_paths = []
for file in os.listdir(ff_path):
    if '.mp4' in file:
        videos_paths.append({
            'src': domain + file,
            'not_scaled_poster': domain + file.replace('.mp4', '_not_scaled.jpg'),
            'poster': domain + file.replace('.mp4', '.jpg')
        })
with open(json_path, 'w') as output_file:
    json.dump(videos_paths, output_file)
